## 5 散点图
散点图的绘制，使用的是`plt.scatter`方法，这个方法有以下参数：

1. `x,y`：分别是x轴和y轴的数据集。两者的数据长度必须一致。
2. `s`：点的尺寸。如果是一个具体的数字，那么散点图的所有点都是一样大小，如果是一个序列，那么这个序列的长度应该和x轴数据量一致，序列中的每个元素代表每个点的尺寸。
3. `c`：点的颜色。可以为具体的颜色，也可以为一个序列或者是一个`cmap`对象。
4. `marker`：标记点，默认是圆点，也可以换成其他的。
5. 其他参数：`https://matplotlib.org/api/_as_gen/matplotlib.pyplot.scatter.html#matplotlib.pyplot.scatter`。
- 例如:需求：探究房屋面积和房屋价格的关系

房屋面积数据：
```py
x = [225.98, 247.07, 253.14, 457.85, 241.58, 301.01,  20.67, 288.64,163.56, 120.06, 207.83, 342.75, 147.9 ,  53.06, 224.72,  29.51,21.61, 483.21, 245.25, 399.25, 343.35]
```
房屋价格数据：

```python
y = [196.63, 203.88, 210.75, 372.74, 202.41, 247.61,  24.9 , 239.34, 140.32, 104.15, 176.84, 288.23, 128.79,  49.64, 191.74,  33.1 ,30.74, 400.02, 205.35, 330.64, 283.45]
```
按0-4步骤
```py
import matplotlib.pyplot as plt
import random
# 设置显示中文字体
plt.rcParams['font.sans-serif'] = ['SimHei'] # 指定默认字体
# 设置正常显示符号
plt.rcParams['axes.unicode_minus'] = False 
# 0.准备数据
x = [225.98, 247.07, 253.14, 457.85, 241.58, 301.01,  20.67, 288.64,163.56, 120.06, 207.83, 342.75, 147.9 ,  53.06, 224.72,  29.51,21.61, 483.21, 245.25, 399.25, 343.35]
y = [196.63, 203.88, 210.75, 372.74, 202.41, 247.61,  24.9 , 239.34,140.32, 104.15, 176.84, 288.23, 128.79,  49.64, 191.74,  33.1 ,30.74, 400.02, 205.35, 330.64, 283.45]

# 1.创建画布
plt.figure(figsize=(20, 8), dpi=100)

# 2.绘制散点图
plt.scatter(x, y)

# 3.显示图像
plt.show()
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/88782a5c32fe42d08a1235be97f9a930.png)
### 5.1 绘制多组数据
```py
x=[random.random() for i in range(10)]
y1=[random.random() for i in range(10)]
y2=[random.random() for i in range(10)]

plt.scatter(x,y1,label="第一组数据")
plt.scatter(x,y2,label="第二组数据")

plt.legend()
```
在这里插入图片描述

## 6 饼图
 在`matplotlib`中，可以通过`plt.pie`来实现，其中的参数如下：

1. `x`：饼图的比例序列。
2. `labels`：饼图上每个分块的名称文字。
3. `explode`：设置某几个分块是否要分离饼图。
4. `autopct`：设置比例文字的展示方式。比如保留几个小数等。
5. `shadow`：是否显示阴影。
6. `textprops`：文本的属性（颜色，大小等）。
7. 其他参数：https://matplotlib.org/api/_as_gen/matplotlib.pyplot.pie.html#matplotlib.pyplot.pie
```py
edu = [59104, 1467937, 3579974]
labels = ['第一产业', '第二产业', '第三产业']
explode = [0.5, 0, 0]
colors = ['blue', 'pink', 'purple']

plt.pie(
    x=edu,  # 绘制的数据
    labels=labels,  # 占比的名字
    autopct='%.1f%%',  # 占比数值
    explode=explode,  # 饼图显示分隔间隔
    labeldistance=0.5,  #设置标签文本显示的距离
    textprops={
        'fontsize': 20,
        'color': 'k'
    },
    colors=colors,  # 饼图颜色
)
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/bdc3745586364b7b88dc8215e4be154d.png)

以下另外一组数据
```py
edu=[0.2515,0.3724,0.3336,0.0368,0.0057]
labels=['中专','大专','本科','硕士','其他']

plt.pie(
    x=edu,
    labels=labels,
    
)
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/08fbbe55237d4b99910563424f9c31ab.png)
- 案例
```py
# 设置绘画的主题风格
plt.figure(figsize=(10,8))
# 构造数据
edu = [0.2515,0.3724,0.3336,0.0368,0.0057]
labels = ['中专','大专','本科','硕士','其他']

explode = [0,0.1,0,0,0]  # 用于突出大专
colors=['#FEB748','#EDD25D','#FE4F54','#51B4FF','#dd5555'] # 自定义颜色

plt.axes(aspect='equal')  # 保证饼图是圆  不是默认的椭圆
plt.pie(x=edu,  # 数据
        labels=labels,   # 标签名称
        autopct='%.2f%%',  # 设置百分比格式  保留几位小数   
        colors=colors,  # 使用自定义颜色
#         radius = 1,   # 设置饼图半径
#         center = (80,80),  # 设置圆点
        labeldistance = 1.1, # 设置教育水平标签与圆心的距离
        startangle =30, # 设置饼图的初始角度  逆时针
        textprops = {'fontsize':12, 'color':'k'}, # 设置文本标签的属性值
        explode=explode, # 突出显示大专人群
        pctdistance=0.5,   # 占比和图距离
#         shadow=True,    # 阴影
#         frame=True    # frame 显示
       )
plt.title('失信人员组成')
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/7ec58f0320a24d65bbffc14837e21187.png)

## 7 雷达图
数据图
```py
properties = ['输出', 'KDA', '发育', '团战', '生存']
values = [40, 91, 44, 90, 95, 40]
theta = np.linspace(0, np.pi*2, 6)
plt.polar(theta, values)
plt.xticks(theta, properties, fontproperties=font)
plt.fill(theta, values)
plt.show()
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/63be66359dd0459d813765fba6cac411.png)


**matplotlib官方文档**

> 链接: https://matplotlib.org/index.html
>  [matplotlib官方文档](https://matplotlib.org/index.html)

## 0.numpy介绍

`NumPy`是一个功能强大的`Python`库，主要用于对多维数组执行计算。`NumPy`这个词来源于两个单词-- `Numerical`和`Python`。`NumPy`提供了大量的库函数和操作，可以帮助程序员轻松地进行数值计算。在数据分析和机器学习领域被广泛使用。他有以下几个特点：

1. Numpy内置了并行运算功能，当系统有多个核心时，做某种计算时，numpy会自动做并行计算。
2. Numpy底层使用C语言编写，内部解除了GIL（全局解释器锁），其对数组的操作速度不受Python解释器的限制，效率远高于纯Python代码。
3. 实用的线性代数、傅里叶变换和随机数生成函数。

**安装**
通过 `pip install numpy` 即可安装，如果是 anaconda 环境，默认就安装好了

教程地址：

1. 官网：`https://docs.scipy.org/doc/numpy/user/quickstart.html`。
[numpy官方](https://docs.scipy.org/doc/numpy/user/quickstart.html)
## 1. ndarray介绍
NumPy提供了一个**N维数组类型ndarray**，它描述了**相同类型**的“items”的集合。

-例如
```py
import numpy as np

# 创建 ndarray
```py
score = np.array([
    [80, 89, 86, 67, 79],
    [78, 97, 89, 67, 81],
    [90, 94, 78, 67, 74],
    [91, 91, 90, 67, 69],
    [76, 87, 75, 67, 86],
    [70, 79, 84, 67, 84],
    [94, 92, 93, 67, 64],
    [86, 85, 83, 67, 80]]
)

score
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/e000f0b56ff748a698f40fd26f8a9b3e.png)

### array数组的属性
![在这里插入图片描述](https://img-blog.csdnimg.cn/fb3e7626a512469b93dd61567df68461.png)

```py
"""三维数组"""
score2=np.array(
    [# 一维
        [ # 二维
            [55, 56, 67, 57, 58],  # 三维
            [25, 26, 27, 28, 30],
            [66, 68, 67, 63, 51]
        ],
        
        [ # 二维
            [55, 56, 67, 57, 58],  # 三维
            [25, 26, 27, 28, 30],
            [66, 68, 67, 63, 51]
        ]
    ],dtype='<U11'
)
score2
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/904b375c990a45b69a5bc920d7f9f219.png)
## 2.矩阵
常规建议在jupyter notebook输入`np.arange?`了解等差数列

在jupyter notebook输入`np.logspace`了解等比数列
以下是我在jupyter notebook的截图
![在这里插入图片描述](https://img-blog.csdnimg.cn/eb28ee5441ff40acba4f46b898df41c1.png)
## 3.正态分布
正态分布是一种概率分布。正态分布是具有两个参数μ和σ的连续型随机变量的分布，第一参数μ是服从正态分布的随机变量的均值，第二个参数σ是此随机变量的标准差，所以正态分布记作**N(μ，σ )**。
**μ决定了其位置，其标准差σ**决定了分布的幅度。当μ = 0,σ = 1时的正态分布是标准正态分布。


#### 正态分布创建

+ np.random.randn(d0, d1, …, dn)

  功能：从标准正态分布中返回一个或多个样本值

+ **np.random.normal(loc=0.0, scale=1.0, size=None)**

  loc：float   此概率分布的均值（对应着整个分布的中心centre）

  scale：float   此概率分布的标准差（对应于分布的宽度，scale越大越矮胖，scale越小，越瘦高）

  size：int or tuple of ints   输出的shape，默认为None，只输出一个值

+ np.random.standard_normal(*size=None*)

  返回指定形状的标准正态分布的数组。
- 案例-生成均值为1.75，标准差为1的正态分布数据，100000000个
```py
import matplotlib.pyplot as plt

# 生成均值为1.75，标准差为1的正态分布数据，100000000个
x1 = np.random.normal(loc=1.75, scale=1, size=100000)

import matplotlib.pyplot as plt

# 1.创建画布
plt.figure(figsize=(20, 8), dpi=100)

# 2.绘制图像
plt.hist(x1, 1000)

# 3.显示图像
plt.show()
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/8ece762b6c4743cd960efaf6e9b473c6.png)
## 4.均匀分布
+ np.random.rand(*d0*, *d1*, *...*, *dn*)
  + 返回**[0.0，1.0)**内的一组均匀分布的数。
+ **np.random.uniform(low=0.0, high=1.0, size=None)**
  + 功能：从一个均匀分布[low,high)中随机采样，注意定义域是左闭右开，即包含low，不包含high.
  + 参数介绍:
    + low: 采样下界，float类型，默认值为0；
    + high: 采样上界，float类型，默认值为1；
    + size: 输出样本数目，为int或元组(tuple)类型，例如，size=(m,n,k), 则输出m*n*k个样本，缺省时输出1个值。
  + 返回值：ndarray类型，其形状和参数size中描述一致。
+ np.random.randint(*low*, *high=None*, *size=None*, *dtype='l'*)
  + 从一个均匀分布中随机采样，生成一个整数或N维整数数组
  + 取数范围：若high不为None时，取[low,high)之间随机整数，否则取值[0,low)之间随机整数。
 - 案例
```py
import matplotlib.pyplot as plt

# 生成均匀分布的随机数
x2 = np.random.uniform(-1, 1, 100000000)

# 画图看分布状况
# 1）创建画布
plt.figure(figsize=(10, 10), dpi=100)

# 2）绘制直方图
plt.hist(x=x2, bins=1000)  # x代表要使用的数据，bins表示要划分区间数

```
在vscode的截图
![在这里插入图片描述](https://img-blog.csdnimg.cn/932d7b9ba2fd4d4788b11e7b541ed1c9.png)


![在这里插入图片描述](https://img-blog.csdnimg.cn/83f193203b8646a7b05b519825f45aa2.png)