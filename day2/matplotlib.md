# day 2 数据可视化-matplotlib
## 1 matplotlib
点击了解:[matplotlib官网链接](https://matplotlib.org/index.html)
`matplotlib`是一个	Python的2D图库,以下是终端安装代码
```
pip install matplotlib
```
## 2 基本使用
```python
import matplotlib.pyplot as plt
# 0.准备数据
x = [1, 2, 3, 4, 5, 6, 7]
y_shanghai = [17, 17, 18, 15, 11, 11, 13]
# 1.创建画布
plt.figure(figsize=(20, 8), dpi=100)   
# 2.绘制图像
plt.plot(x, y_shanghai)
# 3.图像显示
plt.show()
```
![图像](https://img-blog.csdnimg.cn/a5c007807e9544ababb940241b6f19bb.png)

## 3 直折线
*画出某城市11点到12点1小时内每分钟的温度变化折线图，
温度范围在15度~18度*
```python
import matplotlib.pyplot as plt
plt.rcParams['font.sans-serif'] = ['SimHei'] # 指定默认字体
plt.rcParams['axes.unicode_minus'] = False # 设置正常显示符号
# 0.准备数据
x_ticks_label = ["11点{}分".format(i) for i in x]
y_ticks = range(40)

# 1.创建画布
plt.figure(figsize=(20, 8), dpi=80)

# 2.绘制图像
plt.plot(x, y_shanghai)
plt.xticks(x[::5], x_ticks_label[::5])
plt.yticks(y_ticks[::5])
plt.grid(True, linestyle="--",alpha=1)
# 3.图像显示
plt.show()
```
![图示](https://img-blog.csdnimg.cn/ca44bc41f7d449e482d7c9d9870240d0.png)
## 4 条形图
### 4.1 直方图
2019年贺岁片票房的数据
```py
#票房单位亿元
movies = {
    "流浪地球":40.78,
    "飞驰人生":15.77,
    "疯狂的外星人":20.83,
    "新喜剧之王":6.10,
    "廉政风云":1.10,
    "神探蒲松龄":1.49,
    "小猪佩奇过大年":1.22,
    "熊出没·原始时代":6.71
}
```
展示电影和票房的代码如下:
```python
import matplotlib.pyplot as plt
import random
# 设置显示中文字体
plt.rcParams['font.sans-serif'] = ['SimHei'] # 指定默认字体
# 设置正常显示符号
plt.rcParams['axes.unicode_minus'] = False 


plt.figure(figsize=(20, 8), dpi=100)

movies = {
    "流浪地球": 40.78,
    "飞驰人生": 15.77,
    "疯狂的外星人": 20.83,
    "新喜剧之王": 6.10,
    "廉政风云": 1.10,
    "神探蒲松龄": 1.49,
    "小猪佩奇过大年": 1.22,
    "熊出没·原始时代": 6.71
}

plt.bar(range(len(movies)), list(movies.values()))
plt.xticks(range(len(movies)), list(movies.keys()))
plt.grid()
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/f6b77b51d0e34d2b96add94b7dfbe298.png)
### 4.2 分组直方图
2019年春节贺岁片前五天的电影票房记录
```py
movies = {
    "流浪地球": [2.01, 4.59, 7.99, 11.83, 16],
    "飞驰人生": [3.19, 5.08, 6.73, 8.10, 9.35],
    "疯狂的外星人": [4.07, 6.92, 9.30, 11.29, 13.03],
    "新喜剧之王": [2.72, 3.79, 4.45, 4.83, 5.11],
    "廉政风云": [0.56, 0.74, 0.83, 0.88, 0.92],
    "神探蒲松龄": [0.66, 0.95, 1.10, 1.17, 1.23],
    "小猪佩奇过大年": [0.58, 0.81, 0.94, 1.01, 1.07],
    "熊出没·原始时代": [1.13, 1.96, 2.73, 3.42, 4.05]
}
```
示例代码如下：
```py
import matplotlib.pyplot as plt

plt.rcParams['font.sans-serif'] = ['SimHei']  # 指定默认字体
plt.rcParams['axes.unicode_minus'] = False

movies = {
    "流浪地球": [2.01, 4.59, 7.99, 11.83, 16],
    "飞驰人生": [3.19, 5.08, 6.73, 8.10, 9.35],
    "疯狂的外星人": [4.07, 6.92, 9.30, 11.29, 13.03],
    "新喜剧之王": [2.72, 3.79, 4.45, 4.83, 5.11],
    "廉政风云": [0.56, 0.74, 0.83, 0.88, 0.92],
    "神探蒲松龄": [0.66, 0.95, 1.10, 1.17, 1.23],
    "小猪佩奇过大年": [0.58, 0.81, 0.94, 1.01, 1.07],
    "熊出没·原始时代": [1.13, 1.96, 2.73, 3.42, 4.05]
}
plt.figure(figsize=(20, 8))
width = 0.75
bin_width = width / 5

ind = range(0, len(movies))

movie_data = list(movies.values())
print(movie_data)

every_day = []
for i in range(len(movie_data[0])):
    every_day.append([
        movie_data[0][i],
        movie_data[1][i],
        movie_data[2][i],
        movie_data[3][i],
        movie_data[4][i],
        movie_data[5][i],
        movie_data[6][i],
        movie_data[7][i],
    ])

print(every_day)

for index in range(len(every_day)):
    day_tickets = every_day[index]
    xs = [i - (bin_width * (2 - index)) for i in ind]
    plt.bar(xs, day_tickets, width=bin_width, label="第%d天" % (index + 1))
    # 添加坐标上的数字
    for ticket, x in zip(day_tickets, xs):
        plt.annotate(ticket, xy=(x, ticket), xytext=(x - 0.1, ticket + 0.1))

# 设置图例
plt.legend()
plt.ylabel("单位：亿")
plt.title("春节前5天电影票房记录")
# 设置x轴的坐标
plt.xticks(ind, movies.keys())
#添加网格
plt.grid(True)
#展示图表 
plt.show() 
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/7bb6280cdbb64ebc8a72aee4fbcb009a.png)