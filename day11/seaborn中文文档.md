> 官方文档：http://seaborn.pydata.org/index.html
>
> 官方数据集: https://github.com/mwaskom/seaborn-data

# Seaborn 简介

Seaborn 是一个基于 [matplotlib](https://matplotlib.org/) 且数据结构与 [pandas](https://pandas.pydata.org/) 统一的统计图制作库。

这里列出了一些 seaborn 的功能：

* 计算[多变量](http://seaborn.pydata.org/examples/faceted_lineplot.html#faceted-lineplot)间[关系](http://seaborn.pydata.org/examples/scatter_bubbles.html#scatter-bubbles)的面向数据集接口
* 可视化类别变量的[观测](http://seaborn.pydata.org/examples/jitter_stripplot.html#jitter-stripplot)与[统计](http://seaborn.pydata.org/examples/pointplot_anova.html#pointplot-anova)
* 可视化[单变量](http://seaborn.pydata.org/examples/distplot_options.html#distplot-options)或[多变量](http://seaborn.pydata.org/examples/joint_kde.html#joint-kde)分布并与其子数据集[比较](http://seaborn.pydata.org/examples/horizontal_boxplot.html#horizontal-boxplot)
* 控制[线性回归](http://seaborn.pydata.org/examples/anscombes_quartet.html#anscombes-quartet)的不同[因变量](http://seaborn.pydata.org/examples/logistic_regression.html#logistic-regression)并进行参数估计与作图
* 对复杂数据进行易行的整体[结构](http://seaborn.pydata.org/examples/scatterplot_matrix.html#scatterplot-matrix)可视化
* 对[多表统计图](http://seaborn.pydata.org/examples/faceted_histogram.html#faceted-histogram)的制作高度抽象并简化可视化过程
* 提供多个内建[主题](http://seaborn.pydata.org/tutorial/aesthetics.html#aesthetics-tutorial)渲染 matplotlib 的图像样式
* 提供[调色板](http://seaborn.pydata.org/tutorial/color_palettes.html#palette-tutorial)工具生动再现数据

Seaborn 框架旨在以数据可视化为中心来挖掘与理解数据。它提供的面向数据集制图函数主要是对行列索引和数组的操作，包含对整个数据集进行内部的语义映射与统计整合，以此生成富于信息的图表。



为了安装最新版本的 seaborn, 可以 `pip`命令:

```python
pip install seaborn
```



下面是一些 🌰 的喵：

```python
import seaborn as sns
sns.set()
tips = sns.load_dataset("tips")
sns.relplot(x="total_bill", y="tip", col="time",
            hue="smoker", style="smoker", size="size",
            data=tips)
```

![http://seaborn.pydata.org/_images/introduction_1_0.png](assets/edfd168858231883b7e55e685f2f4e0d.jpg)


上面发生了什么呢？我们一步步分析：

1 我们装载 seaborn，这是这个例子唯一需要的 python 库。

```python
import seaborn as sns
```

此处，seaborn 实际调用了 matplotlib 作图。虽然很多任务都可以直接使用 seaborn 提供的函数来完成，不过一些更深层或者更加个性化的可能需要直接使用 matplotlib 来实现，[下面](#intro-plot-customization)会详细介绍这一点。如果需要获得交互式体验，推荐使用 Jupyter/IPython 工具并开启 [matplotlib 模式](https://ipython.readthedocs.io/en/stable/interactive/plotting.html)。否则，在使用交互式工具时如果想要看到图片需要调用 matplotlib.pyplot.show 函数。

2 我们设置并使用 seaborn 默认的主题、尺寸大小以及调色板。

```python
sns.set()
```

这里改变了 [matplotlib rcParam 系统](https://matplotlib.org/users/customizing.html) 所以会影响所有 matplotlib 图像的显示，即使你没有显式的调用 seaborn 修改这些参数。除了缺省的主题，我们提供一些[其他选项](http://seaborn.pydata.org/tutorial/aesthetics.html#aesthetics-tutorial)，你可以独立的控制图片风格与尺寸来将他们迅速应用到演讲当中（例如，在演讲投影中使用一个拥有可阅读字体的图表）。如果你倾向于使用默认 matplotlib 主题或者其他的内置样式，你可以跳过这一部分，只使用 seaborn 提供的制图函数。

3 装载数据集

```python
tips = sns.load_dataset("tips")
```

文档中的代码多数会使用 [`load_dataset()`](generated/seaborn.load_dataset.html#seaborn.load_dataset "seaborn.load_dataset") 去取得样例数据集。这些数据集没什么特殊，都是 pandas 的数据结构（dataframes），所以我们也可以使用 `pandas.read_csv` 或者手动输入创建数据集。在文档中，多数作图都使用 `tips` 数据集，非常无聊但是就说明状况的作用还是可以。`tips` 数据集提供了一种“整洁”的整合数据的方式，所以如果你使用这种方式来整合自己的数据，使用 seaborn 再好不过了。[下面](#intro-tidy-data)详细解释为什么。

4 我们写一个多子图散点图，分配语义变量

```python
sns.relplot(x="total_bill", y="tip", col="time",
            hue="smoker", style="smoker", size="size",
            data=tips)
```

这张图说明了 `tips` 数据集中五个变量的关系，三个是数值变量，另外两个是类别变量。其中 `total_bill` 和 `tip` 这两个数值变量决定了轴上每个点出现的位置，另外一个 `size` 变量影响着出现点的大小。第一个类别变量 `time` 将散点图分为两个子图，第二个类别变量 `smoker` 决定点的形状。

所有上述内容均由 [`relplot`](generated/seaborn.relplot.html#seaborn.relplot "seaborn.relplot") 一次调用生成。注意，在函数调用过程中，我们仅仅使用变量名来划分图像中的不同的语义。如果直接使用 matplotlib ，就必须将变量转换为可视化函数的参数（例如，指定颜色，指定每个类别的制图方式），这在 seaborn 中被自动执行了，以此让使用者将注意力放在他们要解决的问题上。



## 接口抽象

因为不存在可视化数据的最好方式，每一个制图所描述的问题都有自己最合适的可视化方法。seaborn 旨在让可视化方法间的切换变得更容易，有时仅仅需要改变同一个接口中的参数即可。

函数 [`relplot()`](generated/seaborn.relplot.html#seaborn.relplot "seaborn.relplot") 之所以这样命名是因为设计这个函数的初衷是想让他体现多个统计间的关系。散点图可以很好的体现统计数据间的关系，但是如果有一个变量具有时间意义，那线状图可能更好一点，因此，[`relplot()`](generated/seaborn.relplot.html#seaborn.relplot "seaborn.relplot") 函数提供了一个 `kind` 接口可以很方便的用作于改变图像的组织方式。

```python
dots = sns.load_dataset("dots")
sns.relplot(x="time", y="firing_rate", col="align",
            hue="choice", size="coherence", style="choice",
            facet_kws=dict(sharex=False),
            kind="line", legend="full", data=dots);
```

![http://seaborn.pydata.org/_images/introduction_11_0.png](assets/304d64035cdade741a6566356fb5105f.jpg)


参数 `size` 和 `style` 被散点图和线状图共享，但是他们对这些可视化结果产生的影响是不同的（例如，改变点大小和线的样式）。这些细节在实际使用中是无需被关注的，我们只需要将注意力放在组织图像的结构与我们想表达的信息中。

## 估计与误差

我们常常会想知道一个变量的均值函数以便在计算其他变量时作为可用参数。seaborn 的许多函数都可以自动的计算参数估计，这在解决一些问题中是十分必要的。

```python
fmri = sns.load_dataset("fmri")
sns.relplot(x="timepoint", y="signal", col="region",
            hue="event", style="event",
            kind="line", data=fmri);

```

![http://seaborn.pydata.org/_images/introduction_13_0.png](assets/ea7a65c02fd53e88da3b411bce44613e.jpg)


估计统计参数时，seaborn 使用了 bootstrap 方法计算置信区间和误差以表示估计的不确定性程度。

Seaborn 还能实现一些不是很好用语言描述统计估计。比如，使用 [`lmplot()`](generated/seaborn.lmplot.html#seaborn.lmplot "seaborn.lmplot") 可以将一个线性回归模型放在散点图当中去：

```python
sns.lmplot(x="total_bill", y="tip", col="time", hue="smoker",
           data=tips);
```

![http://seaborn.pydata.org/_images/introduction_15_0.png](assets/ed897fd7956612dbde4cb1f80e874a54.jpg)


## 分类图

标准的散点图和线状图用来可视化数值型数据，但是可能有些变量含有分类信息，之前的做法不再合适。我们提供了一些可视化分类变量的函数，在 [`catplot()`](generated/seaborn.catplot.html#seaborn.catplot "seaborn.catplot") 中可以找到。和 [`relplot()`](generated/seaborn.relplot.html#seaborn.relplot "seaborn.relplot") 相似的地方是，写 [`catplot()`](generated/seaborn.catplot.html#seaborn.catplot "seaborn.catplot") 函数的目的是为了提供一个面向数据集的通用接口，不仅可以显示数值变量，同时展示一个或多个类别变量。

在下面这种图中，你可以改变观测的粒聚集度，最好的情况是，所有的观测值都被调整的恰好到不会重叠但是又是沿着类别轴的：

```python
sns.catplot(x="day", y="total_bill", hue="smoker", kind="swarm", data=tips);
```

![http://seaborn.pydata.org/_images/introduction_17_0.png](assets/8c2ce11c1030543cc9193699a7799daa.jpg)


你也可以使用核密度估计来表示这些观测可能来源于的样本：

```python
sns.catplot(x="day", y="total_bill", hue="smoker", kind="violin", split=True, data=tips);
```

![http://seaborn.pydata.org/_images/introduction_19_0.png](assets/b7fc9d043ac684d49db7216d04477181.jpg)


或者你可以在每个嵌套类别变量中求其唯一均值与置信区间：

```python
sns.catplot(x="day", y="total_bill", hue="smoker",
            kind="bar", data=tips);
```

![http://seaborn.pydata.org/_images/introduction_21_0.png](assets/afab9a9586d23bfd75307cf6ba8af4e7.jpg)


## 图与轴的函数

了解 seaborn 提供的不同函数间的区别非常重要，到目前为止我们见到的函数都是基于 figure-level 的函数。由于直接创建包含子图的 matplotlib 图像，数据得以被沿着轴展开，挖掘数据得到了优化。这些函数还可以做一些比较有技巧性的事情，比如把图例放在轴外。我们可以使用 [`FacetGrid`](generated/seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid") 来完成这些事情。

每一个 figure-level 的图像 `kind` （指传给 kind 参数位置的图类别变量）都包含着一个特殊的 axes-level 作为 [`FacetGrid`](generated/seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid") 的对象。例如，散点图实际上使用的是 [`scatterplot()`](generated/seaborn.scatterplot.html#seaborn.scatterplot "seaborn.scatterplot") 函数，条形图使用的是 [`barplot()`](generated/seaborn.barplot.html#seaborn.barplot "seaborn.barplot") 函数，这些函数被称为 axes-level 因为他们只会作用一个独立 matplotlib 图像轴不会影响到其他剩余轴上的子图。

总之就是 figure-level 函数控制整个图像，axes-level 的函数可以和 matplotlib 图进行更复杂的结合，无论其他轴上的子图是否由 seaborn 作出。

```python
import matplotlib.pyplot as pltf, axes = plt.subplots(1, 2, sharey=True, figsize=(6, 4))
sns.boxplot(x="day", y="tip", data=tips, ax=axes[0])
sns.scatterplot(x="total_bill", y="tip", hue="day", data=tips, ax=axes[1]);
```

![http://seaborn.pydata.org/_images/introduction_23_0.png](assets/5d9c323ba09277e74341a23fc4397041.jpg)


使用 figure-level 函数控制图像大小的方法和控制 matplotlib 图的方法有一点不同。figure-level 函数不会设置整个图的大小，而是调整每个组成图的子图的大小，并且对于子图设定高度与比例，不会去分别设定高度和宽度。这样的参数化形式使得控制图表大小更容易了，并不需要非得仔细计算到底结果图会包含多少行、多少列，即使这样容易让人比较晕：

```python
sns.relplot(x="time", y="firing_rate", col="align",
            hue="choice", size="coherence", style="choice",
            height=4.5, aspect=2 / 3,
            facet_kws=dict(sharex=False),
            kind="line", legend="full", data=dots);
```

![http://seaborn.pydata.org/_images/introduction_25_0.png](assets/2b0ae6898e6a1091d872587335b2e5bf.jpg)


区别 figure-level 函数和 axes-level 函数的方法就是去看函数是否携带一个 `ax=` 参数。或者你可以查看他们的输出类型：axes-level 函数返回一个 matplotlib `axes`，figure-level 函数返回一个 [`FacetGrid`](generated/seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")。

## 数据集结构可视化

在 seaborn 中创建多子图的可视化结果有两种方法，两种方法都可以刻画数据集的结构。第一种 
[`jointplot()`](generated/seaborn.jointplot.html#seaborn.jointplot "seaborn.jointplot") 方法注重单对单关系：

```python
iris = sns.load_dataset("iris")
sns.jointplot(x="sepal_length", y="petal_length", data=iris);
```

![http://seaborn.pydata.org/_images/introduction_27_0.png](assets/c4a64a553429646fe040a596413d44e4.jpg)


另一种是 [`pairplot()`](generated/seaborn.pairplot.html#seaborn.pairplot "seaborn.pairplot") ，提供对数据更为全面的可视化。对于每对数据间的关系以及边缘分布都有考察，你可以选择用哪里分类变量作为条件：

```python
sns.pairplot(data=iris, hue="species");
```

![http://seaborn.pydata.org/_images/introduction_29_0.png](assets/61b3a047c35b789c4461b09d5bed86ac.jpg)


这两种方法在数据可视化都提供一些自定义选项，他们都是对于两个高度可自定义的多图作图函数 [`JointGrid`](generated/seaborn.JointGrid.html#seaborn.JointGrid "seaborn.JointGrid") 和 [`PairGrid`](generated/seaborn.PairGrid.html#seaborn.PairGrid "seaborn.PairGrid") 再封装。

## 自定义样式

Seaborn 库选择尽可能美观的设计，并且添加富于信息的标签。如果需要设计更为精致的图片可能会需要多执行一些步骤，这有多种方法。

第一种方法是使用 seaborn 给你的其他的主题。设置了不同的主题或者调色板样式会让整个图的效果都不一样：

```python
sns.set(style="ticks", palette="muted")
sns.relplot(x="total_bill", y="tip", col="time",
            hue="smoker", style="smoker", size="size",
            data=tips);
```

![http://seaborn.pydata.org/_images/introduction_31_0.png](assets/4263c8446521fa44bcb4c6f2bdec4bad.jpg)


如果要仅针对图像设计，所有的 seaborn 函数都接受一系列的可选参数来改变默认的语义映射，比如颜色。（对颜色的恰当选择在数据可视化中非常关键，seaborn 提供了[附加支持]http://seaborn.pydata.org/tutorial/color_palettes.html#palette-tutorial) 来引导调色板的使用）。

最后，当 seaborn 的函数与 matploblib 函数具有显然一致性时（例如 [`scatterplot()`](generated/seaborn.scatterplot.html#seaborn.scatterplot "seaborn.scatterplot") 和 `plt.scatter`），多余的参数会被直接传给 matploblib 层：

```python
sns.relplot(x="total_bill", y="tip", col="time",
            hue="size", style="smoker", size="size",
            palette="YlGnBu", markers=["D", "o"], sizes=(10, 125),
            edgecolor=".2", linewidth=.5, alpha=.75,
            data=tips);
```

![http://seaborn.pydata.org/_images/introduction_33_0.png](assets/506d0705fd945624be2ed50162adb4eb.jpg)


注意 [`relplot()`](generated/seaborn.relplot.html#seaborn.relplot "seaborn.relplot") 或者其他 figure-level 函数，因为当 [`relplot()`](generated/seaborn.relplot.html#seaborn.relplot "seaborn.relplot") 函数传递一些额外的关键字参数到 seaborn 底层的 axes-level 函数时，相当于将这些参数直接传给了底层 matplotlib 函数，这会使得你寻找对应文档变得有些麻烦，不过原则上是可以做到很高的自定义程度的。

有些 figure-level 的函数自定义可以通过传递额外参数到 [`FacetGrid`](generated/seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid") 来实现，你可以使用这个对象的方法来控制图像的属性。甚至可以修改需要被作图的 matploblib 对象的值达到效果：

```python
g = sns.catplot(x="total_bill", y="day", hue="time",
                height=3.5, aspect=1.5,
                kind="box", legend=False, data=tips);g.add_legend(title="Meal")
g.set_axis_labels("Total bill ($)", "")
g.set(xlim=(0, 60), yticklabels=["Thursday", "Friday", "Saturday", "Sunday"])
g.despine(trim=True)
g.fig.set_size_inches(6.5, 3.5)
g.ax.set_xticks([5, 15, 25, 35, 45, 55], minor=True);plt.setp(g.ax.get_yticklabels(), rotation=30);
```

![http://seaborn.pydata.org/_images/introduction_35_0.png](assets/3be223a42bf5ef33ccf970a4e8a79fe3.jpg)


Figure-level 函数作用于需要高效显示数据的情况，所以如果要更精确的调整大小与组织形式，直接使用 matploblib 或者使用对应的 seaborn axes-level 函数。matploblib 具有易于理解和强大的接口，任何有关图像属性的值都可以通过接口来完成设置。最好是将高度抽象的 seaborn 接口和可深度自定义的 matplotlib 接口一起使用来制作达到[出版质量](https://github.com/wagnerlabpapers/Waskom_PNAS_2017)的最终成果。

## 组织数据集

之前提到过，如果你的数据整合的比较好，seaborn 的表现也会很出众。这种能称为做“长型”或者“整洁”数据在[这里](http://vita.had.co.nz/papers/tidy-data.html)被详细解释。这些方式可以大致概括为：

1. 每个变量占有一个列
2. 每个观测占有一个行

你的数据是否整洁了？一种比较好的思考方式是去想你要怎么画你的图，从这一点来讲，变量就是图中一种具有规律展现形式的元素。如果看看样例 `tips` 会比较好：

```python
tips.head()
```

|      | total_bill | tip  | sex    | smoker | day  | time   | size |
| ---- | ---------- | ---- | ------ | ------ | ---- | ------ | ---- |
| 0    | 16.99      | 1.01 | Female | No     | Sun  | Dinner | 2    |
| ---  | ---        | ---  | ---    | ---    | ---  | ---    | ---  |
| 1    | 10.34      | 1.66 | Male   | No     | Sun  | Dinner | 3    |
| ---  | ---        | ---  | ---    | ---    | ---  | ---    | ---  |
| 2    | 21.01      | 3.50 | Male   | No     | Sun  | Dinner | 3    |
| ---  | ---        | ---  | ---    | ---    | ---  | ---    | ---  |
| 3    | 23.68      | 3.31 | Male   | No     | Sun  | Dinner | 2    |
| ---  | ---        | ---  | ---    | ---    | ---  | ---    | ---  |
| 4    | 24.59      | 3.61 | Female | No     | Sun  | Dinner | 4    |
| ---  | ---        | ---  | ---    | ---    | ---  | ---    | ---  |

从某种意义上讲，使用这种组织方式开始可能会感觉尴尬，比如使用时间序列数据，将时间点作为观测值放在列中。我们[之前](#intro-stat-estimation)使用的 `fmri` 数据集展示了一个整洁的时间点在不同行间的组织形式：

```python
fmri.head()
```

|      | subject | timepoint | event | region   | signal    |
| ---- | ------- | --------- | ----- | -------- | --------- |
| 0    | s13     | 18        | stim  | parietal | -0.017552 |
| ---  | ---     | ---       | ---   | ---      | ---       |
| 1    | s5      | 14        | stim  | parietal | -0.080883 |
| ---  | ---     | ---       | ---   | ---      | ---       |
| 2    | s12     | 18        | stim  | parietal | -0.081033 |
| ---  | ---     | ---       | ---   | ---      | ---       |
| 3    | s11     | 18        | stim  | parietal | -0.046134 |
| ---  | ---     | ---       | ---   | ---      | ---       |
| 4    | s10     | 18        | stim  | parietal | -0.037970 |
| ---  | ---     | ---       | ---   | ---      | ---       |

许多 seaborn 函数都提供了画出大范围数据图的功能，虽然在函数性上有所限制。要利用好整合较好的数据集，你肯定会使用 `pandas.melt` 函数来解构一个大的数据集。参考[这篇](https://tomaugspurger.github.io/modern-5-tidy.html) pandas 开发者写的博客。



# 可视化统计关系

统计分析是了解数据集中的变量如何相互关联以及这些关系如何依赖于其他变量的过程。可视化是此过程的核心组件，这是因为当数据被恰当地可视化时，人的视觉系统可以看到指示关系的趋势和模式。

我们将在本教程中讨论三个 seaborn 函数。我们最常用的是[`relplot()`](../generated/seaborn.relplot.html#seaborn.relplot "seaborn.relplot")。这是一个[figure-level](../introduction.html#intro-func-types)的函数，可以用散点图和线图两种通用的方法来可视化统计关系。[`relplot()`](../generated/seaborn.relplot.html#seaborn.relplot "seaborn.relplot")将[`FacetGrid`](../generated/seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid") 与两个[axes-level]()函数组合在一起:

*   [`scatterplot()`](../generated/seaborn.scatterplot.html#seaborn.scatterplot "seaborn.scatterplot") (`kind="scatter"`; 默认值)
*   [`lineplot()`](../generated/seaborn.lineplot.html#seaborn.lineplot "seaborn.lineplot")(`kind="line"`)

正如我们将要看到的，这些函数可能非常有启发性，因为他们使用简单且易于理解的数据表示形式，且仍然能够表示复杂的数据集结构。之所以可以这样，是因为它们可以通过色调、大小和样式的语义映射最多三个额外的变量来增强绘制的二维图形。

```python
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(style="darkgrid")
```

## 用散点图关联变量

散点图是数据可视化的支柱，它通过点云描绘了两个变量的联合分布，其中每个点代表数据集中的一个观测值。这种描述能够使我们通过视觉推断出许多信息，他们之间是否存在任何有意义的关系。

在 seaborn 中有多种方式绘制散点图。当两个变量的是数值型时，最基本的是函数[`scatterplot()`](../generated/seaborn.scatterplot.html#seaborn.scatterplot "seaborn.scatterplot")。在 [类别可视化](categorical.html#categorical-tutorial)，我们将会看到使用散点图来显示类别数据的专用工具。[`scatterplot()`](../generated/seaborn.scatterplot.html#seaborn.scatterplot "seaborn.scatterplot")是[`relplot()`](../generated/seaborn.relplot.html#seaborn.relplot "seaborn.relplot")中`kind`的默认类型(也可以通过`kind="scatter"`来设置):

```python
tips = sns.load_dataset("tips")
sns.relplot(x="total_bill", y="tip", data=tips)
```

![http://seaborn.pydata.org/_images/relational_4_0.png](assets/da2b66e95b2bbd08f2c58046234e97b1.jpg)


虽然这些点是以二维绘制的，但可以通过根据第三个变量对点进行着色来将另一个维度添加到绘图中。在 seaborn 中，这被称为使用“色调语义”，因为该点的颜色获得了意义：


```python
sns.relplot(x="total_bill", y="tip", hue="smoker", data=tips)
```

![http://seaborn.pydata.org/_images/relational_6_0.png](assets/7e2cd86a1f38e486133cd69728feed62.jpg)


为了强调类别之间的差异并提高可访问性，可以为每个类别使用不同的标记样式：

```python
sns.relplot(x="total_bill", y="tip", hue="smoker", style="smoker", data=tips)
```

![http://seaborn.pydata.org/_images/relational_8_0.png](assets/9240094a48ef05fa3a34fab7ea6bb093.jpg)



也可以通过单独改变每个点的色调和样式来表示四个变量。但是这应该谨慎，因为眼睛对形状的敏感度远低于对颜色的敏感度:

```python
sns.relplot(x="total_bill", y="tip", hue="smoker", style="time", data=tips);
```

![http://seaborn.pydata.org/_images/relational_10_0.png](assets/36180ad55b36cb767608c261d92e2918.jpg)



在上面的例子中，色调语义表示类别，所以使用了默认的[定性调色板](color_palettes.html#palette-tutorial)。如果色调语义表示数值(特别是，如果它可以转换为浮点数)，默认的颜色切换到顺序调色板:

```python
sns.relplot(x="total_bill", y="tip", hue="size", data=tips);
```

![http://seaborn.pydata.org/_images/relational_12_0.png](assets/e2261011757746831cd8de1e799bf2e5.jpg)


在这两种情况下，您都可以自定义调色板，有多种方式可以实现。在这里，我们使用[`cubehelix_palette()`](../generated/seaborn.cubehelix_palette.html#seaborn.cubehelix_palette "seaborn.cubehelix_palette")的字符串接口自定义一个顺序调色板:

```python
sns.relplot(x="total_bill", y="tip", hue="size", palette="ch:r=-.5,l=.75", data=tips)
```

![http://seaborn.pydata.org/_images/relational_14_0.png](assets/3a9f5f15fbcf65ae0763da92544620c3.jpg)


第三个语义变量改变每个点的大小:

```python
sns.relplot(x="total_bill", y="tip", size="size", data=tips);
```

![http://seaborn.pydata.org/_images/relational_16_0.png](assets/c89bfa6f82af190645b6a97983037d2f.jpg)


与[`matplotlib.pyplot.scatter()`](https://matplotlib.org/api/_as_gen/matplotlib.pyplot.scatter.html#matplotlib.pyplot.scatter "(in Matplotlib v2.2.2)")不同，变量的值不用于直接决定点的面积。数据单位中的值范围被规范化为面积单位的范围，这个范围可以自定义:

```python
sns.relplot(x="total_bill", y="tip", size="size", sizes=(15, 200), data=tips);
```

![http://seaborn.pydata.org/_images/relational_18_0.png](assets/c622412b599649be9b4d765223de73f5.jpg)


在[`scatterplot()`](../generated/seaborn.scatterplot.html#seaborn.scatterplot "seaborn.scatterplot")
API 示例中展示了更多如何通过自定义使用不同语义来显示统计关系的示例。

## 强调线图的连续性

散点图是非常有效的，但是没有通用的最优可视化类型。相反，可视表示应该适应数据集的细节以及您试图用图表回答的问题。

对于某些数据集，您可能希望了解一个变量中的变化关于时间的函数，或者类似的连续变量。在这种情况下，一个很好的选择是绘制线图。
在 seaborn 中，这可以通过[`lineplot()`](../generated/seaborn.lineplot.html#seaborn.lineplot "seaborn.lineplot")函数直接实现，也可以通过设置[`relplot()`](../generated/seaborn.relplot.html#seaborn.relplot "seaborn.relplot")的参数`kind="line"`来实现:

```python
df = pd.DataFrame(dict(time=np.arange(500),
                       value=np.random.randn(500).cumsum()))
g = sns.relplot(x="time", y="value", kind="line", data=df)
g.fig.autofmt_xdate()
```

![http://seaborn.pydata.org/_images/relational_21_0.png](assets/41ec1c6b7e5de5ac6cc5f6020ac029fc.jpg)


由于[`lineplot()`](../generated/seaborn.lineplot.html#seaborn.lineplot "seaborn.lineplot")假设您想要将`y`绘制为`x`的函数，默认行为是在绘制之前按数字`x`对数据进行排序。但是，这可以被禁用：

```python
df = pd.DataFrame(np.random.randn(500, 2).cumsum(axis=0), columns=["x", "y"])
sns.relplot(x="x", y="y", sort=False, kind="line", data=df)
```

![http://seaborn.pydata.org/_images/relational_23_0.png](assets/2d731687030e9e2dab13cbdb46017e91.jpg)


### 聚合和表示不确定性

更复杂的数据集将对`x`变量的相同值有多个观测值。seaborn 的默认行为是通过绘制平均值及 95%的置信区间，在每个`x`周围聚合多个测量值:

```python
fmri = sns.load_dataset("fmri")
sns.relplot(x="timepoint", y="signal", kind="line", data=fmri);
```

![http://seaborn.pydata.org/_images/relational_25_0.png](assets/c1b2a3e1708607a80c705b690a9c6c5a.jpg)


置信区间是使用 bootstrapping 计算的，对于较大的数据集，它可能是时间密集型的。因此，可以禁用它们:

```python
sns.relplot(x="timepoint", y="signal", ci=None, kind="line", data=fmri)
```

![http://seaborn.pydata.org/_images/relational_27_0.png](assets/95ca0b5950c77a49aceb9f22e148ba9c.jpg)


尤其是对于较大的数据，另一个不错的选择是通过绘制标准差，而不是置信区间来表示分布在每个时间点的分布范围:

```python
sns.relplot(x="timepoint", y="signal", kind="line", ci="sd", data=fmri)
```

![http://seaborn.pydata.org/_images/relational_29_0.png](assets/318e42d583df0fc8e01c6d7c565d758c.jpg)


可以通过设置`estimator`参数为`None`，来完全停用聚合。当数据在每个点上有多个观察值时，这可能会产生奇怪的效果。

```python
sns.relplot(x="timepoint", y="signal", estimator=None, kind="line", data=fmri)
```

![http://seaborn.pydata.org/_images/relational_31_0.png](assets/5edaaa0dfdffc4b78552a1a5b68f5491.jpg)


### 绘制子集数据

函数[`lineplot()`](../generated/seaborn.lineplot.html#seaborn.lineplot "seaborn.lineplot")与[`scatterplot()`](../generated/seaborn.scatterplot.html#seaborn.scatterplot "seaborn.scatterplot")具有相同的灵活性：它可以通过修改绘图元素的色调，大小和样式来显示最多三个附加变量。它使用于[`scatterplot()`](../generated/seaborn.scatterplot.html#seaborn.scatterplot "seaborn.scatterplot")相同的 API，这意味着我们不需要停下来考虑控制 matplotlib 中线条与点外观的参数。

在[`lineplot()`](../generated/seaborn.lineplot.html#seaborn.lineplot "seaborn.lineplot")中使用语义也将决定数据的聚合方式。例如，添加具有两个级别的色调语义将绘图分成两行以及错误带，每个都着色以指示它们对应于哪个数据集。

```python
sns.relplot(x="timepoint", y="signal", hue="event", kind="line", data=fmri);
```

![http://seaborn.pydata.org/_images/relational_33_0.png](assets/a9e30b276189c5c26d3a9959798a84a6.jpg)


在线条图中添加样式语义默认情况下会改变线条中的破折号模式：

```python
sns.relplot(x="timepoint", y="signal", hue="region", style="event",
            kind="line", data=fmri);
```

![http://seaborn.pydata.org/_images/relational_35_0.png](assets/b51b0bae21d924ef1f739aabdd811e31.jpg)


但您可以通过每次观察时使用的标记识别子集，或者使用短划线或代替它们：

```python
sns.relplot(x="timepoint", y="signal", hue="region", style="event",
            dashes=False, markers=True, kind="line", data=fmri)
```

![http://seaborn.pydata.org/_images/relational_37_0.png](assets/68010acf9a074a4aa94ad7e89b9a819e.jpg)


与散点图一样，要谨慎使用多个语义制作线图。虽然有时提供信息，但它们也很难解析和解释。但当您只检查一个附加变量的变化时，更改线条的颜色和样式也很有用。当打印成黑白或有色盲的人观看时，这可以使绘图更容易访问：

```python
sns.relplot(x="timepoint", y="signal", hue="event", style="event",
            kind="line", data=fmri)
```

![http://seaborn.pydata.org/_images/relational_39_0.png](assets/e04dba52166d9d9333fcd7d824a3bb02.jpg)


当您使用重复测量数据（即，您有多次采样的单位）时，您还可以单独绘制每个采样单位，而无需通过语义区分它们。这样可以避免使图例混乱：

```python
sns.relplot(x="timepoint", y="signal", hue="region",
            units="subject", estimator=None,
            kind="line", data=fmri.query("event == 'stim'"))
```

![http://seaborn.pydata.org/_images/relational_41_0.png](assets/21df9f9baaa60a010973e5b26a1e7259.jpg)


[`lineplot()`](../generated/seaborn.lineplot.html#seaborn.lineplot "seaborn.lineplot")中默认的色彩映射和图例的处理还取决于色调语义是类别还是数值：

```python
dots = sns.load_dataset("dots").query("align == 'dots'")
sns.relplot(x="time", y="firing_rate",
            hue="coherence", style="choice",
            kind="line", data=dots)
```

![http://seaborn.pydata.org/_images/relational_43_0.png](assets/1e1bc5f34e003c074af116c6f2a0cd58.jpg)


可能会发生这样的情况：即使`hue`变量是数值，它也很难用线性色标表示。如下示例，其中`hue`变量的级别以对数方式缩放。您可以通过传递列表或字典为每一行提供特定的颜色值：

```python
palette = sns.cubehelix_palette(light=.8, n_colors=6)
sns.relplot(x="time", y="firing_rate",
            hue="coherence", style="choice",
            palette=palette,
            kind="line", data=dots)
```

![http://seaborn.pydata.org/_images/relational_45_0.png](assets/a1749fc5a5cf2bb2e4b3c4c741c1b8f7.jpg)


或者您可以更改色彩映射的规范化方式：

```python
from matplotlib.colors import LogNormpalette = sns.cubehelix_palette(light=.7, n_colors=6)
sns.relplot(x="time", y="firing_rate",
            hue="coherence", style="choice",
            hue_norm=LogNorm(),
            kind="line", data=dots)
```

![http://seaborn.pydata.org/_images/relational_47_0.png](assets/fe7b4873c67199b180714300c4b65def.jpg)


第三个语义，`size`改变线的宽度：

```python
sns.relplot(x="time", y="firing_rate",
            size="coherence", style="choice",
            kind="line", data=dots)
```

![http://seaborn.pydata.org/_images/relational_49_0.png](assets/3ab581b389567a79a8bf4ddd79ec9356.jpg)


虽然`size`变量通常是数值型的，但是也可以用线宽映射为类别变量。在这样做的时候要小心，因为除了“粗”线和“细”线之外，很难区分更多。然而，当线具有高频变异性时，破折号很难被察觉，因此在这种情况下，使用不同的宽度可能更有效:

```python
sns.relplot(x="time", y="firing_rate",
           hue="coherence", size="choice",
           palette=palette,
           kind="line", data=dots)
```

![http://seaborn.pydata.org/_images/relational_51_0.png](assets/de02eb5b7f92879b7db8e43542d3bef9.jpg)


### 用日期数据绘图

线图通常用于可视化与实际日期和时间相关的数据。这些函数以原始格式将数据传递给底层的 matplotlib 函数，因此他们可以利用 matplotlib 在 tick 标签中设置日期格式的功能。但是所有这些格式化都必须在 matplotlib 层进行，您应该参考 matplotlib 文档来了解它是如何工作的：

```python
df = pd.DataFrame(dict(time=pd.date_range("2017-1-1", periods=500),
                       value=np.random.randn(500).cumsum()))
g = sns.relplot(x="time", y="value", kind="line", data=df)
g.fig.autofmt_xdate()
```

![http://seaborn.pydata.org/_images/relational_53_0.png](assets/ec42ca3d421c2355f3f6ef3ce2b26cfe.jpg)


## 显示与切面的多种关系

我们在本教程中强调，虽然这些函数可以同时显示几个语义变量，但这样做并不总是有效的。但是，当你想要了解两个变量之间的关系如何依赖于多个其他变量时呢？

最好的方法可能是多次绘制。因为[`relplot()`](../generated/seaborn.relplot.html#seaborn.relplot "seaborn.relplot")基于[`FacetGrid`](../generated/seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")，所以这很容易做到。要显示附加变量的影响，而不是将其分配给图中的一个语义角色，而是使用它来“切面”可视化。这意味着您可以创建多个轴并在每个轴上绘制数据的子集:

```python
sns.relplot(x="total_bill", y="tip", hue="smoker", col="time", data=tips)
```

![http://seaborn.pydata.org/_images/relational_55_0.png](assets/68f664bef239fbf14e3fa497f5ff9a02.jpg)


您还可以通过这种方式显示两个变量的影响：一个是通过在列上切面而另一个是在行上切面。当您开始向网格添加更多变量时，您可能希望减小图形大小。请记住，大小[`FacetGrid`](../generated/seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")由每个切面的高度和长宽比参数化的：

```python
sns.relplot(x="timepoint", y="signal", hue="subject",
            col="region", row="event", height=3,
            kind="line", estimator=None, data=fmri)
```

![http://seaborn.pydata.org/_images/relational_57_0.png](assets/6f53de8393d846c1c350ac9a5e85c900.jpg)


当您想要检查一个变量的多个级别的效果时，在列上对该变量进行切面处理，然后将切面“包装”到行中:

```python
sns.relplot(x="timepoint", y="signal", hue="event", style="event",
            col="subject", col_wrap=5,
            height=3, aspect=.75, linewidth=2.5,
            kind="line", data=fmri.query("region == 'frontal'"))
```

![http://seaborn.pydata.org/_images/relational_59_0.png](assets/f6baefd0e5308da156602144fc329c25.jpg)


这些可视化通常被称为格点图，它们非常有效，因为它们以总体模式和与这些模式的偏差的数据格式来呈现数据，便于眼睛观察。虽然你应该利用[`scatterplot()`](../generated/seaborn.scatterplot.html#seaborn.scatterplot "seaborn.scatterplot")和[`relplot()`](../generated/seaborn.relplot.html#seaborn.relplot "seaborn.relplot")提供的灵活性，但一定要记住，几个简单的图通常比一个复杂的图更有效。

# 可视化分类数据


在[绘制关系图](relational.html#relational-tutorial)的教程中，我们学习了如何使用不同的可视化方法来展示数据集中多个变量之间的关系。在示例中，我们专注于两个数值变量之间的主要关系。如果其中一个主要变量是“可分类的”（能被分为不同的组），那么我们可以使用更专业的可视化方法。

在 seaborn 中，有几种不同的方法可以对分类数据进行可视化。类似于[`relplot()`](../generated/seaborn.relplot.html#seaborn.relplot "seaborn.relplot")与[`scatterplot()`](../generated/seaborn.scatterplot.html#seaborn.scatterplot "seaborn.scatterplot")或者[`lineplot()`](../generated/seaborn.lineplot.html#seaborn.lineplot "seaborn.lineplot")之间的关系，有两种方法可以制作这些图。有许多 axes-level 函数可以用不同的方式绘制分类数据，还有一个 figure-level 接口[`catplot()`](../generated/seaborn.catplot.html#seaborn.catplot "seaborn.catplot")，可以对它们进行统一的高级访问。

将不同的分类图类型视为三个不同的家族，这是很有帮助的。下面我们将详细讨论，它们是：

分类散点图：

*   [`stripplot()`](../generated/seaborn.stripplot.html#seaborn.stripplot "seaborn.stripplot") (with `kind="strip"`; the default)
*   [`swarmplot()`](../generated/seaborn.swarmplot.html#seaborn.swarmplot "seaborn.swarmplot") (with `kind="swarm"`)

分类分布图：

*   [`boxplot()`](../generated/seaborn.boxplot.html#seaborn.boxplot "seaborn.boxplot") (with `kind="box"`)
*   [`violinplot()`](../generated/seaborn.violinplot.html#seaborn.violinplot "seaborn.violinplot") (with `kind="violin"`)
*   [`boxenplot()`](../generated/seaborn.boxenplot.html#seaborn.boxenplot "seaborn.boxenplot") (with `kind="boxen"`)

分类估计图：

*   [`pointplot()`](../generated/seaborn.pointplot.html#seaborn.pointplot "seaborn.pointplot") (with `kind="point"`)
*   [`barplot()`](../generated/seaborn.barplot.html#seaborn.barplot "seaborn.barplot") (with `kind="bar"`)
*   [`countplot()`](../generated/seaborn.countplot.html#seaborn.countplot "seaborn.countplot") (with `kind="count"`)

这些家族使用不同的粒度级别来表示数据，我们应该根据实际情况来决定到底要使用哪个。它们有统一的 API，所以我们可以轻松地在不同类型之间进行切换，并从多个角度来观察数据。

在本教程中，我们主要关注 figure-level 接口[`catplot()`](../generated/seaborn.catplot.html#seaborn.catplot "seaborn.catplot")。这个函数是上述每个函数更高级别的接口，因此当我们显示每种绘图时都会引用它们，不清楚的话可以随时查看特定类型的 API 文档。

```python
import seaborn as sns
import matplotlib.pyplot as plt
sns.set(style="ticks", color_codes=True)
```

## 分类散点图

在[`catplot()`](../generated/seaborn.catplot.html#seaborn.catplot "seaborn.catplot")中，数据默认使用散点图表示。在 seaborn 中有两种不同的分类散点图，它们采用不同的方法来表示分类数据。 其中一种是属于一个类别的所有点，将沿着与分类变量对应的轴落在相同位置。[`stripplot()`](../generated/seaborn.stripplot.html#seaborn.stripplot "seaborn.stripplot")方法是[`catplot()`](../generated/seaborn.catplot.html#seaborn.catplot "seaborn.catplot")中 kind 的默认参数，它是用少量随机“抖动”调整分类轴上的点的位置：

```python
tips = sns.load_dataset("tips")
sns.catplot(x="day", y="total_bill", data=tips);
```

![http://seaborn.pydata.org/_images/categorical_4_0.png](assets/8b6d8073c4f09f7584b0ff62e5d28d0d.jpg)

`jitter`参数控制抖动的大小，你也可以完全禁用它：

```python
sns.catplot(x="day", y="total_bill", jitter=False, data=tips)
```

![http://seaborn.pydata.org/_images/categorical_6_0.png](assets/69f7923f9645a5a6bd0152889dc41109.jpg)


另一种方法使用防止它们重叠的算法沿着分类轴调整点。我们可以用它更好地表示观测分布，但是只适用于相对较小的数据集。这种绘图有时被称为“beeswarm”，可以使用 seaborn 中的[`swarmplot()`](../generated/seaborn.swarmplot.html#seaborn.swarmplot "seaborn.swarmplot")绘制，通过在[`catplot()`](../generated/seaborn.catplot.html#seaborn.catplot "seaborn.catplot")中设置`kind="swarm"`来激活：

```python
sns.catplot(x="day", y="total_bill", kind="swarm", data=tips);
```

![http://seaborn.pydata.org/_images/categorical_8_0.png](assets/6f14991596118430a71f67d61d84933e.jpg)


与关系图类似，可以通过使用`hue`语义向分类图添加另一个维。（分类图当前不支持`size`和`style`语义 ）。 每个不同的分类绘图函数以不同方式处理`hue` 语义。对于散点图，我们只需要更改点的颜色：

```python
sns.catplot(x="day", y="total_bill", hue="sex", kind="swarm", data=tips);
```

![http://seaborn.pydata.org/_images/categorical_10_0.png](assets/5ad59a428adab872b085f94007435de3.jpg)


与数值数据不同，如何沿着轴顺序排列分类变量并不总是显而易见的。通常，seaborn 分类绘图函数试图从数据中推断出类别的顺序。 如果您的数据具有 pandas 中的`Categorical`数据类型，则可以在此处设置类别的默认顺序。如果传递给分类轴的变量看起来是数字，则将对级别进行排序。但是数据仍然被视为分类并在分类轴上的序数位置（特别是 0，1，......）处理，即使用数字来标记它们：

```python
sns.catplot(x="size", y="total_bill", kind="swarm", data=tips.query("size != 3"))
```

![http://seaborn.pydata.org/_images/categorical_12_0.png](assets/0dc6d842f7d01eca73fa0b7426a6d152.jpg)


选择默认排序的另一个选项是获取数据集中出现的类别级别。也可以使用`order`参数在特定图表的基础上控制排序。在同一图中绘制多个分类图时，这很重要，我们将在下面看到更多：

```python
sns.catplot(x="smoker", y="tip", order=["No", "Yes"], data=tips)
```

![http://seaborn.pydata.org/_images/categorical_14_0.png](assets/ff513e6aaa65c95e4855ad207cee0757.jpg)


我们已经提到了“分类轴”的概念。在这些示例中，它始终对应于水平轴。但是将分类变量放在垂直轴上通常很有帮助（特别是当类别名称相对较长或有许多类别时）。为此，我们交换轴上的变量赋值：

```python
sns.catplot(x="total_bill", y="day", hue="time", kind="swarm", data=tips)
```

![http://seaborn.pydata.org/_images/categorical_16_0.png](assets/8755b1ae3c5d3d74addc89c78230b77c.jpg)


## 类别内观察点的分布

随着数据集的大小增加，分类散点图中每个类别可以提供的值分布信息受到限制。当发生这种情况时，有几种方法可以总结分布信息，以便于我们可以跨分类级别进行简单比较。

### 箱线图

第一个是熟悉的[`boxplot()`](../generated/seaborn.boxplot.html#seaborn.boxplot "seaborn.boxplot")。它可以显示分布的三个四分位数值以及极值。“胡须”延伸到位于下四分位数和上四分位数的 1.5 IQR 内的点，超出此范围的观察值会独立显示。这意味着箱线图中的每个值对应于数据中的实际观察值。

```python
sns.catplot(x="day", y="total_bill", kind="box", data=tips)
```

![http://seaborn.pydata.org/_images/categorical_18_0.png](assets/808e3bf946312498595c712d13d5401a.jpg)


添加`hue`语义, 语义变量的每个级别的框沿着分类轴移动，因此它们将不会重叠：

```python
sns.catplot(x="day", y="total_bill", hue="smoker", kind="box", data=tips)
```

![http://seaborn.pydata.org/_images/categorical_20_0.png](assets/1f4ad95dbd00806b549ece0b87386cf9.jpg)


此行为被称为“dodging”，默认开启，因为我们假定语义变量嵌套在主分类变量中。如果不是这样，可以禁用 dodging：

```python
tips["weekend"] = tips["day"].isin(["Sat", "Sun"])
sns.catplot(x="day", y="total_bill", hue="weekend",
            kind="box", dodge=False, data=tips)
```

![http://seaborn.pydata.org/_images/categorical_22_0.png](assets/3690d2883f41b811443d3bfd92e7a9d7.jpg)


一个相关的函数[`boxenplot()`](../generated/seaborn.boxenplot.html#seaborn.boxenplot "seaborn.boxenplot")可以绘制一个与 Box-plot 类似的图。它为了显示更多信息而对分布的形状进行了优化，比较适合于较大的数据集：

```python
diamonds = sns.load_dataset("diamonds")
sns.catplot(x="color", y="price", kind="boxen",
            data=diamonds.sort_values("color"))
```

![http://seaborn.pydata.org/_images/categorical_24_0.png](assets/79a1d5cfd5409b9ac27b12fa7ec657d6.jpg)


### 小提琴图

另一种方法是[`violinplot()`](../generated/seaborn.violinplot.html#seaborn.violinplot "seaborn.violinplot")，它将箱线图与[分布](distributions.html#distribution-tutorial)教程中描述的核密度估算程序结合起来：

```python
sns.catplot(x="total_bill", y="day", hue="time",
            kind="violin", data=tips)
```

![http://seaborn.pydata.org/_images/categorical_26_0.png](assets/d9dc602fc56dec13039df115c589dd05.jpg)


该方法使用核密度估计来提供更丰富的值分布描述。此外，violin 中还显示了来自箱线图的四分位数和 whikser 值。缺点是由于 violinplot 使用了 KDE，我们需要调整一些额外参数，与箱形图相比增加了一些复杂性：

```python
sns.catplot(x="total_bill", y="day", hue="time",
            kind="violin", bw=.15, cut=0,
            data=tips);
```

![http://seaborn.pydata.org/_images/categorical_28_0.png](assets/93d6927062ff93035052ed79d6e8e224.jpg)


当 hue 参数只有两个级别时，也可以“拆分”violins，这样可以更有效地利用空间：

```python
sns.catplot(x="day", y="total_bill", hue="sex",
            kind="violin", split=True, data=tips);
```

![http://seaborn.pydata.org/_images/categorical_30_0.png](assets/a450fb151022958086f1733274e4f66b.jpg)


最后，在 violin 内的绘图有几种选项，包括显示每个独立的观察而不是摘要箱线图值的方法：

```python
sns.catplot(x="day", y="total_bill", hue="sex",
            kind="violin", inner="stick", split=True,
            palette="pastel", data=tips);
```

![http://seaborn.pydata.org/_images/categorical_32_0.png](assets/a1fee05f41f1d43a9d9a6f1b9c301e0d.jpg)


我们也可以将[`swarmplot()`](../generated/seaborn.swarmplot.html#seaborn.swarmplot "seaborn.swarmplot")或`striplot()`与箱形图或 violin plot 结合起来，展示每次观察以及分布摘要：

```python
g = sns.catplot(x="day", y="total_bill", kind="violin", inner=None, data=tips)
sns.swarmplot(x="day", y="total_bill", color="k", size=3, data=tips, ax=g.ax);
```

![http://seaborn.pydata.org/_images/categorical_34_0.png](assets/26ac3dbbb98ec93352f776de6e6d2b65.jpg)


## 类别内的统计估计

对于其他应用程序，你可能希望显示值的集中趋势估计，而不是显示每个类别中的分布。Seaborn 有两种主要方式来显示这些信息。重要的是，这些功能的基本 API 与上面讨论的 API 相同。

### 条形图

实现这一目标的是我们熟悉的条形图。在 seaborn 中，[`barplot()`](../generated/seaborn.barplot.html#seaborn.barplot "seaborn.barplot")函数在完整数据集上运行并应用函数来获取估计值（默认情况下取平均值）。当每个类别中有多个观察值时，它还使用自举来计算估计值周围的置信区间，并使用误差条绘制：

```python
titanic = sns.load_dataset("titanic")
sns.catplot(x="sex", y="survived", hue="class", kind="bar", data=titanic);
```

![http://seaborn.pydata.org/_images/categorical_36_0.png](assets/727bcad15c428cfdd74d27db87677157.jpg)


条形图的一个特例是，当你想要显示每个类别中的观察数量而不是计算第二个变量的统计数据时。这类似于分类而非定量变量的直方图。在 seaborn 中，使用[`countplot()`](../generated/seaborn.countplot.html#seaborn.countplot "seaborn.countplot")函数很容易实现：

```python
sns.catplot(x="deck", kind="count", palette="ch:.25", data=titanic)
```

![http://seaborn.pydata.org/_images/categorical_38_0.png](assets/9788e48ccbd895c1e539c7de634be115.jpg)


无论是[`barplot()`](../generated/seaborn.barplot.html#seaborn.barplot "seaborn.barplot")还是[`countplot()`](../generated/seaborn.countplot.html#seaborn.countplot "seaborn.countplot")，都可以使用上面讨论的所有选项，也可以调用每个函数的文档示例中的其他选项：

```python
sns.catplot(y="deck", hue="class", kind="count",
            palette="pastel", edgecolor=".6",
            data=titanic)
```

![http://seaborn.pydata.org/_images/categorical_40_0.png](assets/642ca1bac62921f4290064ab256538bb.jpg)


### 点图

[`pointplot()`](../generated/seaborn.pointplot.html#seaborn.pointplot "seaborn.pointplot")函数提供了另一种可视化相同信息的样式。此函数还对另一个轴上的高度估计值进行编码，但不是显示一个完整的条形图，而是绘制点估计值和置信区间。另外，[`pointplot()`](../generated/seaborn.pointplot.html#seaborn.pointplot "seaborn.pointplot")连接来自相同`hue`类别的点。我们可以很容易的看出主要关系如何随着色调语义的变化而变化，因为人类的眼睛很擅长观察斜率的差异：

```python
sns.catplot(x="sex", y="survived", hue="class", kind="point", data=titanic);
```

![http://seaborn.pydata.org/_images/categorical_42_0.png](assets/687f85f5e0e06a68e6150c0533ff1748.jpg)


当分类函数缺少关系函数中的`style`语义时, 将 markers 和 linestyles 与色调一起改变，以制作最大可访问的图形并在黑白中重现良好，这仍然是一个好主意：

```python
sns.catplot(x="class", y="survived", hue="sex",
            palette={"male": "g", "female": "m"},
            markers=["^", "o"], linestyles=["-", "--"],
            kind="point", data=titanic);
```

![http://seaborn.pydata.org/_images/categorical_44_0.png](assets/77fc98bcda361af76626399c4a1e7ad8.jpg)


## 绘制“宽格式”数据

虽然优选使用“长形式”或“整齐”数据，但这些函数也可以应用于各种“宽格式”的数据，包括 pandas DataFrames 或二维 numpy 数组。这些对象应该直接传递给`data`参数： 

```python
iris = sns.load_dataset("iris")
sns.catplot(data=iris, orient="h", kind="box");
```

![http://seaborn.pydata.org/_images/categorical_46_0.png](assets/5307ba94c8d5a2ecca04c697414a762a.jpg)


另外，axes-level 函数接受 Pandas 或 numpy 对象的向量，而不是`DataFrame`中的变量：

```python
sns.violinplot(x=iris.species, y=iris.sepal_length);
```

![http://seaborn.pydata.org/_images/categorical_48_0.png](assets/426ace59c775cc095c6128e35bcff1b3.jpg)


要控制上面讨论的函数绘制的图形的大小和形状，你必须使用 matplotlib 命令来进行设置：

```python
f, ax = plt.subplots(figsize=(7, 3))
sns.countplot(y="deck", data=titanic, color="c");
```

![http://seaborn.pydata.org/_images/categorical_50_0.png](assets/b18371fc898cfe7904a1fc240f6fe4e8.jpg)


当你需要一个分类图与一个更复杂的其他类型的图共存时，你可以采取这种方法。

## 显示与 facet 的多种关系

就像[`relplot()`](../generated/seaborn.relplot.html#seaborn.relplot "seaborn.relplot")一样, [`catplot()`](../generated/seaborn.catplot.html#seaborn.catplot "seaborn.catplot")建立在[`FacetGrid`](../generated/seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")上，这意味着很容易添加层面变量来可视化高维关系：

```python
sns.catplot(x="day", y="total_bill", hue="smoker",
            col="time", aspect=.6,
            kind="swarm", data=tips);
```

![http://seaborn.pydata.org/_images/categorical_52_0.png](assets/c8197cd6e74c99a9483c72ed6b43a01a.jpg)


要进一步自定义绘图，我们可以使用它返回的[`FacetGrid`](../generated/seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")对象上的方法：

```python
g = sns.catplot(x="fare", y="survived", row="class",
                kind="box", orient="h", height=1.5, aspect=4,
                data=titanic.query("fare > 0"))
g.set(xscale="log");
```

![http://seaborn.pydata.org/_images/categorical_54_0.png](assets/64c8d092f1c51150e58e0a41dfcad834.jpg)


# 可视化数据集的分布

在处理一组数据时，您通常想做的第一件事就是了解变量的分布情况。本教程的这一章将简要介绍 seaborn 中用于检查单变量和双变量分布的一些工具。 您可能还需要查看[categorical.html]（categorical.html #categical-tutorial）章节中的函数示例，这些函数可以轻松地比较变量在其他变量级别上的分布。

```python
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats
sns.set(color_codes=True)
```

## 绘制单变量分布

在 seaborn 中想要快速查看单变量分布的最方便的方法是使用[`distplot()`](../generated/seaborn.distplot.html#seaborn.distplot "seaborn.distplot")函数。默认情况下，该方法将会绘制直方图[histogram](https://en.wikipedia.org/wiki/Histogram)并拟合[内核密度估计] [kernel density estimate](https://en.wikipedia.org/wiki/Kernel_density_estimation) (KDE).

```python
x = np.random.normal(size=100)
sns.distplot(x);
```

![http://seaborn.pydata.org/_images/distributions_6_0.png](assets/fea324aca2ed4416872749b8352a5412.jpg)


### 直方图

对于直方图我们可能很熟悉，而且 matplotlib 中已经存在`hist`函数。 直方图首先确定数据区间，然后观察数据落入这些区间中的数量来绘制柱形图以此来表征数据的分布情况。
为了说明这一点，让我们删除密度曲线并添加一个 rug plot，它在每个观察值上画一个小的垂直刻度。您可以使用[`rugplot()`](../generated/seaborn.rugplot.html#seaborn.rugplot "seaborn.rugplot") 函数来创建 rugplot 本身，但是也可以在 [`distplot()`](../generated/seaborn.distplot.html#seaborn.distplot "seaborn.distplot")中使用:

```python
sns.distplot(x, kde=False, rug=True);
```

![http://seaborn.pydata.org/_images/distributions_8_0.png](assets/3a0a2053efeea3a9932d764e2d71470d.jpg)


在绘制柱状图时，您的主要选择是要使用的“桶”的数量和放置它们的位置。 [`distplot()`](../generated/seaborn.distplot.html#seaborn.distplot "seaborn.distplot") 使用一个简单的规则来很好地猜测默认情况下正确的数字是多少，但是尝试更多或更少的“桶”可能会揭示数据中的其他特性:

```python
sns.distplot(x, bins=20, kde=False, rug=True);
```

![http://seaborn.pydata.org/_images/distributions_10_0.png](assets/5193c672119d848c7926379d43f7f0cc.jpg)


### 核密度估计

可能你对核密度估计不太熟悉，但它可以是绘制分布形状的有力工具。和直方图一样，KDE 图沿另一个轴的高度，编码一个轴上的观测密度：

```python
sns.distplot(x, hist=False, rug=True);
```

![http://seaborn.pydata.org/_images/distributions_12_0.png](assets/a6d422236da60cc9bd01d12080b60453.jpg)


绘制 KDE 比绘制直方图更需要计算。每个观测值首先被一个以该值为中心的正态(高斯)曲线所取代:

```python
x = np.random.normal(0, 1, size=30)
bandwidth = 1.06 * x.std() * x.size ** (-1 / 5.)
support = np.linspace(-4, 4, 200)
kernels = []for x_i in x:    kernel = stats.norm(x_i, bandwidth).pdf(support)    kernels.append(kernel)    plt.plot(support, kernel, color="r")
sns.rugplot(x, color=".2", linewidth=3);
```

![http://seaborn.pydata.org/_images/distributions_14_0.png](assets/31ee2d7a3dfda467565a2053ac19a38f.jpg)


接下来，对这些曲线求和，计算支持网格(support grid)中每个点的密度值。然后对得到的曲线进行归一化，使曲线下的面积等于 1:

```python
from scipy.integrate import trapzdensity = np.sum(kernels, axis=0)
density /= trapz(density, support)
plt.plot(support, density);
```

![http://seaborn.pydata.org/_images/distributions_16_0.png](assets/d0ff3115fb5935fe56c1bb8123d5ddce.jpg)


我们可以看到，如果在 seaborn 中使用[`kdeplot()`](../generated/seaborn.kdeplot.html#seaborn.kdeplot "seaborn.kdeplot") 函数， 我们可以得到相同的曲线。这个函数也被[`distplot()`](../generated/seaborn.distplot.html#seaborn.distplot "seaborn.distplot")所使用, 但是当您只想要密度估计时，它提供了一个更直接的接口，可以更容易地访问其他选项:

```python
sns.kdeplot(x, shade=True);
```

![http://seaborn.pydata.org/_images/distributions_18_0.png](assets/247df80468d3edbc28836cb1cc56c81c.jpg)


KDE 的带宽（`bw`）参数控制估计与数据的拟合程度，就像直方图中的 bin 大小一样。 它对应于我们在上面绘制的内核的宽度。 默认行为尝试使用常用参考规则猜测一个好的值，但尝试更大或更小的值可能会有所帮助：

```python
sns.kdeplot(x)
sns.kdeplot(x, bw=.2, label="bw: 0.2")
sns.kdeplot(x, bw=2, label="bw: 2")
plt.legend();
```

![http://seaborn.pydata.org/_images/distributions_20_0.png](assets/8a713fe4da039acf9c3a4e70b274b60a.jpg)


正如您在上面所看到的，高斯 KDE 过程的本质意味着估计超出了数据集中最大和最小的值。有可能控制超过极值多远的曲线是由'cut'参数绘制的;然而，这只影响曲线的绘制方式，而不影响曲线的拟合方式:

```python
sns.kdeplot(x, shade=True, cut=0)
sns.rugplot(x);
```

![http://seaborn.pydata.org/_images/distributions_22_0.png](assets/63e498131614f726dd72a90161b58971.jpg)


### 拟合参数分布

您还可以使用 [`distplot()`](../generated/seaborn.distplot.html#seaborn.distplot "seaborn.distplot")

将参数分布拟合到数据集上，并直观地评估其与观测数据的对应程度:

```python
x = np.random.gamma(6, size=200)
sns.distplot(x, kde=False, fit=stats.gamma);
```

![http://seaborn.pydata.org/_images/distributions_24_0.png](assets/cf48dc45f5484db58f3d310e434b11a2.jpg)


## 绘制二元分布

它对于可视化两个变量的二元分布也很有用。在 seaborn 中，最简单的方法就是使用[`jointplot()`](../generated/seaborn.jointplot.html#seaborn.jointplot "seaborn.jointplot")函数，它创建了一个多面板图形，显示了两个变量之间的二元(或联合)关系，以及每个变量在单独轴上的一元(或边际)分布。

```python
mean, cov = [0, 1], [(1, .5), (.5, 1)]data = np.random.multivariate_normal(mean, cov, 200)
df = pd.DataFrame(data, columns=["x", "y"])
```

### 散点图

可视化二元分布最常见的方法是散点图，其中每个观察点都以 _x_ 和 _y_ 值表示。 这类似于二维 rug plot。 您可以使用 matplotlib 的`plt.scatter` 函数绘制散点图, 它也是 [`jointplot()`](../generated/seaborn.jointplot.html#seaborn.jointplot "seaborn.jointplot")函数显示的默认类型的图:

```python
sns.jointplot(x="x", y="y", data=df);
```

![http://seaborn.pydata.org/_images/distributions_28_0.png](assets/66ba868aeef60b82d90c872e188217ed.jpg)


### 六边形“桶”(Hexbin)图

类似于单变量的直方图，用于描绘二元变量关系的图称为 “hexbin” 图,因为它显示了落入六边形“桶”内的观察计数。 此图对于相对较大的数据集最有效。它可以通过调用 matplotlib 中的 `plt.hexbin`函数获得并且在[`jointplot()`](../generated/seaborn.jointplot.html#seaborn.jointplot "seaborn.jointplot")作为一种样式。当使用白色作为背景色时效果最佳。

```python
x, y = np.random.multivariate_normal(mean, cov, 1000).Twith sns.axes_style("white"):    sns.jointplot(x=x, y=y, kind="hex", color="k");
```

![http://seaborn.pydata.org/_images/distributions_30_0.png](assets/621cac508b507f43ba50f91290aea5fd.jpg)


### 核密度估计

也可以使用上面描述的核密度估计过程来可视化二元分布。在 seaborn 中，这种图用等高线图表示， 在[`jointplot()`](../generated/seaborn.jointplot.html#seaborn.jointplot "seaborn.jointplot")中被当作一种样式:

```python
sns.jointplot(x="x", y="y", data=df, kind="kde");
```

![http://seaborn.pydata.org/_images/distributions_32_0.png](assets/3fa9b8716f00e81aa6ca6864cb110e2b.jpg)


您还可以使用[`kdeplot()`](../generated/seaborn.kdeplot.html#seaborn.kdeplot "seaborn.kdeplot")函数绘制二维核密度图。这允许您在一个特定的(可能已经存在的)
matplotlib 轴上绘制这种图，而 [`jointplot()`](../generated/seaborn.jointplot.html#seaborn.jointplot "seaborn.jointplot") 函数能够管理它自己的图:

```python
f, ax = plt.subplots(figsize=(6, 6))
sns.kdeplot(df.x, df.y, ax=ax)
sns.rugplot(df.x, color="g", ax=ax)
sns.rugplot(df.y, vertical=True, ax=ax);
```

![http://seaborn.pydata.org/_images/distributions_34_0.png](assets/5bbf1afea90de1dcab11584fb0169efe.jpg)


如果希望更连续地显示双变量密度，可以简单地增加轮廓层的数量:

```python
f, ax = plt.subplots(figsize=(6, 6))
cmap = sns.cubehelix_palette(as_cmap=True, dark=0, light=1, reverse=True)
sns.kdeplot(df.x, df.y, cmap=cmap, n_levels=60, shade=True);
```

![http://seaborn.pydata.org/_images/distributions_36_0.png](assets/fd8b7fa16dccb291fe1a2148a45e3eba.jpg)


[`jointplot()`](../generated/seaborn.jointplot.html#seaborn.jointplot "seaborn.jointplot")函数使用[`JointGrid`](../generated/seaborn.JointGrid.html#seaborn.JointGrid "seaborn.JointGrid")来管理图形。为了获得更大的灵活性，您可能想直接使用[`JointGrid`](../generated/seaborn.JointGrid.html#seaborn.JointGrid "seaborn.JointGrid")来绘制图形。[`jointplot()`](../generated/seaborn.jointplot.html#seaborn.jointplot "seaborn.jointplot")在绘图后返回[`JointGrid`](../generated/seaborn.JointGrid.html#seaborn.JointGrid "seaborn.JointGrid")对象，您可以使用它添加更多图层或调整可视化的其他方面：

```python
g = sns.jointplot(x="x", y="y", data=df, kind="kde", color="m")
g.plot_joint(plt.scatter, c="w", s=30, linewidth=1, marker="+")
g.ax_joint.collections[0].set_alpha(0)
g.set_axis_labels("$X$", "$Y$");
```

![http://seaborn.pydata.org/_images/distributions_38_0.png](assets/aeaafccce597b72105feb6cf712b0ca2.jpg)


## 可视化数据集中的成对关系

要在数据集中绘制多个成对的双变量分布，您可以使用[`pairplot()`](../generated/seaborn.pairplot.html#seaborn.pairplot "seaborn.pairplot")函数。 这将创建一个轴矩阵并显示 DataFrame 中每对列的关系，默认情况下，它还绘制对角轴上每个变量的单变量分布：

```python
iris = sns.load_dataset("iris")
sns.pairplot(iris);
```

![http://seaborn.pydata.org/_images/distributions_40_0.png](assets/bea67bf34fcd01d7b6f454ae5f563460.jpg)


与[`jointplot()`](../generated/seaborn.jointplot.html#seaborn.jointplot "seaborn.jointplot")和[`JointGrid`](../generated/seaborn.JointGrid.html#seaborn.JointGrid "seaborn.JointGrid")之间的关系非常类似， [`pairplot()`](../generated/seaborn.pairplot.html#seaborn.pairplot "seaborn.pairplot")函数构建在[`PairGrid`](../generated/seaborn.PairGrid.html#seaborn.PairGrid "seaborn.PairGrid")对象之上, 可以直接使用它来获得更大的灵活性：

```python
g = sns.PairGrid(iris)
g.map_diag(sns.kdeplot)
g.map_offdiag(sns.kdeplot, n_levels=6);
```

![http://seaborn.pydata.org/_images/distributions_42_0.png](assets/c65d91122f8de69b16659df5ab31214e.jpg)


# 线性关系可视化


许多数据集包含多定量变量，并且分析的目的通常是将这些变量联系起来。我们[之前讨论](#/docs/5)可以通过显示两个变量相关性的来实现此目的的函数。但是，使用统计模型来估计两组噪声观察量之间的简单关系可能会非常有效。本章讨论的函数将通过线性回归的通用框架实现。

本着图凯(Tukey)精神，seaborn 中的回归图主要用于添加视觉指南，以助于在探索性数据分析中强调存在于数据集的模式。换而言之，seaborn 本身不是为统计分析而生。要获得与回归模型拟合相关定量度量，你应当使用 [statsmodels](https://www.statsmodels.org/). 然而，seaborn 的目标是通过可视化快速简便地 3 探索数据集，因为这样做，如果说不上更，是与通过统计表探索数据集一样重要。

``` python
import numpy as npimport seaborn as snsimport matplotlib.pyplot as plt
```

``` python
sns.set(color_codes=True)
```

``` python
tips = sns.load_dataset("tips")
```

## 绘制线性回归模型的函数

seaborn 中两个主要函数主要用于显示回归确定的线性关系。这些函数，[`regplot()`](../generated/seaborn.regplot.html＃seaborn.regplot"seaborn.regplot") 和 [`lmplot()`](../generated/seaborn.lmplot.html＃seaborn.lmplot"seaborn.lmplot")， 之间密切关联，并且共享核心功能。但是，了解它们的不同之处非常重要，这样你就可以快速为特定工作选择正确的工具。

在最简单的调用中，两个函数都绘制了两个变量，`x`和`y`，然后拟合回归模型`y~x`并绘制得到回归线和该回归的 95%置信区间：

```python
sns.regplot(x="total_bill", y="tip", data=tips);
```

![http://seaborn.pydata.org/_images/regression_7_0.png](assets/06f05fb132ef0dc7f9cca60a16b84f1a.jpg)


```python
sns.lmplot(x="total_bill", y="tip", data=tips);
```

![http://seaborn.pydata.org/_images/regression_8_0.png](assets/ff165b205839381bf1fa11c2df3d176d.jpg)


你应当注意到，除了图形形状不同，两幅结果图是完全一致的。我们会在后面解释原因。目前，要了解的另一个主要区别是[`regplot()`](../generated/seaborn.regplot.html＃seaborn.regplot"seaborn.regplot")接受多种格式的`x`和`y`变量，包括简单的 numpy 数组，pandas `Series`对象，或者作为对传递给`data`的 pandas `DataFrame`对象。相反，[`lmplot()`](../generated/seaborn.lmplot.html＃seaborn.lmplot"seaborn.lmplot")将`data`作为必须参数，`x`和`y`变量必须被指定为字符串。这种数据格式被称为"长格式"或["整齐"](https://vita.had.co.nz/papers/tidy-data.pdf)数据。 除了这种输入的灵活性之外，[`regplot()`](../ generated / seaborn.regplot.html＃seaborn.regplot"seaborn.regplot")拥有[`lmplot()`](../generated/seaborn.lmplot.html＃seaborn.lmplot"seaborn.lmplot")一个子集的功能，所以我们将使用后者来演示它们。

当其中一个变量采用离散值时，可以拟合线性回归。但是，这种数据集生成的简单散点图通常不是最优的：

```python
sns.lmplot(x="size", y="tip", data=tips);
```

![http://seaborn.pydata.org/_images/regression_10_0.png](assets/18916d57b3e69c5de1769a7b26e0f713.jpg)


一种选择是向离散值添加随机噪声("抖动")，以使这些值分布更清晰。需要注意的是，抖动仅用于散点图数据，而不会影响回归线本身拟合：

```python
sns.lmplot(x="size", y="tip", data=tips, x_jitter=.05);
```

![http://seaborn.pydata.org/_images/regression_12_0.png](assets/2e23cc65e61b2659c68ac3746b8ebad1.jpg)


第二种选择是综合每个离散箱中的观测值，以绘制集中趋势的估计值和置信区间：

```python
sns.lmplot(x="size", y="tip", data=tips, x_estimator=np.mean);
```

![http://seaborn.pydata.org/_images/regression_14_0.png](assets/4dda0f476dee5bc15c815a9403521409.jpg)


## 拟合不同模型

上面使用的简单线性回归模型非常容易拟合，但是它不适合某些类型的数据集。[Anscombe 的四重奏](https://en.wikipedia.org/wiki/Anscombe%27s_quartet)数据集展示了一些实例，其中简单线性回归提供了相同的关系估计，然而简单的视觉检查清楚地显示了差异。例如，在第一种情况下，线性回归是一个很好的模型：

```python
anscombe = sns.load_dataset("anscombe")
```

```python
sns.lmplot(x="x", y="y", data=anscombe.query("dataset == 'I'"),
           ci=None, scatter_kws={"s": 80});
```

![http://seaborn.pydata.org/_images/regression_17_0.png](assets/7facbea48d8c72d5400863700e07aa18.jpg)


第二个数据集的线性关系是相同的，但是图表清楚地表明这并不是一个好的模型：

```python
sns.lmplot(x="x", y="y", data=anscombe.query("dataset == 'II'"),
           ci=None, scatter_kws={"s": 80});
```

![http://seaborn.pydata.org/_images/regression_19_0.png](assets/af2ff74dd015731938ea83ab029d73e7.jpg)


在这些存在高阶关系的情况下，[`regplot()`](../generated/seaborn.regplot.html＃seaborn.regplot"seaborn.regplot")和[`lmplot()`](./generated/seaborn.regplot.html#seaborn.regplot"seaborn.regplot")可以拟合多项式回归模型来探索数据集中的简单非线性趋势：

```python
sns.lmplot(x="x", y="y", data=anscombe.query("dataset == 'II'"),
           order=2, ci=None, scatter_kws={"s": 80});
```

![http://seaborn.pydata.org/_images/regression_21_0.png](assets/b2cde7454aa9f4cc76186d93a3cab208.jpg)


"离群值"观察引起的另一个问题是，除了研究中的主要关系之外，由于某种原因导致的偏离：

```python
sns.lmplot(x="x", y="y", data=anscombe.query("dataset == 'III'"),
           ci=None, scatter_kws={"s": 80});
```

![http://seaborn.pydata.org/_images/regression_23_0.png](assets/d9085f52d35beabc56a47c8326aee8cf.jpg)


在存在异常值的情况下，拟合稳健回归可能会很有用，该回归使用了一种不同的损失函数来降低相对较大的残差的权重：

```python
sns.lmplot(x="x", y="y", data=anscombe.query("dataset == 'III'"),
           robust=True, ci=None, scatter_kws={"s": 80});
```

![http://seaborn.pydata.org/_images/regression_25_0.png](assets/696b41cce47f6657e61372ff64381129.jpg)


当`y`变量是二进制时，简单线性回归也"有效"，但提供了难以置信的预测：

```python
tips["big_tip"] = (tips.tip / tips.total_bill) > .15sns.lmplot(x="total_bill", y="big_tip", data=tips,
           y_jitter=.03);
```

![http://seaborn.pydata.org/_images/regression_27_0.png](assets/2b4711757bd351625d2244392491e1cf.jpg)


在这种情况下的解决方案是拟合逻辑回归，使得回归线对给定值`x`显示的估计概率`y=1`。

```python
sns.lmplot(x="total_bill", y="big_tip", data=tips,
           logistic=True, y_jitter=.03);
```

![http://seaborn.pydata.org/_images/regression_29_0.png](assets/afea667bcdb80a09c24a25f5d5d0bd82.jpg)


请注意，逻辑回归估计比简单回归计算密集程度更高(稳健回归也是如此)，并且由于回归线周围的置信区间是使用自举程度计算，你可能希望关闭它来达到更快的迭代(使用`ci=None`)。

一种完全不同的方法是使用[lowess smoother](https://en.wikipedia.org/wiki/Local_regression)拟合非参数回归。尽管它是计算密集型的，这种方法的假设最少，因此目前置信区间根本没有计算：

```python
sns.lmplot(x="total_bill", y="tip", data=tips,
           lowess=True);
```

![http://seaborn.pydata.org/_images/regression_31_0.png](assets/640b99998425ffa57b452eca0b5e129b.jpg)


[`residplot()`](../generated/seaborn.residplot.html#seaborn.residplot "seaborn.residplot") 函数可以用作检查简单回归模型是否适合数据集的有效工具。它拟合并删除简单的线性回归，然后绘制每个观察值的残差值。理想情况下，这些值应随机散步在`y=0`周围：

```python
sns.residplot(x="x", y="y", data=anscombe.query("dataset == 'I'"),
              scatter_kws={"s": 80});
```

![http://seaborn.pydata.org/_images/regression_33_0.png](assets/86f9e6a8b938de1a3aeb9b237b524f0b.jpg)


如果残差中存在结构形状，则表明简单的线性回归不合适：

```python
sns.residplot(x="x", y="y", data=anscombe.query("dataset == 'II'"),
              scatter_kws={"s": 80});
```

![http://seaborn.pydata.org/_images/regression_35_0.png](assets/61c737744b3c54b93ad9bc691e4461bc.jpg)


## 其他变量关系

 上面的图显示了探索一对变量之间关系的许多方法。然而，通常，一个更有趣的问题是"这两个变量之间的关系如何随第三个变量的变化而变化？"这就是[`regplot()`](../generated/seaborn.regplot.html#seaborn.regplot "seaborn.regplot")和[`lmplot()`](../generated/seaborn.lmplot.html#seaborn.lmplot "seaborn.lmplot")的区别所在。[`regplot()`](../generated/seaborn.regplot.html#seaborn.regplot "seaborn.regplot")总是表现单一关系, [`lmplot()`](../generated/seaborn.lmplot.html#seaborn.lmplot "seaborn.lmplot")把[`regplot()`](../generated/seaborn.regplot.html#seaborn.regplot "seaborn.regplot")和 [`FacetGrid`](../generated/seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")结合，以提供一个简单的界面，显示"facet"图的线性回归，使你可以探索与最多三个其他分类变量的交互。

分离关系的最佳方法是在同一轴上绘制两个级别并使用颜色来区分它们：

```python
sns.lmplot(x="total_bill", y="tip", hue="smoker", data=tips);
```

![http://seaborn.pydata.org/_images/regression_37_0.png](assets/c1be6d813a335d32887cfd051ef9167f.jpg)


除了颜色之外，还可以使用不同的散点图标记来使绘图更好地再现为黑白。你还可以完全控制使用的颜色：

```python
sns.lmplot(x="total_bill", y="tip", hue="smoker", data=tips,
           markers=["o", "x"], palette="Set1");
```

![http://seaborn.pydata.org/_images/regression_39_0.png](assets/b065f9278c97b242c9a517aa98c090fa.jpg)


要添加另一个变量，你可以绘制多个"facet"，其中每个级别的变量出现在网络的行或列中：

```python
sns.lmplot(x="total_bill", y="tip", hue="smoker", col="time", data=tips);
```

![http://seaborn.pydata.org/_images/regression_41_0.png](assets/02113b4a6b876c8e3b32dd2bb7eae74c.jpg)


```python
sns.lmplot(x="total_bill", y="tip", hue="smoker",
           col="time", row="sex", data=tips);
```

![http://seaborn.pydata.org/_images/regression_42_0.png](assets/eb0843cd39205e7144eb4acdf1b8356d.jpg)


## 控制绘图的大小和形状

在之前，我们注意到[`regplot()`](../generated/seaborn.regplot.html#seaborn.regplot "seaborn.regplot")和[`lmplot()`](../generated/seaborn.lmplot.html#seaborn.lmplot "seaborn.lmplot")生成的默认图看起来相同，但却具有不同的大小和形状。这是因为[`regplot()`](../generated/seaborn.regplot.html#seaborn.regplot "seaborn.regplot")是一个"轴级"函数，它绘制在特定的轴上。这意味着你可以自己制作多面板图形并精确控制回归图的位置。如果没有明确提供轴对象，它只使用"当前活动"轴，这就是默认绘图与大多数其他 matplotlib 函数具有相同大小和形状的原因。要控制大小，你需要自己创建一个图形对象。

```python
f, ax = plt.subplots(figsize=(5, 6))
sns.regplot(x="total_bill", y="tip", data=tips, ax=ax);
```

![http://seaborn.pydata.org/_images/regression_44_0.png](assets/a94aa08c017f4688743921ccc9d8a4d0.jpg)


相比之下，[`lmplot()`](../generated/seaborn.lmplot.html#seaborn.lmplot "seaborn.lmplot")图的大小和形状是通过[`lmplot()`](http://typora-app/generated/seaborn.lmplot.html#seaborn.lmplot)接口，使用`size`和`aspect`参数控制，这些参数适用于绘图中的每个`facet`，而不是整个图形本身：

```python
sns.lmplot(x="total_bill", y="tip", col="day", data=tips,
           col_wrap=2, height=3);
```

![http://seaborn.pydata.org/_images/regression_46_0.png](assets/fce6798715088ab4d9f615ae89a67b2c.jpg)


```python
sns.lmplot(x="total_bill", y="tip", col="day", data=tips,
           aspect=.5);
```

![http://seaborn.pydata.org/_images/regression_47_0.png](assets/ea6e78ca63d3d86dece589f475f2338d.jpg)


## 在其他情境中绘制回归

其他一些 seaborn 函数在更大，更复杂的图中使用[`regplot()`](../generated/seaborn.regplot.html#seaborn.regplot "seaborn.regplot")。第一个是我们在[发行教程](distributions.html#distribution-tutorial)中引入的[`jointplot()`](../generated/seaborn.jointplot.html#seaborn.jointplot "seaborn.jointplot")。除了前面讨论的绘制风格，[`jointplot()`](../generated/seaborn.jointplot.html#seaborn.jointplot "seaborn.jointplot") 可以使用[`regplot()`](../generated/seaborn.regplot.html#seaborn.regplot "seaborn.regplot")通过传递`kind="reg"`来显示轴上的线性回归拟合：

```python
sns.jointplot(x="total_bill", y="tip", data=tips, kind="reg");
```

![http://seaborn.pydata.org/_images/regression_49_0.png](assets/0a3f41c0a016c66f0f3379c128f550b9.jpg)


使用[`pairplot()`](../generated/seaborn.pairplot.html#seaborn.pairplot "seaborn.pairplot")函数与`kind="reg"`将 [`regplot()`](../generated/seaborn.regplot.html#seaborn.regplot "seaborn.regplot")和[`PairGrid`](../generated/seaborn.PairGrid.html#seaborn.PairGrid "seaborn.PairGrid") 结合起来，来显示数据集中变量的线性关系。请注意这与[`lmplot()`](../generated/seaborn.lmplot.html#seaborn.lmplot "seaborn.lmplot")的不同之处。在下图中，两个轴在第三变量上的两个级别上没有显示相同的关系；相反，[`PairGrid()`](../generated/seaborn.PairGrid.html#seaborn.PairGrid "seaborn.PairGrid")用于显示数据集中变量的不同配对之间的多个关系。

```python
sns.pairplot(tips, x_vars=["total_bill", "size"], y_vars=["tip"],
             height=5, aspect=.8, kind="reg");
```

![http://seaborn.pydata.org/_images/regression_51_0.png](assets/65fcd97ee44e136d797a4d343a58cc4f.jpg)


像[`lmplot()`](../generated/seaborn.lmplot.html#seaborn.lmplot "seaborn.lmplot")，但不像[`jointplot()`](../generated/seaborn.jointplot.html#seaborn.jointplot "seaborn.jointplot")，额外的分类变量调节是通过`hue`参数内置在函数[`pairplot()`](../generated/seaborn.pairplot.html#seaborn.pairplot "seaborn.pairplot")中：

```python
sns.pairplot(tips, x_vars=["total_bill", "size"], y_vars=["tip"],
             hue="smoker", height=5, aspect=.8, kind="reg");
```

![http://seaborn.pydata.org/_images/regression_53_0.png](assets/b166f746ed213b5ac4e495320f99b0fd.jpg)


# 构建结构化多图网格


在探索中等维度数据时，经常需要在数据集的不同子集上绘制同一类型图的多个实例。这种技术有时被称为“网格”或“格子”绘图，它与[“多重小图”](https://en.wikipedia.org/wiki/Small_multiple)的概念有关。这种技术使查看者从复杂数据中快速提取大量信息。 Matplotlib 为绘制这种多轴图提供了很好的支持; seaborn 构建于此之上，可直接将绘图结构和数据集结构关系起来。

要使用网格图功能，数据必须在 Pandas 数据框中，并且必须采用 Hadley Whickam 所谓的 [“整洁”数据](https://vita.had.co.nz/papers/tidy-data.pdf)的形式。简言之，用来画图的数据框应该构造成每列一个变量，每一行一个观察的形式。

至于高级用法，可以直接使用本教程中讨论的对象，以提供最大的灵活性。一些 seaborn 函数（例如`lmplot()`，`catplot()`和`pairplot()`）也在后台使用它们。与其他在没有操纵图形的情况下绘制到特定的（可能已经存在的）matplotlib `Axes`上的“Axes-level” seaborn 函数不同，这些更高级别的函数在调用时会创建一个图形，并且通常对图形的设置方式更加严格。在某些情况下，这些函数或它们所依赖的类构造函数的参数将提供不同的接口属性，如`lmplot()`中的图形大小，你可以设置每个子图的高和宽高比。但是，使用这些对象的函数在绘图后都会返回它，并且这些对象大多都有方便简单的方法来改变图的绘制方式。

```python
import seaborn as sns
import matplotlib.pyplot as plt
```

```python
sns.set(style="ticks")
```

## 基于一定条件的多重小图


当你想在数据集的不同子集中分别可视化变量分布或多个变量之间的关系时，[`FacetGrid`](../generated/seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")类非常有用。 `FacetGrid`最多有三个维：`row`，`col`和`hue`。前两个与轴(axes)阵列有明显的对应关系;将色调变量`hue`视为沿深度轴的第三个维度，不同的级别用不同的颜色绘制。

首先，使用数据框初始化`FacetGrid`对象并指定将形成网格的行，列或色调维度的变量名称。这些变量应是离散的，然后对应于变量的不同取值的数据将用于沿该轴的不同小平面的绘制。例如，假设我们想要在`tips`数据集中检查午餐和晚餐小费分布的差异。

此外，`relplot()`，`catplot()`和`lmplot()`都在内部使用此对象，并且它们在完成时返回该对象，以便进一步调整。

```python
tips = sns.load_dataset("tips")
g = sns.FacetGrid(tips, col="time")
```



![http://seaborn.pydata.org/_images/axis_grids_7_0.png](assets/98101f3cd02be71d90f9d777eca5876f.jpg)


如上初始化网格会设置 matplotlib 图形和轴，但不会在其上绘制任何内容。

在网格上可视化数据的主要方法是 FacetGrid.map()。为此方法提供绘图函数以及要绘制的数据框变量名作为参数。我们使用直方图绘制每个子集中小费金额的分布。

```python
g = sns.FacetGrid(tips, col="time")
g.map(plt.hist, "tip");
```

![http://seaborn.pydata.org/_images/axis_grids_9_0.png](assets/5e335e837566d00501c0389d96625993.jpg)

`map`函数绘制图形并注释轴，生成图。要绘制关系图，只需传递多个变量名称。还可以提供关键字参数，这些参数将传递给绘图函数：

```python
g = sns.FacetGrid(tips, col="sex", hue="smoker")
g.map(plt.scatter, "total_bill", "tip", alpha=.7)
g.add_legend();
```

![http://seaborn.pydata.org/_images/axis_grids_11_0.png](assets/96cf02373c746bde1b58beb44b2f25c8.jpg)


有几个传递给类构造函数的选项可用于控制网格外观。

```python
g = sns.FacetGrid(tips, row="smoker", col="time", margin_titles=True)
g.map(sns.regplot, "size", "total_bill", color=".3", fit_reg=False, x_jitter=.1)
```

![http://seaborn.pydata.org/_images/axis_grids_13_0.png](assets/8da0c248ec165400ddf3c27f13d9a824.jpg)



注意，matplotlib API 并未正式支持`margin_titles`，此选项在一些情况下可能无法正常工作。特别是，它目前不能与图之外的图例同时使用。

通过提供每个面的高度以及纵横比来设置图形的大小：

```python
g = sns.FacetGrid(tips, col="day", height=4, aspect=.5)
g.map(sns.barplot, "sex", "total_bill");
```

```python
/Users/mwaskom/code/seaborn/seaborn/axisgrid.py:715: UserWarning: Using the barplot function without specifying order is likely to produce an incorrect plot.  warnings.warn(warning)
```

![http://seaborn.pydata.org/_images/axis_grids_15_1.png](assets/05a2ee4e2b81712775628943c2135066.jpg)


小图的默认排序由 DataFrame 中的信息确定的。如果用于定义小图的变量是类别变量，则使用类别的顺序。否则，小图将按照各类的出现顺序排列。但是，可以使用适当的`* _order`参数指定任意构面维度的顺序：

```python
ordered_days = tips.day.value_counts().indexg = sns.FacetGrid(tips, row="day", row_order=ordered_days,
                  height=1.7, aspect=4,)
g.map(sns.distplot, "total_bill", hist=False, rug=True);
```

![http://seaborn.pydata.org/_images/axis_grids_17_0.png](assets/0140b6849cae4a202c9a56ad5edfffd6.jpg)


可以用 seaborn 调色板（即可以传递给[`color_palette()`](../generated/seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette")的东西。）还可以用字典将`hue`变量中的值映射到 matplotlib 颜色：

```python
pal = dict(Lunch="seagreen", Dinner="gray")
g = sns.FacetGrid(tips, hue="time", palette=pal, height=5)
g.map(plt.scatter, "total_bill", "tip", s=50, alpha=.7, linewidth=.5, edgecolor="white")
g.add_legend();
```

![http://seaborn.pydata.org/_images/axis_grids_19_0.png](assets/686b61f7ba884a08ccd40cd9aae45791.jpg)


还可以让图的其他方面（如点的形状）在色调变量的各个级别之间变化，这在以黑白方式打印时使图易于理解。为此，只需将一个字典传递给 hue_kws，其中键是绘图函数关键字参数的名称，值是关键字值列表，每个级别为一个色调变量。

```python
g = sns.FacetGrid(tips, hue="sex", palette="Set1", height=5, hue_kws={"marker": ["^", "v"]})
g.map(plt.scatter, "total_bill", "tip", s=100, linewidth=.5, edgecolor="white")
g.add_legend();
```

![http://seaborn.pydata.org/_images/axis_grids_21_0.png](assets/a99d865fc52a457633d59e779fb194f1.jpg)


如果一个变量的水平数过多，除了可以沿着列绘制之外，也可以“包装”它们以便它们跨越多行。执行此 wrap 操作时，不能使用`row`变量。

```python
attend = sns.load_dataset("attention").query("subject <= 12")
g = sns.FacetGrid(attend, col="subject", col_wrap=4, height=2, ylim=(0, 10))
g.map(sns.pointplot, "solutions", "score", color=".3", ci=None);
```

```python
/Users/mwaskom/code/seaborn/seaborn/axisgrid.py:715: UserWarning: Using the pointplot function without specifying order is likely to produce an incorrect plot.  warnings.warn(warning)
```

![http://seaborn.pydata.org/_images/axis_grids_23_1.png](assets/5291ce95bb602839ea9bb9b4e3e7d128.jpg)


使用[`FacetGrid.map()`](../generated/seaborn.FacetGrid.map.html#seaborn.FacetGrid.map "seaborn.FacetGrid.map") （可以多次调用）绘图后，你可以调整绘图的外观。 `FacetGrid`对象有许多方法可以在更高的抽象层次上操作图形。最一般的是`FacetGrid.set()`，还有其他更专业的方法，如`FacetGrid.set_axis_labels()`，它们都遵循内部构面没有轴标签的约定。例如：

```python
with sns.axes_style("white"):    
	g = sns.FacetGrid(tips, row="sex", col="smoker", margin_titles=True, height=2.5)
g.map(plt.scatter, "total_bill", "tip", color="#334488", edgecolor="white", lw=.5);
g.set_axis_labels("Total bill (US Dollars)", "Tip");
g.set(xticks=[10, 30, 50], yticks=[2, 6, 10]);
g.fig.subplots_adjust(wspace=.02, hspace=.02);
```

![http://seaborn.pydata.org/_images/axis_grids_25_0.png](assets/9127880f623ed92358326c68893d22b4.jpg)


对于需要更多自定义的情形，你可以直接使用底层 matplotlib 图形`Figure`和轴`Axes`对象，它们分别作为成员属性存储在`Figure`和轴`Axes`（一个二维数组）中。在制作没有行或列刻面的图形时，你还可以使用`ax`属性直接访问单个轴。

```python
g = sns.FacetGrid(tips, col="smoker", margin_titles=True, height=4)
g.map(plt.scatter, "total_bill", "tip", color="#338844", edgecolor="white", s=50, lw=1)
for ax in g.axes.flat:    
    ax.plot((0, 50), (0, .2 * 50), c=".2", ls="--")
g.set(xlim=(0, 60), ylim=(0, 14));
```

![http://seaborn.pydata.org/_images/axis_grids_27_0.png](assets/a7e016f2ed7f61c499c7f56b2f3d8350.jpg)


## 使用自定义函数

使用[`FacetGrid`](../generated/seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")时，你除了可以使用现有的 matplotlib 和 seaborn 函数，还可以使用自定义函数。但是，这些函数必须遵循一些规则：

1. 它必须绘制到“当前活动的”matplotlib 轴`Axes`上。 `matplotlib.pyplot`命名空间中的函数就是如此。如果要直接使用当前轴的方法，可以调用`plt.gca`来获取对当前`Axes`的引用。
2. 它必须接受它在位置参数中绘制的数据。在内部，[`FacetGrid`](../generated/seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")将为传递给[`FacetGrid.map()`](../generated/seaborn.FacetGrid.map.html#seaborn.FacetGrid.map "seaborn.FacetGrid.map")的每个命名位置参数传递一`Series`数据。
3. 它必须能接受`color`和`label`关键字参数，理想情况下，它会用它们做一些有用的事情。在大多数情况下，最简单的方法是捕获`** kwargs`的通用字典并将其传递给底层绘图函数。

让我们看一下自定义绘图函数的最小示例。这个函数在每个构面采用一个数据向量：

```python
from scipy import statsdef quantile_plot(x, **kwargs):    
    qntls, xr = stats.probplot(x, fit=False)    
    plt.scatter(xr, qntls, **kwargs)

g = sns.FacetGrid(tips, col="sex", height=4)
g.map(quantile_plot, "total_bill");
```

![http://seaborn.pydata.org/_images/axis_grids_29_0.png](assets/f7075e926868bca0186de8da2f906ed9.jpg)


如果你想要制作一个双变量图，编写函数则应该有分别接受 x 轴变量，y 轴变量的参数：

```python
def qqplot(x, y, **kwargs):   
	_, xr = stats.probplot(x, fit=False)   
    _, yr = stats.probplot(y, fit=False)    
    plt.scatter(xr, yr, **kwargs)
    
g = sns.FacetGrid(tips, col="smoker", height=4)
g.map(qqplot, "total_bill", "tip")
```

![http://seaborn.pydata.org/_images/axis_grids_31_0.png](assets/b9d0e6d4b79fc2df3e1a1edcdd37e5ec.jpg)


因为`plt.scatter`接受颜色和标签关键字参数并做相应的处理，所以我们可以毫无困难地添加一个色调构面：

```python
g = sns.FacetGrid(tips, hue="time", col="sex", height=4)
g.map(qqplot, "total_bill", "tip")
g.add_legend()
```

![http://seaborn.pydata.org/_images/axis_grids_33_0.png](assets/9d51f61931446dc9eccd2013f7666eb9.jpg)


这种方法还允许我们使用额外的美学元素来区分色调变量的级别，以及不依赖于分面变量的关键字参数：

```python
g = sns.FacetGrid(tips, hue="time", col="sex", height=4, hue_kws={"marker": ["s", "D"]})
g.map(qqplot, "total_bill", "tip", s=40, edgecolor="w")
g.add_legend()
```

![http://seaborn.pydata.org/_images/axis_grids_35_0.png](assets/894a1105419e7147b562c1576471c410.jpg)


有时候，你需要使用`color`和`label`关键字参数映射不能按预期方式工作的函数。在这种情况下，你需要显式捕获它们并在自定义函数的逻辑中处理它们。例如，这种方法可用于映射`plt.hexbin`，使它与[`FacetGrid`](../generated/seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid") API 匹配：

```python
def hexbin(x, y, color, **kwargs):    
    cmap = sns.light_palette(color, as_cmap=True)    
    plt.hexbin(x, y, gridsize=15, cmap=cmap, **kwargs)
    
with sns.axes_style("dark"):    
    g = sns.FacetGrid(tips, hue="time", col="time", height=4)
    
g.map(hexbin, "total_bill", "tip", extent=[0, 50, 0, 10])
```

![http://seaborn.pydata.org/_images/axis_grids_37_0.png](assets/6e0f3ad155ad03e98d1e10abc9d60e82.jpg)


## 绘制成对数据关系

[`PairGrid`](../generated/seaborn.PairGrid.html#seaborn.PairGrid "seaborn.PairGrid")允许你使用相同的绘图类型快速绘制小子图的网格。在[`PairGrid`](../generated/seaborn.PairGrid.html#seaborn.PairGrid "seaborn.PairGrid")中，每个行和列都分配给一个不同的变量，结果图显示数据集中的每个对变量的关系。这种图有时被称为“散点图矩阵”，这是显示成对关系的最常见方式，但是[`PairGrid`](../generated/seaborn.PairGrid.html#seaborn.PairGrid "seaborn.PairGrid")不仅限于散点图。

了解[`FacetGrid`](../generated/seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")和[`PairGrid`](../generated/seaborn.PairGrid.html#seaborn.PairGrid "seaborn.PairGrid")之间的差异非常重要。前者每个构面显示以不同级别的变量为条件的相同关系。后者显示不同的关系（尽管上三角和下三角组成镜像图）。使用[`PairGrid`](../generated/seaborn.PairGrid.html#seaborn.PairGrid "seaborn.PairGrid")可为你提供数据集中有趣关系的快速，高级的摘要。

该类的基本用法与[`FacetGrid`](../generated/seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")非常相似。首先初始化网格，然后将绘图函数传递给`map`方法，并在每个子图上调用它。还有一个伴侣函数， [`pairplot()`](../generated/seaborn.pairplot.html#seaborn.pairplot "seaborn.pairplot") ，可以更快的绘图。

```python
iris = sns.load_dataset("iris")
g = sns.PairGrid(iris)
g.map(plt.scatter)
```

![http://seaborn.pydata.org/_images/axis_grids_39_0.png](assets/4a1b1a2445b45757efa39039fe666d0f.jpg)


可以在对角线上绘制不同的函数，以显示每列中变量的单变量分布。但请注意，轴刻度与该绘图的计数或密度轴不对应。

```python
g = sns.PairGrid(iris)
g.map_diag(plt.hist)
g.map_offdiag(plt.scatter)
```

![http://seaborn.pydata.org/_images/axis_grids_41_0.png](assets/e6b76ab1dc666e53ba77d9784aa8d168.jpg)


此图的一种常见用法是通过单独的分类变量对观察结果进行着色。例如，iris 数据集三种不同种类的鸢尾花都有四种测量值，因此你可以看到不同花在这些取值上的差异。

```python
g = sns.PairGrid(iris, hue="species")
g.map_diag(plt.hist)
g.map_offdiag(plt.scatter)
g.add_legend()
```

![http://seaborn.pydata.org/_images/axis_grids_43_0.png](assets/31d92e08bdcc758f28b32f9a6f31aff7.jpg)


默认情况下，使用数据集中的每个数值列，但如果需要，你可以专注于特定列。

```python
g = sns.PairGrid(iris, vars=["sepal_length", "sepal_width"], hue="species")
g.map(plt.scatter);
```

![http://seaborn.pydata.org/_images/axis_grids_45_0.png](assets/8bd8dc270d4a00dd45e740cdf68fcf23.jpg)


也可以在上三角和下三角中使用不同的函数来强调关系的不同方面。

```python
g = sns.PairGrid(iris)
g.map_upper(plt.scatter)
g.map_lower(sns.kdeplot)
g.map_diag(sns.kdeplot, lw=3, legend=False);
```

![http://seaborn.pydata.org/_images/axis_grids_47_0.png](assets/05cf1fb72d609a18fa7a4fdfb8ae460f.jpg)


对角线上具有单位关系的方形网格实际上只是一种特殊情况，你也可以在行和列中使用不同的变量进行绘图。

```python
g = sns.PairGrid(tips, y_vars=["tip"], x_vars=["total_bill", "size"], height=4)
g.map(sns.regplot, color=".3")
g.set(ylim=(-1, 11), yticks=[0, 5, 10]);
```

![http://seaborn.pydata.org/_images/axis_grids_49_0.png](assets/1519f7f68ebbc0bf3fa3072a2edb0838.jpg)


当然，美学属性是可配置的。例如，你可以使用不同的调色板（例如，显示色调变量的顺序）并将关键字参数传递到绘图函数中。

```python
g = sns.PairGrid(tips, hue="size", palette="GnBu_d")
g.map(plt.scatter, s=50, edgecolor="white")
g.add_legend();
```

![http://seaborn.pydata.org/_images/axis_grids_51_0.png](assets/472e90723a05c450ec78ccf39888e6e4.jpg)


[`PairGrid`](../generated/seaborn.PairGrid.html#seaborn.PairGrid "seaborn.PairGrid")很灵活，但要快速查看数据集，使用[`pairplot()`](../generated/seaborn.pairplot.html#seaborn.pairplot "seaborn.pairplot")更容易。此函数默认使用散点图和直方图，但会添加一些其他类型（目前，你还可以绘制非对角线上的回归图和对角线上的 KDE）。

```python
sns.pairplot(iris, hue="species", height=2.5);
```

![http://seaborn.pydata.org/_images/axis_grids_53_0.png](assets/27d73e1a33e6845f05c2a4670413c156.jpg)


还可以使用关键字参数控制绘图的美观，函数会返回[`PairGrid`](../generated/seaborn.PairGrid.html#seaborn.PairGrid "seaborn.PairGrid")实例以便进一步调整。

```python
g = sns.pairplot(iris, hue="species", palette="Set2", diag_kind="kde", height=2.5)
```

![http://seaborn.pydata.org/_images/axis_grids_55_0.png](assets/62af2a2066c78c192cde66155a2331d9.jpg)


# 控制图像的美学样式(aesthetics)



绘制有吸引力的图像很十分重要的。当你在探索一个数据集并为你自己做图的时候，制作一些让人看了心情愉悦的图像是很好的。可视化对向观众传达量化的简介也是很重要的，在这种情况下制作能够抓住查看者的注意力并牢牢吸引住他们的图像就更有必要了。

Matplotlib 是高度可定制的，但是很难知道要如何设置图像才能使得图像更加吸引人。Seaborn 提供了许多定制好的主题和高级的接口，用于控制 Matplotlib 所做图像的外观。

```python
import numpy as npimport seaborn as snsimport matplotlib.pyplot as plt
```

让我们定义一个简单的函数来绘制一些偏移正弦波，这将帮助我们看到我们可以调整的能够影响图像风格的不同参数。

```python
def sinplot(flip=1):    x = np.linspace(0, 14, 100)    for i in range(1, 7):        plt.plot(x, np.sin(x + i * .5) * (7 - i) * flip)
```

这是 Matplotlib 默认情况下的绘图外观：

```python
sinplot()
```

![http://seaborn.pydata.org/_images/aesthetics_7_0.png](assets/4784d932a8738cea5085be56ce6f7315.jpg)


为了将图像的风格转变为 seaborn 的默认样式，我们可以 [`set()`](../generated/seaborn.set.html#seaborn.set "seaborn.set") 函数。

```python
sns.set()
sinplot()
```

![http://seaborn.pydata.org/_images/aesthetics_9_0.png](assets/2422a0c0d2c96ec6397babaa6c842d79.jpg)


（注意，在 0.8 之前的 seaborn 版本中， [`set()`](../generated/seaborn.set.html#seaborn.set "seaborn.set") 已经在使用 impory 语句导入的时候就被调用了。但在以后的版本中，必须要显式调用它）。

Seaborn 将 matplotlib 参数分成两个独立的组。第一组设置了图像的美术风格，第二组则对图像中不同的元素进行了控制，使得图像可以很容易地融入不同的环境中。

操作这些参数的接口是两对函数。要控制样式，请使用 [`axes_style()`](../generated/seaborn.axes_style.html#seaborn.axes_style "seaborn.axes_style") 和 [`set_style()`](../generated/seaborn.set_style.html#seaborn.set_style "seaborn.set_style") 函数。要对图像中元素的样式进行修改，请使用 [`plotting_context()`](../generated/seaborn.plotting_context.html#seaborn.plotting_context "seaborn.plotting_context") 和 [`set_context()`](../generated/seaborn.set_context.html#seaborn.set_context "seaborn.set_context") 函数。在这两种情况下（控制图像样式与修改元素样式），第一个函数会返回一个参数字典，第二个函数设置 matplotlib 中相关参数的默认值。

## Seaborn 图像参数

有五个预设的 Seaborn 主题： `darkgrid`，`whitegrid`，`dark`，`white`以及 `ticks`。它们分别适用于不同的应用程序和个人偏好。默认主题为 `darkgrid`。如上所述，坐标方格有助于将制出的图像用作定量信息的查阅表，灰色背景上的白色有助于防止网格与表示数据的行发生竞争。 `whitegrid` 主题类似，但它更适用于包含大量数据元素的绘图：

```python
sns.set_style("whitegrid")
data = np.random.normal(size=(20, 6)) + np.arange(6) / 2sns.boxplot(data=data);
```

![http://seaborn.pydata.org/_images/aesthetics_11_0.png](assets/ba3b8a1ba98a18cbe9f37192b2338932.jpg)


对许多的图像而言，（尤其是在你只是想通过图像来提供给人们一个对数据模式的印象时，比如说作报告时）坐标网格都是不必要的。

```python
sns.set_style("dark")
sinplot()
```

![http://seaborn.pydata.org/_images/aesthetics_13_0.png](assets/4810d616de6fa5d37502c1a2cd931669.jpg)


```python
sns.set_style("white")
sinplot()
```

![http://seaborn.pydata.org/_images/aesthetics_14_0.png](assets/47decb21ce3a9413876a02f2967ef7aa.jpg)


有时，您可能希望为绘图提供一点额外的结构，这正是 tick 样式的用武之地：

```python
sns.set_style("ticks")
sinplot()
```

![http://seaborn.pydata.org/_images/aesthetics_16_0.png](assets/d23eefa56e1ef945c75b43ab4fc651a4.jpg)


## 移除坐标轴

 `white` 样式与 `ticks` 样式的好处是都能删除所不需要的顶部与右部坐标轴。使用 seaborn 中的函数 [`despine()`](../generated/seaborn.despine.html#seaborn.despine "seaborn.despine") 可以来移除它们：

```python
sinplot()
sns.despine()
```

![http://seaborn.pydata.org/_images/aesthetics_18_0.png](assets/b9a7a625477fd6d6c167844f557d14dd.jpg)


有些图的好处在于，可以让坐标的主轴随着数据进行偏移，这可以使用 [`despine()`](../generated/seaborn.despine.html#seaborn.despine "seaborn.despine")函数来完成。当刻度无法覆盖轴的整个范围时，`trim`参数将限制不受影响的坐标轴的范围。

```python
f, ax = plt.subplots()
sns.violinplot(data=data)
sns.despine(offset=10, trim=True);
```

![http://seaborn.pydata.org/_images/aesthetics_20_0.png](assets/7c21c8cb297826c3a45b483039151d9e.jpg)


你也可以通过控制 [`despine()`](../generated/seaborn.despine.html#seaborn.despine "seaborn.despine")的额外参数来删除坐标轴：

```python
sns.set_style("whitegrid")
sns.boxplot(data=data, palette="deep")
sns.despine(left=True)
```

![http://seaborn.pydata.org/_images/aesthetics_22_0.png](assets/4f24815584c7c584945d529f3d9dbfbf.jpg)


## 设置临时图像格式

虽然来回切换很容易，但你也可以在`with`语句中使用 [`axes_style()`](../generated/seaborn.axes_style.html#seaborn.axes_style "seaborn.axes_style") 函数来临时设置绘图参数。 这也允许您使用不同风格的坐标轴制作图形：

```python
f = plt.figure()
with sns.axes_style("darkgrid"):    
    ax = f.add_subplot(1, 2, 1)    
    sinplot()
ax = f.add_subplot(1, 2, 2)
sinplot(-1)
```

![http://seaborn.pydata.org/_images/aesthetics_24_0.png](assets/1b8ec9b9fb72e9193bfefe933d976d37.jpg)


## 覆盖控制 seaborn 样式的元素

如果你想要自己定制 seaborn 的样式，你可以通过给 [`axes_style()`](../generated/seaborn.axes_style.html#seaborn.axes_style "seaborn.axes_style") 与[`set_style()`](../generated/seaborn.set_style.html#seaborn.set_style "seaborn.set_style")函数中的 `rc` 参数传递一个参数字典来实现。请注意，您只能通过此方法覆盖作为样式定义一部分的参数。（但是，更高级别的 [`set()`](../generated/seaborn.set.html#seaborn.set "seaborn.set") 函数会获取任何 matplotlib 参数的字典）。

如果你想看看包含哪些参数，你可以只调用没有参数的函数，这将返回当前设置：

```python
sns.axes_style()
```

```python
{'axes.axisbelow': True, 'axes.edgecolor': '.8', 'axes.facecolor': 'white', 'axes.grid': True, 'axes.labelcolor': '.15', 'axes.spines.bottom': True, 'axes.spines.left': True, 'axes.spines.right': True, 'axes.spines.top': True, 'figure.facecolor': 'white', 'font.family': ['sans-serif'], 'font.sans-serif': ['Arial',  'DejaVu Sans',  'Liberation Sans',  'Bitstream Vera Sans',  'sans-serif'], 'grid.color': '.8', 'grid.linestyle': '-', 'image.cmap': 'rocket', 'lines.solid_capstyle': 'round', 'patch.edgecolor': 'w', 'patch.force_edgecolor': True, 'text.color': '.15', 'xtick.bottom': False, 'xtick.color': '.15', 'xtick.direction': 'out', 'xtick.top': False, 'ytick.color': '.15', 'ytick.direction': 'out', 'ytick.left': False, 'ytick.right': False}
```

然后，您可以设置这些参数的不同版本：

```python
sns.set_style("darkgrid", {"axes.facecolor": ".9"})
sinplot()
```

![http://seaborn.pydata.org/_images/aesthetics_28_0.png](assets/5ebb828c6eed309cb239f2219f35397c.jpg)


## 缩放图像元素

一组独立的参数控制绘图元素的比例，这允许您使用相同的代码来制作在适合使用不同大小图片场景下的图片。

首先，让我们通过调用 [`set()`](../generated/seaborn.set.html#seaborn.set "seaborn.set")来重置默认的参数：

```python
sns.set()
```

按照相对大小的顺序排序，四个预设环境是 `paper`， `notebook`， `talk`与 `poster`。 `notebook`样式是默认样式，上文中的图就是使用该样式绘制的。

```python
sns.set_context("paper")
sinplot()
```

![http://seaborn.pydata.org/_images/aesthetics_32_0.png](assets/1e7ac5787ede28760da23610a7f3926f.jpg)


```python
sns.set_context("talk")
sinplot()
```

![http://seaborn.pydata.org/_images/aesthetics_33_0.png](assets/3152abe0e185f2c164330dac175b8746.jpg)


```python
sns.set_context("poster")
sinplot()
```

![http://seaborn.pydata.org/_images/aesthetics_34_0.png](assets/5788d7f7d4b9f29480e997849d16ee10.jpg)


您现在知道的关于样式函数的大部分内容应该转移到环境函数中。

你可以通过在调用 [`set_context()`](../generated/seaborn.set_context.html#seaborn.set_context "seaborn.set_context") 时指定环境的名字来设置参数，你也可以通过提供一个参数字典来覆盖原有的参数值。

你也在转换环境的时候独立地对字符元素的大小进行缩放。（这个操作也能够顶层的 [`set()`](../generated/seaborn.set.html#seaborn.set "seaborn.set") 函数来实现）。

```python
sns.set_context("notebook", font_scale=1.5, rc={"lines.linewidth": 2.5})
sinplot()
```

![http://seaborn.pydata.org/_images/aesthetics_36_0.png](assets/2b8879fda28a3be3acac977e15cbeebb.jpg)


同样的，你也可以暂时的通过嵌套在 `with` 语句下的语句来实现图像的缩放。

样式和环境都可以使用 [`set()`](../generated/seaborn.set.html#seaborn.set "seaborn.set") 函数快速配置。 此函数还设置默认调色板，但更详细的介绍将在本教程的 [下一节](color_palettes.html#palette-tutorial) 进行叙述。


# 选择调色板


颜色在图像风格中比起其他元素显得更为重要。当合理有效地使用颜色时，数据模式会被凸显出来；反之，则会被掩盖。这里有很多数据可视化中关于颜色使用的优秀资源，我推荐阅读这些 Rob Simmon 的[博客文章](https://earthobservatory.nasa.gov/blogs/elegantfigures/2013/08/05/subtleties-of-color-part-1-of-6/)以及这篇更加学术性的[论文](https://cfwebprod.sandia.gov/cfdocs/CompResearch/docs/ColorMapsExpanded.pdf)。 此外，matplotlib 文档也提供了一篇很好的[教程](https://matplotlib.org/users/colormaps.html)来说明一些内置 Colormap 的感知属性。

seaborn 让您在选择与您处理的数据类型和可视化过程中搭配的配色方案变得简单。

```python
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
sns.set()
```

## 创建调色板

使用离散调色板过程中最重要函数是[`color_palette()`](../generated/seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette")。这个函数为许多(但不是全部)可以在 seaborn 中生成颜色的方式提供了一个接口，并且在任何具有`palette`参数的函数的内部都可以使用(以及某些需要多种颜色时具有`color`参数的情况)。

[`color_palette()`](../generated/seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette") 会接受所有的 seaborn 调色板或者 matplotlib Colormap (除了 `jet`, 您永远都不应该使用它). 它还可以获取以任何有效 matplotlib 格式(RGB 元组、十六进制颜色代码或 HTML 颜色名字)指定的颜色列表。返回值始终是 RGB 元组的列表。

最后，在没有参数的情况下调用[`color_palette()`](../generated/seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette")函数将会返回当前默认的颜色循环。

函数[`set_palette()`](../generated/seaborn.set_palette.html#seaborn.set_palette "seaborn.set_palette")接受相同的参数，并将为所有图像设置默认的颜色循环。您也可以在`with`语句中调用[`color_palette()`](../generated/seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette")来临时改变调色板。([参见](#palette-contexts))

在不了解数据特征的情况下，通常也不可能知道哪种调色板或 Colormap 最适合一组数据。接下来，我们将通过三种常见的调色板 _ 定性调色板 _, _ 顺序调色板 _, 和 _ 发散调色板 _ 来拆分介绍[`color_palette()`](../generated/seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette")函数的使用方法以及其他 seaborn 函数。

## 定性调色板

当您想要区分不具有内在顺序的离散数据块时，定性(分类)调色板是最佳方案。

导入 seaborn 的同时，会引入默认的颜色循环，由 6 种颜色构成。并将调用标准 matplotlib 颜色循环，看起来也更加赏心悦目。

```python
current_palette = sns.color_palette()
sns.palplot(current_palette)
```

![http://seaborn.pydata.org/_images/color_palettes_6_0.png](assets/975109fd603a63c12f7e0db1461d265e.jpg)


默认主题有六种变体，分别为`deep`, `muted`, `pastel`, `bright`, `dark`, and `colorblind`。

![http://seaborn.pydata.org/_images/color_palettes_8_0.png](assets/9dee4b50342228bbf515be6a3e60b0e8.jpg)


### 使用循环颜色系统

当您要区分任意数量的类别而不强调任何类别时，最简单的方法是在循环颜色空间中绘制间距相等的颜色(在此颜色空间中，色调会发生变化，同时保持亮度和饱和度不变)。这是大多数 seaborn 函数在处理当需要区分的数据集超过颜色循环中的 6 种颜色时时所使用的默认方法。

最为常用的方法是使用`hls`颜色空间——一种简单的 RGB 值变体。

```python
sns.palplot(sns.color_palette("hls", 8))
```

![http://seaborn.pydata.org/_images/color_palettes_10_0.png](assets/f35d431e80f906b39aa82e1bd8390361.jpg)

[`hls_palette()`](../generated/seaborn.hls_palette.html#seaborn.hls_palette "seaborn.hls_palette")函数允许您控制颜色的亮度(lightness)和饱和度(saturation)。

```python
sns.palplot(sns.hls_palette(8, l=.3, s=.8))
```

![http://seaborn.pydata.org/_images/color_palettes_12_0.png](assets/7787cf3ad144a10c16ce1f2697d4af34.jpg)


然而，由于人类视觉系统的工作方式，RGB 强度很高的颜色也不一定看起来同样强烈。[我们认为](https://en.wikipedia.org/wiki/Color_vision)黄色和绿色是相对较亮的，蓝色是相对较暗的，当目标是与`hls`系统保持一致性时可能会带来一些问题。

为了解决这一问题，seaborn 提供了一个[husl](http://www.hsluv.org/)系统(后来更名为 HSLuv)的接口，这也使选择间隔均匀的色调变得容易，同时使亮度和饱和度都更加均匀。

```python
sns.palplot(sns.color_palette("husl", 8))
```

![http://seaborn.pydata.org/_images/color_palettes_14_0.png](assets/151c643774e7ae51ed523abe3fa5f559.jpg)


类似地，[`husl_palette()`](../generated/seaborn.husl_palette.html#seaborn.husl_palette "seaborn.husl_palette")函数也为这个系统提供了一个更灵活的接口。

### 使用 Color Brewer 调色板

[Color Brewer](http://colorbrewer2.org/)为定性调色板提供了另一种美观的配色方案(同样包含顺序调色板包括和发散调色板，详情见下文)。这些也作为 matplotlib Colormap 存在，但并没有得到很好的处理。在 seaborn 中，当您需要定性(qualitative)的 Color Brewer 方案时，你总是会得到离散的颜色，但这意味着在某些情况下颜色会循环重复。

Color Brewer 的一个很好的特点是它对色盲比较友好。[色盲](https：/en.wikipea.org/wiki/Color_Blinity)有很多种，最为常见的是红绿色盲。通常，对于需要根据颜色进行元素区分时，应该尽量避免使用这两种颜色。

```python
sns.palplot(sns.color_palette("Paired"))
```

![http://seaborn.pydata.org/_images/color_palettes_16_0.png](assets/4a2b106cd5abd71c514edda6c1f06b6e.jpg)


```python
sns.palplot(sns.color_palette("Set2"))
```

![http://seaborn.pydata.org/_images/color_palettes_17_0.png](assets/8d928f7a998a90772bc3defd036444a8.jpg)


为了帮助您从 Color Brewer 库中选取配色方案，seaborn 提供了[`choose_colorbrewer_palette()`](../generated/seaborn.choose_colorbrewer_palette.html#seaborn.choose_colorbrewer_palette "seaborn.choose_colorbrewer_palette")函数。这个函数能够启动交互式组件来帮助您浏览各种选项，修改不同的参数。但是只能在 Jupyter notebook 中使用。

当然，您可能只希望手动指定一组您喜欢的颜色。[`color_palette()`](../generated/seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette")函数会接受一个颜色列表，操作起来也很简单。

```python
flatui = ["#9b59b6", "#3498db", "#95a5a6", "#e74c3c", "#34495e", "#2ecc71"]
sns.palplot(sns.color_palette(flatui))
```

![http://seaborn.pydata.org/_images/color_palettes_19_0.png](assets/e1506db895aa2aaa0d7df99cc7d3d4c3.jpg)


### 使用来自 xkcd color survey 的颜色名字

不久前，[xkcd](https://xkcd.com/)开展了一项[众包工作](https://blog.xkcd.com/2010/05/03/color-survey-results/)来为随机 RGB 颜色命名。产生了[954 个颜色名字](https://xkcd.com/color/rgb/)，您现在可以在 seaborn 中使用`xkcd_rgb`字典来引用它们：

```python
plt.plot([0, 1], [0, 1], sns.xkcd_rgb["pale red"], lw=3)
plt.plot([0, 1], [0, 2], sns.xkcd_rgb["medium green"], lw=3)
plt.plot([0, 1], [0, 3], sns.xkcd_rgb["denim blue"], lw=3);
```

![http://seaborn.pydata.org/_images/color_palettes_21_0.png](assets/85ba77533a6b6eb1cc44b5bf4aed4e1d.jpg)


除了从`xkcd_rgb`字典中提取单一颜色外，您也可以向[`xkcd_palette()`](../generated/seaborn.xkcd_palette.html#seaborn.xkcd_palette "seaborn.xkcd_palette")函数传递一个颜色名字列表。

```python
colors = ["windows blue", "amber", "greyish", "faded green", "dusty purple"]
sns.palplot(sns.xkcd_palette(colors))
```

![http://seaborn.pydata.org/_images/color_palettes_23_0.png](assets/6508f97c0a5563f99b755ccfac8c2b12.jpg)


## 顺序调色板

第二类主要的调色板被称为“顺序调色板”(sequential)，当数据集的范围从相对低值(不感兴趣)到相对高值(很感兴趣)时，最好使用顺序调色板，尽管在某些情况下您可能需要顺序调色板中的离散颜色。在[`kdeplot()`](../generated/seaborn.kdeplot.html#seaborn.kdeplot "seaborn.kdeplot")和[`heatmap()`](../generated/seaborn.heatmap.html#seaborn.heatmap "seaborn.heatmap")函数中使用它们来作为 Colormap 则更为常见(以及类似的 matplotlib 函数)。

在这种情况下使用`jet`（或其他彩虹调色板）等 Colormap 是很常见的，因为色调范围给人的印象是提供有关数据的额外信息。然而，具有较大色调变化的 Colormap 往往会引入数据中不存在的不连续性，并且我们的视觉系统无法自然地将彩虹光谱映射到诸如“高”或“低”的定量区分。导致来这些可视化的结果更加令人困惑，因为它们掩盖了数据中的模式，而不是揭示它们。`jet` 调色板使用了最亮的颜色(黄色和青色)的中间数据值，导致效果是强调无趣的(和任意的)值，而不是强调极端的值。

对于连续性的数据，最好使用色调变化幅度较小，而亮度和饱和度变化幅度较大的配色方案。这种方法会很自然地吸引人们注意数据中相对重要的部分。

Color Brewer 库有大量这样的配色方案，它们以调色板中主要的一种或多种颜色命名。

```python
sns.palplot(sns.color_palette("Blues"))
```

![http://seaborn.pydata.org/_images/color_palettes_25_0.png](assets/b6ac2dda5753920442b42aa6c3f8746c.jpg)


与 matplotlib 类似，您可以通过添加加后缀`_r`来倒置顺序调色板的顺序。

```python
sns.palplot(sns.color_palette("BuGn_r"))
```

![http://seaborn.pydata.org/_images/color_palettes_27_0.png](assets/35ba6c0dab54401105c2b869de17084b.jpg)


seaborn 同样添加了一个小窍门来帮助您创建“深色”调色板，它没有一个很宽的动态范围。在当您需要按顺序映射直线或点时这可能会很有用，因为颜色较亮的线条会比较难以区分。

```python
sns.palplot(sns.color_palette("GnBu_d"))
```

![http://seaborn.pydata.org/_images/color_palettes_29_0.png](assets/12d91b101a0391d48ce055f66e4a910a.jpg)


您可能想要使用[`choose_colorbrewer_palette()`](../generated/seaborn.choose_colorbrewer_palette.html#seaborn.choose_colorbrewer_palette "seaborn.choose_colorbrewer_palette")函数来尝试多种选项，当您希望传递给 seaborn 或者 matplotlib 的返回值为 Colormap 对象时，您可以将`as_cmap`对象设置为`True`。

### 顺序 “cubehelix” 调色板

[cubehelix](https://www.mrao.cam.ac.uk/~dag/CUBEHELIX/) 调色板系统使顺序调色板的亮度产生线性变化，色调也会产生一些变化。这意味着您的 Colormap 在转换为黑白模式时(用于打印)的信息将得到保留，且对色盲友好。

Matplotlib 内置了默认的 cubehelix 版本：

```python
sns.palplot(sns.color_palette("cubehelix", 8))
```

![http://seaborn.pydata.org/_images/color_palettes_32_0.png](assets/d07b38f860b0f58e365e7ea26da8be92.jpg)


Seborn 为 cubehelix 系统提供了一个接口，以便您可以制作各种调色板，这些调色板都具有良好的线性亮度渐变。

由 seborn [`cubehelix_palette()`](../generated/seaborn.cubehelix_palette.html#seaborn.cubehelix_palette "seaborn.cubehelix_palette") 函数返回的默认调色板与 matplotlib 的默认值稍有不同，因为它不会围绕色轮旋转很远，也不会覆盖很宽的强度范围。它还反转顺序，以便让更重要的值的颜色更暗：

```python
sns.palplot(sns.cubehelix_palette(8))
```

![http://seaborn.pydata.org/_images/color_palettes_34_0.png](assets/be20ca200667c3c8849b95c4a6aa846e.jpg)

[`cubehelix_palette()`](../generated/seaborn.cubehelix_palette.html#seaborn.cubehelix_palette "seaborn.cubehelix_palette") 函数的其他参数控制调色板的外观。您将更改的两个主要参数为 `start` (介于 0 到 3 之间的值)和 `rot` —— 旋转次数(任意值，但可能在-1 和 1 之间)。

```python
sns.palplot(sns.cubehelix_palette(8, start=.5, rot=-.75))
```

![http://seaborn.pydata.org/_images/color_palettes_36_0.png](assets/e4e79afcca0586e526385c901b5cf806.jpg)


您还可以控制端点的亮度，甚至反转渐变：

```python
sns.palplot(sns.cubehelix_palette(8, start=2, rot=0, dark=0, light=.95, reverse=True))
```

![http://seaborn.pydata.org/_images/color_palettes_38_0.png](assets/c4e8f173e5519de01ed11bca06911371.jpg)


如同其他 seaborn 函数，您将默认得到一个颜色列表。但您也可以通过修改 `as_cmap=True` 将调色板作为 Colormap 对象的返回值来传递给 seaborn 或 matplotlib 函数。

```python
x, y = np.random.multivariate_normal([0, 0], [[1, -.5], [-.5, 1]], size=300).T
cmap = sns.cubehelix_palette(light=1, as_cmap=True)
sns.kdeplot(x, y, cmap=cmap, shade=True);
```

![http://seaborn.pydata.org/_images/color_palettes_40_0.png](assets/2e48cfc09dde0559e31096f5a067ad3d.jpg)


为了帮助您选择更好的调色板或者 Colormap，您可以在 Jupyter notebook 中使用 [`choose_cubehelix_palette()`](../generated/seaborn.choose_cubehelix_palette.html#seaborn.choose_cubehelix_palette "seaborn.choose_cubehelix_palette") 函数来启动互动界面帮助您测试、修改不同的参数。如果您希望函数返回一个 Colormap(而不是列表)，则在例如像 `hexbin` 这样的函数中设置 `as_Cmap=True`。

### 自定义调色板

为了更简单地生成自定义顺序调色板，您可以使用 [`light_palette()`](../generated/seaborn.light_palette.html#seaborn.light_palette "seaborn.light_palette") 或 [`dark_palette()`](../generated/seaborn.dark_palette.html#seaborn.dark_palette "seaborn.dark_palette") 函数。它们都是以某个颜色为种子，从明向暗或从暗向明渐变，产生顺序调色板。与这些函数相搭配的还有 [`choose_light_palette()`](../generated/seaborn.choose_light_palette.html#seaborn.choose_light_palette "seaborn.choose_light_palette") 和 [`choose_dark_palette()`](../generated/seaborn.choose_dark_palette.html#seaborn.choose_dark_palette "seaborn.choose_dark_palette") 来提供交互式组件便于创建调色板。

```python
sns.palplot(sns.light_palette("green"))
```

![http://seaborn.pydata.org/_images/color_palettes_43_0.png](assets/c5d7ac6e8ae2f2ccf0f40dd4dccda398.jpg)


```python
sns.palplot(sns.dark_palette("purple"))
```

![http://seaborn.pydata.org/_images/color_palettes_44_0.png](assets/de7d739c3094b9d34a7c69356ed9703f.jpg)


这些调色板同样可以被反转。

```python
sns.palplot(sns.light_palette("navy", reverse=True))
```

![http://seaborn.pydata.org/_images/color_palettes_46_0.png](assets/c32dd39f791f7d395dc47b37e1252b78.jpg)


这些调色板同样可以被用来创建 Colormap 对象而不是颜色列表。

```python
pal = sns.dark_palette("palegreen", as_cmap=True)
sns.kdeplot(x, y, cmap=pal);
```

![http://seaborn.pydata.org/_images/color_palettes_48_0.png](assets/16096f5a5796354b892ba4f6ff2dcef1.jpg)


默认情况下，输入可以是任何有效的 matplotlib 颜色。替代解释由 `input` 参数控制。现在，您可以在 `hls` 或 `husl` 空间中提供元组以及默认的 `rgb`，您也可以使用任何有效的 `xkcd` 颜色来生成调色板。

```python
sns.palplot(sns.light_palette((210, 90, 60), input="husl"))
```

![http://seaborn.pydata.org/_images/color_palettes_50_0.png](assets/697bc3366186ebc63715159c7e6f934a.jpg)


```python
sns.palplot(sns.dark_palette("muted purple", input="xkcd"))
```

![http://seaborn.pydata.org/_images/color_palettes_51_0.png](assets/eaa5d2a866409544e7f4f2f2b484e4f5.jpg)


注意，交互式调色板小部件的默认输入空间是 `husl`，它不同于函数本身的默认设置，但是在这种情况下更有用。

## 发散调色板

第三类调色板称为“发散调色板”(diverging)。当数据集的低值和高值都很重要，且数据集中有明确定义的中点时，这会是您的最佳选择。例如，绘制温度相对于基准时间点的变化图时，最好使用发散 Colormap 来同时显示温度相对于基准值的上升和下降

选择良好分散调色板的规则类似于良好的顺序调色板。不过在这种情况时需要注意两端颜色向中间颜色渐变时中间点的颜色不应该喧宾夺主，两端的颜色也应该具有相似的亮度和饱和度。

这里还需要强调的是，应该避免使用红色和绿色，因为需要考虑到[红绿色盲](https://en.wikipedia.org/wiki/Color_blindness)患者的观感。

不出所料，Color Brewer 库也同样提供了一些精心挑选的发散调色板。


```python
sns.palplot(sns.color_palette("BrBG", 7))
```

![http://seaborn.pydata.org/_images/color_palettes_54_0.png](assets/fb741f69016b77f7e6a93fc12ad8420c.jpg)


```python
sns.palplot(sns.color_palette("RdBu_r", 7))
```

![http://seaborn.pydata.org/_images/color_palettes_55_0.png](assets/a6f00f18e0a24fee984824847391d181.jpg)


matplotlib 库中内置的 `coolwarm` 调色板也是一个很好的选择。请注意，这个 Colormap 的中间值和极值之间的对比度较小。

```python
sns.palplot(sns.color_palette("coolwarm", 7))
```

![http://seaborn.pydata.org/_images/color_palettes_57_0.png](assets/650e30fa2dd7a01892cacc0fd1e2ca85.jpg)


### 自定义发散调色板

您可以使用 seaborn 的[`diverging_palette()`](../generated/seaborn.diverging_palette.html#seaborn.diverging_palette "seaborn.diverging_palette")函数来创建自定义 colormap 来描述发散数据(搭配有交互式组件[`choose_diverging_palette()`](../generated/seaborn.choose_diverging_palette.html#seaborn.choose_diverging_palette "seaborn.choose_diverging_palette"))。此函数使用 `husl` 颜色系统来创建发散调色板，您需要在函数中设置两个色调参数(用度表示)，也可以选择设置两端颜色的亮度和饱和度。 使用 `husl` 意味着两端到中间点的色调变化将是平衡的。

```python
sns.palplot(sns.diverging_palette(220, 20, n=7))
```

![http://seaborn.pydata.org/_images/color_palettes_59_0.png](assets/429c1d877c3d7c4b23d82d35ade3603d.jpg)


```python
sns.palplot(sns.diverging_palette(145, 280, s=85, l=25, n=7))
```

![http://seaborn.pydata.org/_images/color_palettes_60_0.png](assets/75b405b71a445bc29d020b75ee4b3632.jpg)


`sep` 参数控制两端到中间点色调变化的间隔。

```python
sns.palplot(sns.diverging_palette(10, 220, sep=80, n=7))
```

![http://seaborn.pydata.org/_images/color_palettes_62_0.png](assets/df43f6b2a11e5c0ee8c60f7ef2962e19.jpg)


也可以将中间点的颜色设置成暗色而非亮色。

```python
sns.palplot(sns.diverging_palette(255, 133, l=60, n=7, center="dark"))
```

![http://seaborn.pydata.org/_images/color_palettes_64_0.png](assets/b0fa1a625cc563e5a13abc41fa080b59.jpg)


## 设置默认调色板

与[`color_palette()`](../generated/seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette")函数相伴随的有[`set_palette()`](../generated/seaborn.set_palette.html#seaborn.set_palette "seaborn.set_palette")。 两者之间的关系与[美学教程](aesthetics.html#aesthetics-tutorial)中介绍的[`set_palette()`](../generated/seaborn.set_palette.html#seaborn.set_palette "seaborn.set_palette")函数和[`color_palette()`](../generated/seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette")函数接受相同参数的关系相类似。但它会更改默认的 matplotlib 参数，以便调色板应用于所有图像。

```python
def sinplot(flip=1):   
    x = np.linspace(0, 14, 100)    
    for i in range(1, 7):        
        plt.plot(x, np.sin(x + i * .5) * (7 - i) * flip)
```

```python
sns.set_palette("husl")
sinplot()
```

![http://seaborn.pydata.org/_images/color_palettes_67_0.png](assets/6c2898fe95dd4d45e166acda2ab730fe.jpg)


您可以在 `with` 语句中通过 [`color_palette()`](../generated/seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette") 函数来临时改变调色板。

```python
with sns.color_palette("PuBuGn_d"):    
    sinplot()
```

![http://seaborn.pydata.org/_images/color_palettes_69_0.png](assets/a276fb7452f1f31a95ec2ec9d810fa20.jpg)

# 接口文档

## seaborn.relplot


```python
seaborn.relplot(x=None, y=None, hue=None, size=None, style=None, data=None, row=None, col=None, col_wrap=None, row_order=None, col_order=None, palette=None, hue_order=None, hue_norm=None, sizes=None, size_order=None, size_norm=None, markers=None, dashes=None, style_order=None, legend='brief', kind='scatter', height=5, aspect=1, facet_kws=None, **kwargs)
```

绘制相关关系图像到 FacetGrid 的图像级别接口。

此函数提供对一些不同轴级别函数的访问，这些函数通过子集的语义映射显示两个变量之间的关系。`kind`参数选择要使用的基础轴级函数：

*   [`scatterplot()`](seaborn.scatterplot.html#seaborn.scatterplot "seaborn.scatterplot") （通过`kind="scatter"`访问；默认为此）
*   [`lineplot()`](seaborn.lineplot.html#seaborn.lineplot "seaborn.lineplot") (通过`kind="line"`访问)

额外的关键字参数会被传递给隐含的函数，因此使用时应当参考对应函数的文档去了解各种选项。

对于数据不同子集的`x`与`y`的绘制关系可以通过`hue`, `size`以及`style`参数控制。这些参数控制使用哪些视觉语义来区分不同的子集。使用所有三个语义类型可以独立展示三个维度，但是这种方式得到的绘制结果难以被理解而且低效。使用多种语义（例如对相同变量同时使用`hue`及`style`）可以使图像更加易懂。

参考[tutorial](http://seaborn.pydata.org/tutorial/relational.html#relational-tutorial)获得更多信息。

绘制后，会返回带有图像的[`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")，随后可以直接进行图像细节调节或者加入其他图层。

值得注意的是，与直接使用隐含函数的方式不同，数据必须以长格式的 DataFrame 传入，同时变量通过`x`, `y`及其他参数指定。

参数：`x, y`：`data`中的变量名

> 输入数据的变量；数据必须为数值型。

`hue`: `data`中的名称，可选

> 将会产生具有不同颜色的元素的变量进行分组。这些变量可以是类别变量或者数值型变量，尽管颜色映射在后面的情况中会有不同的表现。

`size`：`data`中的名称，可选

> 将会产生具有不同尺寸的元素的变量进行分组。这些变量可以是类别变量或者数值型变量，尽管尺寸映射在后面的情况中会有不同的表现。

`style`：`data`中的名称，可选

> 将会产生具有不同风格的元素的变量进行分组。这些变量可以为数值型，但是通常会被当做类别变量处理。

`data`：DataFrame

> 长格式的 DataFrame，每列是一个变量，每行是一个观察值。

`row, col`：`data`中的变量名，可选

> 确定网格的分面的类别变量。

`col_wrap`：int, 可选

> 以此宽度“包裹”列变量，以便列分面跨越多行。与`row`分面不兼容。

`row_order, col_order`：字符串列表，可选

> 以此顺序组织网格的行和/或列，否则顺序将从数据对象中推断。

`palette`：色盘名，列表，或者字典，可选

> 用于`hue`变量的不同级别的颜色。应当是[`color_palette()`](seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette")可以解释的东西，或者将色调级别映射到 matplotlib 颜色的字典。

`hue_order`：列表，可选

> 指定`hue`变量层级出现的顺序，否则会根据数据确定。当`hue`变量为数值型时与此无关。

`hue_norm`：元组或者 Normalize 对象，可选

> 当`hue`变量为数值型时，用于数据单元的 colormap 的标准化。如果`hue`为类别变量则与此无关。

`sizes`：列表、典或者元组，可选

> 当使用`sizes`时，用于确定如何选择尺寸。此变量可以一直是尺寸值的列表或者`size`变量的字典映射。当`size`为数值型时，此变量也可以是指定最小和最大尺寸的元组，这样可以将其他值标准化到这个范围。

`size_order`：列表，可选

> 指定`size`变量层次的表现顺序，不指定则会通过数据确定。当`size`变量为数值型时与此无关。

`size_norm`：元组或者 Normalize 对象，可选

> 当`size`变量为数值型时，用于数据单元的 scaling plot 对象的标准化。

`legend`：“brief”, “full”, 或者 False, 可选

> 用于决定如何绘制坐标轴。如果参数值为“brief”, 数值型的`hue`以及`size`变量将会被用等间隔采样值表示。如果参数值为“full”, 每组都会在坐标轴中被记录。如果参数值为“false”, 不会添加坐标轴数据，也不会绘制坐标轴。

`kind`：string, 可选

> 绘制图的类型，与 seaborn 相关的图一致。可选项为(`scatter`及`line`).

`height`：标量, 可选

> 每个 facet 的高度（英寸）。参见`aspect`。

`aspect`：标量, 可选

> 每个 facet 的长宽比，因此“长宽比*高度”可以得出每个 facet 的宽度（英寸）。

`facet_kws`：dict, 可选

> 以字典形式传给[`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")的其他关键字参数.

`kwargs`：键值对

> 传给后续绘制函数的其他关键字参数。


返回值：`g`：[`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")

> 返回包含图像的[`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")对象，图像可以进一步调整。



示例

使用[`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")的坐标轴布局绘制简单的 facet。

```python
>>> import seaborn as sns
>>> sns.set(style="ticks")
>>> tips = sns.load_dataset("tips")
>>> g = sns.relplot(x="total_bill", y="tip", hue="day", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-relplot-1.png](assets/1c7fa44b0679b71a3e8e8a4e26d75963.jpg)


利用其他变量绘制 facet:

```python
>>> g = sns.relplot(x="total_bill", y="tip", hue="day", col="time", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-relplot-2.png](assets/f0eb2f840a13f8dbd2b829e6bb51c4af.jpg)


绘制两行两列的 facet:

```python
>>> g = sns.relplot(x="total_bill", y="tip", hue="day", col="time", row="sex", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-relplot-3.png](assets/e6f2f0c866185be9801d43c59f348826.jpg)


将多行 facets 转换为多列:

```python
>>> g = sns.relplot(x="total_bill", y="tip", hue="time",
...                 col="day", col_wrap=2, data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-relplot-4.png](assets/a89db144a2f030e70603b92ba8728326.jpg)


利用指定的属性值对每个 facet 使用多种语义变量:

```python
>>> g = sns.relplot(x="total_bill", y="tip", hue="time", size="size",
...                 palette=["b", "r"], sizes=(10, 100),
...                 col="time", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-relplot-5.png](assets/79d93fb6cd8974ec779ead1b35e2aad1.jpg)


使用不同类型的图:

```python
>>> fmri = sns.load_dataset("fmri")
>>> g = sns.relplot(x="timepoint", y="signal",
...                 hue="event", style="event", col="region",
...                 kind="line", data=fmri)
```

![http://seaborn.pydata.org/_images/seaborn-relplot-6.png](assets/f14f4331afb42bd5512f2c24a5140604.jpg)


改变每个 facet 的大小:

```python
>>> g = sns.relplot(x="timepoint", y="signal",
...                 hue="event", style="event", col="region",
...                 height=5, aspect=.7, kind="line", data=fmri)
```

![http://seaborn.pydata.org/_images/seaborn-relplot-7.png](assets/bcbb773f27f65051374518d1d859a486.jpg)


## seaborn.scatterplot


```python
seaborn.scatterplot(x=None, y=None, hue=None, style=None, size=None, data=None, palette=None, hue_order=None, hue_norm=None, sizes=None, size_order=None, size_norm=None, markers=True, style_order=None, x_bins=None, y_bins=None, units=None, estimator=None, ci=95, n_boot=1000, alpha='auto', x_jitter=None, y_jitter=None, legend='brief', ax=None, **kwargs)
```

绘制几个语义分组的散点图。

数据的不同子集的 x 和 y 之间的关系可以用 hue, size, style 这三个参数来控制绘图属性。
这些参数控制用于识别不同子集的视觉语义信息，三种语义类型全部使用的话可以独立显示三个维度，
但是这种风格的绘图很难解释或者说没大部分时候什么效果。使用过多的语义信息
（例如：对同一个变量同时使用 hue 和 style)对作图来说是很有帮助同时也更容易理解。

阅读下面的教程可以 get 更多信息哦。

**参数**:

`x, y`: data 或是向量 data 里面的变量名字，可选择

> 输入数据的变量，必须是数字，可以直接传递数据或引用数据中的列

`hue`： data 或是向量 data 里面的变量名字，可选择

> 将产生不同大小的点的变量进行分组，可以是类别也可以是数字，
> 但是大小映射在后一种情况会有不同的表现

`style`： data 或是向量 data 里面的变量名字，可选择

> 将产生不同标记的点的变量进行分类，可以有一个数字类型，但是这个数字会被当作类别

`data`：DataFrame

> Tidy (“long-form”) dataframe 它的每一列是一个变量，每一行是一个观测值

plaette : 调色板的名字、列表或字典，可选

> 用于不同 level 的 hue 变量的颜色，应该是可以被 color_palette() 执行的 something，
> 或者是一个可以对 matplotlib colors 映射 hue 级别的字典。

`hue_order`：列表，可选

> 对 hue 变量的级别的表象有特定的顺序，否则的话，顺序由 data 决定。当 hue 是数字的时候与它不相关

`hue_norm`：元组或标准化的对象，可选

> 当 hue 变量是数字的时候，应用于 hue 变量的色彩映射的数据单元中的标准化。如果是类别则不相关

`sizes`：列表，字典或元组，可选

> 当使用 size 的时候，用来决定如何选择 sizes 的一个对象。可以一直是一个包含 size 数值的列表，
> 或者是一个映射变量 size 级别到 sizes 的字典。当 size 是数字时，sizes 可以是包含 size 最大值
> 和最小值的元组，其他的值都会标准化到这个元组指定的范围

`size_order`：元组，可选

> size 变量级别表现的特定顺序，否则顺序由 data 决定，当 size 变量是数字时不相关

`size_norm`：元组或标准化的对象，可选

> 当变量 size 是数字时，用于缩放绘图对象的数据单元中的标准化

`makers`：布尔型，列表或字典，可选

> 决定如何绘制不同级别 style 的标志符号。设置为 True 会使用默认的标志符号，或者通过一系列标志
> 或者一个字典映射 style 变量的级别到 markers。设置为 False 会绘制无标志的线。
> Markers 在 matplotlib 中指定

`style_order`：列表，可选

> 对于 style 变量级别表象的特定顺序，否则由 data 决定，当 style 是数字时不相关

`{x,y}_bins`: 元组，矩阵或函数

> 暂时没有什么功能

`units`：{long_form_var}

> 分组特定的样本单元。使用时，将为每个具有适当的语义的单元绘制一根单独的线，
> 但不会添加任何图例条目。 当不需要确切的身份时，可用于显示实验重复的分布。
> 目前没啥作用

`estimator`：pandas 方法的名称，或者可调用的方法或者是 None，可选

> 聚类同一个 x 上多个观察值 y，如果是 None，所有的观察值都会绘制，目前暂无功能

`ci`：整型或 'sd' or None,可选

> 与估算器聚合时绘制的置信区间的大小。 “sd”表示绘制数据的标准偏差。 设置为 None 将跳过自举。 目前无功能。

`n_boot`：整型，可选

> 自举法的数量，用于计算区间的置信度，暂无功能

`alpha`：浮点型

> 设置点的不透明度

`{x,y}_jitter`：布尔或者浮点型

> 暂无功能

`legend`：“brief”, “full”, or False, 可选

> 绘制图例的方式。如果为“brief" 数字 hue 和 size 变量会代表一个样本，即便有不同的值
> if "full", 每一个分组都有图例。if False 不绘制也不添加图例

`ax`：matplotlib 坐标轴，可选

> 绘制图像的坐标对象，否则使用当前坐标轴

`kwargs`：键值映射对

> 在绘制的时候其他的键会传递给 plt.scatter

返回值：ax：matplotlib 坐标轴

> 返回绘制所需的坐标

请参阅官方文档

用线显示两个变量之间的关系以强调连续性。绘制带有一个分类变量的散点图，排列点以显示值的分布。

例子

绘制一个两个变量的简单散点图：

```python
import seaborn as sns; sns.set()
import matplotlib.pyplot as plt
tips = sns.load_dataset('tips')
ax = sns.scatterplot(x='total_bill',y='tip',data=tips)
```

![](assets/clipboard1.png)

通过其他的变量分组并且用不同的颜色展示分组

```python
ax = sns.scatterplot(x='total_bill',y='tip',hue='time',data=tips)
```

![](assets/clipboard2.png)

通过不同的颜色和标记显示分组变量：

```python
ax = sns.scatterplot(x='total_bill',y='tip', hue='time',style='time',data=tips)
```

![](assets/clipboard3.png)

不同的颜色和标志显示两个不同的分组变量：

```python
ax = sns.scatterplot(x='total_bill',y='tip',
                     hue='day',style='time',data=tips)
```

![](assets/clipboard4.png)

用不同大小的点显示一个变量的数量：

```python
ax = sns.scatterplot(x='total_bill',y='tip', size='size',data=tips)
```

![](assets/clipboard5.png)

使用渐变的颜色显示变量的数量：

```python
ax = sns.scatterplot(x='total_bill',y='tip', hue='size', size='size',data=tips)
```

![](assets/clipboard6.png)

用一个不一样的渐变颜色映射：

```python
cmap = sns.cubehelix_palette(dark=.3,light=.8,as_cmap=True)
ax = sns.scatterplot(x='total_bill',y='tip',
                     hue='size', size='size',palette=cmap,data=tips)
```

![](assets/clipboard7.png)

改变点大小的最小值和最大值并在图例中显示所有的尺寸：

```python
cmap = sns.cubehelix_palette(dark=.3,light=.8,as_cmap=True)
ax = sns.scatterplot(x='total_bill',y='tip',
                     hue='size',size='size', sizes=(20,200),
                     legend='full',palette=cmap,data=tips)
```

![](assets/clipboard8.png)

使用一个更小的颜色强度范围：

```python
cmap = sns.cubehelix_palette(dark=.3,light=.8,as_cmap=True)
ax = sns.scatterplot(x='total_bill',y='tip',hue='size',size='size',
                     sizes=(20,200),hue_norm=(0,7), legend='full',data=tips)
```

![](assets/clipboard9.png)

一个类别变量不同的大小，用不同的颜色：

```python
ax = sns.scatterplot(x='total_bill',y='tip',hue='day',size='smoker',
                     palette='Set2',data=tips)
```

![](assets/clipboard10.png)

使用一些特定的标识：

```python
markers = {'Lunch':'s','Dinner':'X'}ax = sns.scatterplot(x='total_bill',y='tip',style='time',
                     markers=markers,data=tips)
```

![](assets/clipboard11.png)

使用 matplotlib 的参数控制绘制属性：

```python
ax = sns.scatterplot(x='total_bill',y='tip',
                     s=100,color='.2',marker='+',data=tips)
```

![](assets/clipboard12.png)

使用 data 向量代替 data frame 名字：

```python
iris = sns.load_dataset('iris')
ax = sns.scatterplot(x=iris.sepal_length,y=iris.sepal_width,
                     hue = iris.species,style=iris.species)
```

![](assets/clipboard13.png)

传递宽格式数据并根据其索引进行绘图:

```python
import numpy as np, pandas as pd; 
plt.close("all")

index = pd.date_range('1 1 2000',periods=100,freq='m',name='date')
data = np.random.randn(100,4).cumsum(axis=0)
wide_df = pd.DataFrame(data,index,['a','b','c','d'])
print(wide_df.head())
ax = sns.scatterplot(data=wide_df)
```

![](assets/clipboard14.png)

## seaborn.lineplot

```python
seaborn.lineplot(x=None, y=None, hue=None, size=None, style=None, data=None, palette=None, hue_order=None, hue_norm=None, sizes=None, size_order=None, size_norm=None, dashes=True, markers=None, style_order=None, units=None, estimator='mean', ci=95, n_boot=1000, sort=True, err_style='band', err_kws=None, legend='brief', ax=None, **kwargs)
```

用不同语义分组绘制线型图

`x`和`y`之间的关系可以使用`hue`，`size`和`style`参数为数据的不同子集显示。这些参数控制用于识别不同子集的视觉语义。通过使用所有三种语义类型，可以独立地显示三个维度，但是这种画图样式可能难以解释并且通常是无效的。使用冗余语义（即同一变量的`hue`和`style`）有助于使图形更易于理解。

请查看[指南](http://seaborn.pydata.org/tutorial/relational.html#relational-tutoria)获取更多信息。

默认情况下，图标在每个`x`值处汇总多个`y`值，并显示集中趋势的估计值和该估计值的置信区间。

参数：`x,y`： `data`或向量数据中变量的名称，可选择。

> 输入数据变量；必须是数字。可以直接传递数据或引用`data`中的列。

`hue`: `data`或向量数据中的变量名，可选。

> 分组变量，将生成具有不同颜色的线条的变量。可以是分类或数字，但颜色映射在后一种情况下的行为会有所不同。

`size`: `data`或向量数据中的变量名，可选。

> 分组变量，将生成具有不同粗细的线条的变量。可以是分类或数字，但大小映射在后一种情况下的行为会有所不同。

`style`: `data`或向量数据中的变量名，可选。

> 分组变量，将生成具有不同样式和/或标记的线条的变量。可以是一种数字形式，但是始终会被视为分类。

`data`: 数据框架。

> 整洁（“长形式”）数据框，其中每列是变量，每行是观察量。

`palette`： 调色板名称，列表或字典，可选。

> 用于`hue`变量的不同级别的颜色。应该是[`color_palette()`](seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette")可以解释的东西，或者是将色调级别映射到 matplotlib 颜色的字典。

`hue_order`：列表，可选。

> 指定`hue`变量级别的出现顺序，否则它们是根据数据确定的。当`hue`变量是数字时不相关。

`hue_norm`： 原则或者时归一化对象，可选。

> 当数值为数字时，应用于`hue`变量的颜色图数据单元的归一化。 如果是分类的，则不相关。

`sizes`：列表，字典，或者元组。可选。

> 确定在使用`size`时如何选择大小的对象。它始终可以是大小值列表或`size`变量与大小的字典映射级别。当`size`是数字时，它也可以是一个元组，指定要使用的最小和最大大小，以便在此范围内对其他值进行规范化。

`size_norm`：原则或者时归一化对象，可选。

> 当`size`变量是数字时，用于缩放绘图对象的数据单元中的归一化。

`dashes`： 布尔值，列表或字典，可选。

> 确定如何为`style`变量的不同级别绘制线条的对象。设置为`True`将使用默认的短划线代码，或者您可以将短划线代码列表或`style`变量的字典映射级别传递给短划线代码。设置为`False`将对所有子集使用实线。线段在 matplotlib 中指定： `(segment, gap)`长度的元组，或用于绘制实线的空字符串。

`markers`： 布尔值，列表或字典，可选。

> 确定如何为`style`变量的不同级别绘制标记的对象。 设置为“True”将使用默认标记，或者您可以传递标记列表或将`style`变量的字典映射到标记。 设置为“False”将绘制无标记线。 标记在 matplotlib 中指定。

`style_order`：列表，可选。

> 指定`style`变量级别的出现顺序，否则它们是从数据中确定的。`style`变量时数字不相关的。

`units`： {long_form_var}

> 对变量识别抽样单位进行分组。使用时，将为每个单元绘制一个单独的行，并使用适当的语义。但不会添加任何图里条目。当不需要确切的身份时，可用于显示实验重复的分布。

`estimator`：pandas 方法的名称或可调用或无，可选。

> 在相同的`x`级别上聚合`y`变量的多个观察值的方法。如果`None`，将绘制所有观察结果。

`ci`：整数或`sd`或 None。可选。

> 与`estimator`聚合时绘制的置信区间大小。`sd`表示绘制数据的标准偏差。设置为`None`将跳过 bootstrap。

`n_boot`：整数，可选。

> 用于计算置信区间的 bootstrap 数。

`sort`：布尔值，可选。

> 如果为真，则数据将按 x 与 y 变量排序，否则行将按照它们在数据集中出现的顺序连接点。

`err_style`: `band`或`bars`，可选。

> 是否用半透明误差带或离散误差棒绘制置信区间。

`err_band`：关键字参数字典。

> 用于控制误差线美观的附加参数。 `kwargs`传递给`ax.fill_between`或`ax.errorbar`，具体取决于`err_style`。

`legend`： `brief`,`full`,或`False`。可选。

> 如何绘制图例。如果`brief`，则数字`hue`和`size`变量将用均匀间隔值的样本表示。如果`full`，则每个组都会在图例中输入一个条目。如果为`False`，则不添加图例数据且不绘制图例。

`ax`：matplotlib 轴。可选。

> 将绘图绘制到的 Axes 对象，否则使用当前轴。

`kwargs`：关键，价值映射。

> 其他关键字参数在绘制时传递给`plt.plot`。 

返回值：`ax`：matplotlib 轴

> 返回 Axes 对象，并在其上绘制绘图。

也可以看看

显示两个变量之间的关系，而不强调`x`变量的连续性。当两个变量时分类时，显示两个变量之间的关系。

例子

绘制单线图，其中错误带显示执行区间：

```python
>>> import seaborn as sns; sns.set()
>>> import matplotlib.pyplot as plt
>>> fmri = sns.load_dataset("fmri")
>>> ax = sns.lineplot(x="timepoint", y="signal", data=fmri)
```

![http://seaborn.pydata.org/_images/seaborn-lineplot-1.png](assets/25ad91543cb2ac105ccfb3efc544658a.jpg)


按另一个变量分组并显示具有不同颜色的组：

```python
>>> ax = sns.lineplot(x="timepoint", y="signal", hue="event",
...                   data=fmri)
```

![http://seaborn.pydata.org/_images/seaborn-lineplot-2.png](assets/5d2d2c464b8872f780180217182517d4.jpg)


使用颜色和线条划线显示分组变量：

```python
>>> ax = sns.lineplot(x="timepoint", y="signal",
...                   hue="event", style="event", data=fmri)
```

![http://seaborn.pydata.org/_images/seaborn-lineplot-3.png](assets/2e99f3ab69ddcc3a94df1ddb15068249.jpg)


使用颜色和线条划线来表示两个不同的分组变量：

```python
>>> ax = sns.lineplot(x="timepoint", y="signal",
...                   hue="region", style="event", data=fmri)
```

![http://seaborn.pydata.org/_images/seaborn-lineplot-4.png](assets/7c717f27fe7c1e02672338abf7cad829.jpg)


使用标记而不是破折号来标识组：

```python
>>> ax = sns.lineplot(x="timepoint", y="signal",
...                   hue="event", style="event",
...                   markers=True, dashes=False, data=fmri)
```

![http://seaborn.pydata.org/_images/seaborn-lineplot-5.png](assets/f82003c5544990262fe9a61a131d93ef.jpg)


显示错误条而不是错误带并绘制标准错误：

```python
>>> ax = sns.lineplot(x="timepoint", y="signal", hue="event",
...                   err_style="bars", ci=68, data=fmri)
```

![http://seaborn.pydata.org/_images/seaborn-lineplot-6.png](assets/a92991eef59fbedc422f330f51697fe4.jpg)


显示实验性重复而不是聚合：

```python
>>> ax = sns.lineplot(x="timepoint", y="signal", hue="event",
...                   units="subject", estimator=None, lw=1,
...                   data=fmri.query("region == 'frontal'"))
```

![http://seaborn.pydata.org/_images/seaborn-lineplot-7.png](assets/d7a78945c0ae60bbd5ee9dfef15baba0.jpg)


使用定量颜色映射：

```python
>>> dots = sns.load_dataset("dots").query("align == 'dots'")
>>> ax = sns.lineplot(x="time", y="firing_rate",
...                   hue="coherence", style="choice",
...                   data=dots)
```

![http://seaborn.pydata.org/_images/seaborn-lineplot-8.png](assets/bc93752a335d8985139584678e19a2b9.jpg)


对 colormap 使用不同的归一化：

```python
>>> from matplotlib.colors import LogNorm
>>> ax = sns.lineplot(x="time", y="firing_rate",
...                   hue="coherence", style="choice",
...                   hue_norm=LogNorm(), data=dots)
```

![http://seaborn.pydata.org/_images/seaborn-lineplot-9.png](assets/1848472fd25a5227de0c58b2fc43c865.jpg)


使用不同的调色板：

```python
>>> ax = sns.lineplot(x="time", y="firing_rate",
...                   hue="coherence", style="choice",
...                   palette="ch:2.5,.25", data=dots)
```

![http://seaborn.pydata.org/_images/seaborn-lineplot-10.png](assets/f758b47e18df4e0b8e31c3a3fb9a1634.jpg)


使用特定颜色值，将 hue 变量视为分类：

```python
>>> palette = sns.color_palette("mako_r", 6)
>>> ax = sns.lineplot(x="time", y="firing_rate",
...                   hue="coherence", style="choice",
...                   palette=palette, data=dots)
```

![http://seaborn.pydata.org/_images/seaborn-lineplot-11.png](assets/f91976efc3d73db2b134c57ba7dd86f3.jpg)


使用定量变量更改线条的宽度：

```python
>>> ax = sns.lineplot(x="time", y="firing_rate",
...                   size="coherence", hue="choice",
...                   legend="full", data=dots)
```

![http://seaborn.pydata.org/_images/seaborn-lineplot-12.png](assets/f7c110a02ff0dffcc8a6c88582b45b70.jpg)


更改用于规范化 size 变量的线宽范围：

```python
>>> ax = sns.lineplot(x="time", y="firing_rate",
...                   size="coherence", hue="choice",
...                   sizes=(.25, 2.5), data=dots)
```

![http://seaborn.pydata.org/_images/seaborn-lineplot-13.png](assets/438199f3698b863dd842a2f680c4a845.jpg)


DataFrame 绘制：

```python
>>> import numpy as np, pandas as pd; plt.close("all")
>>> index = pd.date_range("1 1 2000", periods=100,
...                       freq="m", name="date")
>>> data = np.random.randn(100, 4).cumsum(axis=0)
>>> wide_df = pd.DataFrame(data, index, ["a", "b", "c", "d"])
>>> ax = sns.lineplot(data=wide_df)
```

![http://seaborn.pydata.org/_images/seaborn-lineplot-14.png](assets/25659857727e22d0f951f9c750bb29de.jpg)


系列列表中绘制：

```python
>>> list_data = [wide_df.loc[:"2005", "a"], wide_df.loc["2003":, "b"]]
>>> ax = sns.lineplot(data=list_data)
```

![http://seaborn.pydata.org/_images/seaborn-lineplot-15.png](assets/711ff3dd5a0aef63679fe4174259ee3d.jpg)


绘制单个系列，将 kwargs 传递给`plt.plot`：

```python
>>> ax = sns.lineplot(data=wide_df["a"], color="coral", label="line")
```

![http://seaborn.pydata.org/_images/seaborn-lineplot-16.png](assets/fde16964145ae355ddbbeae37728260a.jpg)


在数据集中出现的点处绘制线条：

```python
>>> x, y = np.random.randn(2, 5000).cumsum(axis=1)
>>> ax = sns.lineplot(x=x, y=y, sort=False, lw=1)
```

![http://seaborn.pydata.org/_images/seaborn-lineplot-17.png](assets/05782b02c2faaf09a504b53a5a526445.jpg)


## seaborn.catplot

```python
seaborn.catplot(x=None, y=None, hue=None, data=None, row=None, col=None, col_wrap=None, estimator=<function mean>, ci=95, n_boot=1000, units=None, order=None, hue_order=None, row_order=None, col_order=None, kind='strip', height=5, aspect=1, orient=None, color=None, palette=None, legend=True, legend_out=True, sharex=True, sharey=True, margin_titles=False, facet_kws=None, **kwargs)
```

seaborn.catplot 是一个将分类图绘制到 FacetGrid 上图级别接口。

这个函数可以访问多个轴级功能，这些轴级功能通过不同的可视化图表展示数字和一个或多个分类变量的关系。`kind` 参数可以选择的轴级基础函数有：

分类散点图:

*   [`stripplot()`](seaborn.stripplot.html#seaborn.stripplot "seaborn.stripplot") (with `kind="strip"`; the default)
*   [`swarmplot()`](seaborn.swarmplot.html#seaborn.swarmplot "seaborn.swarmplot") (with `kind="swarm"`)

分类分布图:

*   [`boxplot()`](seaborn.boxplot.html#seaborn.boxplot "seaborn.boxplot") (with `kind="box"`)
*   [`violinplot()`](seaborn.violinplot.html#seaborn.violinplot "seaborn.violinplot") (with `kind="violin"`)
*   [`boxenplot()`](seaborn.boxenplot.html#seaborn.boxenplot "seaborn.boxenplot") (with `kind="boxen"`)

分类估计图:

*   [`pointplot()`](seaborn.pointplot.html#seaborn.pointplot "seaborn.pointplot") (with `kind="point"`)
*   [`barplot()`](seaborn.barplot.html#seaborn.barplot "seaborn.barplot") (with `kind="bar"`)
*   [`countplot()`](seaborn.countplot.html#seaborn.countplot "seaborn.countplot") (with `kind="count"`)

额外的关键字参数将传递给基础函数，因此，您应参阅每个文档，以查看特定类型的选项.

请注意，与直接使用轴级函数不同, 数据必须在长格式 DataFrame 中传递，并通过将字符串传递给 `x`, `y`, `hue`, 等指定的变量.

与基础绘图函数的情况一样, 如果变量有 `categorical` 数据类型, 则将从对象推断出分类变量的级别及其顺序。否则，您可能必须使用更改 dataframe 排序或使用函数参数(`orient`, `order`, `hue_order`, etc.) 来正确设置绘图。

此函数始终将其中一个变量视为分类，并在相关轴上的序数位置（0,1，... n）处绘制数据，即使数据具有数字或日期类型。

有关更多信息，请参考 [tutorial](http://seaborn.pydata.org/tutorial/categorical.html#categorical-tutorial)。

绘图后，返回带有绘图的 [`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")，可以直接用于调整绘图细节或添加其他图层。

参数：`x, y, hue`： `data` names 中的变量名称

> 用于绘制长格式数据的输入。查看解释示例

`data`：DataFrame

> 用于绘图的长形（整洁）数据集。每列应对应一个变量，每行应对应一个观察。

`row, col`：`data` 中的变量名称, 可选

> 分类变量将决定网格的分面。

`col_wrap`：int, 可选

> 以此宽度“包裹”列变量，以便列面跨越多行。 与`行`方面不兼容。

`estimator`：可调用的映射向量 -&gt; 标量，可选

> 在每个分类箱内估计的统计函数。

`ci`：float 或“sd”或 None，可选

> 在估计值附近绘制置信区间的大小。如果是“sd”，则跳过自举(bootstrapping)并绘制观察的标准偏差。None,如果为`None`，则不执行自举，并且不会绘制错误条。

`n_boot`：int，可选

> 计算置信区间时使用的引导程序迭代次数。

`units`：`数据`或矢量数据中变量的名称,可选

>采样单元的标识符，用于执行多级引导程序并考虑重复测量设计。

`order, hue_order`：字符串列表，可选

> 命令绘制分类级别，否则从数据对象推断级别。

`row_order, col_order`：字符串列表，可选

>命令组织网格的行和/或列，否则从数据对象推断命令。

`kind`：字符串，可选

>要绘制的绘图类型（对应于分类绘图功能的名称。选项包括：“点”，“条形”，“条形”，“群”，“框”，“小提琴”或“盒子”。

`height`：标量，可选

> 每个刻面的高度（以英寸为单位）。另见： `aspect`。

`aspect`：标量，可选

> 每个面的纵横比，因此`aspect * height`给出每个面的宽度，单位为英寸。

`orient`：“v” &#124; “h”, 可选

> 图的方向（垂直或水平）。这通常是从输入变量的 dtype 推断出来的，但可用于指定“分类”变量何时是数字或何时绘制宽格式数据。

`color`：matplotlib 颜色，可选

> 所有元素的颜色，或渐变调色板的种子。

`palette`：调色板名称，列表或字典，可选

> 用于色调变量的不同级别的颜色。应该是 [`color_palette()`](seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette"), 可以解释的东西，或者是将色调级别映射到 matplotlib 颜色的字典。

`legend`：bool, 可选

>  如果为 `True` 并且存在`hue`变量，则在图上绘制图例。t.

`legend_out`：bool, 可选

>  如果为`True`，则图形尺寸将被扩展，图例将绘制在中间右侧的图形之外。


`share{x,y}`：bool, ‘col’, 或 ‘row’ 可选

> 如果为 true，则 facet 将跨行跨越列和/或 x 轴共享 y 轴。

`margin_titles`：bool, 可选

> 如果为`True`，则行变量的标题将绘制在最后一列的右侧。此选项是实验性的，可能无法在所有情况下使用。

`facet_kws`：dict, 可选

> 传递给[`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")的其他关键字参数的字典。

`kwargs`：key, value 配对

> 其他关键字参数将传递给基础绘图函数。


返回值：`g`：[`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")

> 返回[`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")对象及其上的绘图以进一步调整。



例子

绘制单个构面以使用[`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")图例放置：

```python
>>> import seaborn as sns
>>> sns.set(style="ticks")
>>> exercise = sns.load_dataset("exercise")
>>> g = sns.catplot(x="time", y="pulse", hue="kind", data=exercise)
```

![http://seaborn.pydata.org/_images/seaborn-catplot-1.png](assets/0b12363978e10369fc98bd33cb536d85.jpg)


使用不同的绘图类型可视化相同的数据：

```python
>>> g = sns.catplot(x="time", y="pulse", hue="kind",
...                data=exercise, kind="violin")
```

![http://seaborn.pydata.org/_images/seaborn-catplot-2.png](assets/edd1fe3b83d3ec7ff2a1310bc2b87b8c.jpg)


沿列的方面显示第三个分类变量：

```python
>>> g = sns.catplot(x="time", y="pulse", hue="kind",
...                 col="diet", data=exercise)
```

![http://seaborn.pydata.org/_images/seaborn-catplot-3.png](assets/f54e97484ce6dfcae6e1ceafe18503af.jpg)


为构面使用不同的高度和宽高比：

```python
>>> g = sns.catplot(x="time", y="pulse", hue="kind",
...                 col="diet", data=exercise,
...                 height=5, aspect=.8)
```

![http://seaborn.pydata.org/_images/seaborn-catplot-4.png](assets/ff09acb1a4ecf2c617aa1323f8894fe5.jpg)


创建许多列构面并将它们包装到网格的行中：

```python
>>> titanic = sns.load_dataset("titanic")
>>> g = sns.catplot("alive", col="deck", col_wrap=4,
...                 data=titanic[titanic.deck.notnull()],
...                 kind="count", height=2.5, aspect=.8)
```

![http://seaborn.pydata.org/_images/seaborn-catplot-5.png](assets/94e545c07037b66573b94c5d43e41899.jpg)


水平绘图并将其他关键字参数传递给绘图函数：

```python
>>> g = sns.catplot(x="age", y="embark_town",
...                 hue="sex", row="class",
...                 data=titanic[titanic.embark_town.notnull()],
...                 orient="h", height=2, aspect=3, palette="Set3",
...                 kind="violin", dodge=True, cut=0, bw=.2)
```

![http://seaborn.pydata.org/_images/seaborn-catplot-6.png](assets/141f4c98341da1bc3beb4dc78e30df08.jpg)


 使用返回的[`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid") 上的方法来调整演示文稿：

```python
>>> g = sns.catplot(x="who", y="survived", col="class",
...                 data=titanic, saturation=.5,
...                 kind="bar", ci=None, aspect=.6)
>>> (g.set_axis_labels("", "Survival Rate")
...   .set_xticklabels(["Men", "Women", "Children"])
...   .set_titles("{col_name}  {col_var}")
...   .set(ylim=(0, 1))
...   .despine(left=True))  <seaborn.axisgrid.FacetGrid object at 0x...>
```

![http://seaborn.pydata.org/_images/seaborn-catplot-7.png](assets/407bdac663f0df796492a65e635fca81.jpg)


## seaborn.stripplot

```python
seaborn.stripplot(x=None, y=None, hue=None, data=None, order=None, hue_order=None, jitter=True, dodge=False, orient=None, color=None, palette=None, size=5, edgecolor='gray', linewidth=0, ax=None, **kwargs)
```

绘制一个散点图，其中一个变量是分类。

条形图可以单独绘制，但如果您想要显示所有观察结果以及底层分布的某些表示，它也是一个盒子或小提琴图的良好补充。

输入数据可以以多种格式传递，包括：

*   表示为列表，numpy 数组或 pandas Series 对象的数据向量直接传递给`x`，`y`和`hue`参数
*   在这种情况下，`x`，`y`和`hue`变量将决定数据的绘制方式。
*   “wide-form” DataFrame, 用于绘制每个数字列。
*   一个数组或向量列表。

在大多数情况下，可以使用 numpy 或 Python 对象，但最好使用 pandas 对象，因为关联的名称将用于注释轴。另外，您可以使用分组变量的分类类型来控制绘图元素的顺序。

此函数始终将其中一个变量视为分类，并在相关轴上的序数位置（0,1，... n）处绘制数据，即使数据具有数字或日期类型也是如此。

有关更多信息，请参阅[教程](http://seaborn.pydata.org/tutorial/categorical.html#categorical-tutorial)。

参数：`x, y, hue`： `数据`或矢量数据中的变量名称，可选

> 用于绘制长格式数据的输入。查看解释示例。

`data`：DataFrame, 数组, 数组列表, 可选

>用于绘图的数据集。如果 `x` 和 `y` 不存在，则将其解释为宽格式。否则预计它将是长格式的。

`order, hue_order`：字符串列表，可选

>命令绘制分类级别，否则从数据对象推断级别。

`jitter`：float, `True`/`1` 是特殊的，可选

>要应用的抖动量（仅沿分类轴）。 当您有许多点并且它们重叠时，这可能很有用，因此更容易看到分布。您可以指定抖动量（均匀随机变量支持的宽度的一半），或者仅使用`True`作为良好的默认值

`dodge`：bool, 可选

>使用 `hue` 嵌套时，将其设置为 `True` 将沿着分类轴分离不同色调级别的条带。否则，每个级别的点将相互叠加。

`orient`：“v” &#124; “h”, 可选

> 图的方向（垂直或水平）。这通常是从输入变量的 dtype 推断出来的，但可用于指定“分类”变量何时是数字或何时绘制宽格式数据。

`color`：matplotlib 颜色，可选

> 所有元素的颜色，或渐变调色板的种子。

`palette`：调色板名称，列表或字典，可选

> 用于色调变量的不同级别的颜色。应该是 [`color_palette()`](seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette"), 可以解释的东西，或者是将色调级别映射到 matplotlib 颜色的字典。

`size`：float, 可选

> 标记的直径，以磅为单位。（虽然 `plt.scatter` 用于绘制点，但这里的 `size` 参数采用“普通”标记大小而不是大小^ 2，如 `plt.scatter` 。

`edgecolor`：matplotlib 颜色，“灰色”是特殊的，可选的

>每个点周围线条的颜色。如果传递`"灰色"`，则亮度由用于点体的调色板决定。

`linewidth`：float, 可选

> 构图元素的灰线宽度。

`ax`：matplotlib 轴，可选

> 返回 Axes 对象，并在其上绘制绘图。


返回值：`ax`：matplotlib 轴

> 返回 Axes 对象，并在其上绘制绘图。



也可参看

分类散点图，其中点不重叠。可以与其他图一起使用来显示每个观察结果。带有类似 API 的传统盒须图。箱形图和核密度估计的组合。

例子

绘制单个水平条形图：

```python
>>> import seaborn as sns
>>> sns.set(style="whitegrid")
>>> tips = sns.load_dataset("tips")
>>> ax = sns.stripplot(x=tips["total_bill"])
```

![http://seaborn.pydata.org/_images/seaborn-stripplot-1.png](assets/23f314e72f6e5a8952dc9e9ad7e91951.jpg)


通过分类变量对条形图进行分组：

```python
>>> ax = sns.stripplot(x="day", y="total_bill", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-stripplot-2.png](assets/e91e31fb6bd4af0c4a7726101c5498d5.jpg)


添加抖动以显示值的分布：

```python
>>> ax = sns.stripplot(x="day", y="total_bill", data=tips, jitter=True)
```

![http://seaborn.pydata.org/_images/seaborn-stripplot-3.png](assets/bb509cea6c5bc79011b7aae3d06f02af.jpg)


使用较少量的抖动：

```python
>>> ax = sns.stripplot(x="day", y="total_bill", data=tips, jitter=0.05)
```

![http://seaborn.pydata.org/_images/seaborn-stripplot-4.png](assets/187d29ea70c79f73b11ccc7bfa3d2dc3.jpg)


画水平条形图：

```python
>>> ax = sns.stripplot(x="total_bill", y="day", data=tips,
...                    jitter=True)
```

![http://seaborn.pydata.org/_images/seaborn-stripplot-5.png](assets/3f587e4b0523d3c3e09d9950a187d53c.jpg)


围绕要点绘制轮廓：

```python
>>> ax = sns.stripplot(x="total_bill", y="day", data=tips,
...                    jitter=True, linewidth=1)
```

![http://seaborn.pydata.org/_images/seaborn-stripplot-6.png](assets/808d1fbb36b6a8028b4e31ec238af4dc.jpg)


将条带嵌套在第二个分类变量中：

```python
>>> ax = sns.stripplot(x="sex", y="total_bill", hue="day",
...                    data=tips, jitter=True)
```

![http://seaborn.pydata.org/_images/seaborn-stripplot-7.png](assets/6667c226e573f81913cd01ab631ca098.jpg)


在主要分类轴上的不同位置绘制 `hue` 变量的每个级别：

```python
>>> ax = sns.stripplot(x="day", y="total_bill", hue="smoker",
...                    data=tips, jitter=True,
...                    palette="Set2", dodge=True)
```

![http://seaborn.pydata.org/_images/seaborn-stripplot-8.png](assets/877dac04cc4c90a1e6b62a3f4c75cb92.jpg)


通过传递显式顺序来控制条带顺序：

```python
>>> ax = sns.stripplot(x="time", y="tip", data=tips,
...                    order=["Dinner", "Lunch"])
```

![http://seaborn.pydata.org/_images/seaborn-stripplot-9.png](assets/582830f6213892477ca7f7fd8aea05e3.jpg)


绘制具有大点和不同美感的条带：

```python
>>> ax =  sns.stripplot("day", "total_bill", "smoker", data=tips,
...                    palette="Set2", size=20, marker="D",
...                    edgecolor="gray", alpha=.25)
```

![http://seaborn.pydata.org/_images/seaborn-stripplot-10.png](assets/8d98047d8f8f2021d6ee3b3a679f0890.jpg)


在箱形图上绘制观察条带：

```python
>>> ax = sns.boxplot(x="tip", y="day", data=tips, whis=np.inf)
>>> ax = sns.stripplot(x="tip", y="day", data=tips,
...                    jitter=True, color=".3")
```

![http://seaborn.pydata.org/_images/seaborn-stripplot-11.png](assets/4affd3f4e9c07c5d6803b935d775b36f.jpg)


在小提琴情节的顶部绘制观察条带：

```python
>>> ax = sns.violinplot(x="day", y="total_bill", data=tips,
...                     inner=None, color=".8")
>>> ax = sns.stripplot(x="day", y="total_bill", data=tips, jitter=True)
```

![http://seaborn.pydata.org/_images/seaborn-stripplot-12.png](assets/a68cecbc7d2dbcff50e7244cafe440ec.jpg)


使用 [`catplot()`](seaborn.catplot.html#seaborn.catplot "seaborn.catplot") 组合[`stripplot()`](#seaborn.stripplot "seaborn.stripplot")和[`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")。这允许在其他分类变量中进行分组。使用[`catplot()`](seaborn.catplot.html#seaborn.catplot "seaborn.catplot")比直接使用[`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")更安全，因为它确保了跨方面的变量顺序的同步

```python
>>> g = sns.catplot(x="sex", y="total_bill",
...                 hue="smoker", col="time",
...                 data=tips, kind="strip",
...                 jitter=True,
...                 height=4, aspect=.7);
```

![http://seaborn.pydata.org/_images/seaborn-stripplot-13.png](assets/1b02f55ed166cfeb0628691d84bc1958.jpg)


## seaborn.swarmplot

```python
seaborn.swarmplot(x=None, y=None, hue=None, data=None, order=None, hue_order=None, dodge=False, orient=None, color=None, palette=None, size=5, edgecolor='gray', linewidth=0, ax=None, **kwargs)
```

绘制具有非重叠点的分类散点图。

此功能类似于 [`stripplot()`](seaborn.stripplot.html#seaborn.stripplot "seaborn.stripplot"),，但调整点（仅沿分类轴），以便它们不重叠。 这样可以更好地表示值的分布，但不能很好地扩展到大量观察值。这种情节有时被称为“诅咒”

一个群体图可以单独绘制，但如果你想要显示所有观察结果以及底层分布的一些表示，它也是一个盒子或小提琴图的良好补充。

正确排列点需要在数据和点坐标之间进行精确转换。这意味着必须在绘制绘图之前设置非默认轴限制。

输入数据可以以多种格式传递，包括：

*   表示为列表，numpy arrays 或 pandas Series objects 直接传递给`x`，`y`和/或`hue`参数。
*   “长格式” DataFrame, `x`，`y`和`hue`变量将决定数据的绘制方式
*   “宽格式”DataFrame，用于绘制每个数字列。
*   一个数组或向量列表。

在大多数情况下，可以使用 numpy 或 Python 对象，但最好使用 pandas 对象，因为关联的名称将用于注释轴。此外，您可以使用分类类型来分组变量来控制绘图元素的顺序。

此函数始终将其中一个变量视为分类，并在相关轴上的序数位置（0,1，... n）处绘制数据，即使数据具有数字或日期类型也是如此

有关更多信息，请参阅[教程](http://seaborn.pydata.org/tutorial/categorical.html#categorical-tutorial)。

参数：`x, y, hue`：`数据`或矢量数据中的变量名称，可选

> 用于绘制长格式数据的输入。查看解释示例。

`data`：DataFrame, array, or 或数组列表, 可选

> 用于绘图的数据集。 如果 `x` 和 `y` 是不存在的, 会被解释成 wide-form. 否则会被解释成 long-form.

`order, hue_order`：字符串列表，可选

> 命令绘制分类级别，否则从数据对象推断级别。

`dodge`：布尔，可选

> 使用`hue`嵌套时，将其设置为`True`将沿着分类轴分离不同色调级别的条带。 否则，每个级别的点将绘制在一个群中。

`orient`：“v” &#124; “h”, 可选

> 图的方向（垂直或水平）。这通常是从输入变量的 dtype 推断出来的，但可用于指定“分类”变量何时是数字或何时绘制宽格式数据。

`color`：matplotlib color, 可选

> 所有元素的颜色，或渐变调色板的种子。

`palette`：调色板名称, list, or dict, 可选

> 用于`hue`变量的不同级别的颜色。应该是[`color_palette()`](seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette"),可以解释的东西，或者是将色调级别映射到 matplotlib 颜色的字典。

`size`：float, 可选

> 标记的直径，以点为单位。 (尽管`plt.scatter`用于绘制点，但此处的`size`参数采用“普通”标记大小而不是大小^ 2，如`plt.scatter`。

`edgecolor`：matplotlib color, “灰色”是特殊的，可选

> 每个点周围线条的颜色。如果传递`"gray"`，则亮度由用于点体的调色板决定。

`linewidth`：float, 可选

> 构图元素的灰线宽度。

`ax`：matplotlib Axes, 可选

> Axes 对象将绘图绘制到，否则使用当前轴。


返回值：`ax`：matplotlib Axes

> 返回 Axes 对象，并在其上绘制绘图。

参看

带有类似 API 的传统盒须图。框图和核密度估计的组合。散点图，其中一个变量是分类的。可以与其他图一起使用以显示每个观察结果。使用类组合分类图：<cite>FacetGrid</cite>。

例

绘制单个水平群图：

```python
>>> import seaborn as sns
>>> sns.set(style="whitegrid")
>>> tips = sns.load_dataset("tips")
>>> ax = sns.swarmplot(x=tips["total_bill"])
```

![http://seaborn.pydata.org/_images/seaborn-swarmplot-1.png](assets/900c725fafc0f5e475a98f52f4ed7d04.jpg)


通过分类变量对群组进行分组：

```python
>>> ax = sns.swarmplot(x="day", y="total_bill", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-swarmplot-2.png](assets/414037bdbfc9b79cf5f12a30645f7301.jpg)


绘制水平群：

```python
>>> ax = sns.swarmplot(x="total_bill", y="day", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-swarmplot-3.png](assets/b03c6dbcd2d2f71c2e5eafa99b46d96b.jpg)


使用第二个分类变量为点着色：

```python
>>> ax = sns.swarmplot(x="day", y="total_bill", hue="sex", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-swarmplot-4.png](assets/12d5e5950bf28b7027b28766bc41989f.jpg)


沿着分类轴拆分 `hue` 变量的每个级别：

```python
>>> ax = sns.swarmplot(x="day", y="total_bill", hue="smoker",
...                    data=tips, palette="Set2", dodge=True)
```

![http://seaborn.pydata.org/_images/seaborn-swarmplot-5.png](assets/6f9585fcbe42e72521292b80b0fdc97a.jpg)


通过传递显式顺序来控制 swarm 顺序：

```python
>>> ax = sns.swarmplot(x="time", y="tip", data=tips,
...                    order=["Dinner", "Lunch"])
```

![http://seaborn.pydata.org/_images/seaborn-swarmplot-6.png](assets/73bbf8c6208a6e1c0dda89091dd509a4.jpg)


绘制使用更大的点

```python
>>> ax = sns.swarmplot(x="time", y="tip", data=tips, size=6)
```

![http://seaborn.pydata.org/_images/seaborn-swarmplot-7.png](assets/34aa97c61dd6f6176fa2256880526439.jpg)


在箱形图上绘制大量观察结果：

```python
>>> ax = sns.boxplot(x="tip", y="day", data=tips, whis=np.inf)
>>> ax = sns.swarmplot(x="tip", y="day", data=tips, color=".2")
```

![http://seaborn.pydata.org/_images/seaborn-swarmplot-8.png](assets/d992e6312a3ed98025ad0913dbc46228.jpg)


在小提琴图的顶部画出大量的观察结果：

```python
>>> ax = sns.violinplot(x="day", y="total_bill", data=tips, inner=None)
>>> ax = sns.swarmplot(x="day", y="total_bill", data=tips,
...                    color="white", edgecolor="gray")
```

![http://seaborn.pydata.org/_images/seaborn-swarmplot-9.png](assets/735aa7eaadb9afb7a47a2d079b28a10b.jpg)


使用[`catplot()`](seaborn.catplot.html#seaborn.catplot "seaborn.catplot") 去组合 [`swarmplot()`](#seaborn.swarmplot "seaborn.swarmplot") 和 [`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid"). 这允许在其他分类变量中进行分组。 使用 [`catplot()`](seaborn.catplot.html#seaborn.catplot "seaborn.catplot") 比直接使用 [`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid") 更安全,因为它确保了跨 facet 的变量顺序的同步

```python
>>> g = sns.catplot(x="sex", y="total_bill",
...                 hue="smoker", col="time",
...                 data=tips, kind="swarm",
...                 height=4, aspect=.7);
```

![http://seaborn.pydata.org/_images/seaborn-swarmplot-10.png](assets/7c1bc4a2871b9e0dbe2c23ed05fcae1b.jpg)


## seaborn.boxplot

```python
seaborn.boxplot(x=None, y=None, hue=None, data=None, order=None, hue_order=None, orient=None, color=None, palette=None, saturation=0.75, width=0.8, dodge=True, fliersize=5, linewidth=None, whis=1.5, notch=False, ax=None, **kwargs)
```

seaborn.boxplot 接口的作用是绘制箱形图以展现与类别相关的数据分布状况。

箱形图（或盒须图）以一种利于变量之间比较或不同分类变量层次之间比较的方式来展示定量数据的分布。图中矩形框显示数据集的上下四分位数，而矩形框中延伸出的线段（触须）则用于显示其余数据的分布位置，剩下超过上下四分位间距的数据点则被视为“异常值”。

输入数据可以通过多种格式传入，包括：

*   格式为列表，numpy 数组或 pandas Series 对象的数据向量可以直接传递给`x`，`y`和`hue`参数。
*   对于长格式的 DataFrame，`x`，`y`，和`hue`参数会决定如何绘制数据。
*   对于宽格式的 DataFrame，每一列数值列都会被绘制。
*   一个数组或向量的列表。

在大多数情况下，可以使用 numpy 或 Python 对象，但更推荐使用 pandas 对象，因为与数据关联的列名/行名可以用于标注横轴/纵轴的名称。此外，您可以使用分类类型对变量进行分组以控制绘图元素的顺序。

此函数始终将其中一个变量视为分类，并在相关轴上的序数位置(0,1，... n)处绘制数据，即使数据属于数值类型或日期类型也是如此。

更多信息请参阅 [教程](http://seaborn.pydata.org/tutorial/categorical.html#categorical-tutorial)。

参数：`x, y, hue`：`数据`或向量数据中的变量名称，可选

> 用于绘制长格式数据的输入。查看样例以进一步理解。

`data`：DataFrame，数组，数组列表，可选

> 用于绘图的数据集。如果`x`和`y`都缺失，那么数据将被视为宽格式。否则数据被视为长格式。

`order, hue_order`：字符串列表，可选

> 控制分类变量（对应的条形图）的绘制顺序，若缺失则从数据中推断分类变量的顺序。

`orient`：“v” &#124; “h”，可选

> 控制绘图的方向（垂直或水平）。这通常是从输入变量的 dtype 推断出来的，但是当“分类”变量为数值型或绘制宽格式数据时可用于指定绘图的方向。

`color`：matplotlib 颜色，可选

> 所有元素的颜色，或渐变调色板的种子颜色。

`palette`：调色板名称，列表或字典，可选

> 用于`hue`变量的不同级别的颜色。可以从 [`color_palette()`](seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette") 得到一些解释，或者将色调级别映射到 matplotlib 颜色的字典。

`saturation`：float，可选

> 控制用于绘制颜色的原始饱和度的比例。通常大幅填充在轻微不饱和的颜色下看起来更好，如果您希望绘图颜色与输入颜色规格完美匹配可将其设置为`1`。

`width`：float，可选

> 不使用色调嵌套时完整元素的宽度，或主要分组变量一个级别的所有元素的宽度。

`dodge`：bool，可选

> 使用色调嵌套时，元素是否应沿分类轴移动。

`fliersize`：float，可选

> 用于表示异常值观察的标记的大小。

`linewidth`：float，可选

> 构图元素的灰线宽度。

`whis`：float，可选

> 控制在超过高低四分位数时 IQR 的比例，因此需要延长绘制的触须线段。超出此范围的点将被识别为异常值。

`notch`：boolean，可选

> 是否使矩形框“凹陷”以指示中位数的置信区间。还有其他几个参数可以控制凹槽的绘制方式；参见 `plt.boxplot` 以查看关于此问题的更多帮助信息。

`ax`：matplotlib 轴，可选

> 绘图时使用的 Axes 轴对象，否则使用当前 Axes 轴对象。

`kwargs`：键，值映射

> 其他在绘图时传给 `plt.boxplot` 的参数。


返回值：`ax`：matplotlib 轴

> 返回 Axes 对轴象，并在其上绘制绘图。



亦可参见

boxplot 和核密度估计的结合。当一个变量是分类变量的散点图。可以与其他图表结合使用以展示各自的观测结果。分类散点图的特点是其中数据点互不重叠。可以与其他图表结合使用以展示各自的观测结果。

示例

绘制一个单独的横向箱型图：

```python
>>> import seaborn as sns
>>> sns.set(style="whitegrid")
>>> tips = sns.load_dataset("tips")
>>> ax = sns.boxplot(x=tips["total_bill"])
```

![http://seaborn.pydata.org/_images/seaborn-boxplot-1.png](assets/a60c5e81f1e6cf2aeb7d9790851ca60d.jpg)


根据分类变量分组绘制一个纵向的箱型图：

```python
>>> ax = sns.boxplot(x="day", y="total_bill", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-boxplot-2.png](assets/26d08ab02c44cb0a819065f27a1b7f84.jpg)


根据 2 个分类变量嵌套分组绘制一个箱型图：

```python
>>> ax = sns.boxplot(x="day", y="total_bill", hue="smoker",
...                  data=tips, palette="Set3")
```

![http://seaborn.pydata.org/_images/seaborn-boxplot-3.png](assets/d642aa20a439141528ed8a872a4822a4.jpg)


当一些数据为空时根据嵌套分组绘制一个箱型图：

```python
>>> ax = sns.boxplot(x="day", y="total_bill", hue="time",
...                  data=tips, linewidth=2.5)
```

![http://seaborn.pydata.org/_images/seaborn-boxplot-4.png](assets/97751f7296df86f26e8eb70275541082.jpg)


通过显式传入参数指定顺序控制箱型图的显示顺序：

```python
>>> ax = sns.boxplot(x="time", y="tip", data=tips,
...                  order=["Dinner", "Lunch"])
```

![http://seaborn.pydata.org/_images/seaborn-boxplot-5.png](assets/8ac363c4f6fd1e8ef5a4dd62d236ce9d.jpg)


针对 DataFrame 里每一个数值型变量绘制箱型图：

```python
>>> iris = sns.load_dataset("iris")
>>> ax = sns.boxplot(data=iris, orient="h", palette="Set2")
```

![http://seaborn.pydata.org/_images/seaborn-boxplot-6.png](assets/7592f72da714721ad272271754ee8454.jpg)


使用 `hue` 参数无需改变箱型图的位置或宽度：

```python
>>> tips["weekend"] = tips["day"].isin(["Sat", "Sun"])
>>> ax = sns.boxplot(x="day", y="total_bill", hue="weekend",
...                  data=tips, dodge=False)
```

![http://seaborn.pydata.org/_images/seaborn-boxplot-7.png](assets/583a43fccfad9c80a852b79bbd864c3b.jpg)


使用 [`swarmplot()`](seaborn.swarmplot.html#seaborn.swarmplot "seaborn.swarmplot") 展示箱型图顶部的数据点：

```python
>>> ax = sns.boxplot(x="day", y="total_bill", data=tips)
>>> ax = sns.swarmplot(x="day", y="total_bill", data=tips, color=".25")
```

![http://seaborn.pydata.org/_images/seaborn-boxplot-8.png](assets/627c14ca4d7cabe1d122fd0b9d2586bd.jpg)


把 [`catplot()`](seaborn.catplot.html#seaborn.catplot "seaborn.catplot") 与 [`pointplot()`](seaborn.pointplot.html#seaborn.pointplot "seaborn.pointplot") 以及 [`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid") 结合起来使用。这允许您通过额外的分类变量进行分组。使用 [`catplot()`](seaborn.catplot.html#seaborn.catplot "seaborn.catplot") 比直接使用 [`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid") 更为安全，因为它保证了不同切面上变量同步的顺序：

```python
>>> g = sns.catplot(x="sex", y="total_bill",
...                 hue="smoker", col="time",
...                 data=tips, kind="box",
...                 height=4, aspect=.7);
```

![http://seaborn.pydata.org/_images/seaborn-boxplot-9.png](assets/e992687c669c1747d5f18ff4d5e6457d.jpg)


## seaborn.violinplot

```python
seaborn.violinplot(x=None, y=None, hue=None, data=None, order=None, hue_order=None, bw='scott', cut=2, scale='area', scale_hue=True, gridsize=100, width=0.8, inner='box', split=False, dodge=True, orient=None, linewidth=None, color=None, palette=None, saturation=0.75, ax=None, **kwargs)
```

结合箱型图与核密度估计绘图。

小提琴图的功能与箱型图类似。 它显示了一个（或多个）分类变量多个属性上的定量数据的分布，从而可以比较这些分布。与箱形图不同，其中所有绘图单元都与实际数据点对应，小提琴图描述了基础数据分布的核密度估计。

小提琴图可以是一种单次显示多个数据分布的有效且有吸引力的方式，但请记住，估计过程受样本大小的影响，相对较小样本的小提琴可能看起来非常平滑，这种平滑具有误导性。

输入数据可以通过多种格式传入，包括：

*   格式为列表，numpy 数组或 pandas Series 对象的数据向量可以直接传递给`x`，`y`和`hue`参数。
*   对于长格式的 DataFrame，`x`，`y`，和`hue`参数会决定如何绘制数据。
*   对于宽格式的 DataFrame，每一列数值列都会被绘制。
*   一个数组或向量的列表。

在大多数情况下，可以使用 numpy 或 Python 对象，但更推荐使用 pandas 对象，因为与数据关联的列名/行名可以用于标注横轴/纵轴的名称。此外，您可以使用分类类型对变量进行分组以控制绘图元素的顺序。

此函数始终将其中一个变量视为分类，并在相关轴上的序数位置(0,1，... n)处绘制数据，即使数据属于数值类型或日期类型也是如此。

更多信息请参阅 [tutorial](http://seaborn.pydata.org/tutorial/categorical.html#categorical-tutorial)。

参数：`x, y, hue`：`数据`或向量数据中的变量名称，可选

> 用于绘制长格式数据的输入。查看样例以进一步理解。

`data`：DataFrame，数组，数组列表，可选

> 用于绘图的数据集。如果`x`和`y`都缺失，那么数据将被视为宽格式。否则数据被视为长格式。

`order, hue_order`：字符串列表，可选

> 控制分类变量（对应的条形图）的绘制顺序，若缺失则从数据中推断分类变量的顺序。

`bw`：{‘scott’, ‘silverman’, float}，可选

> 内置变量值或浮点数的比例因子都用来计算核密度的带宽。实际的核大小由比例因子乘以每个分箱内数据的标准差确定。

`cut`：float，可选

> 以带宽大小为单位的距离，以控制小提琴图外壳延伸超过内部极端数据点的密度。设置为 0 以将小提琴图范围限制在观察数据的范围内。（例如，在 `ggplot` 中具有与 `trim=True` 相同的效果）

`scale`：{“area”, “count”, “width”}，可选

> 该方法用于缩放每张小提琴图的宽度。若为 `area` ，每张小提琴图具有相同的面积。若为 `count` ，小提琴的宽度会根据分箱中观察点的数量进行缩放。若为 `width` ，每张小提琴图具有相同的宽度。

`scale_hue`：bool，可选

> 当使用色调参数 `hue` 变量绘制嵌套小提琴图时，该参数决定缩放比例是在主要分组变量（`scale_hue=True`）的每个级别内还是在图上的所有小提琴图（`scale_hue=False`）内计算出来的。

`gridsize`：int，可选

> 用于计算核密度估计的离散网格中的数据点数目。

`width`：float，可选

> 不使用色调嵌套时的完整元素的宽度，或主要分组变量的一个级别的所有元素的宽度。

`inner`：{“box”, “quartile”, “point”, “stick”, None}，可选

> 控制小提琴图内部数据点的表示。若为`box`，则绘制一个微型箱型图。若为`quartiles`，则显示四分位数线。若为`point`或`stick`，则显示具体数据点或数据线。使用`None`则绘制不加修饰的小提琴图。

`split`：bool，可选

> 当使用带有两种颜色的变量时，将`split`设置为 True 则会为每种颜色绘制对应半边小提琴。从而可以更容易直接的比较分布。

`dodge`：bool，可选

> 使用色调嵌套时，元素是否应沿分类轴移动。

`orient`：“v” &#124; “h”，可选

> 控制绘图的方向（垂直或水平）。这通常是从输入变量的 dtype 推断出来的，但是当“分类”变量为数值型或绘制宽格式数据时可用于指定绘图的方向。

`linewidth`：float，可选

> 构图元素的灰线宽度。

`color`：matplotlib 颜色，可选

> 所有元素的颜色，或渐变调色板的种子颜色。

`palette`：调色板名称，列表或字典，可选

> 用于`hue`变量的不同级别的颜色。可以从 [`color_palette()`](seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette") 得到一些解释，或者将色调级别映射到 matplotlib 颜色的字典。

`saturation`：float，可选

> 控制用于绘制颜色的原始饱和度的比例。通常大幅填充在轻微不饱和的颜色下看起来更好，如果您希望绘图颜色与输入颜色规格完美匹配可将其设置为`1`。

`ax`：matplotlib 轴，可选

> 绘图时使用的 Axes 轴对象，否则使用当前 Axes 轴对象。


返回值：`ax`：matplotlib 轴

> 返回 Axes 对轴象，并在其上绘制绘图。



亦可参见

一个传统的箱型图具有类似的 API。当一个变量是分类变量的散点图。可以与其他图表结合使用以展示各自的观测结果。分类散点图的特点是其中数据点互不重叠。可以与其他图表结合使用以展示各自的观测结果。

示例

绘制一个单独的横向小提琴图：

```python
>>> import seaborn as sns
>>> sns.set(style="whitegrid")
>>> tips = sns.load_dataset("tips")
>>> ax = sns.violinplot(x=tips["total_bill"])
```

![http://seaborn.pydata.org/_images/seaborn-violinplot-1.png](assets/997c424ce3337d1c47dea839b9dbdcae.jpg)


根据分类变量分组绘制一个纵向的小提琴图：

```python
>>> ax = sns.violinplot(x="day", y="total_bill", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-violinplot-2.png](assets/35c303fe21899eb631a454ab2d09e1bf.jpg)


根据 2 个分类变量嵌套分组绘制一个小提琴图：

```python
>>> ax = sns.violinplot(x="day", y="total_bill", hue="smoker",
...                     data=tips, palette="muted")
```

![http://seaborn.pydata.org/_images/seaborn-violinplot-3.png](assets/518916184779b02556d17a4d7366ba4f.jpg)


绘制分割的小提琴图以比较不同的色调变量：

```python
>>> ax = sns.violinplot(x="day", y="total_bill", hue="smoker",
...                     data=tips, palette="muted", split=True)
```

![http://seaborn.pydata.org/_images/seaborn-violinplot-4.png](assets/556be3cec3d1462768f0cc39618394ee.jpg)


通过显式传入参数指定顺序控制小提琴图的显示顺序：

```python
>>> ax = sns.violinplot(x="time", y="tip", data=tips,
...                     order=["Dinner", "Lunch"])
```

![http://seaborn.pydata.org/_images/seaborn-violinplot-5.png](assets/0a27825dabd4d46b78291be90e8f255e.jpg)


将小提琴宽度缩放为每个分箱中观察到的数据点数目：

```python
>>> ax = sns.violinplot(x="day", y="total_bill", hue="sex",
...                     data=tips, palette="Set2", split=True,
...                     scale="count")
```

![http://seaborn.pydata.org/_images/seaborn-violinplot-6.png](assets/827ec31007a6e721317dd1ef46a8602f.jpg)


将四分位数绘制为水平线而不是迷你箱型图：

```python
>>> ax = sns.violinplot(x="day", y="total_bill", hue="sex",
...                     data=tips, palette="Set2", split=True,
...                     scale="count", inner="quartile")
```

![http://seaborn.pydata.org/_images/seaborn-violinplot-7.png](assets/09cb7121a5be805931cc8ae1c99d3cf9.jpg)


用小提琴图内部的横线显示每个观察到的数据点：

```python
>>> ax = sns.violinplot(x="day", y="total_bill", hue="sex",
...                     data=tips, palette="Set2", split=True,
...                     scale="count", inner="stick")
```

![http://seaborn.pydata.org/_images/seaborn-violinplot-8.png](assets/5c7220bdfc81c3823862c2988af57776.jpg)


根据所有分箱的数据点数目对密度进行缩放：

```python
>>> ax = sns.violinplot(x="day", y="total_bill", hue="sex",
...                     data=tips, palette="Set2", split=True,
...                     scale="count", inner="stick", scale_hue=False)
```

![http://seaborn.pydata.org/_images/seaborn-violinplot-9.png](assets/b8b693d813ee5778cb52a3f3f634fc11.jpg)


使用窄带宽来减少平滑量：

```python
>>> ax = sns.violinplot(x="day", y="total_bill", hue="sex",
...                     data=tips, palette="Set2", split=True,
...                     scale="count", inner="stick",
...                     scale_hue=False, bw=.2)
```

![http://seaborn.pydata.org/_images/seaborn-violinplot-10.png](assets/a8ea9b9779973bf3274cc3f154bb6029.jpg)


绘制横向小提琴图：

```python
>>> planets = sns.load_dataset("planets")
>>> ax = sns.violinplot(x="orbital_period", y="method",
...                     data=planets[planets.orbital_period < 1000],
...                     scale="width", palette="Set3")
```

![http://seaborn.pydata.org/_images/seaborn-violinplot-11.png](assets/7a72774eb92deca2a76e6428021a5bd6.jpg)


不要让密度超出数据中的极端数值：

```python
>>> ax = sns.violinplot(x="orbital_period", y="method",
...                     data=planets[planets.orbital_period < 1000],
...                     cut=0, scale="width", palette="Set3")
```

![http://seaborn.pydata.org/_images/seaborn-violinplot-12.png](assets/eb118f7308dc5e81aa3ed5b5524a1182.jpg)


使用 `hue` 而不改变小提琴图的位置或宽度：

```python
>>> tips["weekend"] = tips["day"].isin(["Sat", "Sun"])
>>> ax = sns.violinplot(x="day", y="total_bill", hue="weekend",
...                     data=tips, dodge=False)
```

![http://seaborn.pydata.org/_images/seaborn-violinplot-13.png](assets/fad2cab7c30663b1f48e1015ad10f310.jpg)


把 [`catplot()`](seaborn.catplot.html#seaborn.catplot "seaborn.catplot") 与 [`violinplot()`](#seaborn.violinplot "seaborn.violinplot") 以及 [`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid") 结合起来使用。这允许您通过额外的分类变量进行分组。使用 [`catplot()`](seaborn.catplot.html#seaborn.catplot "seaborn.catplot") 比直接使用 [`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid") 更为安全，因为它保证了不同切面上变量同步的顺序：

```python
>>> g = sns.catplot(x="sex", y="total_bill",
...                 hue="smoker", col="time",
...                 data=tips, kind="violin", split=True,
...                 height=4, aspect=.7);
```

![http://seaborn.pydata.org/_images/seaborn-violinplot-14.png](assets/49227838b078bcd197f52cd3a7b01865.jpg)


## seaborn.boxenplot

```python
seaborn.boxenplot(x=None, y=None, hue=None, data=None, order=None, hue_order=None, orient=None, color=None, palette=None, saturation=0.75, width=0.8, dodge=True, k_depth='proportion', linewidth=None, scale='exponential', outlier_prop=None, ax=None, **kwargs)
```

为更大的数据集绘制增强的箱型图。

这种风格的绘图最初被命名为“信值图”，因为它显示了大量被定义为“置信区间”的分位数。它类似于绘制分布的非参数表示的箱形图，其中所有特征对应于实际观察的数值点。通过绘制更多分位数，它提供了有关分布形状的更多信息，特别是尾部数据的分布。欲了解更详细的解释，您可以阅读介绍该绘图的论文：

[https://vita.had.co.nz/papers/letter-value-plot.html](https://vita.had.co.nz/papers/letter-value-plot.html)

输入数据可以通过多种格式传入，包括：

*   格式为列表，numpy 数组或 pandas Series 对象的数据向量可以直接传递给`x`，`y`和`hue`参数。
*   对于长格式的 DataFrame，`x`，`y`，和`hue`参数会决定如何绘制数据。
*   对于宽格式的 DataFrame，每一列数值列都会被绘制。
*   一个数组或向量的列表。

在大多数情况下，可以使用 numpy 或 Python 对象，但更推荐使用 pandas 对象，因为与数据关联的列名/行名可以用于标注横轴/纵轴的名称。此外，您可以使用分类类型对变量进行分组以控制绘图元素的顺序。

此函数始终将其中一个变量视为分类，并在相关轴上的序数位置(0,1，... n)处绘制数据，即使数据属于数值类型或日期类型也是如此。

更多信息请参阅 [教程](http://seaborn.pydata.org/tutorial/categorical.html#categorical-tutorial)。

参数：`x, y, hue`：`data`或向量数据中的变量名称，可选

> 用于绘制长格式数据的输入。查看样例以进一步理解。

`data`：DataFrame，数组，数组列表，可选

> 用于绘图的数据集。如果`x`和`y`都缺失，那么数据将被视为宽格式。否则数据被视为长格式。

`order, hue_order`：字符串列表，可选

> 控制分类变量（对应的条形图）的绘制顺序，若缺失则从数据中推断分类变量的顺序。

`orient`：“v” &#124; “h”，可选

> 控制绘图的方向（垂直或水平）。这通常是从输入变量的 dtype 推断出来的，但是当“分类”变量为数值型或绘制宽格式数据时可用于指定绘图的方向。

`color`：matplotlib 颜色，可选

> 所有元素的颜色，或渐变调色板的种子颜色。

`palette`：调色板名称，列表或字典，可选

> 用于`hue`变量的不同级别的颜色。可以从 [`color_palette()`](seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette") 得到一些解释，或者将色调级别映射到 matplotlib 颜色的字典。

`saturation`：float，可选

> 控制用于绘制颜色的原始饱和度的比例。通常大幅填充在轻微不饱和的颜色下看起来更好，如果您希望绘图颜色与输入颜色规格完美匹配可将其设置为`1`。

`width`：float，可选

> 不使用色调嵌套时完整元素的宽度，或主要分组变量一个级别的所有元素的宽度。

`dodge`：bool，可选

> 使用色调嵌套时，元素是否应沿分类轴移动。

`k_depth`：“proportion” &#124; “tukey” &#124; “trustworthy”，可选

> 通过增大百分比的粒度控制绘制的盒形图数目。所有方法都在 Wickham 的论文中有详细描述。每个参数代表利用不同的统计特性对异常值的数量做出不同的假设。

`linewidth`：float，可选

> 构图元素的灰线宽度。

`scale`：“linear” &#124; “exponential” &#124; “area”

> 用于控制增强箱型图宽度的方法。所有参数都会给显示效果造成影响。 “linear” 通过恒定的线性因子减小宽度，“exponential” 使用未覆盖的数据的比例调整宽度， “area” 与所覆盖的数据的百分比成比例。

`outlier_prop`：float，可选

> 被认为是异常值的数据比例。与 `k_depth` 结合使用以确定要绘制的百分位数。默认值为 0.007 作为异常值的比例。该参数取值应在[0,1]范围内。

`ax`：matplotlib 轴，可选

> 绘图时使用的 Axes 轴对象，否则使用当前 Axes 轴对象。

`kwargs`：键，值映射

> 其他在绘制时传递给`plt.plot`和`plt.scatter`参数。


返回值：`ax`：matplotlib 轴

> 返回 Axes 对轴象，并在其上绘制绘图。



亦可参见

boxplot 和核密度估计的结合。一个传统的箱型图具有类似的 API。

示例

绘制一个独立的横向增强箱型图：

```python
>>> import seaborn as sns
>>> sns.set(style="whitegrid")
>>> tips = sns.load_dataset("tips")
>>> ax = sns.boxenplot(x=tips["total_bill"])
```

![http://seaborn.pydata.org/_images/seaborn-boxenplot-1.png](assets/ea7362d005109093ddfe7d7a0039a13e.jpg)


根据分类变量分组绘制一个纵向的增强箱型图：

```python
>>> ax = sns.boxenplot(x="day", y="total_bill", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-boxenplot-2.png](assets/31c79f0cf22d453e10799da960e3e801.jpg)


根据 2 个分类变量嵌套分组绘制一个增强箱型图：

```python
>>> ax = sns.boxenplot(x="day", y="total_bill", hue="smoker",
...                    data=tips, palette="Set3")
```

![http://seaborn.pydata.org/_images/seaborn-boxenplot-3.png](assets/1db91ed8446afc825fa5bba21f1ef278.jpg)


当一些数据为空时根据嵌套分组绘制一个增强箱型图：

```python
>>> ax = sns.boxenplot(x="day", y="total_bill", hue="time",
...                    data=tips, linewidth=2.5)
```

![http://seaborn.pydata.org/_images/seaborn-boxenplot-4.png](assets/2e57af8b26439c244046ff7846601335.jpg)


通过显式传入参数指定顺序控制箱型图的显示顺序：

```python
>>> ax = sns.boxenplot(x="time", y="tip", data=tips,
...                    order=["Dinner", "Lunch"])
```

![http://seaborn.pydata.org/_images/seaborn-boxenplot-5.png](assets/e8fa81696195ce058546e429317075bc.jpg)


针对 DataFrame 里每一个数值型变量绘制增强箱型图：

```python
>>> iris = sns.load_dataset("iris")
>>> ax = sns.boxenplot(data=iris, orient="h", palette="Set2")
```

![http://seaborn.pydata.org/_images/seaborn-boxenplot-6.png](assets/a9e939280daed8ec0712c8e6e6be78fb.jpg)


使用 [`stripplot()`](seaborn.stripplot.html#seaborn.stripplot "seaborn.stripplot") 显示箱型图顶部的数据点：

```python
>>> ax = sns.boxenplot(x="day", y="total_bill", data=tips)
>>> ax = sns.stripplot(x="day", y="total_bill", data=tips,
...                    size=4, jitter=True, color="gray")
```

![http://seaborn.pydata.org/_images/seaborn-boxenplot-7.png](assets/fb3de8051b91bb0be2143717f96c0a7c.jpg)


将 [`catplot()`](seaborn.catplot.html#seaborn.catplot "seaborn.catplot") to combine [`boxenplot()`](#seaborn.boxenplot "seaborn.boxenplot") 以及 [`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid") 结合起来使用。这允许您通过额外的分类变量进行分组。使用 [`catplot()`](seaborn.catplot.html#seaborn.catplot "seaborn.catplot") 比直接使用 [`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid") 更为安全，因为它保证了不同切面上变量同步的顺序：

```python
>>> g = sns.catplot(x="sex", y="total_bill",
...                 hue="smoker", col="time",
...                 data=tips, kind="boxen",
...                 height=4, aspect=.7);
```

![http://seaborn.pydata.org/_images/seaborn-boxenplot-8.png](assets/ef97d95b2084af9b8636c2514545289d.jpg)


## seaborn.pointplot

```python
seaborn.pointplot(x=None, y=None, hue=None, data=None, order=None, hue_order=None, estimator=<function mean>, ci=95, n_boot=1000, units=None, markers='o', linestyles='-', dodge=False, join=True, scale=1, orient=None, color=None, palette=None, errwidth=None, capsize=None, ax=None, **kwargs)
```

通过绘制散点连线显示数据点的估计值和置信区间。

点图代表散点图位置的数值变量的中心趋势估计，并使用误差线提供关于该估计的不确定性的一些指示。

点图比条形图在聚焦一个或多个分类变量的不同级别之间的比较时更为有用。点图尤其善于表现交互作用：一个分类变量的层次之间的关系如何在第二个分类变量的层次之间变化。连接来自相同 `色调` 等级的每个点的线允许交互作用通过斜率的差异进行判断，这使得更容易对几组数据点或数据线的高度进行比较。

重要的一点是点图仅显示平均值（或其他估计值），但在许多情况下，显示分类变量的每个级别的值的分布可能会带有更多信息。在这种情况下，其他绘图方法，例如箱型图或小提琴图可能更合适。

输入数据可以通过多种格式传入，包括：

*   格式为列表，numpy 数组或 pandas Series 对象的数据向量可以直接传递给`x`，`y`和`hue`参数。
*   对于长格式的 DataFrame，`x`，`y`，和`hue`参数会决定如何绘制数据。
*   对于宽格式的 DataFrame，每一列数值列都会被绘制。
*   一个数组或向量的列表。

在大多数情况下，可以使用 numpy 或 Python 对象，但更推荐使用 pandas 对象，因为与数据关联的列名/行名可以用于标注横轴/纵轴的名称。此外，您可以使用分类类型对变量进行分组以控制绘图元素的顺序。

此函数始终将其中一个变量视为分类，并在相关轴上的序数位置(0,1，... n)处绘制数据，即使数据属于数值类型或日期类型也是如此。

更多信息请参阅 [教程](http://seaborn.pydata.org/tutorial/categorical.html#categorical-tutorial)。

参数：`x, y, hue`：`data`或向量数据中的变量名称，可选

> 用于绘制长格式数据的输入。查看样例以进一步理解。

`data`：DataFrame，数组，数组列表，可选

> 于绘图的数据集。如果`x`和`y`都缺失，那么数据将被视为宽格式。否则数据被视为长格式。

`order, hue_order`：字符串列表，可选

> 控制绘图的方向（垂直或水平）。这通常是从输入变量的 dtype 推断出来的，但是当“分类”变量为数值型或绘制宽格式数据时可用于指定绘图的方向。

`estimator`：调用函数实现向量 -&gt; 标量的映射，可选

> 在每个分箱内进行估计的统计函数。

`ci`：float 或 “sd” 或 None，可选

> 在估计值附近绘制置信区间的尺寸大小。如果是“sd”，则跳过引导阶段并绘制观察数据点的标准差。如果为 `None`，则不会执行引导过程，并且不会绘制误差块。

`n_boot`：int，可选

> 计算置信区间时使用的引导迭代次数。

`units`：`data` 或 vector data 中变量的名称，可选

> 采样单元的标识符，用于执行多级引导过程（计算置信区间等）并能够处理重复测量的设定。

`markers`：字符串或字符串列表，可选

> 用于每个`hue`色调的级别的标记。

`linestyles`：字符串或字符串列表，可选

> 用于每个`hue`色调的级别的线条风格。

`dodge`：bool 或 float，可选

> 用于沿着分类轴分离`hue`变量的每个级别数据点的数量。

`join`：bool，可选

> 如果为`True`，则在`hue`级别相同的点估计值之间绘制线条。

`scale`：float，可选

> 绘图元素的比例因子。

`orient`：“v” &#124; “h”，可选

> 控制绘图的方向（垂直或水平）。这通常是从输入变量的 dtype 推断出来的，但是当“分类”变量为数值型或绘制宽格式数据时可用于指定绘图的方向。

`color`：matplotlib 颜色，可选

> 所有元素的颜色，或渐变调色板的种子颜色。

`palette`：调色板名称，列表或字典，可选

> 用于`hue`变量的不同级别的颜色。可以从 [`color_palette()`](seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette") 得到一些解释，或者将色调级别映射到 matplotlib 颜色的字典。

`errwidth`：float，可选

> 误差线（和上下限指示线）的厚度。

`capsize`：float，可选

> 误差线“上下限指示线”的宽度。

`ax`：matplotlib 轴，可选

> 绘图时使用的 Axes 轴对象，否则使用当前 Axes 轴对象。


返回值：`ax`：matplotlib 轴

> 返回 Axes 对轴象，并在其上绘制绘图。



亦可参见

使用线条显示数据点的估计值和置信区间。将分类类别的绘图与该类结合使用：<cite>FacetGrid</cite>。

示例

绘制一组按分类变量分组的纵向点图：

```python
>>> import seaborn as sns
>>> sns.set(style="darkgrid")
>>> tips = sns.load_dataset("tips")
>>> ax = sns.pointplot(x="time", y="total_bill", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-pointplot-1.png](assets/f5eb9519edb052868537ca9735f0f8df.jpg)


通过两个嵌套分组的变量绘制一组纵向的点图：

```python
>>> ax = sns.pointplot(x="time", y="total_bill", hue="smoker",
...                    data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-pointplot-2.png](assets/864eda3b3c2fcc6b0bdb53c84c3dafcf.jpg)


沿着分类轴分离不同色调级别的点：

```python
>>> ax = sns.pointplot(x="time", y="total_bill", hue="smoker",
...                    data=tips, dodge=True)
```

![http://seaborn.pydata.org/_images/seaborn-pointplot-3.png](assets/b7f6772294dcf0d9b7035314c114c54b.jpg)


根据色调级别使用不同的标记和线条样式：

```python
>>> ax = sns.pointplot(x="time", y="total_bill", hue="smoker",
...                    data=tips,
...                    markers=["o", "x"],
...                    linestyles=["-", "--"])
```

![http://seaborn.pydata.org/_images/seaborn-pointplot-4.png](assets/a97ae4389fc8bc946eb62e06c173b3e3.jpg)


绘制一组横向的点图：

```python
>>> ax = sns.pointplot(x="tip", y="day", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-pointplot-5.png](assets/4217fbfe6aaba42c4d18a69c5b8c9fc4.jpg)


不要绘制每个点的连接线：

```python
>>> ax = sns.pointplot(x="tip", y="day", data=tips, join=False)
```

![http://seaborn.pydata.org/_images/seaborn-pointplot-6.png](assets/96a33f32e85dcced62c9fc4ff063fe3d.jpg)


对单层图使用不同的颜色：

```python
>>> ax = sns.pointplot("time", y="total_bill", data=tips,
...                    color="#bb3f3f")
```

![http://seaborn.pydata.org/_images/seaborn-pointplot-7.png](assets/1e3348f06e5cd7876d5bc530b04d3d93.jpg)


为数据点使用不同的调色板：

```python
>>> ax = sns.pointplot(x="time", y="total_bill", hue="smoker",
...                    data=tips, palette="Set2")
```

![http://seaborn.pydata.org/_images/seaborn-pointplot-8.png](assets/d4a4eeea79c55b0ae9d3088746b6503a.jpg)


通过显式传入参数指定顺序控制点的显示顺序：

```python
>>> ax = sns.pointplot(x="time", y="tip", data=tips,
...                    order=["Dinner", "Lunch"])
```

![http://seaborn.pydata.org/_images/seaborn-pointplot-9.png](assets/4c08e24283b6829b3d91e3c23de56923.jpg)


用中位数作为集中趋势的估计：

```python
>>> from numpy import median
>>> ax = sns.pointplot(x="day", y="tip", data=tips, estimator=median)
```

![http://seaborn.pydata.org/_images/seaborn-pointplot-10.png](assets/0ec9398faa407996527db66db46c71f2.jpg)


用误差线显示均值的标准误差：

```python
>>> ax = sns.pointplot(x="day", y="tip", data=tips, ci=68)
```

![http://seaborn.pydata.org/_images/seaborn-pointplot-11.png](assets/f9f6dd93a512624527b38dcc26d97e37.jpg)


显示观测值的标准偏差而不是置信区间：

```python
>>> ax = sns.pointplot(x="day", y="tip", data=tips, ci="sd")
```

![http://seaborn.pydata.org/_images/seaborn-pointplot-12.png](assets/f41526e37f8f11614ea339da0e242c51.jpg)


将“上下限指示线”增加到误差线的顶部和底部：

```python
>>> ax = sns.pointplot(x="day", y="tip", data=tips, capsize=.2)
```

![http://seaborn.pydata.org/_images/seaborn-pointplot-13.png](assets/b7df4cf62c681ea39bd145bdb740bc81.jpg)


将 [`catplot()`](seaborn.catplot.html#seaborn.catplot "seaborn.catplot") 与 [`barplot()`](seaborn.barplot.html#seaborn.barplot "seaborn.barplot") 以及 [`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")结合使用。这允许您通过额外的分类变量进行分组。使用 [`catplot()`](seaborn.catplot.html#seaborn.catplot "seaborn.catplot") 比直接使用 [`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid") 更为安全，因为它保证了不同切面上变量同步的顺序：

```python
>>> g = sns.catplot(x="sex", y="total_bill",
...                 hue="smoker", col="time",
...                 data=tips, kind="point",
...                 dodge=True,
...                 height=4, aspect=.7);
```

![http://seaborn.pydata.org/_images/seaborn-pointplot-14.png](assets/2d47b5f96f08a539147e7f0a71d0aa33.jpg)


## seaborn.barplot

```python
seaborn.barplot(x=None, y=None, hue=None, data=None, order=None, hue_order=None, estimator=<function mean>, ci=95, n_boot=1000, units=None, orient=None, color=None, palette=None, saturation=0.75, errcolor='.26', errwidth=None, capsize=None, dodge=True, ax=None, **kwargs)
```

条形图以矩形条的方式展示数据的点估值和置信区间

条形图用每个矩形的高度来表示数值变量的集中趋势的估计值，并提供误差条来显示估计值得不确定度。条形图的纵轴是从零开始的，这对于 0 值是有意义的情况是非常好的。

对于数据集中的 0 值没有实际意义的情况，散点图可以让您专注于一个或多个分类变量之间的差异。

要注意的是，条形图只显示平均值（或者其他的估计值），但是在大部分情况下，展示数值在不同分类变量上的分布会更有用。如果要表示出数据的分布，用箱型图或者小提琴图会更恰当。

输入数据的格式可以不同，包括：

*   以列表，numpy array 或者 pandas 中的 Series object 表示的向量。这些向量可以直接传入 `x`, `y`, 以及 `hue` 参数。
*   长表, x 值，y 值和色相变量决定了数据是如何绘制的。
*   宽表，每个列的数值都会被绘制出来.
*   数组或者列表的向量。

大多数情况下，您可以使用 numpy 的对象或者 python 的对象，但是用 pandas 对象更好，因为相关的列名会被标注在图标上。 另外，为了控制绘图元素 您也可以可以用分类类型来组合不同的变量。

这个函数始终把一个变量当做分类变量，即使这个数据是数值类型或者日期类型，并且按照序数顺序绘制在相关的轴上。

详情请看[教程](http://seaborn.pydata.org/tutorial/categorical.html#categorical-tutorial)。

参数：`x, y, hue`：`data` 中的变量名词或者向量, optional

> 用于绘制 long-form 数据的变量名称.

`data`：DataFrame, 数组, 数组列表, optional

> 用于绘图的数据集。如果数据集有 x 和 y，数据集会被认为是 long-form，否则会被认为是 wide-form。

`order, hue_order`：字符串列表, optional

> 绘制类别变量的顺序，如果没有，则从数据对象中推断绘图顺序。

`estimator`：映射向量 -&gt; 标量, optional

> 统计函数用于估计每个分类纸条中的值。.

`ci`：float or “sd” or None, optional

> 估计值周围的置信区间大小。如果输入的是 “sd”（标准差），会跳过 bootstrapping 的过程，只绘制数据的标准差。 如果输入是的是`None`, 不会执行 botstrapping，而且错误条也不会绘制。

`n_boot`：int, optional

> 计算置信区间需要的 Boostrap 迭代次数。

`units`：name of variable in `data` or vector data, optional

> 采样单元的标识符，用于执行多级 bootstrap 并解释重复测量设计。

`orient`：“v” &#124; “h”, optional

> 绘图的方向（垂直或水平）。这通常是从输入变量的数据类型推断出来的，但是可以用来指定“分类”变量是数字还是宽格式数据。

`color`：matplotlib color, optional

> 作用于所有元素的颜色，或者渐变色的种子。

`palette`：palette name, list, or dict, optional

> 不同级别的 `hue` 变量的颜色。 颜色要能被 [`color_palette()`]解释(seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette"), 或者一个能映射到 matplotlib 颜色的字典。

`saturation`：float, optional

>  Proportion of the original saturation to draw colors at. Large patches often look better with slightly desaturated colors, but set this to `1` if you want the plot colors to perfectly match the input color spec.

`errcolor`：matplotlib color

> 表示置信区间的线的颜色。

`errwidth`：float, optional

> 误差条的线的厚度。

`capsize`：float, optional

> 误差条端部的宽度。

**dodge** : 布尔型, optional

> When hue nesting is used, whether elements should be shifted along the categorical axis.

`ax`：matplotlib Axes, optional

> 指定一个 Axes 用于绘图，如果不指定，则使用当前的 Axes。

`kwargs`：key, value mappings

> 其他的关键词参数在绘图时通过 `plt.bar` 传入。

返回值：`ax`：matplotlib Axes

> 返回有图表绘制的 Axes 对象。



See also

显示每个分类 bin 中的观察计数。使用散点图图示符显示点估计和置信区间。将分类图与类相结合:<cite>FacetGrid</cite>.

Examples

绘制一组按类别变量分组的垂直条形图：

```python
>>> import seaborn as sns
>>> sns.set(style="whitegrid")
>>> tips = sns.load_dataset("tips")
>>> ax = sns.barplot(x="day", y="total_bill", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-barplot-1.png](assets/9d1addc98b6a35ef0376219c56e7b7fd.jpg)


绘制一组由两个变量嵌套分组的垂直条形图：

```python
>>> ax = sns.barplot(x="day", y="total_bill", hue="sex", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-barplot-2.png](assets/863249efe2403afa4fae2f2b6884d3bd.jpg)


绘制一组水平条形图：

```python
>>> ax = sns.barplot(x="tip", y="day", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-barplot-3.png](assets/c3ea6265eaff0a4bfaec2966088cb66f.jpg)


通过传入一个显式的顺序来控制条柱的顺序：

```python
>>> ax = sns.barplot(x="time", y="tip", data=tips,
...                  order=["Dinner", "Lunch"])
```

![http://seaborn.pydata.org/_images/seaborn-barplot-4.png](assets/9233554272a5e436f6ab85c97a65010c.jpg)


用中值来评估数据的集中趋势：

```python
>>> from numpy import median
>>> ax = sns.barplot(x="day", y="tip", data=tips, estimator=median)
```

![http://seaborn.pydata.org/_images/seaborn-barplot-5.png](assets/2622373fb99932aa42e45c3b151135be.jpg)


用误差条显示平均值的标准误差：

```python
>>> ax = sns.barplot(x="day", y="tip", data=tips, ci=68)
```

![http://seaborn.pydata.org/_images/seaborn-barplot-6.png](assets/d1310bd7e87a8549d1f0b3a1479fc06d.jpg)


展示数据的标准差：

```python
>>> ax = sns.barplot(x="day", y="tip", data=tips, ci="sd")
```

![http://seaborn.pydata.org/_images/seaborn-barplot-7.png](assets/eeb77dac6d8f76d9f715476ce03773c5.jpg)


给误差条增加“端点”：

```python
>>> ax = sns.barplot(x="day", y="tip", data=tips, capsize=.2)
```

![http://seaborn.pydata.org/_images/seaborn-barplot-8.png](assets/5a69e1058d9b8b4b5be6dc15d1bad130.jpg)


使用一个不同的调色盘来绘制图案：

```python
>>> ax = sns.barplot("size", y="total_bill", data=tips,
...                  palette="Blues_d")
```

![http://seaborn.pydata.org/_images/seaborn-barplot-9.png](assets/ef011fca38d3c55dde21ee8363e93e61.jpg)


在不改变条柱的位置或者宽度的前提下，使用 `hue` :

```python
>>> tips["weekend"] = tips["day"].isin(["Sat", "Sun"])
>>> ax = sns.barplot(x="day", y="total_bill", hue="weekend",
...                  data=tips, dodge=False)
```

![http://seaborn.pydata.org/_images/seaborn-barplot-10.png](assets/d38d4ad12b16322a5ed00690bcbcd8b6.jpg)


用同一种颜色绘制所有条柱：

```python
>>> ax = sns.barplot("size", y="total_bill", data=tips,
...                  color="salmon", saturation=.5)
```

![http://seaborn.pydata.org/_images/seaborn-barplot-11.png](assets/4922c693b75b7656b2f16f8fd2dd6509.jpg)


用 `plt.bar` 关键字参数进一步改变图表的样式：

```python
>>> ax = sns.barplot("day", "total_bill", data=tips,
...                  linewidth=2.5, facecolor=(1, 1, 1, 0),
...                  errcolor=".2", edgecolor=".2")
```

![http://seaborn.pydata.org/_images/seaborn-barplot-12.png](assets/20114eb58ca40a3ccf0b20f14f426c83.jpg)


使用 [`catplot()`](seaborn.catplot.html#seaborn.catplot "seaborn.catplot") 来结合 [`barplot()`](#seaborn.barplot "seaborn.barplot") 和 [`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid"). 这允许数据根据额外的类别变量分组。使用 [`catplot()`](seaborn.catplot.html#seaborn.catplot "seaborn.catplot") 比直接使用 [`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid") 更安全, 因为它可以确保变量在不同的 facet 之间保持同步:

```python
>>> g = sns.catplot(x="sex", y="total_bill",
...                 hue="smoker", col="time",
...                 data=tips, kind="bar",
...                 height=4, aspect=.7);
```

![http://seaborn.pydata.org/_images/seaborn-barplot-13.png](assets/a2d8b9c6867b1006b56e5508d5472c86.jpg)


## seaborn.countplot

```python
seaborn.countplot(x=None, y=None, hue=None, data=None, order=None, hue_order=None, orient=None, color=None, palette=None, saturation=0.75, dodge=True, ax=None, **kwargs)
```

seaborn.countplot 使用条形图显示每个类别中观测值的数量。

这个函数可以被认为是针对类别变量的直方图。基本的 API 和选项与[`barplot()`](seaborn.barplot.html#seaborn.barplot "seaborn.barplot")完全相同，因此可以对比学习。

可以通过多种格式传入数据，包括：

*   通过列表、numpy 数组、或者 pandas Series 对象表示的向量数据，数据直接传给`x`, `y`, 和/或`hue`参数。
*   长格式的 DataFrame，此时会通过`x`, `y`以及`hue`变量决定如何绘制数据。
*   宽格式的 DataFrame，此时每个数值型的 column 都会被绘制。
*   数组或者列表形式的向量

在绝大多数情况下，数据格式都可以使用 numpy 或者 Python 对象，但是推荐使用 pandas 对象，因为 pandas 对象中相关的名称会被用于注释坐标轴。此外，可以通过设置分组变量为使用 Categorical 类型来控制绘制元素的顺序。

这个函数总会将变量作为类别变量进行处理，按顺序(0, 1, ... n)在相应坐标轴绘制数据，即使数据为数值或者日期类型。

更多信息参考[tutorial](http://seaborn.pydata.org/tutorial/categorical.html#categorical-tutorial). 

参数：`x, y, hue`: `data`或者向量数据中的变量名，可选

> 用于绘制长格式数据的输入。查看解释示例

`data`：DataFrame, 数组，或者包含数组的列表，可选

> 用于绘制的数据集。如果`x`和`y`不存在，那么会将数据按宽格式进行处理，否则应当为长格式。 

`order, hue_order`：包含字符串的列表，可选

> 类别层级绘制的顺序，否则层级会从数据对象中推测。

`orient`: “v” &#124; “h”, 可选

> 绘制的朝向（竖直或者水平）。通过从输入变量的 dtype 进行推断。当类别变量是数值型或者绘制宽格式数据时，可以进行指定。

`color`: matplotlib 颜色，可选

> 所有元素的颜色，或者渐变调色盘的种子。

`palette`: 调色盘名称，列表或字典，可选

> 用于`hue`变量的不同级别的颜色。应当是[`color_palette()`](seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette")可以解释的东西，或者将色调级别映射到 matplotlib 颜色的字典。

`saturation`: float, 可选

> 在原有饱和度的比例下绘制颜色。大片的图块通常在略微不饱和的颜色下看起来更好，而如果想要绘制的颜色与输入颜色规格完全匹配，应当设置此值为`1`。

`dodge`: bool, 可选

> 当使用色调嵌套时，决定是否沿着类别轴对元素进行移位。

`ax`: matplotlib 轴，可选

> 绘制图像的轴对象，不指定时使用当前轴。

`kwargs`: 键值映射

> 其他关键字参数会被传递给`plt.bar`.


返回值：`ax`: matplotlib 轴

> 返回带有绘图的轴对象。



另请参阅

[`barplot()`](seaborn.barplot.html#seaborn.barplot "seaborn.barplot"): 使用条形图显示点估计和置信区间。

[`factorplot()`](seaborn.factorplot.html#seaborn.factorplot "seaborn.factorplot"): 结合类别图与`FacetGrid`类。

示例

显示单个类别变量的计数值：

```python
>>> import seaborn as sns
>>> sns.set(style="darkgrid")
>>> titanic = sns.load_dataset("titanic")
>>> ax = sns.countplot(x="class", data=titanic)
```

![http://seaborn.pydata.org/_images/seaborn-countplot-1.png](assets/840a3b4206930d48fae0f0d4549b87a0.jpg)


显示两个类别变量的计数值：

```python
>>> ax = sns.countplot(x="class", hue="who", data=titanic)
```

![http://seaborn.pydata.org/_images/seaborn-countplot-2.png](assets/7fde400b120a0b6cd6aca0cb2de690db.jpg)


水平绘制条形图：

```python
>>> ax = sns.countplot(y="class", hue="who", data=titanic)
```

![http://seaborn.pydata.org/_images/seaborn-countplot-3.png](assets/512afaa1b09caf55faf909a34995137a.jpg)


使用不同的颜色色盘：

```python
>>> ax = sns.countplot(x="who", data=titanic, palette="Set3")
```

![http://seaborn.pydata.org/_images/seaborn-countplot-4.png](assets/d3ac1e2e1f590489ef76341664562f87.jpg)


使用`plt.bar`的关键字参数获得不同的显示效果：

```python
>>> ax = sns.countplot(x="who", data=titanic,
...                    facecolor=(0, 0, 0, 0),
...                    linewidth=5,
...                    edgecolor=sns.color_palette("dark", 3))
```

![http://seaborn.pydata.org/_images/seaborn-countplot-5.png](assets/d23d0851c88c12754e0c1e08aac20d01.jpg)


使用[`catplot()`](seaborn.catplot.html#seaborn.catplot "seaborn.catplot")实现结合[`countplot()`](#seaborn.countplot "seaborn.countplot")以及[`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")的效果。这样做可以在额外的类别变量中进行分组。使用[`catplot()`](seaborn.catplot.html#seaborn.catplot "seaborn.catplot")比直接使用[`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")更加安全，因为这样做可以确保跨分面的变量顺序同步：

```python
>>> g = sns.catplot(x="class", hue="who", col="survived",
...                 data=titanic, kind="count",
...                 height=4, aspect=.7);
```

![http://seaborn.pydata.org/_images/seaborn-countplot-6.png](assets/5d75285f3c08542cd9db2296ef737d54.jpg)


## seaborn.jointplot

```python
seaborn.jointplot(x, y, data=None, kind='scatter', stat_func=None, color=None, height=6, ratio=5, space=0.2, dropna=True, xlim=None, ylim=None, joint_kws=None, marginal_kws=None, annot_kws=None, **kwargs)
```

绘制两个变量的双变量及单变量图。

这个函数提供调用[`JointGrid`](seaborn.JointGrid.html#seaborn.JointGrid "seaborn.JointGrid")类的便捷接口，以及一些封装好的绘图类型。这是一个轻量级的封装，如果需要更多的灵活性，应当直接使用[`JointGrid`](seaborn.JointGrid.html#seaborn.JointGrid "seaborn.JointGrid").

参数：`x, y`：strings 或者 vectors

> `data`中的数据或者变量名。

`data`：DataFrame, 可选

> 当`x`和`y`为变量名时的 DataFrame.

`kind`：{ “scatter” &#124; “reg” &#124; “resid” &#124; “kde” &#124; “hex” }, 可选

> 绘制图像的类型。

`stat_func`：可调用的，或者 None, 可选

> 已过时

`color`：matplotlib 颜色, 可选

> 用于绘制元素的颜色。

`height`：numeric, 可选

> 图像的尺寸（方形）。

`ratio`：numeric, 可选

>  中心轴的高度与侧边轴高度的比例

`space`：numeric, 可选

> 中心和侧边轴的间隔大小

`dropna`：bool, 可选

> 如果为 True, 移除`x`和`y`中的缺失值。

`{x, y}lim`：two-tuples, 可选

> 绘制前设置轴的范围。

`{joint, marginal, annot}_kws`：dicts, 可选

> 额外的关键字参数。

`kwargs`：键值对

> 额外的关键字参数会被传给绘制中心轴图像的函数，取代`joint_kws`字典中的项。


返回值：`grid`：[`JointGrid`](seaborn.JointGrid.html#seaborn.JointGrid "seaborn.JointGrid")

> [`JointGrid`](seaborn.JointGrid.html#seaborn.JointGrid "seaborn.JointGrid")对象.



参考

绘制图像的 Grid 类。如果需要更多的灵活性，可以直接使用 Grid 类。

示例

绘制带有侧边直方图的散点图:

```python
>>> import numpy as np, pandas as pd; np.random.seed(0)
>>> import seaborn as sns; sns.set(style="white", color_codes=True)
>>> tips = sns.load_dataset("tips")
>>> g = sns.jointplot(x="total_bill", y="tip", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-jointplot-1.png](assets/48d5020fcbaa6d2f36aae520f84a6234.jpg)


添加回归线及核密度拟合:

```python
>>> g = sns.jointplot("total_bill", "tip", data=tips, kind="reg")
```

![http://seaborn.pydata.org/_images/seaborn-jointplot-2.png](assets/8434c101b75c73a9e1b8dfb89975a2b5.jpg)


将散点图替换为六角形箱体图:

```python
>>> g = sns.jointplot("total_bill", "tip", data=tips, kind="hex")
```

![http://seaborn.pydata.org/_images/seaborn-jointplot-3.png](assets/6d5c569bf97b1f683a2ec921e1031112.jpg)


将散点图和直方图替换为密度估计，并且将侧边轴与中心轴对齐:

```python
>>> iris = sns.load_dataset("iris")
>>> g = sns.jointplot("sepal_width", "petal_length", data=iris,
...                   kind="kde", space=0, color="g")
```

![http://seaborn.pydata.org/_images/seaborn-jointplot-4.png](assets/c2c70e8889861b837b4fd45d707a6616.jpg)


绘制散点图，添加中心密度估计:

```python
>>> g = (sns.jointplot("sepal_length", "sepal_width",
...                    data=iris, color="k")
...         .plot_joint(sns.kdeplot, zorder=0, n_levels=6))
```

![http://seaborn.pydata.org/_images/seaborn-jointplot-5.png](assets/b6895c87c4fa5a7fa1dc151dc3e5b385.jpg)


不适用 Pandas, 直接传输向量，随后给轴命名:

```python
>>> x, y = np.random.randn(2, 300)
>>> g = (sns.jointplot(x, y, kind="hex")
...         .set_axis_labels("x", "y"))
```

![http://seaborn.pydata.org/_images/seaborn-jointplot-6.png](assets/72b1f526c884ba4a6a285f1e8723013e.jpg)


绘制侧边图空间更大的图像:

```python
>>> g = sns.jointplot("total_bill", "tip", data=tips,
...                   height=5, ratio=3, color="g")
```

![http://seaborn.pydata.org/_images/seaborn-jointplot-7.png](assets/ddcf0a83320e56c75f2d5d15ff29c874.jpg)


传递关键字参数给后续绘制函数:

```python
>>> g = sns.jointplot("petal_length", "sepal_length", data=iris,
...                   marginal_kws=dict(bins=15, rug=True),
...                   annot_kws=dict(stat="r"),
...                   s=40, edgecolor="w", linewidth=1)
```

![http://seaborn.pydata.org/_images/seaborn-jointplot-8.png](assets/0fa41716b87d635876d921fc9ab967ea.jpg)


## seaborn.pairplot

```python
seaborn.pairplot(data, hue=None, hue_order=None, palette=None, vars=None, x_vars=None, y_vars=None, kind='scatter', diag_kind='auto', markers=None, height=2.5, aspect=1, dropna=True, plot_kws=None, diag_kws=None, grid_kws=None, size=None)
```

绘制数据集中的成对关系

默认情况下，此函数将创建一个 Axes 网络，以便`data`中的每个变量将在 y 轴上共享一行，并在 x 轴上共享一列。对角轴的处理方式并不同，以此绘制一个图表来显示该列中变量的数据的单变量分布。

还可以显示变量的子集或在行和列上绘制不同的变量。

这是[`PairGrid`](seaborn.PairGrid.html#seaborn.PairGrid "seaborn.PairGrid")的高级界面，旨在简化一些常见的样式。如果你需要更多的灵活性，你应该直接使用[`PairGrid`](seaborn.PairGrid.html#seaborn.PairGrid "seaborn.PairGrid")。

参数：`data`：数据框架

> 整洁（长形式）数据框，其中每列是变量，每行是观察量。

`hue`：字符串（变量名），可选。

> `data`中的变量将绘图方面映射到不同的颜色。

`hue_order`：字符串列表。

> 命令调色板中的色调变量的级别。

`palette`：字典或 seaborn 调色板。

> 用于映射`hue`变量的颜色集。如果是字典，关键字应该是`hue`变量中的值。

`vars`：变量名列表，可选。

> 要使用的`data`中的变量，否则每一列使用数字的数据类型。

`{x, y}_vars`：变量名列表，可选。

> `data`中的变量分别用于图的行和列；即制作非方形图。

`kind`：{‘scatter’, ‘reg’}, 可选。

> 一种非等同关系的图类型

`diag_kind`：{‘auto’, ‘hist’, ‘kde’}, 可选

> 对角线子图的一种图形。默认值取决于是否使用`hue`。

`markers`：单个 matplotlit 标记代码或列表，可选

> 要么是用于所有数据点的标记，要么是长度和色调变量中的级别数相同的标记列表，这样不同颜色的点也会有不同的散点图标记。

`height`：标量，可选。

> 每个刻面的高度（以英寸为单位）

`aspect`：标量，可选。

> Aspect\*height 给出每个刻面的宽度（以英寸为单位）

`dropna`：布尔值，可选。

> 在绘图之前删除数据中的缺失值。

`{plot, diag, grid}_kws`：字典，可选。

> 关键字参数的字典。

返回值：`grid`：PairGrid

> 返回底层的`PairGrid`实例以进一步调整。

也可以看看

子图网络可以更灵活地绘制成对关系。

范例

绘制联合关系地散点图和单变量分布的直方图：

```python
>>> import seaborn as sns; sns.set(style="ticks", color_codes=True)
>>> iris = sns.load_dataset("iris")
>>> g = sns.pairplot(iris)
```

![http://seaborn.pydata.org/_images/seaborn-pairplot-1.png](assets/712c7f2cff4ed2c31b99bf5200838ae1.jpg)


通过绘图元素的颜色显示分类变量的不同级别：

```python
>>> g = sns.pairplot(iris, hue="species")
```

![http://seaborn.pydata.org/_images/seaborn-pairplot-2.png](assets/98c1e58e7427e940294084d2b4582ac8.jpg)


使用不同的调色板：

```python
>>> g = sns.pairplot(iris, hue="species", palette="husl")
```

![http://seaborn.pydata.org/_images/seaborn-pairplot-3.png](assets/f6d3d4030bc93eefcb054749cf90c36b.jpg)


为`hue`变量的每个级别使用不同的标记：

```python
>>> g = sns.pairplot(iris, hue="species", markers=["o", "s", "D"])
```

![http://seaborn.pydata.org/_images/seaborn-pairplot-4.png](assets/6840daa5bd6d421f05969aeb76c74032.jpg)


绘制变量的子集：

```python
>>> g = sns.pairplot(iris, vars=["sepal_width", "sepal_length"])
```

![http://seaborn.pydata.org/_images/seaborn-pairplot-5.png](assets/87b11c84b7ec8551c2edf9a2ef987014.jpg)


绘制更大的图：

```python
>>> g = sns.pairplot(iris, height=3,
...                  vars=["sepal_width", "sepal_length"])
```

![http://seaborn.pydata.org/_images/seaborn-pairplot-6.png](assets/5e89b880399cf894d124b590416e7fb5.jpg)


在行和列中绘制不同的变量：

```python
>>> g = sns.pairplot(iris,
...                  x_vars=["sepal_width", "sepal_length"],
...                  y_vars=["petal_width", "petal_length"])
```

![http://seaborn.pydata.org/_images/seaborn-pairplot-7.png](assets/35f33d0939e4e10f46437f5690ad766b.jpg)


对单变量图使用核密度估计：

```python
>>> g = sns.pairplot(iris, diag_kind="kde")
```

![http://seaborn.pydata.org/_images/seaborn-pairplot-8.png](assets/cc2c3fb8414e50f2d9f95ab073457c23.jpg)


将线性回归模型拟合到散点图：

```python
>>> g = sns.pairplot(iris, kind="reg")
```

![http://seaborn.pydata.org/_images/seaborn-pairplot-9.png](assets/74befbc8e8cbc89474dd845fb49329ca.jpg)


将关键字参数传递给底层函数（直接使用[`PairGrid`](seaborn.PairGrid.html#seaborn.PairGrid "seaborn.PairGrid")可能更容易）

```python
>>> g = sns.pairplot(iris, diag_kind="kde", markers="+",
...                  plot_kws=dict(s=50, edgecolor="b", linewidth=1),
...                  diag_kws=dict(shade=True))
```

![http://seaborn.pydata.org/_images/seaborn-pairplot-10.png](assets/0b688ef9f59240384ca84839d9707ce7.jpg)


## seaborn.distplot 



```python
seaborn.distplot(a, bins=None, hist=True, kde=True, rug=False, fit=None, hist_kws=None, kde_kws=None, rug_kws=None, fit_kws=None, color=None, vertical=False, norm_hist=False, axlabel=None, label=None, ax=None)
```

灵活绘制单变量观测值分布图。

该函数结合了 matplotlib 中的 `hist`函数（自动计算一个默认的合适的 bin 大小）、seaborn 的[`kdeplot()`](seaborn.kdeplot.html#seaborn.kdeplot "seaborn.kdeplot")和[`rugplot()`](seaborn.rugplot.html#seaborn.rugplot "seaborn.rugplot")函数。它还可以拟合`scipy.stats`分布并在数据上绘制估计的 PDF（概率分布函数）。

参数：`a`：Series、1 维数组或者列表。 

> 观察数据。如果是具有`name`属性的 Series 对象，则该名称将用于标记数据轴。

`bins`：matplotlib hist()的参数，或 None。可选参数。  

> 直方图 bins（柱）的数目，若填 None，则默认使用 Freedman-Diaconis 规则指定柱的数目。

`hist`：布尔值，可选参数。

> 是否绘制（标准化）直方图。

`kde`：布尔值，可选参数。

> 是否绘制高斯核密度估计图。

`rug`：布尔值，可选参数。

> 是否在横轴上绘制观测值竖线。

`fit`：随机变量对象，可选参数。

> 一个带有*fit*方法的对象，返回一个元组，该元组可以传递给*pdf*方法一个位置参数，该位置参数遵循一个值的网格用于评估 pdf。

`{hist, kde, rug, fit}_kws`：字典，可选参数。

> 底层绘图函数的关键字参数。

`color`：matplotlib color，可选参数。

> 可以绘制除了拟合曲线之外所有内容的颜色。

`vertical`：布尔值，可选参数。

> 如果为 True，则观测值在 y 轴显示。

`norm_hist`：布尔值，可选参数。

> 如果为 True，则直方图的高度显示密度而不是计数。如果绘制 KDE 图或拟合密度，则默认为 True。

`axlabel`：字符串，False 或者 None，可选参数。

> 横轴的名称。如果为 None，将尝试从 a.name 获取它；如果为 False，则不设置标签。

`label`：字符串，可选参数。

> 图形相关组成部分的图例标签。

`ax`：matplotlib axis，可选参数。

> 若提供该参数，则在参数设定的轴上绘图。

返回值：`ax`：matplotlib Axes

> 返回 Axes 对象以及用于进一步调整的绘图。

**另请参见**

[`kdeplot`](seaborn.kdeplot.html#seaborn.kdeplot "seaborn.kdeplot")

显示具有核密度估计图的单变量或双变量分布。

[`rugplot`](seaborn.rugplot.html#seaborn.rugplot "seaborn.rugplot")

绘制小的垂直线以显示分布中的每个观测值。

**范例**

显示具有核密度估计的默认图和使用参考规则自动确定 bin 大小的直方图：

```python
>>> import seaborn as sns, numpy as np
>>> sns.set(); np.random.seed(0)
>>> x = np.random.randn(100)
>>> ax = sns.distplot(x)
```

![http://seaborn.pydata.org/_images/seaborn-distplot-1.png](assets/dfbc2ec93ea21c479448cd2e25976945.jpg)


使用 Pandas 对象获取信息轴标签：

```python
>>> import pandas as pd
>>> x = pd.Series(x, name="x variable")
>>> ax = sns.distplot(x)
```

![http://seaborn.pydata.org/_images/seaborn-distplot-2.png](assets/d33f6bf00c0886595c68d970316b0717.jpg)


使用核密度估计和小的垂直线绘制分布图：

```python
>>> ax = sns.distplot(x, rug=True, hist=False)
```

![http://seaborn.pydata.org/_images/seaborn-distplot-3.png](assets/85548cb1190d0ad63fbaf33d0966b16d.jpg)


使用直方图和最大似然高斯分布拟合绘制分布图：

```python
>>> from scipy.stats import norm
>>> ax = sns.distplot(x, fit=norm, kde=False)
```

![http://seaborn.pydata.org/_images/seaborn-distplot-4.png](assets/d3a9c4026d4ce70e54b250057cd2062b.jpg)


在垂直轴上绘制分布图：

```python
>>> ax = sns.distplot(x, vertical=True)
```

![http://seaborn.pydata.org/_images/seaborn-distplot-5.png](assets/0a53d5e52e4ce61295a6c2b5fc4bf6c8.jpg)


更改所有绘图元素的颜色：

```python
>>> sns.set_color_codes()
>>> ax = sns.distplot(x, color="y")
```

![http://seaborn.pydata.org/_images/seaborn-distplot-6.png](assets/fac6ba03c6573e299e4eee00c32999fb.jpg)


将特定参数传递给基础绘图函数：

```python
>>> ax = sns.distplot(x, rug=True, rug_kws={"color": "g"},
...                   kde_kws={"color": "k", "lw": 3, "label": "KDE"},
...                   hist_kws={"histtype": "step", "linewidth": 3,
...                             "alpha": 1, "color": "g"})
```

![http://seaborn.pydata.org/_images/seaborn-distplot-7.png](assets/fce8e5a984297be3f6f8b5d4c5369a78.jpg)


## seaborn.kdeplot



```python
seaborn.kdeplot(data, data2=None, shade=False, vertical=False, kernel='gau', bw='scott', gridsize=100, cut=3, clip=None, legend=True, cumulative=False, shade_lowest=True, cbar=False, cbar_ax=None, cbar_kws=None, ax=None, **kwargs)
```

拟合并绘制单变量或双变量核密度估计图。

参数：`data`：一维阵列

> 输入数据

**data2：一维阵列，可选。

> 第二输入数据。如果存在，将估计双变量 KDE。 

`shade`：布尔值，可选参数。

> 如果为 True，则在 KDE 曲线下方的区域中增加阴影（或者在数据为双变量时使用填充的轮廓绘制）。

`vertical`：布尔值，可选参数。

> 如果为 True，密度图将显示在 x 轴。

`kernel`：{‘gau’ &#124; ‘cos’ &#124; ‘biw’ &#124; ‘epa’ &#124; ‘tri’ &#124; ‘triw’ }，可选参数

>  要拟合的核的形状代码，双变量 KDE 只能使用高斯核。

`bw`：{‘scott’ &#124; ‘silverman’ &#124; scalar &#124; pair of scalars }，可选参数

> 用于确定双变量图的每个维的核大小、标量因子或标量的参考方法的名称。需要注意的是底层的计算库对此参数有不同的交互：`statsmodels`直接使用它，而`scipy`将其视为数据标准差的缩放因子。

`gridsize`：整型数据，可选参数。

> 评估网格中的离散点数。

`cut`：标量，可选参数。

> 绘制估计值以从极端数据点切割* bw。

`clip`：一对标量，可选参数。

> 用于拟合 KDE 图的数据点的上下限值。可以为双变量图提供一对（上，下）边界。

`legend`：布尔值，可选参数。

> 如果为 True，为绘制的图像添加图例或者标记坐标轴。

`cumulative`：布尔值，可选参数。

> 如果为 True，则绘制 kde 估计图的累积分布。

`shade_lowest`：布尔值，可选参数。

> 如果为 True，则屏蔽双变量 KDE 图的最低轮廓。绘制单变量图或“shade = False”时无影响。当你想要在同一轴上绘制多个密度时，可将此参数设置为“False”。

`cbar`：布尔值，可选参数。

> 如果为 True 并绘制双变量 KDE 图，为绘制的图像添加颜色条。

`cbar_ax`：matplotlib axes，可选参数。

> 用于绘制颜色条的坐标轴，若为空，就在主轴绘制颜色条。

`cbar_kws`：字典，可选参数。

> `fig.colorbar（）`的关键字参数。

`ax`：matplotlib axes，可选参数。

> 要绘图的坐标轴，若为空，则使用当前轴。

`kwargs`：键值对

> 其他传递给`plt.plot（）`或`plt.contour {f}`的关键字参数，具体取决于是绘制单变量还是双变量图。

返回值：`ax`：matplotlib Axes

> 绘图的坐标轴。

**另请参见**

[`distplot`](seaborn.distplot.html#seaborn.distplot "seaborn.distplot")

灵活绘制单变量观测值分布图。

[`jointplot`](seaborn.jointplot.html#seaborn.jointplot "seaborn.jointplot")

绘制一个具有双变量和边缘分布的联合数据集。

范例

绘制一个简单的单变量分布：

```python
>>> import numpy as np; np.random.seed(10)
>>> import seaborn as sns; sns.set(color_codes=True)
>>> mean, cov = [0, 2], [(1, .5), (.5, 1)]>>> x, y = np.random.multivariate_normal(mean, cov, size=50).T
>>> ax = sns.kdeplot(x)
```

![http://seaborn.pydata.org/_images/seaborn-kdeplot-1.png](assets/467839dfd41d95bdf1eb6d992d54a81f.jpg)




在密度曲线下使用不同的颜色着色：

```python
>>> ax = sns.kdeplot(x, shade=True, color="r")
```

![http://seaborn.pydata.org/_images/seaborn-kdeplot-2.png](assets/98bd4d30d5719cd930a50603d08aa30e.jpg)




绘制一个双变量分布：

```python
>>> ax = sns.kdeplot(x, y)
```

![http://seaborn.pydata.org/_images/seaborn-kdeplot-3.png](assets/1a4a764cf63a098fe9db3dc317e88058.jpg)




使用填充轮廓：

```python
>>> ax = sns.kdeplot(x, y, shade=True)
```

![http://seaborn.pydata.org/_images/seaborn-kdeplot-4.png](assets/566a36de7b39f66e34fa323e7153fa42.jpg)




使用更多的轮廓级别和不同的调色板：

```python
>>> ax = sns.kdeplot(x, y, n_levels=30, cmap="Purples_d")
```

![http://seaborn.pydata.org/_images/seaborn-kdeplot-5.png](assets/73c68c911f3d8aaf827238e97a8dc560.jpg)




使用窄带宽：

```python
>>> ax = sns.kdeplot(x, bw=.15)
```

![http://seaborn.pydata.org/_images/seaborn-kdeplot-6.png](assets/cf85bc1dbfbb0399b3008f079b88d570.jpg)




在纵轴上绘制密度分布：

```python
>>> ax = sns.kdeplot(y, vertical=True)
```

![http://seaborn.pydata.org/_images/seaborn-kdeplot-7.png](assets/62b886998fa52840a228f283aa862506.jpg)




将密度曲线限制在数据范围内:

```python
>>> ax = sns.kdeplot(x, cut=0)
```

![http://seaborn.pydata.org/_images/seaborn-kdeplot-8.png](assets/1d21f3969de9de67e8acd0e2486c5e04.jpg)




为轮廓添加一个颜色条:

```python
>>> ax = sns.kdeplot(x, y, cbar=True)
```

![http://seaborn.pydata.org/_images/seaborn-kdeplot-9.png](assets/341dd0f98ddca59219e6289b038c8784.jpg)




为双变量密度图绘制两个阴影：

```python
>>> iris = sns.load_dataset("iris")
>>> setosa = iris.loc[iris.species == "setosa"]>>> virginica = iris.loc[iris.species == "virginica"]>>> ax = sns.kdeplot(setosa.sepal_width, setosa.sepal_length,
...                  cmap="Reds", shade=True, shade_lowest=False)
>>> ax = sns.kdeplot(virginica.sepal_width, virginica.sepal_length,
...                  cmap="Blues", shade=True, shade_lowest=False)
```

![http://seaborn.pydata.org/_images/seaborn-kdeplot-10.png](assets/6ffe5e9bcaba45c14f423cae61e6e743.jpg)




## seaborn.rugplot



```python
seaborn.rugplot(a, height=0.05, axis='x', ax=None, **kwargs)
```

将数组中的数据点绘制为轴上的棒状标识。

参数：`a`：向量

> 1 维的观察数组。

`height`：标量, 可选

> 以比例形式表示的坐标轴上棒状标识的高度。

`axis`：{‘x’ &#124; ‘y’}, 可选

> 需要画 rugplot 的坐标轴

`ax`：matplotlib 轴, 可选

> 进行绘制的坐标轴; 未指定的话设定为当前轴。

`kwargs`：键值对

> 被传递给`LineCollection`的其他关键字参数。


返回值：`ax`：matplotlib Axex 对象

> 在其上进行绘图的 Axex 对象。


## seaborn.lmplot

```python
seaborn.lmplot(x, y, data, hue=None, col=None, row=None, palette=None, col_wrap=None, height=5, aspect=1, markers='o', sharex=True, sharey=True, hue_order=None, col_order=None, row_order=None, legend=True, legend_out=True, x_estimator=None, x_bins=None, x_ci='ci', scatter=True, fit_reg=True, ci=95, n_boot=1000, units=None, order=1, logistic=False, lowess=False, robust=False, logx=False, x_partial=None, y_partial=None, truncate=False, x_jitter=None, y_jitter=None, scatter_kws=None, line_kws=None, size=None)
```

在 FacetGrid 对象上绘制数据和回归模型。

这个函数结合了 [`regplot()`](seaborn.regplot.html#seaborn.regplot "seaborn.regplot") 和 [`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")。 它预期作为一个能够将回归模型运用在数据集处于不同条件下的子数据集的方便的接口

在考虑如何将变量分配到不同方面时，一般规则是使用 `hue` 进行最重要的比较，然后使用  `col`  和  `row `。 但是，请始终考虑您的特定数据集以及您正在创建的可视化目标。

估算回归模型有许多互斥的选项。 有关详细信息，请参阅 [tutorial](http://seaborn.pydata.org/tutorial/regression.html#regression-tutorial) 。

此函数的参数涵盖了 [`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")中的大多数选项，尽管这样，偶尔还是会出现您需要直接使用该类和 [`regplot()`](seaborn.regplot.html#seaborn.regplot "seaborn.regplot") 的情况。

参数：`x, y`：字符串，可选

> 输入变量; 这些应该是`data`中的列名。

`data`：DataFrame

> Tidy (“long-form”)格式的 DataFrame，其中每列为一个变量，每行为一个观测样本。

`hue, col, row`：字符串

> 定义数据子集的变量，将在网格中的不同构面上绘制。 请参阅`* _order`参数以控制此变量的级别顺序。

`palette`： 调色板名称，列表或字典，可选

> 用于`hue`变量的不同级别的颜色。 应该是 [`color_palette()`](seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette")可以解释的东西，或者是将色调级别映射到 matplotlib 颜色的字典。

`col_wrap`：整数，可选

> 以此宽度“包裹”列变量，以便列分面（facet）跨越多行。 与`row` 分面（facet）不兼容。

`height`： 标量，可选

> 每个分面（facet）的高度（以英寸为单位）。 另见：`aspect`。

`aspect`：标量，可选

> 每个分面（facet）的纵横比，因此`aspect * height`给出每个分面（facet）的宽度，单位为英寸。

`markers`：matplotlib 标记代码或标记代码列表，可选

> 散点图的标记。如果是列表，列表中的每个标记将用于`hue`变量的每个级别。

`share{x,y}`：布尔值，‘col’,或 ‘row’ ，可选

> 如果为 true，则分面（facet）之间将跨列共享 y 轴和/或跨行共享 x 轴。

`{hue,col,row}_order`：列表，可选

> 分面变量的级别顺序。在默认情况下，这将是级别在“data”中出现的顺序，或者，如果变量是 pandas 的分类类别变量，则为类别的顺序。

`legend`：布尔值，可选

> 如果为“True”并且有一个`hue`变量，则添加一个图例。

`legend_out`：布尔值，可选

> 如果为“True”，图形尺寸将被扩展，图例将被绘制在图像中部右侧之外。

`x_estimator`：可调用的映射向量->标量，可选

> 将此函数应用于`x`的每个唯一值并绘制结果的估计值。当`x`是离散变量时，这是十分有用的。如果给出`x_ci`，则该估计将被引导并且将绘制置信区间。

`x_bins`：整数或向量，可选

> 将`x`变量加入离散区间，然后估计中心趋势和置信区间。 此分箱仅影响散点图的绘制方式; 回归仍然适合原始数据。该参数被解释为均匀大小（不必要间隔）的箱的数量或箱中心的位置。使用此参数时，它意味着`x_estimator`的默认值为`numpy.mean`。

`x_ci`：“ci”。“sd”， 在[0,100]间的整数或 None，可选

> 绘制“x”离散值的集中趋势时使用的置信区间的大小。 如果为`“ci”`，遵循`ci`参数的值。 如果是“sd”，则跳过 bootstrapping 并显示每个 bin 中观察值的标准偏差。

`scatter`：布尔值，可选

> 如果为 `True`，则绘制带有基础观测值（或`x_estimator` 值）的散点图。

`fit_reg`：布尔值，可选

> 如果为 `True`，则估计并绘制与 `x` 和 `y` 变量相关的回归模型。

`ci`：在[0,100]间的整数或 None，可选

> 回归估计的置信区间的大小。这将使用回归线周围的半透明带绘制。 使用自助法（bootstrap）估计置信区间; 对于大型数据集，建议通过将此参数设置为 None 来避免该计算。

`n_boot`：整数，可选

> 用于估计`ci`的自助法（bootstrap）重采样数。 默认值试图在时间和稳定性之间找到平衡; 你可能希望为“最终”版本的图像增加此值。

`units`：`data`中的变量名，可选

> 如果`x`和`y`观察结果嵌套在采样单元中，则可以在此处指定。在通过对所有的单元和观察样本（在单元内）执行重新采样的多级自助法（multilevel bootstrap）来计算置信区间时将考虑这一点。 否则，这不会影响估计或绘制回归的方式。

`order`：整数，可选

> 如果`order`大于 1，使用`numpy.polyfit`来估计多项式回归。

`logistic`：布尔值，可选

> 如果为“True”，则假设`y`是二元变量并使用`statsmodels`来估计逻辑回归模型。 请注意，这比线性回归的计算密集程度要大得多，因此您可能希望减少引导程序重新采样（`n_boot`）的数量或将 `ci`设置为“无”。

`lowess`：布尔值，可选

> 如果为“True”，则使用`statsmodels`来估计非参数 lowess 模型（局部加权线性回归）。 请注意，目前无法为此类模型绘制置信区间。

`robust`：布尔值，可选

> 如果为“True”，则使用`statsmodels`来估计稳健回归。 这将削弱异常值。 请注意，这比标准线性回归的计算密集程度要大得多，因此您可能希望减少引导程序重新采样（`n_boot`）的数量或将 `ci`设置为“无”。

`logx`：布尔值，可选

> 如果为 `True`，则估计形式 y~log（x）的线性回归，但在输入空间中绘制散点图和回归模型。 请注意，`x`必须为正才能正常工作。

`{x,y}_partial`： `data`中的字符串或矩阵

> 混淆（Confounding）变量以在绘图之前退回`x`或`y`变量。

`truncate`：布尔值，可选

> 默认情况下，绘制回归线以在绘制散点图后填充 x 轴限制。 如果`truncate`是`True`，它将改为受到数据本身限制的限制。

`{x,y}_jitter`：浮点数，可选

> 将此大小的均匀随机噪声添加到“x”或“y”变量中。 在拟合回归之后，噪声被添加到数据的副本中，并且仅影响散点图的外观。 在绘制采用离散值的变量时，这会很有用。

`{scatter,line}_kws`：字典

> 传递给`plt.scatter`和`plt.plot`的附加关键字参数。



也可以查看

绘制数据和条件模型 fit.Subplot 网格用于绘制条件关系。合并  [`regplot()`](seaborn.regplot.html#seaborn.regplot "seaborn.regplot") 和 [`PairGrid`](seaborn.PairGrid.html#seaborn.PairGrid "seaborn.PairGrid") （与`kind =“reg”`一起使用时）。

注意

 [`regplot()`](seaborn.regplot.html#seaborn.regplot "seaborn.regplot") 与 [`lmplot()`](#seaborn.lmplot "seaborn.lmplot") 函数是紧密关联的，但是前者是一个坐标轴级别的函数，而后者则是一个联合了[`regplot()`](seaborn.regplot.html#seaborn.regplot "seaborn.regplot") 与 [`FacetGrid `](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")的图像级别的函数。

示例

这些例子集中在基本的回归模型图上，以展示各种方面的选项; 请参阅 [`regplot()`](seaborn.regplot.html#seaborn.regplot "seaborn.regplot") 文档，以演示绘制数据和模型的其他选项。 还有其他一些如何使用 [`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid") 文档中的返回对象操作绘图的示例。

绘制两个变量之间的简单线性关系：

```python
>>> import seaborn as sns; sns.set(color_codes=True)
>>> tips = sns.load_dataset("tips")
>>> g = sns.lmplot(x="total_bill", y="tip", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-lmplot-1.png](assets/86032eef9f0a340dfa9834cee96b10d4.jpg)




条件在第三个变量上并绘制不同颜色的水平：

```python
>>> g = sns.lmplot(x="total_bill", y="tip", hue="smoker", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-lmplot-2.png](assets/ed002aaba578f2b1a6ae8d4677a52900.jpg)




使用不同的标记和颜色，以便绘图更容易再现为黑白：

```python
>>> g = sns.lmplot(x="total_bill", y="tip", hue="smoker", data=tips,
...                markers=["o", "x"])
```

![http://seaborn.pydata.org/_images/seaborn-lmplot-3.png](assets/770464118d2458a098652af00c46525a.jpg)




使用不同的调色板：

```python
>>> g = sns.lmplot(x="total_bill", y="tip", hue="smoker", data=tips,
...                palette="Set1")
```

![http://seaborn.pydata.org/_images/seaborn-lmplot-4.png](assets/a314683a7f14a4e8a552aca52002c9b5.jpg)




使用字典将`hue`级别映射到颜色：

```python
>>> g = sns.lmplot(x="total_bill", y="tip", hue="smoker", data=tips,
...                palette=dict(Yes="g", No="m"))
```

![http://seaborn.pydata.org/_images/seaborn-lmplot-5.png](assets/cc594c98f73700b14c9817d68ebc89c6.jpg)




绘制不同列中第三个变量的级别：

```python
>>> g = sns.lmplot(x="total_bill", y="tip", col="smoker", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-lmplot-6.png](assets/3c50ee123e8192e2899983b1be19d34b.jpg)




更改构面的高度和纵横比：

```python
>>> g = sns.lmplot(x="size", y="total_bill", hue="day", col="day",
...                data=tips, height=6, aspect=.4, x_jitter=.1)
```

![http://seaborn.pydata.org/_images/seaborn-lmplot-7.png](assets/47ce7b8d629a8d762bea59bc674e5490.jpg)




将列变量的级别换行为多行：

```python
>>> g = sns.lmplot(x="total_bill", y="tip", col="day", hue="day",
...                data=tips, col_wrap=2, height=3)
```

![http://seaborn.pydata.org/_images/seaborn-lmplot-8.png](assets/798a1691371e0506570d9f1f72d95fe4.jpg)




两个变量上的条件形成一个完整的网格：

```python
>>> g = sns.lmplot(x="total_bill", y="tip", row="sex", col="time",
...                data=tips, height=3)
```

![http://seaborn.pydata.org/_images/seaborn-lmplot-9.png](assets/36048a5d4503fddb7cf3479a473188f2.jpg)




在返回的 [`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid") 实例上使用方法来进一步调整图像：

```python
>>> g = sns.lmplot(x="total_bill", y="tip", row="sex", col="time",
...                data=tips, height=3)
>>> g = (g.set_axis_labels("Total bill (US Dollars)", "Tip")
...       .set(xlim=(0, 60), ylim=(0, 12),
...            xticks=[10, 30, 50], yticks=[2, 6, 10])
...       .fig.subplots_adjust(wspace=.02))
```

![http://seaborn.pydata.org/_images/seaborn-lmplot-10.png](assets/11fc790735a7bdc59ac4a30d6af7e6d2.jpg)




## seaborn.regplot

```python
seaborn.regplot(x, y, data=None, x_estimator=None, x_bins=None, x_ci='ci', scatter=True, fit_reg=True, ci=95, n_boot=1000, units=None, order=1, logistic=False, lowess=False, robust=False, logx=False, x_partial=None, y_partial=None, truncate=False, dropna=True, x_jitter=None, y_jitter=None, label=None, color=None, marker='o', scatter_kws=None, line_kws=None, ax=None)
```

绘制数据和线性回归模型拟合。

估算回归模型有许多互斥的选项。查看这个[教程](http://seaborn.pydata.org/tutorial/regression.html#regression-tutorial) 来了解更多的信息。

参数：**x，y：字符串，序列（series）或者是向量数组（vector array）**

> 输入变量。 如果是字符串，应该与`data`中的列名相对应。 使用 pandas 对象时，轴将被 Series 的名字标记。

`data`：DataFrame

> Tidy (“long-form”)格式的 DataFrame，其中每列为一个变量，每行为一个观测样本。

`x_estimator`：可调用的映射向量 -&gt;标量，可选 

> 将此函数应用于`x`的每个唯一值并绘制结果的估计值。当`x`是离散变量时，这是十分有用的。如果给出`x_ci`，则该估计将被引导并且将绘制置信区间。

`x_bins`：整数或向量，可选

> 将`x`变量加入离散区间，然后估计中心趋势和置信区间。 此分箱仅影响散点图的绘制方式; 回归仍然适合原始数据。该参数被解释为均匀大小（不必要间隔）的箱的数量或箱中心的位置。使用此参数时，它意味着`x_estimator`的默认值为`numpy.mean`。

`x_ci`："ci"，'sd'，位于 [0, 100]之间的整数或 None，可选

> 绘制“x”离散值的集中趋势时使用的置信区间的大小。 如果为`“ci”`，遵循`ci`参数的值。 如果是“sd”，则跳过 bootstrapping 并显示每个 bin 中观察值的标准偏差。

`scatter`：布尔值，可选

> 如果为 `True`，则绘制带有基础观测值（或`x_estimator` 值）的散点图。

`fit_reg`：布尔值，可选

> 如果为 `True`，则估计并绘制与 `x` 和 `y` 变量相关的回归模型。

`ci`：位于 [0, 100]之间的整数或 None，可选

> 回归估计的置信区间的大小。这将使用回归线周围的半透明带绘制。 使用自助法（bootstrap）估计置信区间; 对于大型数据集，建议通过将此参数设置为 None 来避免该计算。

`n_boot`：整数，可选

> 用于估计`ci`的自助法（bootstrap）重采样数。 默认值试图在时间和稳定性之间找到平衡; 你可能希望为“最终”版本的图像增加此值。

`units`： `data`,中的变量名，可选

> 如果`x`和`y`观察结果嵌套在采样单元中，则可以在此处指定。在通过对所有的单元和观察样本（在单元内）执行重新采样的多级自助法（multilevel bootstrap）来计算置信区间时将考虑这一点。 否则，这不会影响估计或绘制回归的方式。

`order`：整数，可选

> 如果`order`大于 1，使用`numpy.polyfit`来估计多项式回归。

`logistic`：布尔值，可选

> 如果为“True”，则假设`y`是二元变量并使用`statsmodels`来估计逻辑回归模型。 请注意，这比线性回归的计算密集程度要大得多，因此您可能希望减少引导程序重新采样（`n_boot`）的数量或将 `ci`设置为“无”。

`lowess`：布尔值，可选

> 如果为“True”，则使用`statsmodels`来估计非参数 lowess 模型（局部加权线性回归）。 请注意，目前无法为此类模型绘制置信区间。

`robust`：布尔值，可选

> 如果为“True”，则使用`statsmodels`来估计稳健回归。 这将削弱异常值。 请注意，这比标准线性回归的计算密集程度要大得多，因此您可能希望减少引导程序重新采样（`n_boot`）的数量或将 `ci`设置为“无”。

`logx`：布尔值，可选

> 如果为 `True`，则估计形式 y~log（x）的线性回归，但在输入空间中绘制散点图和回归模型。 请注意，`x`必须为正才能正常工作。

`{x,y}_partial`： `data` 中的字符串或矩阵

> 混淆（Confounding）变量以在绘图之前退回`x`或`y`变量。

`truncate`：布尔值，可选

> 默认情况下，绘制回归线以在绘制散点图后填充 x 轴限制。 如果`truncate`是`True`，它将改为受到数据本身限制的限制。

`{x,y}_jitter`：浮点数，可选

> 将此大小的均匀随机噪声添加到“x”或“y”变量中。 在拟合回归之后，噪声被添加到数据的副本中，并且仅影响散点图的外观。 在绘制采用离散值的变量时，这会很有用。

`label`：字符串

> 要应用于散点图或回归线（如果`scatter`为'False`）的标签，以便在图例中使用。

`color`：matplotlib 颜色

> 适用于所有绘图元素的颜色; 将被`scatter_kws`或`line_kws`中传递的颜色取代。

`marker`：matplotlib 标记代码或标记代码列表，可选

> 散点图的标记。

`{scatter,line}_kws`：字典

> 传递给`plt.scatter`和`plt.plot`的附加关键字参数。

`ax`：matplotlib Axes 对象，可选

> 绘制到指定轴对象，否则在当前轴对象上绘图。


返回值：`ax`：matplotlib Axes 对象

> 包含了图像的 Axes 对象。



也可以看看

结合 [`regplot()`](#seaborn.regplot "seaborn.regplot") 和 [`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid") 来绘制数据集中的多个线性关系。 结合 [`regplot()`](#seaborn.regplot "seaborn.regplot") 和 [`JointGrid`](seaborn.JointGrid.html#seaborn.JointGrid "seaborn.JointGrid") （与`kind="reg"`一起使用时）。结合  [`regplot()`](#seaborn.regplot "seaborn.regplot") 和 [`PairGrid`](seaborn.PairGrid.html#seaborn.PairGrid "seaborn.PairGrid") （当用于` kind =“reg”`）。绘制线性回归模型的残差。

注意

 [`regplot()`](#seaborn.regplot "seaborn.regplot") 和 [`lmplot()`](seaborn.lmplot.html#seaborn.lmplot "seaborn.lmplot") 函数密切相关，但是前者是坐标轴级别的函数，而后者是结合了[`regplot()`](#seaborn.regplot "seaborn.regplot") 和 [`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")的图像级别的函数。

通过 [`jointplot()`](seaborn.jointplot.html#seaborn.jointplot "seaborn.jointplot") 和 [`pairplot()`](seaborn.pairplot.html#seaborn.pairplot "seaborn.pairplot") 函数来组合 [`regplot()`](#seaborn.regplot "seaborn.regplot") 和 [`JointGrid`](seaborn.JointGrid.html#seaborn.JointGrid "seaborn.JointGrid") 或 [`PairGrid`](seaborn.PairGrid.html#seaborn.PairGrid "seaborn.PairGrid") 是十分容易的，虽然这些函数不直接接受所有 [`regplot()`](#seaborn.regplot "seaborn.regplot")的参数。

例子

绘制 DataFrame 中两个变量之间的关系：

```python
>>> import seaborn as sns; sns.set(color_codes=True)
>>> tips = sns.load_dataset("tips")
>>> ax = sns.regplot(x="total_bill", y="tip", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-regplot-1.png](assets/99b1873131479cf9f24377991b06cbdb.jpg)




利用两个定义为 numpy 数组的变量进行绘图; 使用不同的颜色：

```python
>>> import numpy as np; np.random.seed(8)
>>> mean, cov = [4, 6], [(1.5, .7), (.7, 1)]>>> x, y = np.random.multivariate_normal(mean, cov, 80).T
>>> ax = sns.regplot(x=x, y=y, color="g")
```

![http://seaborn.pydata.org/_images/seaborn-regplot-2.png](assets/b6422e805157f85b21973dd3266dcb3f.jpg)




利用两个定义为 pandas Series 的变量来进行绘图; 使用不同的标记：

```python
>>> import pandas as pd
>>> x, y = pd.Series(x, name="x_var"), pd.Series(y, name="y_var")
>>> ax = sns.regplot(x=x, y=y, marker="+")
```

![http://seaborn.pydata.org/_images/seaborn-regplot-3.png](assets/2749fd423c61cc0419daeeec8d8aa467.jpg)




使用 68％置信区间，该区间对应于估计的标准误差：

```python
>>> ax = sns.regplot(x=x, y=y, ci=68)
```

![http://seaborn.pydata.org/_images/seaborn-regplot-4.png](assets/17710001d51c2a58f06feca00a0eaa56.jpg)




使用离散的`x`变量进行绘图并添加一些抖动：

```python
>>> ax = sns.regplot(x="size", y="total_bill", data=tips, x_jitter=.1)
```

![http://seaborn.pydata.org/_images/seaborn-regplot-5.png](assets/823e73942bde25e25637964d2bcd7acf.jpg)




绘制一个离散的`x`变量，显示唯一值的均值和置信区间：

```python
>>> ax = sns.regplot(x="size", y="total_bill", data=tips,
...                  x_estimator=np.mean)
```

![http://seaborn.pydata.org/_images/seaborn-regplot-6.png](assets/b2bb1b6b97e36328f09b122b92dd52bf.jpg)




将连续的变量划分为分离的区间并进行绘图：

```python
>>> ax = sns.regplot(x=x, y=y, x_bins=4)
```

![http://seaborn.pydata.org/_images/seaborn-regplot-7.png](assets/90def53f341cf365a39051cbb1e17f61.jpg)




拟合高阶多项式回归并截断模型预测：

```python
>>> ans = sns.load_dataset("anscombe")
>>> ax = sns.regplot(x="x", y="y", data=ans.loc[ans.dataset == "II"],
...                  scatter_kws={"s": 80},
...                  order=2, ci=None, truncate=True)
```

![http://seaborn.pydata.org/_images/seaborn-regplot-8.png](assets/1eb024fe4ee82e1fd71c47c29ebf1856.jpg)




拟合稳健回归并且不绘制置信区间：

```python
>>> ax = sns.regplot(x="x", y="y", data=ans.loc[ans.dataset == "III"],
...                  scatter_kws={"s": 80},
...                  robust=True, ci=None)
```

![http://seaborn.pydata.org/_images/seaborn-regplot-9.png](assets/83369998db2c4eb1e99c856c538f5cb2.jpg)




对数据运用逻辑回归; 抖动 y 变量并使用较少的 bootstrap 迭代：

```python
>>> tips["big_tip"] = (tips.tip / tips.total_bill) > .175
>>> ax = sns.regplot(x="total_bill", y="big_tip", data=tips,
...                  logistic=True, n_boot=500, y_jitter=.03)
```

![http://seaborn.pydata.org/_images/seaborn-regplot-10.png](assets/b7d4fc0e5dd7fd0d56b558fc3316841a.jpg)




使用 log(x) 拟合回归模型并截断模型预测：

```python
>>> ax = sns.regplot(x="size", y="total_bill", data=tips,
...                  x_estimator=np.mean, logx=True, truncate=True)
```

![http://seaborn.pydata.org/_images/seaborn-regplot-11.png](assets/9c01b014a76320a976b7d86173685435.jpg)




## seaborn.residplot

```python
seaborn.residplot(x, y, data=None, lowess=False, x_partial=None, y_partial=None, order=1, robust=False, dropna=True, label=None, color=None, scatter_kws=None, line_kws=None, ax=None)
```

绘制线性回归的残差。

此函数将在 x 上回归 y（可能作为鲁棒或多项式回归），然后绘制残差的散点图。 你可以选择将局部加权回归散点平滑法（LOWESS）拟合到残差图，这有助于确定残差是否存在结构。

参数：`x`： 向量或字符串

> 预测变量数据中的数据或列名称。

`y`：向量或字符串

> 响应变量的数据中的数据或列名称。

`data`：DataFrame, 可选

> 如果 *x* 和 *y* 是列名，则指定使用的 DataFrame

`lowess`： 布尔值, 可选

> 将局部加权回归散点平滑法（LOWESS）应用到残差散点图中。

`{x, y}_partial`：矩阵或字符串，可选

> 具有与 x 相同的第一维的矩阵或数据中的列名称。这些变量被视为有误的，并在绘制之前从 x 或 y 变量中删除。

`order`：整数，可选

> 计算残差时拟合多项式的阶数。

`robust`：布尔值，可选

> 在计算残差时拟合稳健的线性回归。

`dropna`：布尔值，可选

> 如果为 True，则在拟合和绘图时忽略缺少的数据。

`label`：字符串，可选

> 将在任何图的图例中使用的标签。

`color`：matplotlib 颜色，可选

> 用于绘图的所有元素的颜色。

`{scatter, line}_kws`： 字典，可选

> 用于绘制图像的组件而传递给 scatter() 和 plot() 的其他关键字参数。

`ax`：matplotlib 轴，可选

> 绘制到指定轴对象，否则在当前轴对象上绘图，如果轴不存在则创建一个新轴。


返回值：ax：matplotlib Axes 对象

> 带有回归图像的轴对象



也可以看看

[`regplot`](http://seaborn.pydata.org/generated/seaborn.regplot.html#seaborn.regplot)



绘制一个简单的线性回归模型

[`jointplot`](http://seaborn.pydata.org/generated/seaborn.jointplot.html#seaborn.jointplot)

边际分布。

## seaborn.heatmap

```python
seaborn.heatmap(data, vmin=None, vmax=None, cmap=None, center=None, robust=False, annot=None, fmt='.2g', annot_kws=None, linewidths=0, linecolor='white', cbar=True, cbar_kws=None, cbar_ax=None, square=False, xticklabels='auto', yticklabels='auto', mask=None, ax=None, **kwargs)
```

将矩形数据绘制为颜色编码矩阵。

这是一个坐标轴级的函数，如果没有提供给`ax`参数，它会将热力图绘制到当前活动的轴中。除非`cbar`为 False 或为`cbar_ax`提供单独的 Axes，否则将使用此轴空间的一部分绘制颜色图。

参数：`data`：矩形数据集

> 可以强制转换为 ndarray 格式数据的 2 维数据集。如果提供了 Pandas DataFrame 数据，索引/列信息将用于标记列和行。

`vmin, vmax`：浮点型数据，可选参数。

> 用于锚定色彩映射的值，否则它们是从数据和其他关键字参数推断出来的。

`cmap`：matplotlib 颜色条名称或者对象，或者是颜色列表，可选参数。

> 从数据值到颜色空间的映射。 如果没有提供，默认值将取决于是否设置了“center”。

`center`：浮点数，可选参数。

> 绘制有色数据时将色彩映射居中的值。 如果没有指定，则使用此参数将更改默认的`cmap`。

`robust`：布尔值，可选参数。

> 如果是 True，并且`vmin`或`vmax`为空，则使用稳健分位数而不是极值来计算色彩映射范围。

`annot`:布尔值或者矩形数据，可选参数。

> 如果为 True，则在每个热力图单元格中写入数据值。 如果数组的形状与`data`相同，则使用它来代替原始数据注释热力图。

`fmt`：字符串，可选参数。

> 添加注释时要使用的字符串格式代码。

`annot_kws`：字典或者键值对，可选参数。

> 当`annot`为 True 时，`ax.text`的关键字参数。

`linewidths`：浮点数，可选参数。

> 划分每个单元格的行的宽度。

`linecolor`：颜色，可选参数

> 划分每个单元的线条的颜色。

`cbar`：布尔值，可选参数。

> 描述是否绘制颜色条。

`cbar_kws`：字典或者键值对，可选参数。

> *fig.colorbar*的关键字参数。

`cbar_ax`：matplotlib Axes，可选参数。

> 用于绘制颜色条的轴，否则从主轴获取。

`square`：布尔值，可选参数。

> 如果为 True，则将坐标轴方向设置为“equal”，以使每个单元格为方形。

`xticklabels, yticklabels`：“auto”，布尔值，类列表值，或者整形数值，可选参数。

> 如果为 True，则绘制数据框的列名称。如果为 False，则不绘制列名称。如果是列表，则将这些替代标签绘制为 xticklabels。如果是整数，则使用列名称，但仅绘制每个 n 标签。如果是“auto”，将尝试密集绘制不重叠的标签。

`mask`：布尔数组或者 DataFrame 数据，可选参数。

> 如果为空值，数据将不会显示在`mask`为 True 的单元格中。 具有缺失值的单元格将自动被屏蔽。

`ax`：matplotlib Axes，可选参数。

> 绘制图的坐标轴，否则使用当前活动的坐标轴。

`kwargs`：其他关键字参数。

> 所有其他关键字参数都传递给`ax.pcolormesh`。

返回值：`ax`：matplotlib Axes

> 热力图的轴对象。

**另请参见**

[`clustermap`](seaborn.clustermap.html#seaborn.clustermap "seaborn.clustermap")

使用分层聚类绘制矩阵以排列行和列。

范例

为 numpy 数组绘制热力图：

```python
>>> import numpy as np; np.random.seed(0)
>>> import seaborn as sns; sns.set()
>>> uniform_data = np.random.rand(10, 12)
>>> ax = sns.heatmap(uniform_data)
```

![http://seaborn.pydata.org/_images/seaborn-heatmap-1.png](assets/2dcc622657bb409719bb9c747e0456c4.jpg)


更改默认的 colormap 范围：

```python
>>> ax = sns.heatmap(uniform_data, vmin=0, vmax=1)
```

![http://seaborn.pydata.org/_images/seaborn-heatmap-2.png](assets/6600f3e11ba368f49ec94d562dd39b4a.jpg)


使用发散色图绘制以 0 为中心的数据的热力图：

```python
>>> normal_data = np.random.randn(10, 12)
>>> ax = sns.heatmap(normal_data, center=0)
```

![http://seaborn.pydata.org/_images/seaborn-heatmap-3.png](assets/519a9ac2d3daa6dd6160e4f71bf0e342.jpg)


使用特定的行和列标签绘制 dataframe：

```python
>>> flights = sns.load_dataset("flights")
>>> flights = flights.pivot("month", "year", "passengers")
>>> ax = sns.heatmap(flights)
```

![http://seaborn.pydata.org/_images/seaborn-heatmap-4.png](assets/dd89a846e99f54ceffc01f58b1545cc1.jpg)


使用整数格式的数字值注释每个小单元格：

```python
>>> ax = sns.heatmap(flights, annot=True, fmt="d")
```

![http://seaborn.pydata.org/_images/seaborn-heatmap-5.png](assets/e28bcf9d1f72aedbb18ec128da502f35.jpg)


在每个单元格之间添加线：

```python
>>> ax = sns.heatmap(flights, linewidths=.5)
```

![http://seaborn.pydata.org/_images/seaborn-heatmap-6.png](assets/970cf8333ec7a6b8eec115254ad34265.jpg)


使用不同的 colormap：

```python
>>> ax = sns.heatmap(flights, cmap="YlGnBu")
```

![http://seaborn.pydata.org/_images/seaborn-heatmap-7.png](assets/e18cb02ed3ad1b91b540951f2912539b.jpg)


将 colormap 置于特定值的中心：

```python
>>> ax = sns.heatmap(flights, center=flights.loc["January", 1955])
```

![http://seaborn.pydata.org/_images/seaborn-heatmap-8.png](assets/cf3d8b2d64f574209d01cfe330f3927b.jpg)


绘制每个其他列标签，而不绘制行标签：

```python
>>> data = np.random.randn(50, 20)
>>> ax = sns.heatmap(data, xticklabels=2, yticklabels=False)
```

![http://seaborn.pydata.org/_images/seaborn-heatmap-9.png](assets/a3dadd3fc0e3dcd7aba22164979fb558.jpg)


不绘制颜色条：

```python
>>> ax = sns.heatmap(flights, cbar=False)
```

![http://seaborn.pydata.org/_images/seaborn-heatmap-10.png](assets/be698c1c447b1398ee3d4501aef2415c.jpg)


在不同的坐标轴方向绘制颜色条：

```python
>>> grid_kws = {"height_ratios": (.9, .05), "hspace": .3}>>> f, (ax, cbar_ax) = plt.subplots(2, gridspec_kw=grid_kws)
>>> ax = sns.heatmap(flights, ax=ax,
...                  cbar_ax=cbar_ax,
...                  cbar_kws={"orientation": "horizontal"})
```

![http://seaborn.pydata.org/_images/seaborn-heatmap-11.png](assets/be6577fc41adf407960f66d71436521f.jpg)


使用遮罩绘制矩阵中的一部分

```python
>>> corr = np.corrcoef(np.random.randn(10, 200))
>>> mask = np.zeros_like(corr)
>>> mask[np.triu_indices_from(mask)] = True
>>> with sns.axes_style("white"):
...     ax = sns.heatmap(corr, mask=mask, vmax=.3, square=True)
```

![http://seaborn.pydata.org/_images/seaborn-heatmap-12.png](assets/f683b3bba7ec2d231b917ed55aa858d1.jpg)


## seaborn.clustermap

```python
seaborn.clustermap(data, pivot_kws=None, method='average', metric='euclidean', z_score=None, standard_scale=None, figsize=None, cbar_kws=None, row_cluster=True, col_cluster=True, row_linkage=None, col_linkage=None, row_colors=None, col_colors=None, mask=None, **kwargs)
```

将矩阵数据集绘制成分层聚类热图。

参数：**data：2D array-like**

> 用于聚类的矩形数据，不能包含 NA。

`pivot_kws`：字典，可选。

> 如果数据是整齐的数据框架，可以为 pivot 提供关键字参数以创建矩形数据框架。

`method`：字符串，可选。

> 用于计算聚类的链接方法。有关更多信息，请参阅文档 scipy.cluster.hierarchy.linkage [https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html](https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html)

`metric`：字符串，可选。

> 用于数据的距离度量。有关更多选项，请参阅 scipy.spatial.distance.pdist 文档。 [https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.pdist.html](https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.pdist.html) 要对行和列使用不同的度量（或方法），您可以子集构造每个链接矩阵，并将它们提供为`{row, col}_linkage.`

`z_score`：int 或 None,可选。

> 0（行）或 1（列）。是否计算行或列的 z 分数。Z 得分为 z = (x - mean)/std，因此每行（列）中的值将减去行（列）的平均值，然后除以行（列）的标准偏差。这可确保每行（列）的均值为 0，方差为 1.

`standard_scale`：int 或 None, 可选。

> 0（行）或 1（列）。是否标准化该维度，即每行或每列的含义，减去最小值并将每个维度除以其最大值。

**figsize: 两个整数的元组, 可选。**

> 要创建的图形的大小。

`cbar_kws`：字典, 可选。

> 要传递给`heatmap`中的`cbar_kws`的关键字参数，例如向彩条添加标签。

`{row,col}_cluster`：布尔值, 可选。

> 如果为真，则对{rows, columns}进行聚类。

`{row,col}_linkage`：numpy.array, 可选。

> 行或列的预计算链接矩阵。有关特定格式，请参阅 scipy.cluster.hierarchy.linkage.

`{row,col}_colors`：list-like 或 pandas DataFrame/Series, 可选。

> 要为行或列标记的颜色列表。用于评估组内的样本是否聚集在一起。可以使用嵌套列表或 DataFrame 进行多种颜色级别的标注。如果以 DataFrame 或 Series 形式提供，则从 DataFrames 列名称或 Series 的名称中提取颜色标签。DataFrame/Series 颜色也通过索引与数据匹配，确保以正确的顺序绘制颜色。

`mask`：布尔数组或 DataFrame, 可选。

> 如果通过，数据将不会显示在`mask`为真的单元格中。具有缺失值的单元格将自动被屏蔽。仅用于可视化，不用于计算。

`kwargs`：其他关键字参数。

> 所有其他关键字参数都传递给`sns.heatmap`


返回值：`clustergrid`：ClusterGrid

> ClusterGrid 实例。



注意点：

返回的对象有一个`savefig`方法，如果要保存图形对象而不剪切树形图，则应使用该方法。

要访问重新排序的行索引，请使用：`clustergrid.dendrogram_row.reordered_in`

列索引, 请使用: `clustergrid.dendrogram_col.reordered_ind`

范例

绘制聚类热图。

```python
>>> import seaborn as sns; sns.set(color_codes=True)
>>> iris = sns.load_dataset("iris")
>>> species = iris.pop("species")
>>> g = sns.clustermap(iris)
```

![http://seaborn.pydata.org/_images/seaborn-clustermap-1.png](assets/a7da372ba56ceec7b8b9e01f418bb1e3.jpg)




使用不同的相似性指标。

```python
>>> g = sns.clustermap(iris, metric="correlation")
```



![http://seaborn.pydata.org/_images/seaborn-clustermap-2.png](assets/e102a295ca812e5085369488cedb3dac.jpg)


使用不同的聚类方法。

```python
>>> g = sns.clustermap(iris, method="single")
```

![http://seaborn.pydata.org/_images/seaborn-clustermap-3.png](assets/64c061b1d37ef563dc9827fbb5c671a1.jpg)


使用不同的色彩映射并忽略色彩映射限制中的异常值。

```python
>>> g = sns.clustermap(iris, cmap="mako", robust=True)
```

![http://seaborn.pydata.org/_images/seaborn-clustermap-4.png](assets/b9161032a5adb7fb6694c185246664aa.jpg)


改变图的大小。

```python
>>> g = sns.clustermap(iris, figsize=(6, 7))
```

![http://seaborn.pydata.org/_images/seaborn-clustermap-5.png](assets/f7ed7c41c4e6dbed729d2a3ea8e2d0ff.jpg)


绘制其原始组织中的一个轴。

```python
>>> g = sns.clustermap(iris, col_cluster=False)
```

![http://seaborn.pydata.org/_images/seaborn-clustermap-6.png](assets/3acbff477d462457ef54fe167311c30e.jpg)


添加彩色标签。

```python
>>> lut = dict(zip(species.unique(), "rbg"))
>>> row_colors = species.map(lut)
>>> g = sns.clustermap(iris, row_colors=row_colors)
```

![http://seaborn.pydata.org/_images/seaborn-clustermap-7.png](assets/da7ff8019c2f5f473c8c8958fd395e76.jpg)


标准化列中的数据。

```python
>>> g = sns.clustermap(iris, standard_scale=1)
```

![http://seaborn.pydata.org/_images/seaborn-clustermap-8.png](assets/4c9cb45bac2fd0bc0e8ebf7b6411ecc9.jpg)


正规化行内数据。

```python
>>> g = sns.clustermap(iris, z_score=0)
```

![http://seaborn.pydata.org/_images/seaborn-clustermap-9.png](assets/5e7f4bf9cb93adb18b8275b2e6477182.jpg)


## seaborn.FacetGrid

```python
class seaborn.FacetGrid(data, row=None, col=None, hue=None, col_wrap=None, sharex=True, sharey=True, height=3, aspect=1, palette=None, row_order=None, col_order=None, hue_order=None, hue_kws=None, dropna=True, legend_out=True, despine=True, margin_titles=False, xlim=None, ylim=None, subplot_kws=None, gridspec_kws=None, size=None)
```

用于绘制条件关系的多图网格。

```python
__init__(data, row=None, col=None, hue=None, col_wrap=None, sharex=True, sharey=True, height=3, aspect=1, palette=None, row_order=None, col_order=None, hue_order=None, hue_kws=None, dropna=True, legend_out=True, despine=True, margin_titles=False, xlim=None, ylim=None, subplot_kws=None, gridspec_kws=None, size=None)
```

初始化 matplotlib 画布和 FacetGrid 对象。

该类将数据集映射到由行和列组成的网格中的多个轴上，这些轴与数据集中变量的级别对应。它产生的图通常被称为“lattice”，“trellis”或“small-multiple”图形。

它还可以用`hue`参数表示第三个变量的级别，该参数绘制不同颜色的不同数据子集。它使用颜色来解析第三维度上的元素，但是只绘制相互重叠的子集，并且不会像接受“hue”的坐标轴级函数那样为特定的可视化定制“hue”参数。

当使用从数据集推断语义映射的 seaborn 函数时，必须注意在各个方面之间同步这些映射。在大多数情况下，使用图形级函数（例如[`relplot()`](seaborn.relplot.html#seaborn.relplot "seaborn.relplot")或[`catplot()`](seaborn.catplot.html#seaborn.catplot "seaborn.catplot")）比直接使用[`FacetGrid`](seaborn.FacetGrid.html#seaborn.FacetGrid "seaborn.FacetGrid")更好。

基本工作流程是使用数据集和用于构造网格的变量初始化 FacetGrid 对象。然后，通过调用[`FacetGrid.map()`](seaborn.FacetGrid.map.html#seaborn.FacetGrid.map "seaborn.FacetGrid.map")或[`FacetGrid.map_dataframe()`](seaborn.FacetGrid.map_dataframe.html#seaborn.FacetGrid.map_dataframe "seaborn.FacetGrid.map_dataframe")，可以将一个或多个绘图函数应用于每个子集。最后，可以使用其他方法调整绘图，以执行更改轴标签、使用不同刻度或添加图例等操作。有关详细信息，请参阅下面的详细代码示例。

更多相关信息请参阅[`教程`](http://seaborn.pydata.org/tutorial/axis_grids.html#grid-tutorial)。

参数：`data`：DataFrame 数据。

> 整洁的（“长形式”）dataframe 数据，其中每一列是一个变量，每一行是一个观察实例。

`row, col, hue`：字符串。

> 定义数据子集的变量，这些变量将在网格的不同方面绘制。请参阅`*_order`参数以控制此变量的级别顺序。

`col_wrap`：整形数值，可选参数。

> 以此参数值来限制网格的列维度，以便列面跨越多行。与`row`面不兼容。

`share{x,y}`：布尔值，'col' 或 'row'可选

> 如果为 true，则跨列共享 y 轴或者跨行共享 x 轴。

`height`：标量，可选参数。

> 每个图片的高度设定（以英寸为单位）。另见：*aspect*

`aspect`：标量，可选参数。

> 每个图片的纵横比，因此 aspect * height 给出每个图片的宽度，单位为英寸。

`palette`：调色板名称，列表或字典，可选参数。

> 用于色调变量的不同级别的颜色。应为[`color_palette()`](seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette")可以解释的参数，或者是将色调级别映射到 matplotlib 颜色的字典。

`{row,col,hue}_order`：列表，可选参数。

> 对所给命令级别进行排序。默认情况下，这将是在数据中显示的级别，或者，如果变量是 pandas 分类，则为类别顺序。

`hue_kws`：参数-列表值的映射字典

> 插入到绘图调用中的其他关键字参数，使得其他绘图属性在色调变量的级别上有所不同（例如散点图中的标记）。

`legend_out`：布尔值，可选参数。

> 如果为 True，则图形尺寸将被扩展，图例将绘制在中间右侧的图形之外。

`despine`：布尔值，可选参数。

> 从图中移除顶部和右侧边缘框架。

`margin_titles`：布尔值，可选参数。

> 如果为 True，则行变量的标题将绘制在最后一列的右侧。此选项是实验性的，可能无法在所有情况下使用。

`{x, y}lim`：元组，可选参数。

> 每个图片上每个轴的限制（仅当 share {x，y}为 True 时才相关）。

`subplot_kws`：字典，可选参数。

> 传递给 matplotlib subplot（s）方法的关键字参数字典。

`gridspec_kws`：字典，可选参数。

> 传递给 matplotlib 的`gridspec`模块（通过`plt.subplots`）的关键字参数字典。需要 matplotlib> = 1.4，如果`col_wrap`不是`None`，则忽略它。

**另请参见**

用于绘制成对关系的子图网格。

[`relplot`](#seaborn.relplot "seaborn.relplot")

结合关系图和[`FacetGrid`](#seaborn.FacetGrid "seaborn.FacetGrid")。

[`catplot`](#seaborn.catplot "seaborn.catplot")

结合分类图和[`FacetGrid`](#seaborn.FacetGrid "seaborn.FacetGrid")。

[`lmplot`](#seaborn.lmplot "seaborn.lmplot")

结合回归图和[`FacetGrid`](#seaborn.FacetGrid "seaborn.FacetGrid")。

范例

使用 tips 数据集初始化 2x2 网格图：

```python
>>> import seaborn as sns; sns.set(style="ticks", color_codes=True)
>>> tips = sns.load_dataset("tips")
>>> g = sns.FacetGrid(tips, col="time", row="smoker")
```

![http://seaborn.pydata.org/_images/seaborn-FacetGrid-1.png](assets/b8699392ad92687d3ac264d00b00ec9b.jpg)


在每个子图绘制一个单变量图：

```python
>>> import matplotlib.pyplot as plt
>>> g = sns.FacetGrid(tips, col="time",  row="smoker")
>>> g = g.map(plt.hist, "total_bill")
```

![http://seaborn.pydata.org/_images/seaborn-FacetGrid-2.png](assets/ee99e47d6e0d2262037bca2e7fdb9772.jpg)


（注意，没有必要重新捕获返回的变量;它是相同的对象，但在示例中这样做使得处理 doctests 更加方便）。

将其他关键字参数传递给映射函数：

```python
>>> import numpy as np
>>> bins = np.arange(0, 65, 5)
>>> g = sns.FacetGrid(tips, col="time",  row="smoker")
>>> g = g.map(plt.hist, "total_bill", bins=bins, color="r")
```

![http://seaborn.pydata.org/_images/seaborn-FacetGrid-3.png](assets/7bd763d402e774603d6f5e7c48c2369a.jpg)


在每个子图绘制一个双变量函数：

```python
>>> g = sns.FacetGrid(tips, col="time",  row="smoker")
>>> g = g.map(plt.scatter, "total_bill", "tip", edgecolor="w")
```

![http://seaborn.pydata.org/_images/seaborn-FacetGrid-4.png](assets/2aca4924009e92a55bc1579fc086d36c.jpg)



将其中一个变量分配给绘图元素的颜色：

```python
>>> g = sns.FacetGrid(tips, col="time",  hue="smoker")
>>> g = (g.map(plt.scatter, "total_bill", "tip", edgecolor="w")
...       .add_legend())
```

![http://seaborn.pydata.org/_images/seaborn-FacetGrid-5.png](assets/c53a49f5b838d50776627fc8910138ce.jpg)


更改每个子图的高度和纵横比：

```python
>>> g = sns.FacetGrid(tips, col="day", height=4, aspect=.5)
>>> g = g.map(plt.hist, "total_bill", bins=bins)
```

![http://seaborn.pydata.org/_images/seaborn-FacetGrid-6.png](assets/ca19289fa3db7f35bf27fa3f09db128e.jpg)


指定绘图元素的顺序：

```python
>>> g = sns.FacetGrid(tips, col="smoker", col_order=["Yes", "No"])
>>> g = g.map(plt.hist, "total_bill", bins=bins, color="m")
```

![http://seaborn.pydata.org/_images/seaborn-FacetGrid-7.png](assets/d84004bd86dc3645488324a3fbb3b060.jpg)


使用不同的调色板：

```python
>>> kws = dict(s=50, linewidth=.5, edgecolor="w")
>>> g = sns.FacetGrid(tips, col="sex", hue="time", palette="Set1",
...                   hue_order=["Dinner", "Lunch"])
>>> g = (g.map(plt.scatter, "total_bill", "tip", **kws)
...      .add_legend())
```

![http://seaborn.pydata.org/_images/seaborn-FacetGrid-8.png](assets/fe9b97ad8c3e66c3a2514249fcd62ee6.jpg)


使用字典将色调级别映射到颜色：

```python
>>> pal = dict(Lunch="seagreen", Dinner="gray")
>>> g = sns.FacetGrid(tips, col="sex", hue="time", palette=pal,
...                   hue_order=["Dinner", "Lunch"])
>>> g = (g.map(plt.scatter, "total_bill", "tip", **kws)
...      .add_legend())
```

![http://seaborn.pydata.org/_images/seaborn-FacetGrid-9.png](assets/9299602c8a3b49e2a9095ad9ea69f07f.jpg)


另外，为色调级别使用不同的标记：

```python
>>> g = sns.FacetGrid(tips, col="sex", hue="time", palette=pal,
...                   hue_order=["Dinner", "Lunch"],
...                   hue_kws=dict(marker=["^", "v"]))
>>> g = (g.map(plt.scatter, "total_bill", "tip", **kws)
...      .add_legend())
```

![http://seaborn.pydata.org/_images/seaborn-FacetGrid-10.png](assets/92494d907a4bd738426ff755349e4992.jpg)


将包含多个级别的列变量“换行”到行中：

```python
>>> att = sns.load_dataset("attention")
>>> g = sns.FacetGrid(att, col="subject", col_wrap=5, height=1.5)
>>> g = g.map(plt.plot, "solutions", "score", marker=".")
```

![http://seaborn.pydata.org/_images/seaborn-FacetGrid-11.png](assets/3a3de5e84041d929dbf5cbade67f93e6.jpg)


定义一个自定义双变量函数来映射到网格：

```python
>>> from scipy import stats
>>> def qqplot(x, y, **kwargs):
...     _, xr = stats.probplot(x, fit=False)
...     _, yr = stats.probplot(y, fit=False)
...     plt.scatter(xr, yr, **kwargs)
>>> g = sns.FacetGrid(tips, col="smoker", hue="sex")
>>> g = (g.map(qqplot, "total_bill", "tip", **kws)
...       .add_legend())
```

![http://seaborn.pydata.org/_images/seaborn-FacetGrid-12.png](assets/a84563a49e5578ddeff33795280887f1.jpg)


定义一个使用`DataFrame`对象的自定义函数，并接受列名作为位置变量：

```python
>>> import pandas as pd
>>> df = pd.DataFrame(...     data=np.random.randn(90, 4),
...     columns=pd.Series(list("ABCD"), name="walk"),
...     index=pd.date_range("2015-01-01", "2015-03-31",
...                         name="date"))
>>> df = df.cumsum(axis=0).stack().reset_index(name="val")
>>> def dateplot(x, y, **kwargs):
...     ax = plt.gca()
...     data = kwargs.pop("data")
...     data.plot(x=x, y=y, ax=ax, grid=False, **kwargs)
>>> g = sns.FacetGrid(df, col="walk", col_wrap=2, height=3.5)
>>> g = g.map_dataframe(dateplot, "date", "val")
```

![http://seaborn.pydata.org/_images/seaborn-FacetGrid-13.png](assets/2017a8a703bdcc077baea030a758721d.jpg)


绘图后使用不同的轴标签：

```python
>>> g = sns.FacetGrid(tips, col="smoker", row="sex")
>>> g = (g.map(plt.scatter, "total_bill", "tip", color="g", **kws)
...       .set_axis_labels("Total bill (US Dollars)", "Tip"))
```

![http://seaborn.pydata.org/_images/seaborn-FacetGrid-14.png](assets/52d5efab2c08ec72873b3e66fea66d14.jpg)


设置每个子图共享的其他属性：

```python
>>> g = sns.FacetGrid(tips, col="smoker", row="sex")
>>> g = (g.map(plt.scatter, "total_bill", "tip", color="r", **kws)
...       .set(xlim=(0, 60), ylim=(0, 12),
...            xticks=[10, 30, 50], yticks=[2, 6, 10]))
```

![http://seaborn.pydata.org/_images/seaborn-FacetGrid-15.png](assets/308a2f3dd32f77b25254d6650cce7be8.jpg)


为子图标题使用不同的模板：

```python
>>> g = sns.FacetGrid(tips, col="size", col_wrap=3)
>>> g = (g.map(plt.hist, "tip", bins=np.arange(0, 13), color="c")
...       .set_titles("{col_name} diners"))
```

![http://seaborn.pydata.org/_images/seaborn-FacetGrid-16.png](assets/b68bba1b9871f75b818870d08a9e3523.jpg)


收紧每个子图:

```python
>>> g = sns.FacetGrid(tips, col="smoker", row="sex",
...                   margin_titles=True)
>>> g = (g.map(plt.scatter, "total_bill", "tip", color="m", **kws)
...       .set(xlim=(0, 60), ylim=(0, 12),
...            xticks=[10, 30, 50], yticks=[2, 6, 10])
...       .fig.subplots_adjust(wspace=.05, hspace=.05))
```

![http://seaborn.pydata.org/_images/seaborn-FacetGrid-17.png](assets/30bd1c7aa657fabb90851a094c8d0a1e.jpg)



|方法|属性|
|------|----|
| [`__init__`](#seaborn.FacetGrid.__init__ seaborn.FacetGrid.__init__")(data[, row, col, hue, col_wrap, …]) | 初始化 matplotlib 画布和 FacetGrid 对象。 |
| `add_legend`([legend_data, title, label_order]) | 绘制一个图例，可能将其放在轴外并调整图形大小。|
| `despine`(\**kwargs) | 从子图中移除轴的边缘框架。 |
| `facet_axis`(row_i, col_j) | 使这些索引识别的轴处于活动状态并返回。 |
| `facet_data`() | 用于每个子图的名称索引和数据子集的生成器。 |
| [`map`](seaborn.FacetGrid.map.html#seaborn.FacetGrid.map "seaborn.FacetGrid.map")(func, *args, \**kwargs) | 将绘图功能应用于每个子图的数据子集。 |
| [`map_dataframe`](seaborn.FacetGrid.map_dataframe.html#seaborn.FacetGrid.map_dataframe "seaborn.FacetGrid.map_dataframe")(func, *args, \**kwargs) | 像`.map`一样，但是将 args 作为字符串传递并在 kwargs 中插入数据。 |
| `savefig`(*args, \**kwargs) | 保存图片。 |
| `set`(\**kwargs) | 在每个子图集坐标轴上设置属性。|
| `set_axis_labels`([x_var, y_var]) | 在网格的左列和底行设置轴标签。 |
| `set_titles`([template, row_template, …]) | 在每个子图上方或网格边缘绘制标题。 |
| `set_xlabels`([label]) | 在网格的底行标记 x 轴。 |
| `set_xticklabels`([labels, step]) | 在网格的底行设置 x 轴刻度标签。 |
| `set_ylabels`([label]) | 在网格的左列标记 y 轴。 |
| `set_yticklabels`([labels]) | 在网格的左列上设置 y 轴刻度标签。 |
| `ax` | 轻松访问单个坐标轴。 |

## seaborn.FacetGrid.map

```python
FacetGrid.map(func, *args, **kwargs)
```

将绘图函数应用于每个方面的数据子集。

参数：`func`：可调用

> 一个接受数据和关键字参数的绘图函数。它必须绘制到当前活动的 matplotlib 轴并采用`color`关键字参数。如果在`hue`维度上进行分面，必须也使用`label`关键字参数。

`args`：字符串

> 数据的列名，用于标识要绘制数据的变量。每个变量的数据按照调用中指定变量的顺序传递给`func`。

`kwargs`：关键字参数

> 所有的关键字参数都被传递给绘图函数。


返回值：`self`：对象

> 返回自身。


## seaborn.FacetGrid.map_dataframe

```python
FacetGrid.map_dataframe(func, *args, **kwargs)
```

和 `.map`类似，但是将 args 作为字符串传递并将数据插入到 kwargs 中.

此方法适用于使用接受长格式 DataFrame 作为`data`关键字参数并使用字符串变量名访问该 DataFrame 中的数据的函数进行绘图。

参数：`func`：可调用

> 一个接受数据和关键字参数的绘图函数。与`map`方法不同,此处使用的函数必须“理解”Pandas 对象。它也必须绘制到当前活动的 matpltolib 轴并采用`color`关键字参数。如果在<cite>hue</cite>维度上进行分面，必须也使用<cite>label</cite>关键字参数。

`args`：字符串

> 数据的列名，用于标识要绘制数据的变量。每个变量的数据按照调用中指定变量的顺序传递给<cite>func</cite>。

`kwargs`：关键字参数

> 所有的关键字参数被传递给绘图函数。


返回值：`self`：对象

> 返回自身。


## seaborn.PairGrid

```python
class seaborn.PairGrid(data, hue=None, hue_order=None, palette=None, hue_kws=None, vars=None, x_vars=None, y_vars=None, diag_sharey=True, height=2.5, aspect=1, despine=True, dropna=True, size=None)
```

用于绘制数据集中成对关系的子图网格。

此类将数据集中的每个变量映射到多个轴的网格中的列和行。可以使用不同的轴级绘图函数来绘制上三角和下三角的双变量图，并且对角线上可以显示每个变量的边际分布。 

它还可以通过`hue`参数用不同颜色绘制不同的数据子集来表示附加级别的条件化。这使用颜色来解析第三维的元素，但只是在彼此之上绘制子集，并且不会像接受`hue`的轴级函数那样为特定可视化定制`hue`参数。

参考[教程](http://seaborn.pydata.org/tutorial/axis_grids.html#grid-tutorial)获取更多信息。

```python
__init__(data, hue=None, hue_order=None, palette=None, hue_kws=None, vars=None, x_vars=None, y_vars=None, diag_sharey=True, height=2.5, aspect=1, despine=True, dropna=True, size=None)
```

初始化绘图和 PairGrid 对象。

参数：`data`：DataFrame 格式

> 整洁（长形式）数据框，其中每列是一个变量，每行是一个观察。

`hue`：字符串 （变量名）, 可选

> `data`中的变量，将绘图的不同面映射为不同的颜色。

`hue_order`：字符串列表

> 调色板中色调变量的等级顺序

`palette`：字典或者 seaborn 调色板

> 用于映射`hue`变量的颜色集.如果是一个字典，键应为`hue`变量中的值。

`hue_kws`：参数字典 -&gt; 值列表映射

> 其它的关键字参数，通过插入到绘图调用中使得其它的绘图属性在色调变量的不同水平上变化（例如散点图中的标记）。

`vars`：变量名列表, 可选

> 使用`data`中的变量，否则使用一个数值型数据类型的每一列。

`{x, y}_vars`：变量名列表，可选

> 将`data`中的变量分别用于图的行和列，即制作非方形图。

`height`：标量，可选

> 每个刻面的高度（以英寸为单位）。

`aspect`：标量，可选

> aspect 和 height 的乘积得出每个刻面的宽度（以英寸为单位）。

`despine`：布尔值，可选

> 从图中移除顶部和右侧脊柱。

`dropna`：布尔值，可选

> 在绘图之前删除数据中的缺失值。



另见

轻松绘制[`PairGrid`](#seaborn.PairGrid "seaborn.PairGrid")的常用用法。用于绘制条件关系的子图网格。

例子

为每个成对关系绘制一个散点图：

```python
>>> import matplotlib.pyplot as plt
>>> import seaborn as sns; sns.set()
>>> iris = sns.load_dataset("iris")
>>> g = sns.PairGrid(iris)
>>> g = g.map(plt.scatter)
```

![http://seaborn.pydata.org/_images/seaborn-PairGrid-1.png](assets/34fc6de3cfc117757cc0e5f658a06928.jpg)


在对角线上显示单变量分布：

```python
>>> g = sns.PairGrid(iris)
>>> g = g.map_diag(plt.hist)
>>> g = g.map_offdiag(plt.scatter)
```

![http://seaborn.pydata.org/_images/seaborn-PairGrid-2.png](assets/7e2b84efc57efecba1f8c1bfad570874.jpg)


(实际上没有必要每次都获取返回值，因为它是同一个对象，但它使得更容易处理文档测试)。

使用分类变量对点进行着色：

```python
>>> g = sns.PairGrid(iris, hue="species")
>>> g = g.map_diag(plt.hist)
>>> g = g.map_offdiag(plt.scatter)
>>> g = g.add_legend()
```

![http://seaborn.pydata.org/_images/seaborn-PairGrid-3.png](assets/6df84055fcde05fec3e0b9794967ad71.jpg)


使用不同的样式显示多个直方图：

```python
>>> g = sns.PairGrid(iris, hue="species")
>>> g = g.map_diag(plt.hist, histtype="step", linewidth=3)
>>> g = g.map_offdiag(plt.scatter)
>>> g = g.add_legend()
```

![http://seaborn.pydata.org/_images/seaborn-PairGrid-4.png](assets/b269cdc188b02e86196c919d1bd87402.jpg)


绘制变量的子集

```python
>>> g = sns.PairGrid(iris, vars=["sepal_length", "sepal_width"])
>>> g = g.map(plt.scatter)
```

![http://seaborn.pydata.org/_images/seaborn-PairGrid-5.png](assets/81ab9afd2faa6448de3c2b5a73ea3320.jpg)


将其它关键字参数传给函数。

```python
>>> g = sns.PairGrid(iris)
>>> g = g.map_diag(plt.hist, edgecolor="w")
>>> g = g.map_offdiag(plt.scatter, edgecolor="w", s=40)
```

![http://seaborn.pydata.org/_images/seaborn-PairGrid-6.png](assets/ff58dc3588f11a9e3f9291926a0c86ff.jpg)


对行和列使用不同的变量：

```python
>>> g = sns.PairGrid(iris,
...                  x_vars=["sepal_length", "sepal_width"],
...                  y_vars=["petal_length", "petal_width"])
>>> g = g.map(plt.scatter)
```

![http://seaborn.pydata.org/_images/seaborn-PairGrid-7.png](assets/0d8364e9bb731fa5d89b998087ba0635.jpg)


在上三角和下三角使用不同的函数：

```python
>>> g = sns.PairGrid(iris)
>>> g = g.map_upper(plt.scatter)
>>> g = g.map_lower(sns.kdeplot, cmap="Blues_d")
>>> g = g.map_diag(sns.kdeplot, lw=3, legend=False)
```

![http://seaborn.pydata.org/_images/seaborn-PairGrid-8.png](assets/58e8af844e6f097dc5995d7a2be70b0e.jpg)


为每个分类级别使用不同的颜色和标记：

```python
>>> g = sns.PairGrid(iris, hue="species", palette="Set2",
...                  hue_kws={"marker": ["o", "s", "D"]})
>>> g = g.map(plt.scatter, linewidths=1, edgecolor="w", s=40)
>>> g = g.add_legend()
```

![http://seaborn.pydata.org/_images/seaborn-PairGrid-9.png](assets/c9a2068151affcefc99a7b280ab0a52b.jpg)



|   方法   |   属性   |
| ---- | ---- |
|    `__init__`(data[, hue, hue_order, palette, …])   |   初始化绘图和 PairGrid 对象。   |
|   `add_legend`([legend_data, title, label_order])   |   绘制一个图例，可能将其放在轴外并调整图形大小。   |
| [`map`](seaborn.PairGrid.map.html#seaborn.PairGrid.map "seaborn.PairGrid.map")(func, `**kwargs`) | 在每一个子图上用相同的函数绘制。  |
| [`map_diag`](seaborn.PairGrid.map_diag.html#seaborn.PairGrid.map_diag "seaborn.PairGrid.map_diag")(func, `**kwargs`) | 在每个对角线子图上用一个单变量函数绘制。 |
| [`map_lower`](seaborn.PairGrid.map_lower.html#seaborn.PairGrid.map_lower "seaborn.PairGrid.map_lower")(func, `**kwargs`) | 在下三角子图上用一个双变量函数绘制。|
| [`map_offdiag`](seaborn.PairGrid.map_offdiag.html#seaborn.PairGrid.map_offdiag "seaborn.PairGrid.map_offdiag")(func, `**kwargs`) | 在非对角线子图上用一个双变量函数绘制。 |
| [`map_upper`](seaborn.PairGrid.map_upper.html#seaborn.PairGrid.map_upper "seaborn.PairGrid.map_upper")(func, `**kwargs`) | 在上三角子图上用一个双变量函数绘制。|
| `savefig`(*args, `**kwargs`) | 保存图。 |
| `set`(`**kwargs`) | 在每个子图轴上设置属性。 |

## seaborn.PairGrid.map

```python
PairGrid.map(func, **kwargs)
```

在每个子图中用相同的函数绘制。

参数：`func`：可调用的绘图函数

> 必须将 x,y 数组作为位置参数并绘制到“当前活动”的 matplotlib 轴上。还需要接受名为`color`和`label`的 kwargs。


## seaborn.PairGrid.map_diag

```python
PairGrid.map_diag(func, **kwargs)
```

在每一个对角线子图上用一个单变量函数绘制。

参数：`func`：可调用的绘图函数

>必须将 x,y 数组作为位置参数并绘制到“当前活动”的 matplotlib 轴上。还需要接受名为 color 和 label 的 kwargs。


## seaborn.PairGrid.map_offdiag

```python
PairGrid.map_offdiag(func, **kwargs)
```

在非对角线子图上用一个双变量函数绘图。

参数：`func`：可调用的绘图函数

> 必须将 x,y 数组作为位置参数并绘制到“当前活动”的 matplotlib 轴上。还需要接受名为 color 和 label 的 kwargs。


## seaborn.PairGrid.map_lower

```python
PairGrid.map_lower(func, **kwargs)
```

在下对角线子图上用一个双变量函数绘图。

参数：`func`：可调用的绘图函数

> 必须将 x,y 数组作为位置参数并绘制到“当前活动”的 matplotlib 轴上。还需要接受名为`color`和`label`的 kwargs。


## seaborn.PairGrid.map_upper

```python
PairGrid.map_upper(func, **kwargs)
```

在上对角线子图上用一个双变量函数绘图。

参数：`func`：可调用的绘图函数

> 必须将 x,y 数组作为位置参数并绘制到“当前活动”的 matplotlib 轴上。还需要接受名为 color 和 label 的 kwargs。


## seaborn.JointGrid

```python
class seaborn.JointGrid(x, y, data=None, height=6, ratio=5, space=0.2, dropna=True, xlim=None, ylim=None, size=None)
```

用于绘制具有边际单变量图的双变量图的网格。

```python
__init__(x, y, data=None, height=6, ratio=5, space=0.2, dropna=True, xlim=None, ylim=None, size=None)
```

设置子图的网格。

参数：`x, y`：字符串或向量

> 在 `data`中的数据或变量名

`data`：DataFrame, 可选

> 当 `x` and `y` 是变量名的时候为 DataFrame。

`height`：数字

> 图中每一条边的大小（以英寸为单位）

`ratio`：数字

> 联合轴大小与边缘轴高度的比率。

`space`：数字，可选

> 联合轴和边缘轴之间的空间

`dropna`：bool, 可选

> 如果为 True，则删除 `x` 和 `y`中缺少的观察结果。

`{x, y}lim`：二元组，可选

> 在绘图之前设置轴限制。

也可以看看

用于绘制具有多种不同默认绘图类型的双变量图的高级界面。

例子：

初始化图形，但不在其上绘制任何图形：

```python
>>> import seaborn as sns; sns.set(style="ticks", color_codes=True)
>>> tips = sns.load_dataset("tips")
>>> g = sns.JointGrid(x="total_bill", y="tip", data=tips)
```

![http://seaborn.pydata.org/_images/seaborn-JointGrid-1.png](assets/a0e79dac9add2a97da1c95241a6122ab.jpg)


使用默认参数添加绘图：

```python
>>> g = sns.JointGrid(x="total_bill", y="tip", data=tips)
>>> g = g.plot(sns.regplot, sns.distplot)
```

![http://seaborn.pydata.org/_images/seaborn-JointGrid-2.png](assets/f984c858bd63441ea9761d632cb76d2c.jpg)


分别绘制联合分布图和边缘直方图，这可以以更精细的级别控制其他参数：

```python
>>> import matplotlib.pyplot as plt
>>> g = sns.JointGrid(x="total_bill", y="tip", data=tips)
>>> g = g.plot_joint(plt.scatter, color=".5", edgecolor="white")
>>> g = g.plot_marginals(sns.distplot, kde=False, color=".5")
```

![http://seaborn.pydata.org/_images/seaborn-JointGrid-3.png](assets/3e159b4a38edb79ede76d93a55e2acb9.jpg)


分别绘制两个边缘直方图：

```python
>>> import numpy as np
>>> g = sns.JointGrid(x="total_bill", y="tip", data=tips)
>>> g = g.plot_joint(plt.scatter, color="m", edgecolor="white")
>>> _ = g.ax_marg_x.hist(tips["total_bill"], color="b", alpha=.6,
...                      bins=np.arange(0, 60, 5))
>>> _ = g.ax_marg_y.hist(tips["tip"], color="r", alpha=.6,
...                      orientation="horizontal",
...                      bins=np.arange(0, 12, 1))
```

![http://seaborn.pydata.org/_images/seaborn-JointGrid-4.png](assets/1db698012d05626321ac93ffb7668a2c.jpg)


添加注释，其中包含总结双变量关系的统计信息：

```python
>>> from scipy import stats
>>> g = sns.JointGrid(x="total_bill", y="tip", data=tips)
>>> g = g.plot_joint(plt.scatter,
...                  color="g", s=40, edgecolor="white")
>>> g = g.plot_marginals(sns.distplot, kde=False, color="g")
>>> g = g.annotate(stats.pearsonr)
```

![http://seaborn.pydata.org/_images/seaborn-JointGrid-5.png](assets/fa99a0a13450712a4f2b13d983b1e766.jpg)


使用自定义的函数和注释格式

```python
>>> g = sns.JointGrid(x="total_bill", y="tip", data=tips)
>>> g = g.plot_joint(plt.scatter,
...                  color="g", s=40, edgecolor="white")
>>> g = g.plot_marginals(sns.distplot, kde=False, color="g")
>>> rsquare = lambda a, b: stats.pearsonr(a, b)[0] ** 2
>>> g = g.annotate(rsquare, template="{stat}: {val:.2f}",
...                stat="$R^2$", loc="upper left", fontsize=12)
```

![http://seaborn.pydata.org/_images/seaborn-JointGrid-6.png](assets/a8307f7ba7809b63c523168fde9e9379.jpg)


移除联合轴和边缘轴之间的空间：

```python
>>> g = sns.JointGrid(x="total_bill", y="tip", data=tips, space=0)
>>> g = g.plot_joint(sns.kdeplot, cmap="Blues_d")
>>> g = g.plot_marginals(sns.kdeplot, shade=True)
```

![http://seaborn.pydata.org/_images/seaborn-JointGrid-7.png](assets/5beaabfceb79e2eef9563fc3044dd5f6.jpg)


绘制具有相对较大边缘轴的较小图：

```python
>>> g = sns.JointGrid(x="total_bill", y="tip", data=tips,
...                   height=5, ratio=2)
>>> g = g.plot_joint(sns.kdeplot, cmap="Reds_d")
>>> g = g.plot_marginals(sns.kdeplot, color="r", shade=True)
```

![http://seaborn.pydata.org/_images/seaborn-JointGrid-8.png](assets/bfc4c60af4e09992569375d51943de88.jpg)


设置轴的限制：

```python
>>> g = sns.JointGrid(x="total_bill", y="tip", data=tips,
...                   xlim=(0, 50), ylim=(0, 8))
>>> g = g.plot_joint(sns.kdeplot, cmap="Purples_d")
>>> g = g.plot_marginals(sns.kdeplot, color="m", shade=True)
```

![http://seaborn.pydata.org/_images/seaborn-JointGrid-9.png](assets/3a85305cd59104b4d9403deb570373cc.jpg)


方法

[`__init__`](#seaborn.JointGrid.__init__ "seaborn.JointGrid.__init__")(x, y[, data, height, ratio, space, …]) | 设置子图的网格设置子图的网格。

`annotate`(func[, template, stat, loc]) | 用关于关系的统计数据来标注绘图。

[`plot`](seaborn.JointGrid.plot.html#seaborn.JointGrid.plot "seaborn.JointGrid.plot")(joint_func, marginal_func[, annot_func]) | 绘制完整绘图的快捷方式。

 [`plot_joint`](seaborn.JointGrid.plot_joint.html#seaborn.JointGrid.plot_joint "seaborn.JointGrid.plot_joint")(func, **kwargs) | 绘制 `x` 和 `y`的双变量图。

[`plot_marginals`](seaborn.JointGrid.plot_marginals.html#seaborn.JointGrid.plot_marginals "seaborn.JointGrid.plot_marginals")(func, **kwargs) | 分别绘制 `x` 和 `y` 的单变量图。

 `savefig`(*args, **kwargs) | 封装 figure.savefig 默认为紧边界框。

`set_axis_labels`([xlabel, ylabel]) |在双变量轴上设置轴标签。

## seaborn.JointGrid.plot



```python
JointGrid.plot(joint_func, marginal_func, annot_func=None)
```

绘制完整绘图的快捷方式。

直接使用 `plot_joint` 和 `plot_marginals` 进行更多控制.

参数：**joint_func, marginal_func：可调用**

> 绘制双变量和单变量图的函数。

返回值：`self`：JointGrid 实例

> 返回 `self`.

## seaborn.JointGrid.plot_joint



```python
JointGrid.plot_joint(func, **kwargs)
```

绘制 `x` and `y`的双变量图。

参数：`func`：可调用的绘图函数

> 这必须将两个一维数据数组作为前两个位置参数，并且必须在“当前”轴上绘制。

`kwargs`：键，值映射

> 关键字参数传递给绘图函数。

返回值：`self`：JointGrid 实例

> 返回 `self`.

## seaborn.JointGrid.plot_marginals



```python
JointGrid.plot_marginals(func, **kwargs)
```

分别绘制 `x` 和 `y` 的单变量图。

参数：`func`：可调用的绘图函数

> 这必须将一维数据数组作为第一个位置参数，它必须在“当前”轴上绘图，并且它必须接受“垂直”关键字参数以垂直定向图的度量维度。

`kwargs`：键，值映射

> 关键字参数传递给绘图函数。

返回值：`self`：JointGrid 实例

> 返回 `self`.

## seaborn.set



```python
seaborn.set(context='notebook', style='darkgrid', palette='deep', font='sans-serif', font_scale=1, color_codes=True, rc=None)
```

一步设定自定义图表参数。

每个参数可以被直接或者间接设定，参见下方的引用参数以获取更多信息。

参数：context：字符串或者字典

> 绘图上下文参数，参见[`plotting_context()`](https://github.com/apachecn/seaborn-doc-zh/blob/master/docs/seaborn.plotting_context.html#seaborn.plotting_context)

style：字符串或者字典

> 坐标轴样式参数，参见[`axes_style()`](https://github.com/apachecn/seaborn-doc-zh/blob/master/docs/seaborn.axes_style.html#seaborn.axes_style)

palette：字符串或者序列

> 调色板，参见[`color_palette()`](https://github.com/apachecn/seaborn-doc-zh/blob/master/docs/seaborn.color_palette.html#seaborn.color_palette)

font：字符串

> 字体，参见 matplotlib 字体管理

font_scale：浮点数，可选的

> 独立缩放因子，以独立缩放字体元素的大小。

color_codes：布尔值

> 如果为真并且调色板是一个 seaborn 调色板，重新映射速记颜色代码（比如，"b"，"g"，"r"，等等）到调色板上的颜色。

rc：字典或者 None

> rc 参数字典映射以覆盖以上参数。

## seaborn.axes_style



```python
seaborn.axes_style(style=None,rc=None)
```

返回一个参数数组作为图表的自定义风格。

它会影响诸如坐标轴的颜色，是否默认启用网格，和其他自定义元素。

这个函数返回一个对象，该对象可以在 with 语句中使用以临时改变样式参数。

参数：style：字典,None,或者{darkgrid, whitegrid, dark, white, ticks}其中一个。

> 一个参数字典或者一个预配置集的名称。

rc：字典，可选的

> 给定参数映射以覆盖预先设定的（默认的）seaborn 样式参数字典

参见

给一个 seaborn 主题设定 matplotlib 参数，返回一个参数字典，以缩放图标元素并可以定义图表的调色板。

例子：

```python
>>> st = axes_style("whitegrid")
```

```python
>>> set_style("ticks", {"xtick.major.size": 8, "ytick.major.size": 8})
```

```python
>>> import matplotlib.pyplot as plt
>>> with axes_style("white"):
...     f, ax = plt.subplots()
...     ax.plot(x, y)
```


## seaborn.set_style



```python
seaborn.set_style(style=None, rc=None)
```

设定图表的自定义风格。

它会影响诸如坐标轴的颜色，网格默认是否开启和其他自定义元素。

参数：style：字典,None,或者{darkgrid, whitegrid, dark, white, ticks}其中一个。

> 一个参数字典或者一个预配置集的名称。

rc：字典，可选

> 一个字典映射去覆盖 seaborn 样式字典中的预设值。这仅仅会更新那些被认为是样式定义一部分的参数。

参见

返回一个参数字典或者使用 with 语句以临时设置 style.set 的参数来缩放图表的元素或者设定图表的默认调色板。

例子：

```python
>>> set_style("whitegrid")
```

```python
>>> set_style("ticks", {"xtick.major.size": 8, "ytick.major.size": 8})
```


## seaborn.plotting_context



```python
seaborn.plotting_context(context=None, font_scale=1, rc=None)
```

以 dict 形式返回参数，用以缩放图形的元素。

这些参数可以影响诸如标签大小，线条和绘图的其他元素，但不会影响整体样式。基础文本时"notebook"，和其他文本"paper"，"talk"和"poster"，它们分别是 0.8，1.3 和 1.6 的 notebook 参数版本。

该函数返回一个对象，该对象可以在`with`语句中使用，临时改变文本参数。

参数：`context`：dict, None 或者是{paper, notebook, talk, poster}其中一个

> 参数集或者是预设集合的名字

`font_scale`：浮点数，可选

> 单独的缩放因子可以独立缩放字体元素大小

`rc`：dict，可选

> 参数映射以覆盖预设的 seaborn 的文本字典中的值。这只更新被视为文本定义的一部分的参数。

也可参见

设置 matplotlib 参数以调整绘图元素返回定义图形样式的参数的 dict，定义绘图的调色板。

示例：

```python
>>> c = plotting_context("poster")
```

```python
>>> c = plotting_context("notebook", font_scale=1.5)
```

```python
>>> c = plotting_context("talk", rc={"lines.linewidth": 2})
```

```python
>>> import matplotlib.pyplot as plt
>>> with plotting_context("paper"):
...		f, ax = plt.subplots()
... 	ax.plot(x, y)
```


## seaborn.set_context



```python
seaborn.set_context(context=None, font_scale=1, rc=None)
```

设置绘图文本参数。

这些参数可以影响诸如标签大小，线条和绘图的其他元素，但不会影响整体样式。基础文本时"notebook"，和其他文本"paper"，"talk"和"poster"，它们分别是 0.8，1.3 和 1.6 的 notebook 参数版本。

参数：`context`：dict, None 或者是{paper, notebook, talk, poster}其中一个

> 参数集或者是预设集合的名字

`font_scale`：浮点数，可选

> 单独的缩放因子可以独立缩放字体元素大小

`rc`：dict，可选

> 参数映射以覆盖预设的 seaborn 的上下文字典中的值。这只更新被视为上下文定义的一部分的参数。

也可参见

返回一个 rc 参数的字典，或者在`with`语句中使用临时设置 context.set 图样式的默认为数字的默认调色板

示例

```python
>>> set_context("paper")
```

```python
>>> set_context("talk", font_scale=1.4)
```

```python
>>> set_context("talk", rc={"lines.linewidth": 2})
```


## seaborn.set_color_codes



```python
seaborn.set_color_codes(palette='deep')
```

改变 matplotlib 颜色缩写词的解释方式。

调用此方法将改变 matplotlib 在后续图表中解释缩写词，例如"b"或"g"的方式。

参数： `palette`: {deep, muted, pastel, dark, bright, colorblind}

> 预命名的 seaborn 调色板，用作颜色的来源

参见

可以通过高级 seaborn 样式管理器设置颜色代码。也可以通过设置 matplotlib 颜色循环功能设置颜色代码。

示例：

将 matplotlib 颜色代码映射到默认的 seaborn 调色板。

```python
>>> import matplotlib.pyplot as plt
>>> import seaborn as sns; sns.set()
>>> sns.set_color_codes()
>>> _ = plt.plot([0, 1], color="r")
```

![http://seaborn.pydata.org/_images/seaborn-set_color_codes-1.png](assets/f7331a33eaedf9eae59191642f7a64e4.jpg)


使用不同的 seaborn 调色板

```python
>>> sns.set_color_codes("dark")
>>> _ = plt.plot([0, 1], color="g")
>>> _ = plt.plot([0, 2], color="m")
```

![http://seaborn.pydata.org/_images/seaborn-set_color_codes-2.png](assets/35544ecf6de962c91ab860527035f39f.jpg)



## seaborn.reset_defaults



 ```python
seaborn.reset_defaults()
 ```

重置所有 RC 设置至默认设置。

## seaborn.reset_orig



```python
seaborn.reset_orig()
```

将所有 RC 参数恢复为最初设置(相对于自定义 rc 而言)。


## seaborn.set_palette



```python
seaborn.set_palette(palette, n_colors=None, desat=None, color_codes=False)
```

通过 searborn 调色板设置 matplotlib 色彩循环

参数：`palette`：seaborn color paltte &#124; matplotlib colormap &#124; hls &#124; husl

> 调色板参数。 应该可以被 [`color_palette()`](seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette") 函数处理。

`n_colors`：int

> 色彩循环中的颜色数量。默认数量与`palette`模式有关, 查看[`color_palette()`](seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette")文档了解更多内容。

`desat`：float

> 每种颜色去饱和的比例。

`color_codes`：bool

> 如果为`True`，并且`palette`是 seaborn 调色板, 则将颜色代码简写 (例如“b”, “g”, “r”等等)映射到当前调色板上。



另外

在`with`语句中临时设置调色板或色彩循环。设置参数以调整绘图元素的默认参数。

例子

```python
>>> set_palette("Reds")
```

```python
>>> set_palette("Set1", 8, .75)
```

## seaborn.color_palette



```python
seaborn.color_palette(palette=None, n_colors=None, desat=None)
```

返回一个颜色列表来定义一个调色板。

```python
Available seaborn palette names:
```

有 deep, muted, bright, pastel, dark, colorblind 六种颜色模式

```python
Other options:
```

matplotlib Colormap 的名字、‘ch:<cubehelix arguments>’, ‘hls’, ‘husl’，或任一 matplotlib 接受的不同格式颜色列表。

调用此函数并设置 `palette=None` 会返回当前 matplotlib 色彩循环。

matplotlib 调色板的顺序可以通过在调色板名称后添加 “_r” 来倒置，同样，添加 “_d” 可以将调色板设置为深色模式。（这些选项为互斥属性，返回的颜色列表同样可以被取反）

可以在 `with` 语句中使用此函数来为一个或多个点临时改变调色板。

参考这篇 [教程](http://seaborn.pydata.org/tutorial/color_palettes.html#palette-tutorial) 来获取更多信息。

参数：**palette：None, string, or sequence, optional**

> 调色板或者 None 值来返回给当前调色板。如果是序列，输入颜色会被使用，可能会被循环化并降低饱和度。


`n_colors`：int, 可选

> 调色板中的颜色数。如果为 None，则默认值将取决于调色板的指定方式。已命名调色板默认有 6 种颜色，抓取当前调色板或传递颜色列表不会更改颜色数，除非作出指定。要求比调色板中存在的颜色更多的颜色会导致调色板循环化。

`desat`：float, 可选

> 每种颜色的去饱和比例。


返回值：`palette`：RGB 元组序列。

> 调色板。操作类似于列表，但可以用作上下文管理器，并具有转换为十六进制颜色代码的 `as_hex` 方法。



另外

设置所有的默认颜色循环。重新分配颜色代码，如 “b”、“g” 等。从 seaborn 调色板中选择颜色。

例子

不带参数的调用将返回当前默认颜色循环中的所有颜色：

```python
>>> import seaborn as sns; sns.set()
>>> sns.palplot(sns.color_palette())
```

![http://seaborn.pydata.org/_images/seaborn-color_palette-1.png](assets/9a5bb2b7fbd94af1db1d911dfbec1f7c.jpg)


显示另一个 “seaborn 调色板”，具有与默认 matplotlib 颜色循环相同的基本色调顺序，但颜色更吸引人。默认情况下，使用调色板名称进行调用将返回 6 种颜色：

```python
>>> sns.palplot(sns.color_palette("muted"))
```

![http://seaborn.pydata.org/_images/seaborn-color_palette-2.png](assets/f675cc14d11fb5585e5b1829cb29b569.jpg)


使用一个内置 matplotlib clolormap 的离散值：

```python
>>> sns.palplot(sns.color_palette("RdBu", n_colors=7))
```

![http://seaborn.pydata.org/_images/seaborn-color_palette-3.png](assets/0aa3ba1dbc0e103e4d7b96c4de278ec9.jpg)


创建自定义 cubehelix 调色板：

```python
>>> sns.palplot(sns.color_palette("ch:2.5,-.2,dark=.3"))
```

![http://seaborn.pydata.org/_images/seaborn-color_palette-4.png](assets/6e3b5f94591e119cae1ead4b24d26c9e.jpg)


使用一个明确的 matplotlib 调色板并降低一些饱和度：

```python
>>> sns.palplot(sns.color_palette("Set1", n_colors=8, desat=.5))
```

![http://seaborn.pydata.org/_images/seaborn-color_palette-5.png](assets/ce60a356e2dcd3cbe9cc67f1128c09c7.jpg)


创建 “dark”（深色）matplotlib 顺序调色板变体。(当对应于有序变量的多条线或点进行着色时，如果您不希望最轻的线不可见，则可以使用此选项)：

```python
>>> sns.palplot(sns.color_palette("Blues_d"))
```

![http://seaborn.pydata.org/_images/seaborn-color_palette-6.png](assets/fcc8ab8bf79674eb14cd31eead15dfd1.jpg)


作为上下文管理器使用：

```python
>>> import numpy as np, matplotlib.pyplot as plt
>>> with sns.color_palette("husl", 8):
...    _ = plt.plot(np.c_[np.zeros(8), np.arange(8)].T)
```

![http://seaborn.pydata.org/_images/seaborn-color_palette-7.png](assets/5900496ed8560e0dbee0628adb29f5e9.jpg)


## seaborn.husl_palette



```python
seaborn.husl_palette(n_colors=6, h=0.01, s=0.9, l=0.65)
```

在 HUSL 色调空间中获得一组均匀间隔的颜色。

h, s, 和 l 值应该在 0 和 1 之间。

参数：`n_colors`：int

> 调色板中的颜色数

`h`：float

> 第一个色调

`s`：float

> 饱和度

`l`：float

> 亮度


返回值：`palette`：seaborn 调色板

> 类似列表的颜色对象的 RGB 元组。



另外

在 HSL 系统中使用等间距圆形色调创建一个调色板。

例子

使用默认参数创建一个有 10 种颜色的调色板：

```python
>>> import seaborn as sns; sns.set()
>>> sns.palplot(sns.husl_palette(10))
```

![http://seaborn.pydata.org/_images/seaborn-husl_palette-1.png](assets/6c7a1ce9ff5aa3607ad3bf275bd012fc.jpg)


创建一个以不同色调值开头的 10 种颜色的调色板：

```python
>>> sns.palplot(sns.husl_palette(10, h=.5))
```

![http://seaborn.pydata.org/_images/seaborn-husl_palette-2.png](assets/e576eaf00adad68b231a4c2b8845d6e0.jpg)


创建一个比默认颜色更暗的 10 种颜色的调色板：

```python
>>> sns.palplot(sns.husl_palette(10, l=.4))
```

![http://seaborn.pydata.org/_images/seaborn-husl_palette-3.png](assets/752dc56ea71ef769a622efe0d5ea1b56.jpg)


创建 10 种颜色的调色板，其饱和度低于默认值：

```python
>>> sns.palplot(sns.husl_palette(10, s=.4))
```

![http://seaborn.pydata.org/_images/seaborn-husl_palette-4.png](assets/47c55edddae366e8d447c323d69b197f.jpg)


## seaborn.hls_palette



```python
seaborn.hls_palette(n_colors=6, h=0.01, l=0.6, s=0.65)
```

在 HLS 色调空间中获取一组均匀间隔的颜色。

h, s, 和 l 值应该在 0 和 1 之间。

参数：`n_colors`：int

> 调色板中的颜色数

`h`：float

> 第一个色调

`l`：float

> 亮度

`s`：float

> 饱和度


返回值：`palette`：seaborn 调色板

> 类似列表的颜色对象的 RGB 元组。



另外

在 HUSL 系统中使用等间距圆形色调创建一个调色板。

例子

使用默认参数创建一个有 10 种颜色的调色板：

```python
>>> import seaborn as sns; sns.set()
>>> sns.palplot(sns.hls_palette(10))
```

![http://seaborn.pydata.org/_images/seaborn-hls_palette-1.png](assets/5147a9e7a6cd2453c2e80dd967724f7e.jpg)


创建一个以不同色调值开头的 10 种颜色的调色板：

```python
>>> sns.palplot(sns.hls_palette(10, h=.5))
```

![http://seaborn.pydata.org/_images/seaborn-hls_palette-2.png](assets/077b00661754439549e60127485a6a4a.jpg)


创建一个比默认颜色更暗的 10 种颜色的调色板：

```python
>>> sns.palplot(sns.hls_palette(10, l=.4))
```

![http://seaborn.pydata.org/_images/seaborn-hls_palette-3.png](assets/fdcac2831e87bea8b0a208095eb15adc.jpg)


创建 10 种颜色的调色板，其饱和度低于默认值：

```python
>>> sns.palplot(sns.hls_palette(10, s=.4))
```

![http://seaborn.pydata.org/_images/seaborn-hls_palette-4.png](assets/98715db5f9ee15781a7c49624ec8dafc.jpg)


## seaborn.cubehelix_palette



```python
seaborn.cubehelix_palette(n_colors=6, start=0, rot=0.4, gamma=1.0, hue=0.8, light=0.85, dark=0.15, reverse=False, as_cmap=False)
```

用 cubehelix 系统制作顺序调色板。

生成亮度呈线性减小(或增大)的 colormap。这意味着 colormap 在转换为黑白模式时(用于打印)的信息将得到保留，且对色盲友好。“cubehelix” 也可以作为基于 matplotlib 的调色板使用，但此函数使用户可以更好地控制调色板的外观，并且具有一组不同的默认值。

除了使用这个函数，还可以在 seaborn 中使用字符串速记生成 cubehelix 调色板。 请参见下面的示例。

参数：`n_colors`：intclipboard13.png

> 调色板中的颜色数。

`start`：float, 0 &lt;= start &lt;= 3

> 第一个色调。

`rot`：float

> 围绕调色板范围内的色相控制盘旋转。

`gamma`：float 0 &lt;= gamma

> Gamma 系数用以强调较深 (Gamma < 1) 或较浅 (Gamma > 1) 的颜色。

`hue`：float, 0 &lt;= hue &lt;= 1

> 颜色的饱和度。

`dark`：float 0 &lt;= dark &lt;= 1

> 调色板中最暗颜色的强度。

`light`：float 0 &lt;= light &lt;= 1

> 调色板中最浅颜色的强度。

`reverse`：bool

> 如果为 True 值，则调色板将从暗到亮。

`as_cmap`：bool

> 如果为 True 值，则返回 matplotlib colormap 而不是颜色列表。


返回值：`palette or cmap`：seaborn 调色板或者 matplotlib colormap

> 类似列表的颜色对象的 RGB 元组，或者可以将连续值映射到颜色的 colormap 对象，具体取决于 `as_cmap` 参数的值。



另外

启动交互式小部件以调整 cubehelix 调色板参数。创建具有暗低值的连续调色板。创建具有亮低值的连续调色板。

参考

Green, D. A. (2011). “一种用于显示天文强度图像的配色方案”. Bulletin of the Astromical Society of India, Vol. 39, p. 289-295.

例子

生成默认调色板：

```python
>>> import seaborn as sns; sns.set()
>>> sns.palplot(sns.cubehelix_palette())
```

![http://seaborn.pydata.org/_images/seaborn-cubehelix_palette-1.png](assets/144ee4446ca7ff02dd03b831c6144dbb.jpg)


从相同的起始位置向后旋转：

```python
>>> sns.palplot(sns.cubehelix_palette(rot=-.4))
```

![http://seaborn.pydata.org/_images/seaborn-cubehelix_palette-2.png](assets/e558f322ef6826a8b67e10ff25919759.jpg)


使用不同的起点和较短的旋转：

```python
>>> sns.palplot(sns.cubehelix_palette(start=2.8, rot=.1))
```

![http://seaborn.pydata.org/_images/seaborn-cubehelix_palette-3.png](assets/664ee3a6a4dfcace7697e385779f371b.jpg)


反转亮度渐变方向：

```python
>>> sns.palplot(sns.cubehelix_palette(reverse=True))
```

![http://seaborn.pydata.org/_images/seaborn-cubehelix_palette-4.png](assets/13804c3cf58c9d108ea1d638b643451f.jpg)


生成一个 colormap 对象：

```python
>>> from numpy import arange
>>> x = arange(25).reshape(5, 5)
>>> cmap = sns.cubehelix_palette(as_cmap=True)
>>> ax = sns.heatmap(x, cmap=cmap)
```

![http://seaborn.pydata.org/_images/seaborn-cubehelix_palette-5.png](assets/17fa14f23873e6b75933971c609b4fe5.jpg)


使用完整的亮度范围：

```python
>>> cmap = sns.cubehelix_palette(dark=0, light=1, as_cmap=True)
>>> ax = sns.heatmap(x, cmap=cmap)
```

![http://seaborn.pydata.org/_images/seaborn-cubehelix_palette-6.png](assets/ced91ab8985cc6cb41a2b85e098f14e4.jpg)


使用 [`color_palette()`](seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette") 函数接口：

```python
>>> sns.palplot(sns.color_palette("ch:2,r=.2,l=.6"))
```

![http://seaborn.pydata.org/_images/seaborn-cubehelix_palette-7.png](assets/da5c0a4d6279edb54b0bac3bdfe7fed5.jpg)


## seaborn.dark_palette



```python
seaborn.dark_palette(color, n_colors=6, reverse=False, as_cmap=False, input='rgb')
```

制作一个混合深色和 `color` 模式的顺序调色板。

这种调色板适用于数据集的范围从相对低值(不感兴趣)到相对高值(很感兴趣)时。

可以通过多种方式指定  `color` 参数，包括用于在 matplotlib 中定义颜色的所有选项，以及由 seborn 处理的其他几个颜色空间。也可以使用 XKCD color survey 中的颜色名字数据库。

如果您在使用 IPython notebook，您还可以通过 [`choose_dark_palette()`](seaborn.choose_dark_palette.html#seaborn.choose_dark_palette "seaborn.choose_dark_palette") 函数交互式选择调色板。

参数：`color`：高值的基色

> 十六进制、RGB 元组或者颜色名字。

`n_colors`：int, 可选

> 调色板中的颜色数。

`reverse`：bool, 可选

> 如果为 True 值，则反转混合的方向。

`as_cmap`：bool, optional

> 如果为 True 值，则返回 matplotlib colormap 而不是列表。

`input`：{‘rgb’, ‘hls’, ‘husl’, xkcd’}

> 用于解释输入颜色的颜色空间。前三个选项适用于元组输入，后者适用于字符串输入。


返回值：`palette or cmap`：seaborn color palette or matplotlib colormap

> 类似列表的颜色对象的 RGB 元组，或者可以将连续值映射到颜色的 colormap 对象，具体取决于 as_cmap 参数的值。



另外

创建具有暗低值的连续调色板。创建有两种颜色的发散调色板。

例子

从一个 HTML 颜色生成一个调色板：

```python
>>> import seaborn as sns; sns.set()
>>> sns.palplot(sns.dark_palette("purple"))
```

![http://seaborn.pydata.org/_images/seaborn-dark_palette-1.png](assets/d206a78223b0280bfffbfd7cdb262145.jpg)


生成亮度降低的调色板：

```python
>>> sns.palplot(sns.dark_palette("seagreen", reverse=True))
```

![http://seaborn.pydata.org/_images/seaborn-dark_palette-2.png](assets/eac4541fd6a3e7b30eb535e0a1aab99f.jpg)


从 HUSL 空间种子生成选项板：

```python
>>> sns.palplot(sns.dark_palette((260, 75, 60), input="husl"))
```

![http://seaborn.pydata.org/_images/seaborn-dark_palette-3.png](assets/6e81b4c302189f59de63f96dbee3a197.jpg)


生成一个 colormap 对象：

```python
>>> from numpy import arange
>>> x = arange(25).reshape(5, 5)
>>> cmap = sns.dark_palette("#2ecc71", as_cmap=True)
>>> ax = sns.heatmap(x, cmap=cmap)
```

![http://seaborn.pydata.org/_images/seaborn-dark_palette-4.png](assets/f153ef2783202a95337d9ea723fe2984.jpg)


## seaborn.light_palette



```python
seaborn.light_palette(color, n_colors=6, reverse=False, as_cmap=False, input='rgb')
```

制作一个混合浅色和 `color` 模式的顺序调色板。

这种调色板适用于数据集的范围从相对低值(不感兴趣)到相对高值(很感兴趣)时。

可以通过多种方式指定 `color` 参数，包括用于在 matplotlib 中定义颜色的所有选项，以及由 seborn 处理的其他几个颜色空间。也可以使用 XKCD color survey 中的颜色名字数据库。

如果您在使用 IPython notebook，您还可以通过 [`choose_light_palette()`](seaborn.choose_light_palette.html#seaborn.choose_light_palette "seaborn.choose_light_palette") 函数交互式选择调色板。

参数：`color`：高值的基色

> 十六进制、`input` 中的元组或者颜色名字。

`n_colors`：int, 可选

> 调色板中的颜色数。

`reverse`：bool, 可选

> 如果为 True 值，则反转混合的方向。

`as_cmap`：bool, 可选

> 如果为 True 值，则返回 matplotlib colormap 而不是列表。

`input`：{‘rgb’, ‘hls’, ‘husl’, xkcd’}

> 用于解释输入颜色的颜色空间。前三个选项适用于元组输入，后者适用于字符串输入。

返回值：`palette or cmap`：seaborn color palette or matplotlib colormap

> 类似列表的颜色对象的 RGB 元组，或者可以将连续值映射到颜色的 colormap 对象，具体取决于 as_cmap 参数的值。



另外

创建具有暗低值的连续调色板。创建有两种颜色的发散调色板。

例子

从一个 HTML 颜色生成一个调色板：

```python
>>> import seaborn as sns; sns.set()
>>> sns.palplot(sns.light_palette("purple"))
```

![http://seaborn.pydata.org/_images/seaborn-light_palette-1.png](assets/74a27bb78efdcf1f93f6c4c77035bb88.jpg)


生成亮度降低的调色板：

```python
>>> sns.palplot(sns.light_palette("seagreen", reverse=True))
```

![http://seaborn.pydata.org/_images/seaborn-light_palette-2.png](assets/1b8a02d12771dd98fcd113bf3a601e27.jpg)


从 HUSL 空间种子生成选项板：

```python
>>> sns.palplot(sns.light_palette((260, 75, 60), input="husl"))
```

![http://seaborn.pydata.org/_images/seaborn-light_palette-3.png](assets/2d899c79eea5f0341a72b193b32ee84b.jpg)


生成一个 colormap 对象：

```python
>>> from numpy import arange
>>> x = arange(25).reshape(5, 5)
>>> cmap = sns.light_palette("#2ecc71", as_cmap=True)
>>> ax = sns.heatmap(x, cmap=cmap)
```

![http://seaborn.pydata.org/_images/seaborn-light_palette-4.png](assets/dc9db2a17529e652fe4c5a187892a2eb.jpg)


## seaborn.diverging_palette



```python
seaborn.diverging_palette(h_neg, h_pos, s=75, l=50, sep=10, n=6, center='light', as_cmap=False)
```

在两个 HUSL 颜色直接建立一个发散调色板。

如果您在使用 IPython notebook，您还可以通过 [`choose_diverging_palette()`](seaborn.choose_diverging_palette.html#seaborn.choose_diverging_palette "seaborn.choose_diverging_palette") 函数交互式选择调色板。

参数：`h_neg, h_pos`：float in [0, 359]

> 图的正负范围的锚定色调

`s`：[0, 100] 范围内的浮点数，可选

> 图的两个范围的锚定饱和度

`l`：[0, 100] 范围内的浮点数，可选

> 图的两个范围的锚定亮度

`n`：int，可选

> 调色板中的颜色数（如果为 not，返回一个 colormap）

`center`：{“light”, “dark”}, 可选

> 调色板中心为亮或暗

`as_cmap`：bool, 可选

> 如果为 true，返回一个 matplotlib colormap 而不是一个颜色列表。


返回值：`palette or cmap`：seaborn color palette or matplotlib colormap

> 类似列表的颜色对象的 RGB 元组，或者可以将连续值映射到颜色的 colormap 对象，具体取决于 `as_cmap` 参数的值。




另外

创建具有暗值的连续调色板。创建具有亮值的连续调色板。

例子

生成一个蓝-白-红调色板：

```python
>>> import seaborn as sns; sns.set()
>>> sns.palplot(sns.diverging_palette(240, 10, n=9))
```

![http://seaborn.pydata.org/_images/seaborn-diverging_palette-1.png](assets/35c6cb5f121884b387bc70e68ed2a22d.jpg)


生成一个更亮的绿-白-紫调色板：

```python
>>> sns.palplot(sns.diverging_palette(150, 275, s=80, l=55, n=9))
```

![http://seaborn.pydata.org/_images/seaborn-diverging_palette-2.png](assets/bc461a32c1b91013ce1dbb7e3a78d94c.jpg)


生成一个蓝-黑-红调色板:

```python
>>> sns.palplot(sns.diverging_palette(250, 15, s=75, l=40,
...                                   n=9, center="dark"))
```

![http://seaborn.pydata.org/_images/seaborn-diverging_palette-3.png](assets/9003952e6b318f40954e978ce39dd033.jpg)


生成一个 colormap 对象:

```python
>>> from numpy import arange
>>> x = arange(25).reshape(5, 5)
>>> cmap = sns.diverging_palette(220, 20, sep=20, as_cmap=True)
>>> ax = sns.heatmap(x, cmap=cmap)
```

![http://seaborn.pydata.org/_images/seaborn-diverging_palette-4.png](assets/f0eb41dc444960d4e07f9de19e5e8568.jpg)


## seaborn.blend_palette



```python
seaborn.blend_palette(colors, n_colors=6, as_cmap=False, input='rgb')
```

创建在颜色列表之间混合的调色板。

参数：`colors`：由 `input` 解释的各种格式的颜色序列。

> 十六进制码，html 颜色名字，或者 `input` 空间中的元组。

`n_colors`：int, 可选

> 调色板中的颜色数。

`as_cmap`：bool, 可选

> 如果为 True 值，则返回 matplotlib colormap 而不是列表。


返回值：`palette or cmap`：seaborn 调色板或 matplotlib colormap

> 类似列表的颜色对象的 RGB 元组，或者可以将连续值映射到颜色的 colormap 对象，具体取决于 `as_cmap` 参数的值。


## seaborn.xkcd_palette



```python
seaborn.xkcd_palette(colors)
```

使用来自 xkcd color survey 的颜色名字生成调色板。

查看完整的 xkcd 颜色列表： [https://xkcd.com/color/rgb/](https://xkcd.com/color/rgb/)

这是一个简单的 `seaborn.xkcd_rgb` 字典的装饰器。

参数：`colors`：字符串列表

> `seaborn.xkcd_rgb` 字典中的 key 的列表。


返回值：`palette`：seaborn 调色板

> 类似列表的颜色对象的 RGB 元组，或者可以将连续值映射到颜色的 colormap 对象，具体取决于 `as_cmap` 参数的值。



另外

使用 Crayola crayon colors 创建调色板。

## seaborn.crayon_palette



```python
seaborn.crayon_palette(colors)
```

通过 Crayola crayons 颜色名字生成调色板。

颜色列表在这里获取： [https://en.wikipedia.org/wiki/List_of_Crayola_crayon_colors](https://en.wikipedia.org/wiki/List_of_Crayola_crayon_colors)

这是一个简单的 `seaborn.crayons` 字典的装饰器。

参数：`colors`：字符串列表

> List of keys in the `seaborn.crayons` dictionary.


返回值：`palette`：seaborn 调色板

> Returns the list of colors as rgb tuples in an object that behaves like other seaborn color palettes.返回一个类似 serborn 调色板的对象，其中包括 RGB 元组构成的的颜色列表。



另外

使用来自 xkcd color survey 的颜色名字创建调色板。

## seaborn.mpl_palette



```python
seaborn.mpl_palette(name, n_colors=6)
```

从一个 matplotlib 调色板中返回离散颜色。

请注意，这会正确处理定性的 colorbrewer 调色板，但如果您要求的颜色多于特定的定性调色板，提供的颜色将会比您预期的少。相反，使用 [`color_palette()`](seaborn.color_palette.html#seaborn.color_palette "seaborn.color_palette") 函数请求一个定性 colorbrewer 调色板将会返回预期数目的颜色，但是是循环型的。

如果您在使用 IPython notebook，您还可以通过 [`choose_colorbrewer_palette()`](seaborn.choose_colorbrewer_palette.html#seaborn.choose_colorbrewer_palette "seaborn.choose_colorbrewer_palette") 函数交互式选择调色板。

参数：`name`：string

> 调色板名字，应该是一个被命名的 matplotlib colormap。

`n_colors`：int

> 调色板中离散颜色的个数。


返回值：`palette or cmap`：seaborn 调色板或者 matplotlib colormap

> 类似列表的颜色对象的 RGB 元组，或者可以将连续值映射到颜色的 colormap 对象，具体取决于 `as_cmap` 参数的值。

例子

生成一个含有 8 种颜色的定性 colorbrewer 调色板：

```python
>>> import seaborn as sns; sns.set()
>>> sns.palplot(sns.mpl_palette("Set2", 8))
```

![http://seaborn.pydata.org/_images/seaborn-mpl_palette-1.png](assets/df633411e4b51b4749de8c5349b438cc.jpg)


生成一个连续的 colorbrewer 调色板：

```python
>>> sns.palplot(sns.mpl_palette("Blues"))
```

![http://seaborn.pydata.org/_images/seaborn-mpl_palette-2.png](assets/e369a8cb7348d971f2471d297addf456.jpg)


生成一个发散调色板：

```python
>>> sns.palplot(sns.mpl_palette("seismic", 8))
```

![http://seaborn.pydata.org/_images/seaborn-mpl_palette-3.png](assets/2690d286a343ff53a43d15d54fc86e91.jpg)


生成一个 “dark” 顺序调色板：

```python
>>> sns.palplot(sns.mpl_palette("GnBu_d"))
```

![http://seaborn.pydata.org/_images/seaborn-mpl_palette-4.png](assets/dc21c66a962fb741f9f8f63c0374fae1.jpg)


## seaborn.choose_colorbrewer_palette



```python
seaborn.choose_colorbrewer_palette(data_type, as_cmap=False)
```

在 ColorBrewer 集中选择一个调色板。

这些调色板内置于 matplotlib 库中，可以通过名字在 seaborn 函数中调用，或者作为对象返回给这个函数。

参数：`data_type`：{‘sequential’, ‘diverging’, ‘qualitative’}

> 描述您想要实现可视化的数据类型。查看 serborn 调色板文档来了解更多如何取值。注意，您可以传递子字符串（例如，‘q’ 代表 ‘qualitative‘）

`as_cmap`：bool

> 如果为 True 值，则返回 matplotlib colormap 而不是离散颜色的列表。

返回值：`pal or cmap`：颜色列表或 matplotlib colormap

> 可以被传递给 plotting 函数的对象。



另外

创建具有暗低值的连续调色板。创建具有亮低值的连续调色板。从选中的颜色中创建发散调色板。使用 cubehelix 创建连续调色板或者 colormap。

## seaborn.choose_cubehelix_palette



```python
seaborn.choose_cubehelix_palette(as_cmap=False)
```

启动交互式小部件以创建顺序 cubehelix 调色板。

这与 [`cubehelix_palette()`](seaborn.cubehelix_palette.html#seaborn.cubehelix_palette "seaborn.cubehelix_palette") 函数相对应。这种调色板在数据集的范围从相对低值(不感兴趣)到相对高值(很感兴趣)时很有用。cubehelix 系统允许调色板在整个范围内具有更多的色调变化，这有助于区分更广泛的值。

需要 2.0 以上版本 IPython，必须在 notebook 中使用。

参数：`as_cmap`：bool

> 如果为 True 值，则返回 matplotlib colormap 而不是离散颜色的列表。


返回值：`pal or cmap`：颜色列表或 matplotlib colormap

> 可以被传递给 plotting 函数的对象。

另外

使用 cubehelix 创建连续调色板或者 colormap。

## seaborn.choose_light_palette



```python
seaborn.choose_light_palette(input='husl', as_cmap=False)
```

启动交互式小部件以创建亮色顺序调色板。

与 [`light_palette()`](seaborn.light_palette.html#seaborn.light_palette "seaborn.light_palette") 函数相对应。 这种调色板在数据集的范围从相对低值(不感兴趣)到相对高值(很感兴趣)时很有用。

需要 2.0 以上版本 IPython，必须在 notebook 中使用。

参数：`input`：{‘husl’, ‘hls’, ‘rgb’}

> 色彩空间用于定义种子值。请注意，此默认值与 [`light_palette()`](seaborn.light_palette.html#seaborn.light_palette "seaborn.light_palette") 的默认输入值不同。

参数：`as_cmap`：bool

> 如果为 True 值，则返回 matplotlib colormap 而不是离散颜色的列表。


返回值：`pal or cmap`：颜色列表或 matplotlib colormap

> 可以被传递给 plotting 函数的对象。

另外

创建具有暗低值的连续调色板。创建具有亮低值的连续调色板。使用 cubehelix 创建连续调色板或者 colormap。

## seaborn.choose_dark_palette



```python
seaborn.choose_dark_palette(input='husl', as_cmap=False)
```

启动交互式小部件以创建暗色顺序调色板。

与 [`dark_palette()`](seaborn.dark_palette.html#seaborn.dark_palette "seaborn.dark_palette") 函数相对应。这种调色板在数据集的范围从相对低值(不感兴趣)到相对高值(很感兴趣)时很有用。

需要 2.0 以上版本 IPython，必须在 notebook 中使用。

参数：`input`：{‘husl’, ‘hls’, ‘rgb’}

> 色彩空间用于定义种子值。请注意，此默认值与 [`dark_palette()`](seaborn.dark_palette.html#seaborn.dark_palette "seaborn.dark_palette") 的默认输入值不同。


参数：`as_cmap`：bool

> 如果为 True 值，则返回 matplotlib colormap 而不是离散颜色的列表。


返回值：`pal or cmap`：颜色列表或 matplotlib colormap

> 可以被传递给 plotting 函数的对象。

另外

创建具有暗低值的连续调色板。创建具有亮低值的连续调色板。使用 cubehelix 创建连续调色板或者 colormap。

## seaborn.choose_diverging_palette



```python
seaborn.choose_diverging_palette(as_cmap=False)
```

启动交互式小部件以创建一个发散调色板。

这与 [`diverging_palette()`](seaborn.diverging_palette.html#seaborn.diverging_palette "seaborn.diverging_palette") 函数相对应。这种调色板适用于对低值和高值都感兴趣并且中间点也有意义的数据。（例如，相对与某些基准值的分数变化）。

需要 2.0 以上版本 IPython，必须在 notebook 中使用。

参数：`as_cmap`：bool

> 如果为 True 值，则返回 matplotlib colormap 而不是离散颜色的列表。


返回值：`pal or cmap`：颜色列表或 matplotlib colormap

> 可以被传递给 plotting 函数的对象。

另外

生成一个发散调色板或者 colormap。从 colorbrewer 集中交互式选择调色板，包括发散调色板。

## seaborn.load_dataset



```python
seaborn.load_dataset(name, cache=True, data_home=None, **kws)
```

从在线库中获取数据集（需要联网）。

参数：`name`：字符串

> 数据集的名字 (`name`.csv on [https://github.com/mwaskom/seaborn-data](https://github.com/mwaskom/seaborn-data))。 您可以通过 `get_dataset_names()` 获取可用的数据集。

`cache`：boolean, 可选

> 如果为 True，则在本地缓存数据并在后续调用中使用缓存。

`data_home`：string, 可选

> 用于存储缓存数据的目录。 默认情况下使用 ~/seaborn-data/

`kws`：dict, 可选

> 传递给 pandas.read_csv


## seaborn.despine



```python
seaborn.despine(fig=None, ax=None, top=True, right=True, left=False, bottom=False, offset=None, trim=False)
```

从图中移除顶部和右侧脊柱。

```python
fig : matplotlib 值, 可选
```

去除所有轴脊柱，默认使用当前数值。

```python
ax : matplotlib 轴, 可选
```

去除特定的轴脊柱。

```python
top, right, left, bottom : boolean, 可选
```

如果为 True，去除脊柱。

```python
offset : int or dict, 可选
```

绝对距离（以磅为单位）应将脊椎移离轴线（负值向内移动脊柱）。 单个值适用于所有脊柱; 字典可用于设置每侧的偏移值。

```python
trim : bool, 可选
```

如果为 True，则将脊柱限制为每个非去除脊柱的轴上的最小和最大主刻度。

返回值：None |
| --- | --- |

## seaborn.desaturate



```python
seaborn.desaturate(color, prop)
```

减少颜色的饱和度。

参数：`color`：matplotlib 颜色

> 十六进制，rgb 元组或者 html 颜色名字

`prop`：float

> 颜色的饱和度将乘以该值

返回值：`new_color`：rgb 元组

> RGB 元组表示去饱和的颜色代码


## seaborn.saturate



```python
seaborn.saturate(color)
```

返回具有相同色调的完全饱和的颜色。

参数：`color`：matplotlib 颜色

> 十六进制，rgb 元组或者 html 颜色名字


返回值：`new_color`：rgb 元组

> RGB 元组表示的饱和颜色代码


## seaborn.set_hls_values



```python
seaborn.set_hls_values(color, h=None, l=None, s=None)
```

独立修改一个颜色的 h, l 或 s 值。

参数：`color`：matplotlib 颜色

> 十六进制，rgb 元组或者 html 颜色名字

`h, l, s`：0 到 1 之间的浮点数，或者为 None

> hls 空间中的新值。


返回值：`new_color`：rgb 元组

> RGB 元组表示中的新颜色代码







