# 1 安装pyecharts和简单使用
**pyecharts官方文档:**
https://pyecharts.org/#/zh-cn/intro
[pyecharts官方文档](https://pyecharts.org/#/zh-cn/intro)

注意! 以下在jupyter notebook渲染,并非pycharm

一定要认真看官方文档!!!
一定要认真看官方文档!!!
一定要认真看官方文档!!!

```bash
#终端pip安装
pip install pyecharts

#查看一下安装的版本
import pyecharts

pyecharts.__version__
```

```python
# 示例
from pyecharts.charts import Bar

bar = Bar()
bar.add_xaxis(["衬衫", "羊毛衫", "雪纺衫", "裤子", "高跟鞋", "袜子"])
bar.add_yaxis("商家A", [5, 20, 36, 10, 75, 90])
# render 会生成本地 HTML 文件，默认会在当前目录生成 render.html 文件
# 也可以传入路径参数，如 bar.render("charts.html")
bar.render()
```
## 1.2 使用notebook

```python
# 在jupyter notebook 中直接渲染，非常方便调试
bar = Bar()
bar.add_xaxis(["衬衫", "羊毛衫", "雪纺衫", "裤子", "高跟鞋", "袜子"])
bar.add_yaxis("商家A",[5,20,35,12,70,89])
# bar.add_yaxis("商家B",[10,60,45,17,60,54])
# 生成本地html文件，默认render.html
bar.render('mybar.html')

# 在 notebook 渲染
bar.render_notebook()
```
![案例](https://img-blog.csdnimg.cn/b02d1c4d73ad469280cabd31b860b924.png)

```python
from pyecharts.charts import Bar
from pyecharts import options as opts

# V1 版本开始支持链式调用
# 你所看到的格式其实是 `black` 格式化以后的效果
# 可以执行 `pip install black` 下载使用
#bar = (
#    Bar()
#    .add_xaxis(["衬衫", "羊毛衫", "雪纺衫", "裤子", "高跟鞋", "袜子"])
#    .add_yaxis("商家A", [5, 20, 36, 10, 75, 90])
#    .set_global_opts(title_opts=opts.TitleOpts(title="主标题", subtitle="副标题"))
#    # 或者直接使用字典参数
#    # .set_global_opts(title_opts={"text": "主标题", "subtext": "副标题"})
#)
#bar.render()

# 不习惯链式调用的开发者依旧可以单独调用方法
bar = Bar()
bar.add_xaxis(["衬衫", "羊毛衫", "雪纺衫", "裤子", "高跟鞋", "袜子"])
bar.add_yaxis("商家A", [5, 20, 36, 10, 75, 90])
bar.set_global_opts(title_opts=opts.TitleOpts(title="主标题", subtitle="副标题"))
bar.render('test1.html')

# 在 notebook 渲染
bar.render_notebook()
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/bf0ad5b92a854fc1ac496f7cd99ad9e4.png)

```python
from pyecharts import options as opts
from pyecharts.charts import Bar
#Faker 可以生成各种类型的随机数据，例如数字、字符串、日期等，常用于在图表中生成模拟数据。
from pyecharts.faker import Faker


c=(
    #链式调用,比较符合前段语法
    Bar()
    .add_xaxis(Faker.choose())
    .add_yaxis("商家A",Faker.values())
    .set_global_opts(title_opts=opts.TitleOpts(title='Bar-基本实例',subtitle="我是副标题"))

)

c.render_notebook()
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/ded1b568de7b43f9b1c5f259c6755158.png)
## 1.3 主题设置

```python
from pyecharts import options as opts  # 配置项功能
from pyecharts.charts import Bar
from pyecharts.faker import Faker
from pyecharts.globals import ThemeType  # 绘图主题功能


c = (
    # 链式调用, 比较符合前端语法
    Bar(init_opts=opts.InitOpts(theme=ThemeType.DARK))  # 初始化配置项
    .add_xaxis(Faker.choose())
    .add_yaxis("商家A", Faker.values())
    .set_global_opts(title_opts=opts.TitleOpts(title="Bar-基本示例", subtitle="我是副标题"))
)
c.render_notebook()
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/4f4059129513497295aa4863c8aca3e1.png)
# 2 全局配置项
## 2.1 初始化配置项

```python
from pyecharts import options as opts # 配置项功能
from pyecharts.charts import Bar
from pyecharts.faker import Faker # 虚假数据模块
from pyecharts.globals import ThemeType  #绘图主题功能

c=(
    Bar(
        #初始化配置项,写到图像初始化对象的括号里
        init_opts=opts.InitOpts(
            width='500px',
            height='300px',
            bg_color='Cyan',
            theme=ThemeType.DARK
        )
    )
    .add_xaxis(Faker.choose())
    .add_yaxis("商家A",Faker.values())
    .set_global_opts(title_opts=opts.TitleOpts(title="Bar-基本示例",subtitle="我是副级标题"))

)


c.render_notebook()
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/b695ce2d39414bfda868f65d99ac55e8.png)
## 2.2 标题配置项

```python
from pyecharts import options as opts # 配置项功能
from pyecharts.charts import Bar
from pyecharts.faker import Faker # 虚假数据模块
from pyecharts.globals import ThemeType  #绘图主题功能

c=(
    Bar()
    .add_xaxis(Faker.choose())
    .add_yaxis("商家A",Faker.values())
    .set_global_opts(
        # 标题配置项
        opts.TitleOpts(
            title='我是主标题',  # 主标题文本
            title_link='https://www.baidu.com',  # 主标题跳转链接
            subtitle='我是副标题', # 副标题文本,
            subtitle_link='https://www.douban.com',  # 副标题跳转链接
            pos_left='50%',  # 左边距
            pos_top='20px',  # 顶部边距
            # 修改文字样式
            title_textstyle_opts=opts.TextStyleOpts(color='red')
        )
    )
        

)
c.render_notebook()
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/b205cb3201ab4d2d8ae08f6cfbe22220.png)

## 2.3 区域缩放配置项

```python
from pyecharts import options as opts
from pyecharts.charts import Bar
from pyecharts.faker import Faker
from pyecharts.globals import ThemeType

c=(
    Bar()
    .add_xaxis(['北京','北京','北京','北京','北京','北京','北京'])
    .add_yaxis("商家A",[34,35,36,75,85,84,20])
    
    .set_global_opts(
        title_opts=opts.TitleOpts(title='我是主标题'),
        # 区域缩放配置项
        datazoom_opts=opts.DataZoomOpts(
#             type_='inside',#组件类型 inside内链式,默认是slider滑动式
            orient='vertical' #垂直布局,对于Y轴进行区域缩放
        )
        
    )
)
c.render_notebook()
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/86147aad4c0048a7969ee8a5975be712.png)


## 2.4 图例配置项目

```python
from pyecharts import options as opts  # 配置项功能
from pyecharts.charts import Bar
from pyecharts.faker import Faker  # 虚假数据模块
from pyecharts.globals import ThemeType  # 绘图主题功能

c = (
    Bar()
    .add_xaxis(Faker.choose())
    .add_yaxis("商家A", Faker.values())
    .add_yaxis("商家B", Faker.values())
    .add_yaxis("商家C", Faker.values())
    .add_yaxis("商家D", Faker.values())
    .add_yaxis("商家E", Faker.values())
    .add_yaxis("商家F", Faker.values())
    .add_yaxis("商家G", Faker.values())
    .add_yaxis("商家H", Faker.values())
    .add_yaxis("商家I", Faker.values())
    .add_yaxis("商家J", Faker.values())
    .add_yaxis("商家K", Faker.values())
    .add_yaxis("商家L", Faker.values())
    .add_yaxis("商家M", Faker.values())
    .add_yaxis("商家N", Faker.values())
    .add_yaxis("商家O", Faker.values())

    .set_global_opts(
        title_opts=opts.TitleOpts(title='我是主标题'),
        
        # 区域缩放配置项
        datazoom_opts=opts.DataZoomOpts(is_show=False),
        
        # 图例配置项
        legend_opts=opts.LegendOpts(
            type_='scroll',
            pos_left='20%',
            legend_icon='circle', #图例样式
#             orient='virtical',
            page_icon_size=30,
            is_page_animation=True  # 用于判断页面是否具有动画效果
        )
    )
)

c.render_notebook()
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/2a8515a8de954831bc368df1381f8e9f.png)

## 2.5 视觉映射配置项

```python
from pyecharts import options as opts
from pyecharts.charts import Map
from pyecharts.faker import Faker

c=(
    Map()
    .add("test1",[list(z) for z in zip(Faker.guangdong_city,Faker.values())],"广东")
    .set_global_opts(
        title_opts=opts.TitleOpts(title="Map-广东地图"),
        
        #视觉映射配置项,常用于地图
        visualmap_opts=opts.VisualMapOpts(
            is_piecewise=True,
#             orient='virtical'
            orient='horizontal'
        )
    )

)

c.render_notebook()
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/357ce6c1dde04417852197808a7b5b4d.png)

## 2.6 工具箱配置项
```python
from pyecharts import options as opts  # 配置项功能
from pyecharts.charts import Bar
from pyecharts.faker import Faker  # 虚假数据模块
from pyecharts.globals import ThemeType  # 绘图主题功能

c = (
    Bar()
    .add_xaxis(Faker.choose())
    .add_yaxis("A", Faker.values())
    .add_yaxis("B", Faker.values())
    .add_yaxis("C", Faker.values())
    
    
    .set_global_opts(
        # 标题配置项
        title_opts=opts.TitleOpts(title='我是主标题'),
        
        # 区域缩放配置项
        datazoom_opts=opts.DataZoomOpts(is_show=False),
        
        # 图例配置项
        legend_opts=opts.LegendOpts(),
        
        # 工具箱配置项
        toolbox_opts=opts.ToolboxOpts(
            orient='vertical',
            pos_left="80%",
            feature=opts.ToolBoxFeatureOpts(
                save_as_image=opts.ToolBoxFeatureSaveAsImageOpts(is_show=True)
                #save_as_image=opts.ToolBoxFeatureSaveAsImageOpts(is_show=False) 是在使用 pyecharts 库创建一个图表时，设置保存为图片的选项。其中，opts.ToolBoxFeatureSaveAsImageOpts(is_show=False) 是一个 ToolBoxFeatureSaveAsImageOpts 类的实例化对象，用于配置保存为图片的功能。
#is_show=False 表示在生成的图片中不显示保存按钮。如果设置为 True，则在图片中会显示一个保存按钮，用户可以通过点击该按钮将图表保存为图片文件。
            )
        )
    )
)
c.render_notebook()
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/bb6bc44e63af4b579c30f94404165672.png)

## 2.7 提示框配置项

```python
from pyecharts import options as opts  # 配置项功能
from pyecharts.charts import Bar
from pyecharts.faker import Faker  # 虚假数据模块
from pyecharts.globals import ThemeType  # 绘图主题功能

c = (
    Bar()
    .add_xaxis(Faker.choose())
    .add_yaxis("商家A", Faker.values())
    .add_yaxis("商家B", Faker.values())
    .add_yaxis("商家C", Faker.values())
    
    
    .set_global_opts(
        # 标题配置项
        title_opts=opts.TitleOpts(title='我是主标题'),
        
        # 区域缩放配置项
        datazoom_opts=opts.DataZoomOpts(is_show=False),
        
        # 图例配置项
        legend_opts=opts.LegendOpts(),
        
        # 工具箱配置项
        toolbox_opts=opts.ToolboxOpts(is_show=True),
        
        # 提示框配置项
        tooltip_opts=opts.TooltipOpts(
            trigger_on='click',
#             axis_pointer_type='shadow'
        )
    )
)
c.render_notebook()
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/425a1040f39643569eefcf0b2533050e.png)

## 2.8 坐标轴配置项

```python
from pyecharts import options as opts  # 配置项功能
from pyecharts.charts import Bar
from pyecharts.faker import Faker  # 虚假数据模块
from pyecharts.globals import ThemeType  # 绘图主题功能

c = (
    Bar(init_opts=opts.InitOpts(theme=ThemeType.LIGHT)).add_xaxis(
        Faker.choose()).add_yaxis("商家A", Faker.values()).add_yaxis(
            "商家B",
            Faker.values()).add_yaxis("商家C", Faker.values()).set_global_opts(
                # 标题配置项
                title_opts=opts.TitleOpts(title='我是主标题'),

                # 区域缩放配置项
                datazoom_opts=opts.DataZoomOpts(is_show=False),

                # 图例配置项
                legend_opts=opts.LegendOpts(),

                # 工具箱配置项
                toolbox_opts=opts.ToolboxOpts(is_show=True),

                # 提示框配置项
                tooltip_opts=opts.TooltipOpts(),

                # 坐标轴配置项, 需要在关键字基础上执行操作的轴
                xaxis_opts=opts.AxisOpts(
                    name='X轴',
                    name_location='center',
                    name_gap=20,
                    name_rotate=60,
                    axistick_opts=opts.AxisTickOpts(is_inside=True)),
                yaxis_opts=opts.AxisOpts(
                    name='Y轴',
                    name_location='center',
                    name_gap=20,
                    name_rotate=60,
                    axistick_opts=opts.AxisTickOpts(is_inside=True))))
c.render_notebook()
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/712f5b0cd4b74234bd07bf15109b081d.png)