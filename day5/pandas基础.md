## 1.Pandas介绍

官网：http://pandas.pydata.org/
[Pandas官网](http://pandas.pydata.org/)
Pandas是一个强大的分析结构化数据的工具集，基于NumPy构
建，提供了**高级数据结构**和**数据操作**工具，它是使Python成为强大
而高效的数据分析环境的重要因素之一。
- 一个强大的分析和操作大型结构化数据集所需的工具集
- 基础是NumPy，提供了高性能矩阵的运算
- 提供了大量能够快速便捷地处理数据的函数和方法
- 应用于数据挖掘，数据分析
- 提供数据清洗功能

## 2.数据读取与存储
我们的数据大部分存在于文件当中，所以pandas会支持复杂的IO
操作，pandas的API支持众多的文件格式，如CSV、SQL、XLS、
JSON、HDF5。
### 2.1 CSV
####  2.1.1read_csv
Signature:
pd.read_csv(
    filepath_or_buffer: 'FilePath | ReadCsvBuffer[bytes] | ReadCsvBuffer[str]',
    *,
    sep: 'str | None | lib.NoDefault' = <no_default>,
    delimiter: 'str | None | lib.NoDefault' = None,
    names: 'Sequence[Hashable] | None | lib.NoDefault' = <no_default>,
    index_col: 'IndexLabel | Literal[False] | None' = None,
    usecols: 'list[HashableT] | Callable[[Hashable], bool] | None' = None,
    dtype: 'DtypeArg | None' = None,
	encoding:'utf-8'
)
```py
# 打开一个文件
df=pd.read_csv(
    './data/stock_day.csv',
    sep=',',
    encoding='utf-8',
    #usecols=[3,6],  # 读取指定的列,第三列,第六列
    usecols=['high','low'],
    engine='c' #指定读取的引擎,速度快,兼容性相对不好
              )
df
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/b0c5883c55034c00abe46f3bfcdde542.png)

####  2.1.2 to_csv
```py
# 保存用 数据对象.to...
df.to_csv(
    './data/csv文件的保存.csv',
    index=False,   # index=False 保存的时候不设置索引
    columns=['high']  # 指定保存的列

)
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/97c5deb4bf14433db4e21328ae509576.png)
### 2.2 JSON
#### 2.2.1 pandas.read_json介绍
JSON是我们常用的一种数据交换格式，前面在前后端的交互经常用到，也会在存储的时候选择这种格式。所以我们需要知道Pandas如何进行读取和存储JSON格式。
`pandas.read_json(path_or_buf=None, orient=None, typ='frame', lines=False)`
- 将JSON格式准换成默认的Pandas DataFrame格式

- orient : string,Indication of expected JSON string format.

   orient: string，表示期望的JSON字符串格式。

  - 'split' : dict like {index -> [index], columns -> [columns], data -> [values]}

    - split 将索引总结到索引，列名到列名，数据到数据。将三部分都分开了

  - 'records' : list like [{column -> value}, ... , {column -> value}]

    - records 以`columns：values`的形式输出

  - 'index' : dict like {index -> {column -> value}}

    - index 以`index：{columns：values}...`的形式输出

  - 'columns' : dict like {column -> {index -> value}}

    ,默认该格式

    - colums 以`columns:{index:values}`的形式输出

- lines : boolean, default False

  - 按照每行读取json对象

- typ : default ‘frame’， 指定转换成的对象类型series或者dataframe

#### 2.2.2 案例
读取json文件
```py
df=pd.read_json('./data/info-records.json',encoding='utf-8')
df
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/6e8ec47f45064071b708c4d05fd5e1c1.png)
```py
df.to_json(
    './data/info-split1.json',
    orient='split'
    # 我们发现数据是unicode编码保存
)
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/c0584c280a4b400790423e8e7048ef54.png)
```py
df.to_json(
    './data/info-split2.json',
    orient='split',
    force_ascii=False  # 默认会使用Unicode编码保存, force_ascii=False 不使用默认编码
)
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/f39ff867bced46ce838cdbc70cd068fd.png)

```py
"""读取split类型json文件"""
df2 = pd.read_json(
    './data/info-split2.json',
    orient='split'
)
df2  # 当保存json的时候指定了json文件类型, 那么在读取的时候也要指定类型读取
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/2a54efa36ecf42c4b10ea2b16cc1429d.png)
```py
"""HDF文件操作"""
day_close=pd.read_hdf('./data/day_close.h5')
day_close
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/f163aaf5effb480fa92ef991f501847b.png)

```py
day_close.to_hdf('./data/test1.h5',key='aaa')  
#必须指定key,aaa是加密的一种,以二进制保存
```

##  3.Excel文件操作
可能错误 Missing optional dependency 'openpyxl'.  Use pip or conda to install openpyxl.
原因是缺少依赖模块 -->   `pip install openpyxl`
```py
# sheet_name 指定读取表的名字
df = pd.read_excel('./data/网易新闻.xlsx', sheet_name='表1')  # 如果工作簿中有多张表, 没有指定读取哪张, 默认就是第一张表
df

```

```py
#  usecols 指定读取表格列
df = pd.read_excel('./data/网易新闻.xlsx', sheet_name='表1', usecols=['标题', '类别', '新闻来源'])  
# 如果工作簿中有多张表, 没有指定读取哪张, 默认就是第一张表
df
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/81f29d96228b4545861f78fd8992c011.png)

```python
df = pd.read_excel('./data/网易新闻.xlsx', 
                   sheet_name='表1', 
                   usecols=['标题', '类别', '新闻来源'],
                  skiprows=[1, 3], # 忽略行读取, 可以跳行读取
                   nrows=10  # 指定读取的行数
                  )  
df
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/436a81f8b74545cbafd9db13cabedb77.png)
## 4pandas数据结构

### 4.1Series
Series是一个类似于一维数组的数据结构，它能够保存任何类型的数据，比如整数、字符串、浮点数等，主要由一组数据和与之相关的索引两部分构成。

```python
pd.Series(
    data=None,
    index=None,
    dtype: 'Dtype | None' = None,
    name=None,
    copy: 'bool | None' = None,
    fastpath: 'bool' = False,
)
```

```python
pd.Series(
    data=['a', 'b', 'c', 'd', 'e']  # 指定数据, 默认会用自然数作为行索引
)

series_1 = pd.Series(
                    data=['a','b','c','e'],
                    index=['f','g','h','i'], # 指定行索引
                    dtype=str
)
series_1

series_1['h']  # Series对象是根据行索引取值

```
![在这里插入图片描述](https://img-blog.csdnimg.cn/d6eff191165d4d45992a9765456d4e16.png)

```python
"""通过字典构建Series"""
series_2 = pd.Series(data={'red': 100, 'blue': 200, 'green': 300, 'gray': 400})
series_2

"""Series对象属性和方法"""
print('取Series行索引:', series_2.index)
print('取Series数据:', series_2.values)
print('统计Series数据中不重复的数据个数:\n', series_2.value_counts())
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/8c089ee4850d4c66be130cce49cc41dd.png)

## 5.DataFrame
![在这里插入图片描述](https://img-blog.csdnimg.cn/915e1d2efe6b4e90a32bd24a3484d04f.png)


```python
arr = [[92, 55, 78, 50, 50],
       [71, 76, 50, 48, 96],
       [45, 84, 78, 51, 68],
       [81, 91, 56, 54, 76],
       [86, 66, 77, 67, 95],
       [46, 86, 56, 61, 99],
       [46, 95, 44, 46, 56],
       [80, 50, 45, 65, 57],
       [41, 93, 90, 41, 97],
       [65, 83, 57, 57, 40]]
#DataFrame 既有行索引也有列索引, 默认是从0开始的自然数
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/d23873598ce34b2e891d4d8bf81ed7b9.png)

```python
df = pd.DataFrame(
    data=arr,
    index=[f"同学{i}" for i in range(1, 11)], # 指定行索引
    columns=['语文', '数学', '英语', '政治', '体育']  # 指定列索引
)
df
```


```python
df['语文']  #单独取一列
df[['数学', '英语']]

# 单独取一个数据, 先列再行
df['语文']['同学4']

print('形状(shape): \t', df.shape)
print('取行索引(index): \t', df.index)
print('取列索引(columns): \t', df.columns)

print('取数据(values): \n', df.values)
print('转置(T): \n', df.T)

print('取头部数据(head): \n', df.head(2))
print('取尾部数据(head): \n', df.tail(3))
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/58b0b513e46144e3a96e57ee0e6c0ead.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/086a2d12653a482dbd5f8763f6daf8e4.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/09fc846d21fd48a79d3203edb7afb754.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/b24f9d3085314275b6019724786219e2.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/d2dd406b39cf49d19a6ffce72dc0d5f5.png)
## 6 索引操作

```python
df = pd.DataFrame(
    data={'year': [2012, 2013, 2014, 2015],
         'shop': ['天上人间', '天之道', '沐林足浴', '浴皇大帝'],
         'sale': [55, 40, 84, 99]}
)
df
```
### 6.1设置索引
```python
# 基于数据列设置行索引
# 对于原有的对象不会产生修改
df2 = df.set_index('year')
df2

# 还原索引
df2.reset_index()  
```
### 6.2索引的切片和取值

```python
df = pd.read_csv('./data/stock_day.csv', encoding='utf-8')
df

# 不需要 'ma5', 'ma10', 'ma20', 'v_ma5', 'v_ma10', 'v_ma20', 'turnover' 列数据

# 1. 在读取时候使用 usecols
# 2. df对象[[]]  取多列
# 3. drop 方法, 默认是根据数据行删除数据
#    默认 axis=0 是操作数据行, 如果axis=1, 就可以删除列 
df.drop(['ma5', 'ma10', 'ma20', 'v_ma5', 'v_ma10', 'v_ma20', 'turnover'], 
        axis=1,
        inplace=True,  # 基于源数据做修改, 在很多方法里面有有此参数
       )

df
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/a79f6b57d22e4743a3a82e72d8289052.png)

- 索引切片操作
	- loc 根据索引的名字进行切片取值
	- iloc 根据数字索引进行切片取值

"""切片(loc)"""

```python
# 切片是先行再列
# 在DataFrame中根据索引名字切片的作用域是闭区间
df2.loc['二': '三', 'Unnamed: 0': 'volume']

"""loc 与 iloc 组合使用"""

# df2.index 取行索引对象
df2.loc[df2.index[1:3], 'Unnamed: 0': 'volume']


"""loc 与 iloc 组合使用"""

# get_indexer 将索引的名字转化成数字
df2.iloc[1:3, df2.columns.get_indexer(df2.columns[:6])]
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/c2cd5c2aeaed4a5a93d813ccf341103f.png)
