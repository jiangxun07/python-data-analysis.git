# miniconda环境搭建和Jupyter Notebook入门使用
## 1 miniconda环境搭建和jupyter安装与配置

1,配置conda镜像源,使用清华的镜像源加速,“Win”+R ，输入cmd进入cmd终端，输入以下指令回车执行

```
conda config --add channelshttps://mirrors.tuna.tsinghua.edu.cn/anaconda/pkg
s/free/
```
miniconda解释器安装好,继续在终端执行以下命令:
2 安装Jupyter包
```
conda install jupyter notebook 
```
3 拓展模块

```
conda install -c conda-forge jupyter_contrib_nbextensions
```
4 更新nbconvert，不然可能不适配

```
conda update nbconvert
```
5 适配模块

```
jupyter contrib nbextension install --user
```

6 拓展插件

```
conda install -c conda-forge jupyter_nbextensions_configurator
```
7 适配插件

```
jupyter nbextensions_configurator enable --user
```
8 安装pep8代码规范的模块

```
pip install autopep8
```
9 安装拓展包依赖的第三方功能模块
```
pip install yapf
```
10 输入命令
环境搭建好后，在命令行下输入 `jupyter notebook` 命令,会自动打开默认浏览器, 少数win11系统不会自动打开浏览器.点击链接也可进入

![在这里插入图片描述](https://img-blog.csdnimg.cn/caf896b1c79c4f558152bc10fe21330c.jpeg#pic_center)

11 打开`jupyter notebook`以后，在`Nbextensions`选项下勾选配置选项，如下图所示：
![在这里插入图片描述](https://img-blog.csdnimg.cn/3a30c69c15b2480d9e50fd9a2834e9fb.jpeg#pic_center)
# 2 Jupyter Notebook入门使用
##### 1 cell 
cell：一对In Out会话被视作一个代码单元，称为cell

##### 2 Jupyter支持两种模式：
- 编辑模式（Enter）
  - 命令模式下`回车Enter`或`鼠标双击`cell进入编辑模式
  - 可以**操作cell内文本**或代码，剪切／复制／粘贴移动等操作
- 命令模式（Esc）
  - 按`Esc`退出编辑，进入命令模式
  - 可以**操作cell单元本身**进行剪切／复制／粘贴／移动等操作

##### 3 快捷键操作

- 两种模式通用快捷键
  - **`Shift+Enter`，执行本单元代码，并跳转到下一单元**
  - **`Ctrl+Enter`，执行本单元代码，留在本单元**

- **命令模式**：按ESC进入
  - Y ：在命令模式下转入代码状态
  - M ：在命令模式下切换到 Markdown
  - R ：普通文本，运行不会输出结果
  - L ：为当前cell加上行号
  - A：在该单元格的上方插入新单元格
  - B：在该单元格的下方插入新单元格
  - X：剪切选中的单元
  - C：复制选中的单元
  - V：粘贴到下方单元
  - DD：删除选中的单元（敲两个D）
  ##### 4 打印九九乘法表
  ![九九乘法表](https://img-blog.csdnimg.cn/544c0c33e8b8455f91b359ba1d0711c8.png)